<?php
/**
 * @file
 * Contains helping functions.
 */

/**
 * Update IRIS menus.
 */
function aelia_iris_profile_update_menus() {
  global $user;
  $update_user = $user;
  $user = user_load(1);
  $options = array(
    'create_content' => FALSE,
    'link_to_content' => TRUE,
    'remove_menu_items' => TRUE,
  );
  $path = drupal_get_path('profile', 'aelia_iris_profile');
  $bo_menu_file_path = $path . '/menus/menu-back-office-menu-export.txt';
  menu_import_file($bo_menu_file_path, 'menu-back-office-menu', $options);
  $user = $update_user;
}

/**
 * Update URL aliases for existing content(pages, news, merch rubrics).
 */
function aelia_iris_profile_update_url_aliases() {
  // Update merch rubric aliases.
  $vocabulary = taxonomy_vocabulary_machine_name_load('merchandising_rubric');
  $terms = taxonomy_get_tree($vocabulary->vid);
  $tids = array();
  foreach ($terms as $term) {
    $tids[] = $term->tid;
  }
  pathauto_taxonomy_term_update_alias_multiple($tids, 'bulkupdate');

  // Update pages and news aliases.
  $nids = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', array('page', 'news'), 'IN')
    ->execute()->fetchCol();
  pathauto_node_update_alias_multiple($nids, 'bulkupdate');
}
