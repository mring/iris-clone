<?php

/**
 * @file
 * Contains helping functions.
 */

/**
 * Checks whether promotion already exists.
 *
 * @param string $groupment
 *   From 'Promotion line': Operation number+Activity sector+Slot+Grouping key.
 */
function iris_promotion_generation_existing_promotion($groupment) {
  return db_select('field_data_field_groupment', 'fdfg')
    ->fields('fdfg', array('entity_id'))
    ->condition('field_groupment_value', $groupment)->execute()->fetchField();
}

/**
 * Create groupment for promotion line.
 *
 * @param object $node
 *   Promotion line node.
 *
 * @throws Exception
 *   If any of gropument item is missing.
 *
 * @return array
 *   Array containing concatenated groupment string and used for this items.
 */
function iris_promotion_generation_create_groupment($node) {
  try {
    $emw_node = entity_metadata_wrapper('node', $node);

    // Promotion line values.
    $operation_id = $emw_node->field_operation->raw();
    $activity_sector_id = $emw_node->field_promo_line_activity_sector->raw();
    $slot_id = $emw_node->field_promo_slot->raw();
    $grouping_key = $emw_node->field_grouping_key->value();

    // Array to create groupment.
    $groupment_items = array(
      'operation_id' => $operation_id,
      'activity_sector_id' => $activity_sector_id,
      'slot_id' => $slot_id,
      'grouping_key' => $grouping_key,
    );

    foreach ($groupment_items as $key => $value) {
      if (!isset($value) || empty($value)) {
        throw new Exception(t('Value !value is missing'), array('!value' => $key));
      }
    }

    $groupment = implode('_', $groupment_items);

    return array($groupment, $groupment_items);
  }
  catch (EntityMetadataWrapperException $exc) {
    watchdog('iris_promotion_generation', 'EntityMetadataWrapper exception in %function() <pre>@trace</pre>', array(
      '%function' => __FUNCTION__,
      '@trace' => $exc->getTraceAsString(),
    ), WATCHDOG_ERROR);
  }
}

/**
 * Creates new promotion if doen't exists or adds promotion line to old one.
 *
 * @param array $item
 *   Array containing necessary data.
 *
 * @return array
 *   Info about operation result.
 */
function iris_promotion_generation_add_edit_promotion($item) {
  global $user;
  $data = $item->data;

  $groupment = $data['groupment'];
  $groupment_items = $data['groupment_items'];
  $node = $data['node'];
  if (isset($data['op']) && $data['op'] == 'update') {
    $original_groupment = $data['original_groupment'];
    $original_node = $data['original_node'];

    if ($original_node) {
      $item = new stdClass();
      $item->data = array(
        'original_groupment' => $original_groupment,
        'original_node' => $original_node,
      );
      $item->item_id = $node->nid;
      iris_promotion_generation_rm_from_original_promotion($item);
    }
  }

  $promotion_id = iris_promotion_generation_existing_promotion($groupment);

  try {
    $operation = node_load($groupment_items['operation_id']);
    $emw_operation = entity_metadata_wrapper('node', $operation);

    if ($promotion_id) {
      // Add promotion line to an existing promotion.
      $promotion = node_load($promotion_id);
      $emw_promotion = entity_metadata_wrapper('node', $promotion);
      $emw_promotion->field_promotion_lines[] = $node->nid;
      if ($operation->status) {
        $emw_promotion->status = NODE_PUBLISHED;
      }
      $emw_promotion->status_processed = TRUE;
    }
    else {
      // Create new promotion.
      $promotion = entity_create('node', array('type' => IRIS_PROMOTION_GENERATION_CT));
      $promotion->uid = $user->uid;
      $promotion->title = $node->title;

      $emw_promotion = entity_metadata_wrapper('node', $promotion);
      $emw_promotion->field_operation_number = $operation->title;
      $emw_promotion->field_groupment = $groupment;
      $emw_promotion->field_grouping_key = $groupment_items['grouping_key'];
      $emw_promotion->field_operation_start_date = $emw_operation->field_operation_start_date->value();
      $emw_promotion->field_operation_end_date = $emw_operation->field_operation_end_date->value();
      $emw_promotion->field_promo_line_activity_sector->set($groupment_items['activity_sector_id']);
      $emw_promotion->field_promo_slot->set($groupment_items['slot_id']);
      $emw_promotion->field_promotion_lines->set(array($node->nid));
      if ($operation->status) {
        $emw_promotion->status = NODE_PUBLISHED;
      }
      $emw_promotion->status_processed = TRUE;
    }
    $emw_promotion->save();
  }
  catch (EntityMetadataWrapperException $exc) {
    watchdog('iris_promotion_generation', 'EntityMetadataWrapper exception in %function() <pre>@trace</pre>', array(
      '%function' => __FUNCTION__,
      '@trace' => $exc->getTraceAsString(),
    ), WATCHDOG_ERROR);
    return array(
      'status' => ADVANCEDQUEUE_STATUS_FAILURE,
      'result' => 'Processed ' . $item->item_id,
    );
  }

  return array(
    'status' => ADVANCEDQUEUE_STATUS_SUCCESS,
    'result' => 'Processed ' . $item->item_id,
  );
}

/**
 * Removes promotion line from promotion after groupment change.
 *
 * @param array $item
 *   Array containing necessary data.
 *
 * @return array
 *   Info about operation result.
 */
function iris_promotion_generation_rm_from_original_promotion($item) {
  $data = $item->data;

  $original_groupment = $data['original_groupment'];
  $original_node = $data['original_node'];
  $original_promotion_id = iris_promotion_generation_existing_promotion($original_groupment);
  if (!$original_promotion_id) {
    return '';
  }
  $original_promotion = node_load($original_promotion_id);

  try {
    $emw_original_promotion = entity_metadata_wrapper('node', $original_promotion);
    $promotion_lines_ids = $emw_original_promotion->field_promotion_lines->raw();

    foreach ($promotion_lines_ids as $delta => $promotion_line_id) {
      if ($promotion_line_id == $original_node->nid) {
        $emw_original_promotion->field_promotion_lines[$delta]->set(NULL);
        unset($promotion_lines_ids[$delta]);
        break;
      }
    }

    if (!empty($promotion_lines_ids)) {
      $emw_original_promotion->save();
    }
    else {
      $emw_original_promotion->delete();
    }
  }
  catch (EntityMetadataWrapperException $exc) {
    watchdog('iris_promotion_generation', 'EntityMetadataWrapper exception in %function() <pre>@trace</pre>', array(
      '%function' => __FUNCTION__,
      '@trace' => $exc->getTraceAsString(),
    ), WATCHDOG_ERROR);

    return array(
      'status' => ADVANCEDQUEUE_STATUS_FAILURE,
      'result' => 'Processed ' . $item->item_id,
    );
  }

  return array(
    'status' => ADVANCEDQUEUE_STATUS_SUCCESS,
    'result' => 'Processed ' . $item->item_id,
  );
}

/**
 * Updates promotions dates.
 *
 * @param array $item
 *   Array containing necessary data.
 *
 * @return array
 *   Info about operation result.
 */
function iris_promotion_generation_update_promotion_dates($item) {
  $promotion = node_load($item->data);

  try {
    $emw_promotion = entity_metadata_wrapper('node', $promotion);
    if ($emw_promotion->field_promotion_lines->value()) {
      $start_date = $emw_promotion->field_promotion_lines[0]->field_operation->field_operation_start_date->value();
      $end_date = $emw_promotion->field_promotion_lines[0]->field_operation->field_operation_end_date->value();
      $emw_promotion->field_operation_start_date->set($start_date);
      $emw_promotion->field_operation_end_date->set($end_date);
      $emw_promotion->save();
    }
  }
  catch (EntityMetadataWrapperException $exc) {
    watchdog('iris_promotion_generation', 'EntityMetadataWrapper exception in %function() <pre>@trace</pre>', array(
      '%function' => __FUNCTION__,
      '@trace' => $exc->getTraceAsString(),
    ), WATCHDOG_ERROR);

    return array(
      'status' => ADVANCEDQUEUE_STATUS_FAILURE,
      'result' => 'Updated node with nid ' . $item->item_id,
    );
  }

  return array(
    'status' => ADVANCEDQUEUE_STATUS_SUCCESS,
    'result' => 'Updated node with nid ' . $item->item_id,
  );
}
