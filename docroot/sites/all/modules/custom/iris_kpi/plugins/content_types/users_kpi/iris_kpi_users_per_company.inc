<?php

/**
 * @file
 * Users per company CTools plugin file.
 */

$plugin = array(
  'title' => t('Users per company'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_users_per_company_content_type_render',
);

/**
 * Render callback.
 */
function iris_kpi_users_per_company_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = t('Total number of users per Company');
  $block->content = '';

  $table_header = array(
    array(
      'data' => t('Company'),
      'class' => 'p60',
    ),
    t('Number of users'),
  );

  $table_rows = _iris_users_kpi_get_users_count_per_company();

  $chart_hearder = array();
  $chart_rows = $chart_rows2 = array();

  foreach ($table_rows as $key => $row) {
    $chart_hearder[$key] = str_replace('<br>', ', ', $row['role_name']);
    $chart_rows[$key] = $chart_rows2[$key] = $row['users_count'];
  }

  array_multisort($chart_rows2, SORT_DESC, $chart_hearder);

  $settings = array();
  $settings['chart']['usersChartC'] = array(
    'header' => $chart_hearder,
    'rows' => array($chart_rows2),
    'columns' => array('Users chart'),
    'chartType' => 'PieChart',
    'containerId' => 'usersChartC',
    'options' => array(
      'height' => 550,
      'chartArea' => array(
        'width' => '70%',
        'height' => '90%',
      ),
      'forceIFrame' => FALSE,
      'title' => '',
      'legend' => array(
        'textStyle' => array(
          'fontSize' => 12,
        ),
      ),
    ),
  );

  draw_chart($settings);
  $content = '<div id="usersChartC" class="users-kpi-chart"></div>';

  array_multisort($chart_rows, SORT_DESC, $table_rows);

  $content .= '<div class="users-kpi-table">' . theme('table', array(
    'header' => $table_header,
    'rows' => $table_rows,
    'empty' => t('No data for this interval.'),
  )) . '</div>';

  $block->content = $content;

  return $block;
}

/**
 * Helper function get users count per company.
 */
function _iris_users_kpi_get_users_count_per_company() {
  $roles = array();

  $params = _iris_kpi_get_users_params_from_url();
  $italy_tids = _iris_users_kpi_get_italy_companies_tids();

  $query = db_select('users', 'u');
  $query->addField('t', 'name', 'cname');
  $query->addExpression('COUNT(DISTINCT u.uid)', 'ucount');
  $query->join('users_roles', 'ur', 'ur.uid = u.uid');
  $query->join('role', 'r', 'r.rid = ur.rid');
  $query->join('field_data_field_user_company', 'uc', 'uc.entity_id = u.uid');
  $query->join('taxonomy_term_data', 't', 't.tid = uc.field_user_company_tid');

  $query->condition('r.name', 'administrator', '<>');
  $query->condition('uc.bundle', 'user');
  $query->condition('u.login', $params['from'], '>=');
  $query->condition('u.login', $params['to'], '<=');

  if ($params['company']) {
    $query->condition('uc.field_user_company_tid', $params['company']);
  }
  else {
    $query->condition('uc.field_user_company_tid', $italy_tids, 'NOT IN');
  }

  $query->groupBy('t.name');
  $query->orderBy('ucount', 'DESC');

  $result = $query->execute()->fetchAll();

  foreach ($result as $record) {
    $roles[] = array(
      'role_name' => $record->cname,
      'users_count' => $record->ucount,
    );
  }
  if (!$params['company']) {
    $roles[] = array(
      'role_name' => _iris_users_kpi_get_companies_rowname($italy_tids),
      'users_count' => _iris_users_kpi_get_users_count_by_tids($params['from'], $params['to'], $italy_tids),
    );
  }

  return $roles;
}

/**
 * Get Italy companies TIDs.
 */
function _iris_users_kpi_get_italy_companies_tids() {
  $italy_companies_codes = array('ITR1', 'ITV1');
  $italy_companies_tids = array();
  foreach ($italy_companies_codes as $code) {
    $tid = iris_core_get_company_by_code($code);
    if ($tid) {
      $italy_companies_tids[] = $tid;
    }
  }
  return $italy_companies_tids;
}

/**
 * Get companies original term name.
 */
function _iris_users_kpi_get_companies_rowname($tids) {
  $query = db_select('taxonomy_term_data', 'ttd');
  $query->fields('ttd', array('name'));
  $query->condition('ttd.tid', $tids, 'IN');
  $result = $query->execute()->fetchCol();

  return implode('<br>', $result);
}

/**
 * Helper function get users count per TIDs.
 */
function _iris_users_kpi_get_users_count_by_tids($from, $to, $tids) {
  $query = db_select('users', 'u');
  $query->addField('t', 'name', 'cname');
  $query->join('users_roles', 'ur', 'ur.uid = u.uid');
  $query->join('role', 'r', 'r.rid = ur.rid');
  $query->join('field_data_field_user_company', 'uc', 'uc.entity_id = u.uid');
  $query->join('taxonomy_term_data', 't', 't.tid = uc.field_user_company_tid');

  $query->condition('r.name', 'administrator', '<>');
  $query->condition('uc.bundle', 'user');
  $query->condition('u.login', $from, '>=');
  $query->condition('u.login', $to, '<=');

  $query->condition('uc.field_user_company_tid', $tids, 'IN');

  $result = $query->execute()->rowCount();
  return $result;
}
