<?php

/**
 * @file
 * Users filters CTools plugin file.
 */

$plugin = array(
  'title' => t('Users filters'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_users_filters_content_type_render',
);

/**
 * Render callback.
 */
function iris_kpi_users_filters_content_type_render($subtype, $conf, $panel_args, $context) {
  $form = drupal_get_form('iris_kpi_users_form');
  $block = new stdClass();
  $block->title = NULL;
  $block->content = render($form);

  return $block;
}

/**
 * Provide a simple form to change time periods.
 */
function iris_kpi_users_form() {
  $form = array();
  $params = drupal_get_query_parameters();

  // Load some javascript for datepicker.
  drupal_add_library('system', 'ui.datepicker');
  drupal_add_js('jQuery(document).ready(function(){jQuery( ".pickadate" ).datepicker({
      dateFormat: "dd/mm/yy",
      autoSize: true
    });});', 'inline');

  $companies = _iris_kpi_get_terms_for_select('company_code');

  $form['filters'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'exposed-filters',
      ),
    ),
  );

  $form['filters']['from'] = array(
    '#title' => t('Last connection date: start'),
    '#type' => 'textfield',
    '#size' => 10,
    '#attributes' => array('class' => array('pickadate')),
    '#default_value' => isset($_GET['from']) ? filter_xss($_GET['from']) : '',
  );

  $form['filters']['to'] = array(
    '#title' => t('Last connection date: end'),
    '#type' => 'textfield',
    '#size' => 10,
    '#attributes' => array('class' => array('pickadate')),
    '#default_value' => isset($_GET['to']) ? filter_xss($_GET['to']) : '',
  );

  $form['filters']['company'] = array(
    '#title' => t('Company'),
    '#type' => 'select',
    '#options' => $companies,
    '#default_value' => isset($params['company']) ? $params['company'] : '',
  );

  $form['filters']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#attributes' => array('id' => 'edit-submit-users-filters'),
  );

  return $form;
}

/**
 * Validate handler for iris_kpi_users_form().
 */
function iris_kpi_users_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $format_source = 'd/m/Y H:i:s';
  $format_target = 'U';

  if (isset($values['from']) && !empty($values['from'])) {
    if ($date = DateTime::createFromFormat($format_source, $values['from'] . ' 00:00:00')) {
      $from = $date->format($format_target);
    }
    else {
      form_set_error('from', t('The from date must be formatted correctly %format.', array('%format' => '(DD/MM/YY)')));
    }
  }
  if (isset($values['to']) && !empty($values['to'])) {
    if (isset($values['to']) && ($date = DateTime::createFromFormat($format_source, $values['to'] . ' 00:00:00'))) {
      $to = $date->format($format_target);
    }
    else {
      form_set_error('to', t('The to date must be formatted correctly %format.', array('%format' => '(DD/MM/YY)')));
    }
  }

  // Make sure from is less than to.
  if (isset($from) && isset($to) && ($from > $to)) {
    form_set_error('from', t('The from date must be before the to date'));
  }
}

/**
 * Submit handler for iris_kpi_users_form()
 */
function iris_kpi_users_form_submit(&$form, $form_state) {
  $query = array();

  if ($form_state['values']['from']) {
    $query['from'] = $form_state['values']['from'];
  }
  if ($form_state['values']['to']) {
    $query['to'] = $form_state['values']['to'];
  }
  if ($form_state['values']['company']) {
    $query['company'] = $form_state['values']['company'];
  }

  drupal_goto($_GET['q'], array('query' => array($query)));
}
