<?php

/**
 * @file
 * Users per role CTools plugin file.
 */

$plugin = array(
  'title' => t('Users per role'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_users_per_role_content_type_render',
);

/**
 * Render callback.
 */
function iris_kpi_users_per_role_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = t('Total number of users per role');
  $block->content = '';

  $table_header = array(
    array(
      'data' => t('Role'),
      'class' => 'p60',
    ),
    t('Number of users'),
  );
  $table_rows = _iris_users_kpi_get_users_count_per_role();

  $chart_hearder = array();
  $chart_rows = array();

  foreach ($table_rows as $key => $row) {
    $chart_hearder[$key] = $row['role_name'];
    $chart_rows[$key] = $row['users_count'];
  }

  $settings = array();
  $settings['chart']['usersChartR'] = array(
    'header' => $chart_hearder,
    'rows' => array($chart_rows),
    'columns' => array('Users chart'),
    'chartType' => 'PieChart',
    'containerId' => 'usersChartR',
    'options' => array(
      'forceIFrame' => FALSE,
      'title' => '',
      'height' => 450,
      'legend' => array(
        'textStyle' => array(
          'fontSize' => 12,
        ),
      ),
    ),
  );

  draw_chart($settings);
  $content = '<div id="usersChartR" class="users-kpi-chart"></div>';

  array_multisort($chart_rows, SORT_DESC, $table_rows);

  $content .= '<div class="users-kpi-table">' . theme('table', array(
    'header' => $table_header,
    'rows' => $table_rows,
    'empty' => t('No data for this interval.'),
  )) . '</div>';

  $block->content = $content;

  return $block;
}

/**
 * Hepler function get users count per role.
 */
function _iris_users_kpi_get_users_count_per_role() {
  $roles = array();

  $params = _iris_kpi_get_users_params_from_url();

  $query = db_select('users_roles', 'ur');
  $query->addField('r', 'name', 'name');
  $query->addExpression('COUNT(DISTINCT ur.uid)', 'ucount');
  $query->join('role', 'r', 'r.rid = ur.rid');
  $query->join('users', 'u', 'ur.uid = u.uid');

  if ($params['company']) {
    $query->join('field_data_field_user_company', 'uc', 'uc.entity_id = u.uid');
  }

  $query->condition('r.name', 'administrator', '<>');
  $query->condition('u.login', $params['from'], '>=');
  $query->condition('u.login', $params['to'], '<=');

  if ($params['company']) {
    $query->condition('uc.field_user_company_tid', $params['company']);
  }

  $query->groupBy('r.name');
  $query->orderBy('ucount', 'DESC');

  $result = $query->execute()->fetchAll();

  foreach ($result as $record) {
    $roles[] = array(
      'role_name' => drupal_ucfirst($record->name),
      'users_count' => $record->ucount,
    );
  }

  return $roles;
}
