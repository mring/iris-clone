<?php

/**
 * @file
 * Total news per type CTools plugin file.
 */

$plugin = array(
  'title' => t('Total news per type'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_news_per_type_content_type_render',
);

/**
 * Render callback.
 */
function iris_kpi_news_per_type_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = t('Total number of news per type');
  $block->content = '';

  $table_header = array(
    array(
      'data' => t('News type'),
      'class' => 'p60',
    ),
    t('Number of News'),
  );
  $table_rows = _iris_news_kpi_get_news_per_type();

  $chart_hearder = array();
  $chart_rows = array();

  foreach ($table_rows as $key => $row) {
    $chart_hearder[$key] = $row['tname'];
    $chart_rows[$key] = $row['news_count'];
  }

  $settings = array();
  $settings['chart']['newsChartPerType'] = array(
    'header' => $chart_hearder,
    'rows' => array($chart_rows),
    'columns' => array('News per Type'),
    'chartType' => 'PieChart',
    'containerId' => 'newsChartPerType',
    'options' => array(
      'forceIFrame' => FALSE,
      'title' => '',
      'height' => 450,
      'legend' => array(
        'textStyle' => array(
          'fontSize' => 12,
        ),
      ),
    ),
  );

  draw_chart($settings);
  $content = '<div id="newsChartPerType" class="news-kpi-chart"></div>';

  array_multisort($chart_rows, SORT_DESC, $table_rows);

  $content .= '<div class="news-kpi-table-with-chart">' . theme('table', array(
    'header' => $table_header,
    'rows' => $table_rows,
    'empty' => t('No data for this interval.'),
  )) . '</div>';

  $block->content = $content;

  return $block;
}

/**
 * Helper function get news per type.
 */
function _iris_news_kpi_get_news_per_type() {
  $news = array();
  $params = _iris_kpi_get_news_params_from_url();

  global $language;

  $query = db_select('node', 'n');
  $query->addField('ttd', 'name', 'tname');
  $query->addField('tn', 'name_field_value', 'ttname');
  $query->addExpression('COUNT(DISTINCT n.nid)', 'ncount');
  $query->join('field_data_field_news_type', 'nt', 'nt.entity_id = n.nid');
  $query->join('taxonomy_term_data', 'ttd', 'ttd.tid = nt.field_news_type_tid');
  $query->leftJoin('field_data_name_field', 'tn', 'tn.entity_id = ttd.tid AND tn.language = :lang', array(':lang' => $language->language));

  if ($params['universe']) {
    $query->join('field_data_field_activity_domains', 'ad', 'ad.entity_id = n.nid');
    $query->join('field_data_field_universe', 'un', 'un.entity_id = ad.field_activity_domains_tid');
  }

  if ($params['lg']['all']) {
    $query->join('og_membership', 'om', 'om.etid = n.nid');
  }

  $query->condition('n.type', 'news');
  $query->condition('nt.bundle', 'news');

  if ($params['universe']) {
    $query->condition('ad.bundle', 'news');
    $query->condition('un.bundle', 'activity_domain');
    $query->condition('un.field_universe_tid', $params['universe']);
  }

  if ($params['author']) {
    $query->condition('n.uid', $params['author']);
  }

  if ($params['from']) {
    $query->condition('n.created', $params['from'], '>=');
  }

  if ($params['to']) {
    $query->condition('n.created', $params['to'], '<=');
  }

  if ($params['lg']['all']) {
    $query->condition('om.entity_type', 'node');
    $query->condition('om.field_name', 'field_location_group');
    $query->condition('om.gid', $params['lg']['all'], 'IN');
  }

  $query->groupBy('tname');

  $result = $query->execute()->fetchAll();

  foreach ($result as $record) {
    $news[] = array(
      'tname' => $record->ttname ? $record->ttname : $record->tname,
      'news_count' => $record->ncount,
    );
  }

  return $news;
}
