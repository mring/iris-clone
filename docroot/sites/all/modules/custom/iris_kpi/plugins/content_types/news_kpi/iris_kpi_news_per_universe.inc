<?php

/**
 * @file
 * Total news per universe CTools plugin file.
 */

$plugin = array(
  'title' => t('Total news per universe'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_news_per_universe_content_type_render',
);

/**
 * Render callback.
 */
function iris_kpi_news_per_universe_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = t('Total number of news per universe');
  $block->content = '';

  $table_header = array(
    array(
      'data' => t('Universe'),
      'class' => 'p60',
    ),
    t('Number of News'),
  );

  $table_rows = _iris_news_kpi_get_news_per_universe();

  $chart_hearder = array();
  $chart_rows = array();

  foreach ($table_rows as $key => $row) {
    $chart_hearder[$key] = $row['uname'];
    $chart_rows[$key] = $row['news_count'];
  }

  $settings = array();
  $settings['chart']['newsChartPerUniverse'] = array(
    'header' => $chart_hearder,
    'rows' => array($chart_rows),
    'columns' => array('News per Universe'),
    'chartType' => 'PieChart',
    'containerId' => 'newsChartPerUniverse',
    'options' => array(
      'forceIFrame' => FALSE,
      'title' => '',
      'height' => 450,
      'legend' => array(
        'textStyle' => array(
          'fontSize' => 12,
        ),
      ),
    ),
  );

  draw_chart($settings);
  $content = '<div id="newsChartPerUniverse" class="news-kpi-chart"></div>';

  array_multisort($chart_rows, SORT_DESC, $table_rows);

  $content .= '<div class="news-kpi-table-with-chart">' . theme('table', array(
    'header' => $table_header,
    'rows' => $table_rows,
    'empty' => t('No data for this interval.'),
  )) . '</div>';

  $block->content = $content;

  return $block;
}

/**
 * Helper function get news per universe.
 */
function _iris_news_kpi_get_news_per_universe() {
  $news = array();
  $params = _iris_kpi_get_news_params_from_url();

  global $language;

  $query = db_select('node', 'n');
  $query->addField('ttd', 'name', 'uname');
  $query->addField('tn', 'name_field_value', 'tuname');
  $query->addExpression('COUNT(DISTINCT n.nid)', 'ncount');
  $query->join('field_data_field_activity_domains', 'ad', 'ad.entity_id = n.nid');
  $query->join('field_data_field_universe', 'un', 'un.entity_id = ad.field_activity_domains_tid');
  $query->join('taxonomy_term_data', 'ttd', 'ttd.tid = un.field_universe_tid');
  $query->leftJoin('field_data_name_field', 'tn', 'tn.entity_id = ttd.tid AND tn.language = :lang', array(':lang' => $language->language));

  if ($params['lg']['all']) {
    $query->join('og_membership', 'om', 'om.etid = n.nid');
  }

  $query->condition('n.type', 'news');
  $query->condition('ad.bundle', 'news');
  $query->condition('un.bundle', 'activity_domain');

  if ($params['universe']) {
    $query->condition('un.field_universe_tid', $params['universe']);
  }

  if ($params['author']) {
    $query->condition('n.uid', $params['author']);
  }

  if ($params['from']) {
    $query->condition('n.created', $params['from'], '>=');
  }

  if ($params['to']) {
    $query->condition('n.created', $params['to'], '<=');
  }

  if ($params['lg']['all']) {
    $query->condition('om.entity_type', 'node');
    $query->condition('om.field_name', 'field_location_group');
    $query->condition('om.gid', $params['lg']['all'], 'IN');
  }

  $query->groupBy('ttd.tid');

  $result = $query->execute()->fetchAll();

  foreach ($result as $record) {
    $news[] = array(
      'uname' => $record->tuname ? $record->tuname : $record->uname,
      'news_count' => $record->ncount,
    );
  }

  return $news;
}
