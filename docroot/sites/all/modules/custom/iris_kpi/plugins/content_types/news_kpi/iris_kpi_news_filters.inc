<?php

/**
 * @file
 * News filters CTools plugin file.
 */

$plugin = array(
  'title' => t('News filters'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_news_filters_content_type_render',
);

/**
 * Render callback.
 */
function iris_kpi_news_filters_content_type_render($subtype, $conf, $panel_args, $context) {
  $form = drupal_get_form('iris_kpi_news_form');
  $block = new stdClass();
  $block->title = NULL;
  $block->content = render($form);

  return $block;
}

/**
 * Provide a simple form to change time periods.
 */
function iris_kpi_news_form() {
  $form = array();
  $params = drupal_get_query_parameters();

  // Load some javascript for datepicker.
  drupal_add_library('system', 'ui.datepicker');
  drupal_add_js('jQuery(document).ready(function(){jQuery( ".pickadate" ).datepicker({
      dateFormat: "dd/mm/yy",
      autoSize: true
    });});', 'inline');

  $universes = _iris_kpi_get_terms_for_select('universe');

  $field_instance = field_info_instance('node', 'field_location_group', 'news');
  $field = field_info_field_by_id($field_instance['field_id']);
  $lg_options = entityreference_options_list($field, $field_instance);

  $form['filters'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'exposed-filters',
      ),
    ),
  );

  $form['filters']['from'] = array(
    '#title' => t('Creation date: start'),
    '#type' => 'textfield',
    '#size' => 10,
    '#attributes' => array('class' => array('pickadate')),
    '#default_value' => isset($params['from']) ? $params['from'] : '',
  );

  $form['filters']['to'] = array(
    '#title' => t('Creation date: end'),
    '#type' => 'textfield',
    '#size' => 10,
    '#attributes' => array('class' => array('pickadate')),
    '#default_value' => isset($params['to']) ? $params['to'] : '',
  );

  $form['filters']['universe'] = array(
    '#title' => t('Universe'),
    '#type' => 'select',
    '#options' => $universes,
    '#default_value' => isset($params['universe']) ? $params['universe'] : '',
  );

  $form['filters']['author'] = array(
    '#title' => t('Author'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'user/autocomplete',
    '#size' => 30,
    '#default_value' => isset($params['author']) ? $params['author'] : '',
  );

  $form['filters']['lg'] = array(
    '#title' => t('Location group'),
    '#type' => 'select',
    '#options' => $lg_options,
    '#default_value' => isset($params['lg']) ? $params['lg'] : '',
    '#chosen' => TRUE,
    '#multiple' => TRUE,
  );

  $form['filters']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#attributes' => array('id' => 'edit-submit-news-filters'),
  );

  $form['filters']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
  );

  return $form;
}

/**
 * Validate handler for iris_kpi_news_form().
 */
function iris_kpi_news_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $format_source = 'd/m/Y H:i:s';
  $format_target = 'U';

  if (isset($values['from']) && !empty($values['from'])) {
    if ($date = DateTime::createFromFormat($format_source, $values['from'] . ' 00:00:00')) {
      $from = $date->format($format_target);
    }
    else {
      form_set_error('from', t('The from date must be formatted correctly %format.', array('%format' => '(DD/MM/YY)')));
    }
  }
  if (isset($values['to']) && !empty($values['to'])) {
    if (isset($values['to']) && ($date = DateTime::createFromFormat($format_source, $values['to'] . ' 00:00:00'))) {
      $to = $date->format($format_target);
    }
    else {
      form_set_error('to', t('The to date must be formatted correctly %format.', array('%format' => '(DD/MM/YY)')));
    }
  }

  // Make sure from is less than to.
  if (isset($from) && isset($to) && ($from > $to)) {
    form_set_error('from', t('The from date must be before the to date'));
  }
}

/**
 * Submit handler for iris_kpi_news_form()
 */
function iris_kpi_news_form_submit(&$form, $form_state) {
  $query = array();

  if ($form_state['values']['op'] == t('Reset')) {
    drupal_goto(current_path());
  }
  else {
    if ($form_state['values']['from']) {
      $query['from'] = $form_state['values']['from'];
    }
    if ($form_state['values']['to']) {
      $query['to'] = $form_state['values']['to'];
    }
    if ($form_state['values']['universe']) {
      $query['universe'] = $form_state['values']['universe'];
    }
    if ($form_state['values']['author']) {
      $query['author'] = $form_state['values']['author'];
    }
    if ($form_state['values']['lg']) {
      $query['lg'] = $form_state['values']['lg'];
    }

    drupal_goto($_GET['q'], array('query' => array($query)));
  }
}
