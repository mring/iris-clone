<?php

/**
 * @file
 * Total news completed per user CTools plugin file.
 */

$plugin = array(
  'title' => t('Total news completed per user'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_news_completed_per_user_content_type_render',
  'edit form' => 'iris_kpi_news_completed_per_user_content_type_edit_form',
  'admin title' => 'iris_kpi_news_completed_per_user_content_type_admin_title',
);

/**
 * Render callback.
 */
function iris_kpi_news_completed_per_user_content_type_render($subtype, $conf, $panel_args, $context) {
  $type = (isset($conf['type']) && !empty($conf['type'])) ? $conf['type'] : 'highest';
  $sort = ($type == 'lowest') ? 'ASC' : 'DESC';

  $block = new stdClass();
  $block->title = t('Top 10 of the users who processed the @type number of news', array('@type' => t($type)));
  $block->content = '';

  $table_header = array(
    array(
      'data' => t('User'),
      'class' => 'p60',
    ),
    t('Number of News processed'),
  );
  $table_rows = _iris_news_kpi_get_news_completed_per_user($sort);

  $content = '<div class="news-kpi-table-2-cols">' . theme('table', array(
    'header' => $table_header,
    'rows' => $table_rows,
    'empty' => t('No data for this interval.'),
  )) . '</div>';

  $block->content = $content;

  return $block;
}

/**
 * Plugin config form.
 */
function iris_kpi_news_completed_per_user_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $types = array(
    'highest' => 'highest',
    'lowest' => 'lowest',
  );

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => $types,
    '#default_value' => $conf['type'],
    '#description' => t('Please select type of sorting'),
  );

  return $form;
}

/**
 * Plugin config foem submit.
 */
function iris_kpi_news_completed_per_user_content_type_edit_form_submit($form, &$form_state) {
  foreach (array('type') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Admin title function.
 */
function iris_kpi_news_completed_per_user_content_type_admin_title($subtype, $conf) {
  $type = (isset($conf['type']) && !empty($conf['type'])) ? $conf['type'] : 'highest';
  return t('Total news completed per user ') . t($type);
}

/**
 * Helper function get completed news per user.
 */
function _iris_news_kpi_get_news_completed_per_user($sort = 'DESC') {
  $news = array();
  $params = _iris_kpi_get_news_params_from_url();

  $query = db_select('users', 'u');
  $query->addExpression("CONCAT(fn.field_first_name_value, ' ', ln.field_last_name_value)", 'author');
  $query->addExpression('COUNT(DISTINCT epn.endpoints_entity_id)', 'ncount');
  $query->join('users_roles', 'ur', 'ur.uid = u.uid');
  $query->join('role', 'r', 'r.rid = ur.rid');
  $query->join('field_data_field_first_name', 'fn', 'fn.entity_id = u.uid');
  $query->join('field_data_field_last_name', 'ln', 'ln.entity_id = u.uid');
  $query->leftJoin('field_data_field_news_store_author', 'nsa', 'nsa.field_news_store_author_target_id = u.uid');
  $query->leftJoin('field_data_field_news_store_status', 'nss', 'nss.entity_id = nsa.entity_id');
  $query->leftJoin('field_data_field_news_store_deleted', 'nsd', 'nsd.entity_id = nss.entity_id');
  $query->leftJoin('field_data_endpoints', 'epn', 'epn.entity_id = nss.entity_id');
  $query->leftJoin('field_data_endpoints', 'eps', 'eps.entity_id = nss.entity_id');
  $query->leftJoin('node', 'n', 'n.nid = epn.endpoints_entity_id');

  if ($params['universe']) {
    $query->join('field_data_field_activity_domains', 'ad', 'ad.entity_id = n.nid');
    $query->join('field_data_field_universe', 'un', 'un.entity_id = ad.field_activity_domains_tid');
  }

  $query->condition('nss.bundle', 'news_article_status');
  $query->condition('epn.bundle', 'news_article_status');
  $query->condition('nss.field_news_store_status_value', IRIS_KPI_NEWS_COMPLETED_STATUS);
  $query->condition('nsd.field_news_store_deleted_value', 0);
  $query->condition('epn.endpoints_entity_type', 'node');
  $query->condition('fn.entity_type', 'user');
  $query->condition('ln.entity_type', 'user');

  if ($params['universe']) {
    $query->condition('ad.bundle', 'news');
    $query->condition('un.bundle', 'activity_domain');
    $query->condition('un.field_universe_tid', $params['universe']);
  }

  if ($params['author']) {
    $query->condition('n.uid', $params['author']);
  }

  if ($params['from']) {
    $query->condition('n.created', $params['from'], '>=');
  }

  if ($params['to']) {
    $query->condition('n.created', $params['to'], '<=');
  }

  if ($params['lg']['children']) {
    $query->condition('eps.endpoints_entity_id', $params['lg']['children'], 'IN');
  }

  $query->condition('r.name', array('manager', 'sales consultant'), 'IN');

  $query->groupBy('author');
  $query->orderBy('ncount', $sort);
  $query->range(0, 10);

  $result = $query->execute()->fetchAll();

  foreach ($result as $record) {
    $news[] = array(
      'user' => $record->author,
      'news_count' => $record->ncount,
    );
  }

  return $news;
}
