<?php

/**
 * @file
 * Percent news completed/all per universe CTools plugin file.
 */

$plugin = array(
  'title' => t('Percent news completed/all per universe'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_news_completed_percent_per_universe_content_type_render',
);

/**
 * Render callback.
 */
function iris_kpi_news_completed_percent_per_universe_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = t('Processed news per universe');
  $block->content = '';

  $table_header = array(
    array(
      'data' => t('Universe'),
      'class' => 'p60',
    ),
    t('News processed %'),
  );

  $table_rows = _iris_news_kpi_get_news_completed_percent_per_universe();

  $content = '<div class="news-kpi-table-2-cols">' . theme('table', array(
    'header' => $table_header,
    'rows' => $table_rows,
    'empty' => t('No data for this interval.'),
  )) . '</div>';

  $block->content = $content;

  return $block;
}

/**
 * Helper function get completed news percent per univerce.
 */
function _iris_news_kpi_get_news_completed_percent_per_universe() {
  global $language;

  $params = _iris_kpi_get_news_params_from_url();
  $items = array();
  $news = array(
    'all' => array(),
    'done' => array(),
  );

  foreach ($news as $type => $news_items) {
    $query = db_select('field_data_field_news_store_status', 'nss');
    $query->addField('ttd', 'tid', 'universe_id');
    $query->addField('ttd', 'name', 'uname');
    $query->addField('tn', 'name_field_value', 'tuname');
    $query->addField('n', 'nid', 'nid');
    $query->addExpression('COUNT(DISTINCT nss.entity_id)', 'relation_ncount');
    $query->join('field_data_endpoints', 'epn', 'epn.entity_id = nss.entity_id');
    $query->join('field_data_endpoints', 'ept', 'ept.entity_id = nss.entity_id');
    $query->join('field_data_field_news_store_deleted', 'nsd', 'nsd.entity_id = nss.entity_id');
    $query->join('node', 'n', 'n.nid = epn.endpoints_entity_id');
    $query->join('field_data_field_activity_domains', 'ad', 'ad.entity_id = n.nid');
    $query->join('field_data_field_universe', 'un', 'un.entity_id = ad.field_activity_domains_tid');
    $query->join('taxonomy_term_data', 'ttd', 'ttd.tid = un.field_universe_tid');
    $query->join('field_data_field_linked_with_domains', 'fld', 'fld.entity_id = nss.entity_id');
    $query->leftJoin('field_data_name_field', 'tn', 'tn.entity_id = ttd.tid AND tn.language = :lang', array(':lang' => $language->language));

    // Calculate only when sore univerce is the same as in news.
    $query->join('field_data_field_activity_domains', 'fads', 'fads.entity_id = ept.endpoints_entity_id');
    $query->join('field_data_field_universe', 'fus', 'fus.entity_id = fads.field_activity_domains_tid');
    $query->where("fus.field_universe_tid = ttd.tid");

    if ($params['lg']['all']) {
      $query->join('og_membership', 'om', 'om.etid = n.nid');
    }

    $query->condition('nss.bundle', 'news_article_status');
    $query->condition('epn.bundle', 'news_article_status');
    $query->condition('epn.endpoints_entity_type', 'node');
    $query->condition('ad.bundle', 'news');
    $query->condition('un.bundle', 'activity_domain');
    $query->condition('fld.bundle', 'news_article_status');
    $query->condition('fld.field_linked_with_domains_value', 1);
    $query->condition('nsd.field_news_store_deleted_value', 0);

    if ($type == 'done') {
      $query->condition('nss.field_news_store_status_value', IRIS_KPI_NEWS_COMPLETED_STATUS);
    }

    if ($params['universe']) {
      $query->condition('un.field_universe_tid', $params['universe']);
    }

    if ($params['author']) {
      $query->condition('n.uid', $params['author']);
    }

    if ($params['from']) {
      $query->condition('n.created', $params['from'], '>=');
    }

    if ($params['to']) {
      $query->condition('n.created', $params['to'], '<=');
    }

    if ($params['lg']['all']) {
      $query->condition('om.entity_type', 'node');
      $query->condition('om.field_name', 'field_location_group');
      $query->condition('om.gid', $params['lg']['all'], 'IN');
    }

    if ($params['lg']['children']) {
      $query->condition('ept.endpoints_entity_id', $params['lg']['children'], 'IN');
    }

    $query->groupBy('ttd.tid');
    $query->distinct();
    $result = $query->execute()->fetchAll();

    foreach ($result as $record) {
      $news[$type][$record->universe_id] = array(
        'universe' => $record->tuname ? $record->tuname : $record->uname,
        'news_count' => $record->relation_ncount,
      );
    }
  }

  foreach ($news['all'] as $id => $all_item) {
    $items[$id] = array(
      'universe' => $all_item['universe'],
    );

    if (isset($news['done'][$id]['news_count'])) {
      $items[$id]['percent'] = round($news['done'][$id]['news_count'] / $all_item['news_count'] * 100, 1) . ' %';
    }
    else {
      $items[$id]['percent'] = 0 . ' %';
    }
  }

  unset($news);
  usort($items, '_iris_news_kpi_sort_by_percent');

  return $items;
}
