<?php

/**
 * @file
 * News created per month CTools plugin file.
 */

$plugin = array(
  'title' => t('News created per month'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_news_created_per_month_content_type_render',
);

/**
 * Render callback.
 */
function iris_kpi_news_created_per_month_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = t('Total number of created news per month');
  $block->content = '';

  $table_header = array(
    array(
      'data' => t('Month'),
      'class' => 'p60',
    ),
    t('Number of News created'),
    t('Average / Day'),
  );
  $table_rows = _iris_news_kpi_get_news_created_per_month();

  $chart_hearder = array();
  $chart_rows = array();

  foreach ($table_rows as $key => $row) {
    $chart_hearder[$key] = $row['month'];
    $chart_rows[$key] = $row['news_count'];
  }

  $settings = array();
  $settings['chart']['newsChartPerMonth'] = array(
    'header' => $chart_hearder,
    'rows' => array($chart_rows),
    'columns' => array(t('News per Month')),
    'chartType' => 'LineChart',
    'containerId' => 'newsChartPerMonth',
    'options' => array(
      'forceIFrame' => FALSE,
      'title' => '',
      'height' => 450,
      'legend' => array(
        'textStyle' => array(
          'fontSize' => 12,
        ),
      ),
    ),
  );

  draw_chart($settings);
  $content = '<div id="newsChartPerMonth" class="news-kpi-chart"></div>';

  array_multisort($chart_rows, SORT_DESC, $table_rows);

  $content .= '<div class="news-kpi-table-with-chart">' . theme('table', array(
    'header' => $table_header,
    'rows' => $table_rows,
    'empty' => t('No data for this interval.'),
  )) . '</div>';

  $block->content = $content;

  return $block;
}

/**
 * Helper function get news per month.
 */
function _iris_news_kpi_get_news_created_per_month() {
  $news = array();
  $params = _iris_kpi_get_news_params_from_url();

  $query = db_select('node', 'n');
  $query->addExpression("FROM_UNIXTIME(n.created, '%m-%Y')", 'ndate');
  $query->addExpression('COUNT(DISTINCT n.nid)', 'ncount');

  if ($params['universe']) {
    $query->join('field_data_field_activity_domains', 'ad', 'ad.entity_id = n.nid');
    $query->join('field_data_field_universe', 'un', 'un.entity_id = ad.field_activity_domains_tid');
  }

  if ($params['lg']['all']) {
    $query->join('og_membership', 'om', 'om.etid = n.nid');
  }

  $query->condition('n.type', 'news');

  if ($params['universe']) {
    $query->condition('ad.bundle', 'news');
    $query->condition('un.bundle', 'activity_domain');
    $query->condition('un.field_universe_tid', $params['universe']);
  }

  if ($params['author']) {
    $query->condition('n.uid', $params['author']);
  }

  $params['from'] = $params['from'] ? $params['from'] : strtotime('-1 year');

  if ($params['from']) {
    $query->condition('n.created', $params['from'], '>=');
  }

  if ($params['to']) {
    $query->condition('n.created', $params['to'], '<=');
  }

  if ($params['lg']['all']) {
    $query->condition('om.entity_type', 'node');
    $query->condition('om.field_name', 'field_location_group');
    $query->condition('om.gid', $params['lg']['all'], 'IN');
  }

  $query->groupBy('ndate');

  $result = $query->execute()->fetchAll();

  $days_in_month = 30;

  foreach ($result as $record) {
    if ($date = DateTime::createFromFormat('m-Y', $record->ndate)) {
      $record->ndate = t($date->format('F')) . ' ' . $date->format('Y');
      $days_in_month = cal_days_in_month(CAL_GREGORIAN, $date->format('m'), $date->format('Y'));
    }
    $news[] = array(
      'month' => $record->ndate,
      'news_count' => $record->ncount,
      'news_average' => round($record->ncount / $days_in_month),
    );
  }

  return $news;
}
