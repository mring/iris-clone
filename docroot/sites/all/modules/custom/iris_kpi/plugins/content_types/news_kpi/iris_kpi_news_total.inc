<?php

/**
 * @file
 * News total CTools plugin file.
 */

$plugin = array(
  'title' => t('News total'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_news_total_content_type_render',
);

/**
 * Render callback.
 */
function iris_kpi_news_total_content_type_render($subtype, $conf, $panel_args, $context) {
  $total = _iris_news_kpi_get_total_news_count();

  $block = new stdClass();
  $block->title = NULL;
  $block->content = '<p>' . t('Total number of News created: @total', array('@total' => $total['total'])) . '</p>';
  $block->content .= '<p>' . t('Average / day since the beginning: @per_day', array('@per_day' => $total['per_day'])) . '</p>';

  return $block;
}

/**
 * Get news total coun from DB.
 */
function _iris_news_kpi_get_total_news_count() {
  $params = _iris_kpi_get_news_params_from_url();

  if (!$params['from']) {
    $fnquery = db_select('node', 'n');
    $fnquery->addField('n', 'created');
    $fnquery->condition('n.type', 'news');
    $fnquery->orderBy('n.created');
    $fnquery->range(0, 1);

    $fnresult = $fnquery->execute()->fetchCol('created');

    $params['from'] = isset($fnresult[0]) ? $fnresult[0] - 1 : 0;
  }

  $query = db_select('node', 'n');
  $query->addExpression('COUNT(DISTINCT n.nid)', 'ncount');

  if ($params['universe']) {
    $query->join('field_data_field_activity_domains', 'ad', 'ad.entity_id = n.nid');
    $query->join('field_data_field_universe', 'un', 'un.entity_id = ad.field_activity_domains_tid');
  }

  if ($params['lg']['all']) {
    $query->join('og_membership', 'om', 'om.etid = n.nid');
  }

  $query->condition('n.type', 'news');

  if ($params['universe']) {
    $query->condition('ad.bundle', 'news');
    $query->condition('un.bundle', 'activity_domain');
    $query->condition('un.field_universe_tid', $params['universe']);
  }

  if ($params['author']) {
    $query->condition('n.uid', $params['author']);
  }

  if ($params['from']) {
    $query->condition('n.created', $params['from'], '>=');
  }

  if ($params['to']) {
    $query->condition('n.created', $params['to'], '<=');
  }

  if ($params['lg']['all']) {
    $query->condition('om.entity_type', 'node');
    $query->condition('om.field_name', 'field_location_group');
    $query->condition('om.gid', $params['lg']['all'], 'IN');
  }

  // $query->condition('n.status', NODE_PUBLISHED);.
  $result = $query->execute()->fetchCol('ncount');

  $days = round(($params['to'] - $params['from']) / (60 * 60 * 24));
  $total = isset($result[0]) ? $result[0] : 0;
  $per_day = round($total / $days);

  return array(
    'total' => isset($result[0]) ? $result[0] : 0,
    'per_day' => $per_day,
  );
}
