<?php

/**
 * @file
 * Total news completed per store CTools plugin file.
 */

$plugin = array(
  'title' => t('Total news completed per store'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_news_completed_per_store_content_type_render',
);

/**
 * Render callback.
 */
function iris_kpi_news_completed_per_store_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = '';
  $block->content = '';

  $types = array('highest', 'lowest');
  $table_rows = _iris_news_kpi_get_news_completed_per_store($types);

  $table_header = array(
    array(
      'data' => t('Activity domain'),
      'class' => 'p60',
    ),
    array(
      'data' => t('Terminal'),
      'class' => 'p20',
    ),
    array(
      'data' => t('Store'),
      'class' => 'p5',
    ),
    array(
      'data' => t('Number of News processed'),
      'class' => 'p15',
    ),
  );

  foreach ($types as $type) {
    $block->content .= '<h2 class="pane-title">' . t('Top 10 of the stores who processed the @type number of news', array('@type' => t($type))) . '</h2>';
    $block->content .= '<div class="news-kpi-table">';
    $block->content .= theme('table', array(
      'header' => $table_header,
      'rows' => $table_rows[$type],
      'empty' => t('No data for this interval.'),
    ));
    $block->content .= '</div>';
  }

  return $block;
}

/**
 * Helper function get completed news per store.
 */
function _iris_news_kpi_get_news_completed_per_store($types) {
  $items = array();
  $params = _iris_kpi_get_news_params_from_url();
  $news = array(
    'all' => array(),
    'done' => array()
  );

  foreach ($news as $type => $news_items) {
    $query = db_select('field_data_field_news_store_status', 'nss');
    $query->addField('tc', 'entity_id', 'store_id');
    $query->addField('tc', 'field_taxonomy_code_value', 'store');
    $query->addField('ttdt', 'name', 'terminal');
    $query->addExpression("GROUP_CONCAT(DISTINCT sad.field_activity_domains_tid SEPARATOR ',')", 'act_domains');
    $query->addExpression('COUNT(DISTINCT epn.endpoints_entity_id)', 'ncount');
    $query->join('field_data_endpoints', 'epn', 'epn.entity_id = nss.entity_id');
    $query->join('field_data_endpoints', 'ept', 'ept.entity_id = nss.entity_id');
    $query->join('field_data_field_news_store_deleted', 'nsd', 'nsd.entity_id = nss.entity_id');
    $query->join('taxonomy_term_data', 'ttds', 'ttds.tid = ept.endpoints_entity_id');
    $query->join('field_data_field_taxonomy_code', 'tc', 'tc.entity_id = ept.endpoints_entity_id');
    $query->join('og_membership', 'ogm', 'ogm.etid = ept.endpoints_entity_id');
    $query->join('taxonomy_term_data', 'ttdt', 'ttdt.tid = ogm.gid');
    $query->join('field_data_field_activity_domains', 'sad', 'sad.entity_id = ept.endpoints_entity_id');
    $query->join('node', 'n', 'n.nid = epn.endpoints_entity_id');
    $query->join('field_data_field_linked_with_domains', 'fld', 'fld.entity_id = nss.entity_id');

    $query->condition('nss.bundle', 'news_article_status');
    $query->condition('epn.bundle', 'news_article_status');
    $query->condition('epn.endpoints_entity_type', 'node');
    $query->condition('sad.entity_type', 'taxonomy_term');
    $query->condition('ept.endpoints_entity_type', 'taxonomy_term');
    $query->condition('ogm.entity_type', 'taxonomy_term');
    $query->condition('nsd.field_news_store_deleted_value', 0);
    $query->condition('fld.field_linked_with_domains_value', 1);

    if ($type == 'done') {
      $query->condition('nss.field_news_store_status_value', IRIS_KPI_NEWS_COMPLETED_STATUS);
    }

    if ($params['universe']) {
      $query->join('field_data_field_activity_domains', 'ad', 'ad.entity_id = n.nid');
      $query->join('field_data_field_universe', 'un', 'un.entity_id = ad.field_activity_domains_tid');
      $query->condition('ad.bundle', 'news');
      $query->condition('un.field_universe_tid', $params['universe']);
    }

    if ($params['author']) {
      $query->condition('n.uid', $params['author']);
    }

    if ($params['from']) {
      $query->condition('n.created', $params['from'], '>=');
    }

    if ($params['to']) {
      $query->condition('n.created', $params['to'], '<=');
    }

    if ($params['lg']['all']) {
      $query->condition('ogm.gid', $params['lg']['all'], 'IN');
    }

    if ($params['lg']['children']) {
      $query->condition('ept.endpoints_entity_id', $params['lg']['children'], 'IN');
    }

    $query->groupBy('ept.endpoints_entity_id');
    $query->distinct();
    $result = $query->execute()->fetchAll();

    foreach ($result as $record) {
      $domains_ids = explode(',', $record->act_domains);
      $domains = array();
      $terms = taxonomy_term_load_multiple($domains_ids);
      if ($terms) {
        foreach ($terms as $term) {
          $domains[] = $term->name;
        }
      }

      $news[$type][$record->store_id] = array(
        'store'      => $record->store,
        'terminal'   => $record->terminal,
        'act_domain' => implode(', ', $domains),
        'news_count' => $record->ncount,
      );
    }
    unset($result);
  }

  foreach ($news['all'] as $id => $all_item) {
    $items[$id] = array(
      'act_domain' => $all_item['act_domain'],
      'terminal'   => $all_item['terminal'],
      'store'      => $all_item['store'],
    );

    if (isset($news['done'][$id]['news_count'])) {
      $percent = round($news['done'][$id]['news_count'] / $all_item['news_count'] * 100, 1);
      $items[$id]['percent'] = $news['done'][$id]['news_count'];
      $items[$id]['news_count'] = $news['done'][$id]['news_count'] . ' / ' . $all_item['news_count'] . ' - ' . $percent . '%';
    }
    else {
      $items[$id]['news_count'] = 0 . '%';
      $items[$id]['percent'] = 0;
    }
  }

  $elements = array();
  foreach ($types as $type) {
    $elements[$type] = $items;
    if ($type == 'highest') {
      usort($elements[$type], '_iris_news_kpi_sort_by_percent');
    }
    else {
      usort($elements[$type], '_iris_news_kpi_sort_by_percent_reverse');
    }
    $elements[$type] = array_slice($elements[$type], 0, 10);
    foreach ($elements[$type] as &$item) {
      unset($item['percent']);
    }
  }
  unset($news, $items);

  return $elements;
}
