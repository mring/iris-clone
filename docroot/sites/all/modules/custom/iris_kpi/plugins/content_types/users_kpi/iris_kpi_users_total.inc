<?php

/**
 * @file
 * Users total CTools plugin file.
 */

$plugin = array(
  'title' => t('Users total'),
  'single' => TRUE,
  'category' => t('IRIS KPI'),
  'render callback' => 'iris_kpi_users_total_content_type_render',
);

/**
 * Render callback.
 */
function iris_kpi_users_total_content_type_render($subtype, $conf, $panel_args, $context) {
  $total = _iris_users_kpi_get_total_users_count();

  $block = new stdClass();
  $block->title = NULL;
  $block->content = '<p>' . t('Total number of users using IRIS: @total', array('@total' => $total)) . '</p>';

  return $block;
}

/**
 * Get totla users count.
 */
function _iris_users_kpi_get_total_users_count() {
  $params = _iris_kpi_get_users_params_from_url();

  $query = db_select('users', 'u');
  $query->addExpression('COUNT(DISTINCT u.uid)', 'ucount');
  $query->join('users_roles', 'ur', 'ur.uid = u.uid');
  $query->join('role', 'r', 'r.rid = ur.rid');

  if ($params['company']) {
    $query->join('field_data_field_user_company', 'uc', 'uc.entity_id = u.uid');
  }

  $query->condition('r.name', 'administrator', '<>');
  $query->condition('u.login', $params['from'], '>=');
  $query->condition('u.login', $params['to'], '<=');

  if ($params['company']) {
    $query->condition('uc.field_user_company_tid', $params['company']);
  }

  $result = $query->execute()->fetchCol('ucount');

  return isset($result[0]) ? $result[0] : 0;
}
