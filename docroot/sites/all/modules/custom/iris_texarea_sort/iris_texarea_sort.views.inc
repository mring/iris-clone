<?php

/**
 * @file
 * Views code for textarea sort.
 */

/**
 * Implements hook_views_data_alter().
 */
function iris_texarea_sort_views_data_alter(&$data) {
  foreach (field_info_fields() as $field) {
    if ($field['storage']['type'] != 'field_sql_storage') {
      continue;
    }
    if ($field['type'] == 'text_long' || $field['type'] == 'text_with_summary') {
      $module = $field['module'];
      $result = (array) module_invoke($module, 'field_views_data', $field);
      if (empty($result)) {
        $result = _event_sort_field_default_views_data($field);
      }

      if (is_array($result)) {
        $data = drupal_array_merge_deep($result, $data);
      }
    }
  }

  return $data;
}

/**
 * Fork function from views add possibility sort by textarea.
 */
function _event_sort_field_default_views_data($field) {

  $field_types = field_info_field_types();

  // Check the field module is available.
  if (!isset($field_types[$field['type']])) {
    return;
  }

  $data = array();

  $current_table = _field_sql_storage_tablename($field);
  $revision_table = _field_sql_storage_revision_tablename($field);

  $supports_revisions = FALSE;

  // Build the relationships between the field table and the entity tables.
  foreach ($field['bundles'] as $entity => $bundles) {
    $entity_info = entity_get_info($entity);
    if (!empty($entity_info['entity keys']['revision']) && !empty($entity_info['revision table'])) {
      $supports_revisions = TRUE;
    }
  }

  $tables = array();
  $tables[FIELD_LOAD_CURRENT] = $current_table;
  if ($supports_revisions) {
    $tables[FIELD_LOAD_REVISION] = $revision_table;
  }

  foreach ($field['columns'] as $column => $attributes) {
    $sort = 'views_handler_sort';
    $allow_sort = TRUE;
    foreach ($tables as $type => $table) {
      $column_real_name = $field['storage']['details']['sql'][$type][$table][$column];
      // Load all the fields from the table by default.
      $additional_fields = array_values($field['storage']['details']['sql'][$type][$table]);

      if (!empty($allow_sort)) {
        $data[$table][$column_real_name]['sort'] = array(
          'field' => $column_real_name,
          'table' => $table,
          'handler' => $sort,
          'additional fields' => $additional_fields,
          'field_name' => $field['field_name'],
        );
      }
    }
  }
  return $data;
}
