<?php

/**
 * @file
 * Hook implementation and functions for importing through migrate.
 */

/**
 * Registers your module as an implementor of Migrate-based classes.
 *
 * Registers your module as an implementor of Migrate-based classes and provides
 * default configuration for migration processes.
 *
 * @return array
 *   An associative array with the following keys (of which only 'api' is
 *   required):
 *   - api: Always 2 for any module implementing the Migrate 2 API.
 *   - groups: An associative array, keyed by group machine name, defining one
 *     or more migration groups.Each value is an associative array - the 'title'
 *     key defines a user-visible name for the group; any other values are
 *     passed as arguments to all migrations in the group.
 *   - migrations: An associative array, keyed by migration machine name,
 *     defining one or more migrations. Each value is an associative array - any
 *     keys other than the following are passed as arguments to the migration
 *     constructor:
 *     - class_name (required):The name of the class implementing the migration.
 *     - group_name: The machine name of the group containing the migration.
 *     - disable_hooks: An associative array, keyed by hook name, listing hook
 *       implementations to be disabled during migration. Each value is an
 *       array of module names whose implementations of the hook in the key is
 *       to be disabled.
 *   - destination handlers: An array of classes implementing destination
 *     handlers.
 *   - field handlers: An array of classes implementing field handlers.
 *   - wizard classes: An array of classes that provide Migrate UI wizards.
 *   - wizard extenders: An array of classes that extend Migrate UI wizards.
 *     Keys are the wizard classes, values are arrays of extender classes.
 *
 *   See system_hook_info() for all hook groups defined by Drupal core.
 *
 * @see hook_migrate_api_alter()
 */
function iris_import_data_migrate_api() {
  $source_file_en = variable_get('en_iris_import_csv', 'ftp://ADYAX:Aelia%402015%3F@ftp.aelia.com/DEV/ENGLISH/EN_Stucture_Orga_Lagardere_Travel_Duty_Free.CSV');

  $source_file_fr = variable_get('fr_iris_import_csv', 'ftp://ADYAX:Aelia%402015%3F@ftp.aelia.com/DEV/FRENCH/FR_Stucture_Orga_Lagardere_Travel_Duty_Free.CSV');

  // Default format for all content migrations.
  $api = array(
    'api' => 2,
    'groups' => array(
      'iris_organizations' => array(
        'title' => t('Import Organization structure from FTP'),
        'default_format' => 'filtered_html',
      ),
      'iris_promotions' => array(
        // Default format for all content migrations.
        'title' => t('Import workflow from CSV'),
        // French version.
        'default_format' => 'plain_text',
      ),
    ),
    'migrations' => array(
      'CompanyTaxonomy' => array(
        'class_name' => 'CompanyTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_en,
        'language' => 'en',
        'pkcollumn' => 'company_code',
      ),
      'SalesOrganizationTaxonomy' => array(
        'class_name' => 'SalesOrganizationTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_en,
        'dependencies' => array('CompanyTaxonomy'),
        'language' => 'en',
        'pkcollumn' => 'sales_organization_code',
      ),
      'StorePlatformTaxonomy' => array(
        'class_name' => 'StorePlatformTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_en,
        'dependencies' => array('SalesOrganizationTaxonomy'),
        'language' => 'en',
        'pkcollumn' => 'store_platform_code',
      ),
      'TerminalTaxonomy' => array(
        'class_name' => 'TerminalTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_en,
        'dependencies' => array('StorePlatformTaxonomy'),
        'language' => 'en',
        'pkcollumn' => 'terminal_code',
      ),
      'StoreTaxonomy' => array(
        'class_name' => 'StoreTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_en,
        'dependencies' => array('TerminalTaxonomy', 'ActivityDomainTaxonomy'),
        'language' => 'en',
        'pkcollumn' => 'store_code',
      ),
      'ActivityDomainTaxonomy' => array(
        'class_name' => 'ActivityDomainTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_en,
        'language' => 'en',
        'pkcollumn' => 'activity_domain_code',
      ),
      'ActivitySectorTaxonomy' => array(
        'class_name' => 'ActivitySectorTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_en,
        'dependencies' => array('ActivityDomainTaxonomy'),
        'language' => 'en',
        'pkcollumn' => 'activity_sector_code',
      ),
      'CompanyTaxonomyFr' => array(
        'class_name' => 'CompanyTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_fr,
        'language' => 'fr',
        'dependencies' => array('CompanyTaxonomy'),
        'pkcollumn' => 'company_code',
      ),
      'SalesOrganizationTaxonomyFr' => array(
        'class_name' => 'SalesOrganizationTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_fr,
        'dependencies' => array('SalesOrganizationTaxonomy'),
        'language' => 'fr',
        'pkcollumn' => 'sales_organization_code',
      ),
      'StorePlatformTaxonomyFr' => array(
        'class_name' => 'StorePlatformTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_fr,
        'dependencies' => array('StorePlatformTaxonomy'),
        'language' => 'fr',
        'pkcollumn' => 'store_platform_code',
      ),
      'TerminalTaxonomyFr' => array(
        'class_name' => 'TerminalTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_fr,
        'dependencies' => array('TerminalTaxonomy'),
        'language' => 'fr',
        'pkcollumn' => 'terminal_code',
      ),
      'StoreTaxonomyFr' => array(
        'class_name' => 'StoreTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_fr,
        'dependencies' => array('StoreTaxonomy', 'ActivityDomainTaxonomy'),
        'language' => 'fr',
        'pkcollumn' => 'store_code',
      ),
      'ActivityDomainTaxonomyFr' => array(
        'class_name' => 'ActivityDomainTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_fr,
        'dependencies' => array('ActivityDomainTaxonomy'),
        'language' => 'fr',
        'pkcollumn' => 'activity_domain_code',
      ),
      // PROMOTIONS.
      'ActivitySectorTaxonomyFr' => array(
        'class_name' => 'ActivitySectorTaxonomyMigration',
        'group_name' => 'iris_organizations',
        'source_file' => $source_file_fr,
        'dependencies' => array('ActivitySectorTaxonomy'),
        'language' => 'fr',
        'pkcollumn' => 'activity_sector_code',
      ),
      'OperationsPromo' => array(
        'class_name' => 'OperationsPromoMigration',
        'group_name' => 'iris_promotions',
      ),
      'LinesPromo' => array(
        'class_name' => 'LinesPromoMigration',
        'group_name' => 'iris_promotions',
      ),
    ),
  );
  return $api;
}
