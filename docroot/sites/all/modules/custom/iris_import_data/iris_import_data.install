<?php

/**
 * @file
 * Install and update functions for iris import data module.
 */

/**
 * Implements hook_schema().
 */
function iris_import_data_schema() {
  $schema = array();

  $schema['iris_import'] = array(
    'description' => 'The base table for the IrisImport entity',
    'fields' => array(
      'id' => array(
        'description' => 'Primary key of the IrisImport entity',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'Uid of uploader.',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
      ),
      'date' => array(
        'description' => 'Date of import process.',
        'type' => 'int',
        'length' => 12,
        'not null' => TRUE,
      ),
      'completed' => array(
        'description' => 'Date of import completed.',
        'type' => 'int',
        'length' => 12,
        'not null' => FALSE,
      ),
      'status' => array(
        'description' => 'Status of import process.',
        'type' => 'int',
        'length' => 1,
        'not null' => TRUE,
      ),
      'fid' => array(
        'description' => 'FID of import process file.',
        'type' => 'int',
        'length' => 12,
        'not null' => TRUE,
      ),
      'log' => array(
        'description' => 'Logs of import.',
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
      'log_fid' => array(
        'description' => 'Logs file id of import.',
        'type' => 'int',
        'length' => 12,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

/**
 * Apply table schema.
 */
function iris_import_data_update_7101() {
  db_create_table('iris_import', drupal_get_schema_unprocessed('iris_import_data', 'iris_import'));
}

/**
 * Remove all operations that removed but in migrate mapping.
 */
function iris_import_data_update_7102() {
  $query = db_select('migrate_map_operationspromo', 'op');
  $query->fields('op', array('destid1'));
  $query->leftJoin('node', 'n', 'op.destid1=n.nid');
  $query->addField('n', 'nid');
  $query->isNotNull('op.destid1');
  $query->isNull('n.nid');
  $result = $query->execute()->fetchAll();
  $expired_map_entries = array();
  foreach ($result as $row) {
    $expired_map_entries[] = $row->destid1;
  }

  if ($expired_map_entries) {
    db_delete('migrate_map_operationspromo')
      ->condition('destid1', $expired_map_entries, 'IN')->execute();
  }
}

/**
 * Add field logs_fid type.
 */
function iris_import_data_update_7103() {
  $schema = drupal_get_schema('iris_import');
  db_add_field('iris_import', 'log_fid', $schema['fields']['log_fid']);
}

/**
 * Update Migrate table.
 */
function iris_import_data_update_7104() {
  foreach (array(
    'migrate_map_linespromo',
    'migrate_message_linespromo',
  ) as $table) {
    db_change_field($table, 'sourceid2', 'sourceid2', array(
      'description' => 'Operation number of Promotion',
      'type' => 'varchar',
      'length' => 100,
      'not null' => TRUE,
    ));
    db_change_field($table, 'sourceid3', 'sourceid3', array(
      'description' => 'Activity sector',
      'type' => 'varchar',
      'length' => 100,
      'not null' => TRUE,
    ));
    db_change_field($table, 'sourceid4', 'sourceid4', array(
      'description' => 'Slot',
      'type' => 'varchar',
      'length' => 100,
      'not null' => TRUE,
    ));
    db_change_field($table, 'sourceid5', 'sourceid5', array(
      'description' => 'Grouping Key of Promotion line',
      'type' => 'varchar',
      'length' => 100,
      'not null' => TRUE,
    ));
    db_add_field($table, 'sourceid6', array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,
      'description' => 'Slot store',
      'default' => '',
    ));
    if ($table == 'migrate_map_linespromo') {
      db_drop_primary_key($table);
      db_add_primary_key($table, array(
        'sourceid1',
        'sourceid2',
        'sourceid3',
        'sourceid4',
        'sourceid5',
        'sourceid6',
      ));
    }
  }
}

/**
 * Add field completed to export.
 */
function iris_import_data_update_7106() {
  $options = array(
    'description' => 'Date of import completed.',
    'type' => 'int',
    'length' => 12,
  );
  db_add_field('iris_import', 'completed', $options);
}

/**
 * Add field completed to export.
 */
function iris_import_data_update_7107() {
  foreach (array(
    'migrate_map_linespromo',
    'migrate_message_linespromo',
  ) as $table) {
    db_add_field($table, 'sourceid7', array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,
      'description' => 'Promotion rule',
      'default' => '',
    ));
    if ($table == 'migrate_map_linespromo') {
      db_drop_primary_key($table);
      db_add_primary_key($table, array(
        'sourceid1',
        'sourceid2',
        'sourceid3',
        'sourceid4',
        'sourceid5',
        'sourceid6',
        'sourceid7',
      ));
    }
  }
}

/**
 * Prefill mapping table for promoline migration.
 *
 * Prefill mapping table for promoline migration.
 * with new key - promo rule.
 */
function iris_import_data_update_7108(&$sandbox) {
  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['last_nid_processed'] = -1;
    $sandbox['limit'] = 5;
    $query = db_select('migrate_map_linespromo');
    $query->addExpression('COUNT(*)');
    $query->isNotNull('destid1');
    $sandbox['max'] = $query->execute()->fetchField();
  }

  $result = db_select('migrate_map_linespromo', 'l')
    ->fields('l', array('destid1'))->orderBy('l.destid1', 'ASC')
    ->isNotNull('l.destid1')
    ->condition('l.destid1', $sandbox['last_nid_processed'], '>')
    ->range(0, $sandbox['limit'])->execute()->fetchCol();

  foreach ($result as $nid) {
    $node_wrapper = entity_metadata_wrapper('node', $nid);
    $rule = $node_wrapper->field_promotion_rule->value();
    if ($rule) {
      $rule_name = $rule->name;
      db_update('migrate_map_linespromo')
        ->fields(array('sourceid7' => $rule_name))->condition('destid1', $nid)
        ->execute();
    }
  }
  $sandbox['progress'] = $sandbox['progress'] + count($result);
  $sandbox['last_nid_processed'] = end($result);
  $sandbox['#finished'] = !empty($sandbox['max']) ? ($sandbox['progress'] / $sandbox['max']) : $sandbox['#finished'] = 1;
  if ($sandbox['#finished'] === 1) {
    drupal_set_message(t('Updating map table for import lines completed. Count @nodes nodes.', array('@nodes' => $sandbox['max'])));
    watchdog('migrate_prefill_mapping', 'Total update - @total', array('@total' => $sandbox['max']));
  }
}

/**
 * Update all prices with new field - operation this field now is unique.
 *
 * During import search existing price for promo line.
 */
function iris_import_data_update_7111(&$sandbox) {
  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['last_nid_processed'] = -1;
    $sandbox['limit'] = 50;
    $query = db_select('node');
    $query->addExpression('COUNT(*)');
    $query->condition('type', 'promotion_line');
    $sandbox['max'] = $query->execute()->fetchField();
  }

  $result = db_select('node', 'n')
    ->condition('type', 'promotion_line')
    ->fields('n', array('nid'))
    ->orderBy('n.nid', 'ASC')
    ->range(0, $sandbox['limit'])
    ->condition('n.nid', $sandbox['last_nid_processed'], '>')
    ->execute()->fetchCol();

  foreach ($result as $nid) {
    $price_updated = FALSE;
    $pr_line_wrapper = entity_metadata_wrapper('node', $nid);
    $operation = $pr_line_wrapper->field_operation->value();
    $store = $pr_line_wrapper->field_promo_store->value();
    $organization = $pr_line_wrapper->field_promo_sales_organization->value();
    $product = $pr_line_wrapper->field_product->value();

    // Search for all fields.
    if ($product && $operation && $organization) {
      $query = db_select('node', 'n');
      $query->condition('n.type', SAP_NODE_TYPE_PRICE);
      $query->leftJoin('field_data_field_product', 'p', 'n.nid = p.entity_id');
      $query->leftJoin('field_data_field_operation', 'o', 'n.nid = o.entity_id');
      $query->leftJoin('field_data_field_price_sales_organization', 'so', 'n.nid = so.entity_id');
      $query->leftJoin('field_data_field_price_store', 's', 'n.nid = s.entity_id');

      $query->condition('p.field_product_target_id', $product->nid);
      $query->condition('p.deleted', 0);
      $query->condition('o.field_operation_target_id', $operation->nid);
      $query->condition('o.deleted', 0);
      $query->condition('so.field_price_sales_organization_target_id', $organization->tid);
      $query->condition('so.deleted', 0);
      if ($store) {
        $query->condition('s.field_price_store_target_id', $store->tid);
        $query->condition('s.deleted', 0);
      }
      else {
        $or = db_or();
        $or->isNull('s.field_price_store_target_id');
        $or->condition('s.field_price_store_target_id', '');
        $query->condition($or);
      }
      $query->fields('n', array('nid'));
      $nodes = $query->execute()->fetchCol();
      // If we find such price, no need do anything, all fields exists.
      if (!empty($nodes)) {
        $price_updated = TRUE;
      }

      // If no price
      // this mean that possibly we have price without operation filled
      // need check.
      if (!$price_updated && $product && $organization) {
        $query = db_select('node', 'n');
        $query->condition('n.type', SAP_NODE_TYPE_PRICE);
        $query->leftJoin('field_data_field_product', 'p', 'n.nid = p.entity_id');
        $query->leftJoin('field_data_field_operation', 'o', 'n.nid = o.entity_id');
        $query->leftJoin('field_data_field_price_sales_organization', 'so', 'n.nid = so.entity_id');
        $query->leftJoin('field_data_field_price_store', 's', 'n.nid = s.entity_id');

        $query->condition('p.field_product_target_id', $product->nid);
        $query->condition('p.deleted', 0);
        $query->condition('so.field_price_sales_organization_target_id', $organization->tid);
        $query->condition('so.deleted', 0);
        if ($store) {
          $query->condition('s.field_price_store_target_id', $store->tid);
          $query->condition('s.deleted', 0);
        }
        else {
          $or = db_or();
          $or->isNull('s.field_price_store_target_id');
          $or->condition('s.field_price_store_target_id', '');
          $query->condition($or);
        }

        $query->fields('n', array('nid'));
        $nodes = $query->execute()->fetchCol();
        // If we found, in such case operation will be empty -
        // in case this price not processed
        // or could be with operation number -
        // in such case it is already processed or cloned.
        if (!empty($nodes)) {
          // Here need only one node
          // as all nodes in result will have similar data except operation.
          $price_nid = current($nodes);
          $price = node_load($price_nid);
          if (!isset($price->field_operation[LANGUAGE_NONE][0]['target_id'])) {
            // Price not processed as no operation, add it and re-save.
            $price->field_operation[LANGUAGE_NONE][0]['target_id'] = $operation->nid;
            node_save($price);
          }
          elseif ($price->field_operation[LANGUAGE_NONE][0]['target_id'] != $operation->nid) {
            // Operation exist
            // only in such case - node already processed
            // clone it to save all data and change operation.
            $price_new = clone $price;
            $price_new->nid = NULL;
            $price_new->vid = NULL;
            $price_new->tnid = NULL;
            $price_new->log = NULL;
            $price_new->uuid = NULL;
            $price_new->vuuid = NULL;

            $price_new->uid = 1;
            $price_new->created = NULL;
            $price_new->path = NULL;
            $price_new->files = array();
            $price_new->field_operation[LANGUAGE_NONE][0]['target_id'] = $operation->nid;
            node_save($price_new);
          }
        }
      }
    }
  }

  $sandbox['progress'] = $sandbox['progress'] + count($result);
  $sandbox['last_nid_processed'] = end($result);
  $sandbox['#finished'] = !empty($sandbox['max']) ? ($sandbox['progress'] / $sandbox['max']) : $sandbox['#finished'] = 1;
  if ($sandbox['#finished'] === 1) {
    drupal_set_message(t('Updating prices for promo lines completed. Count @nodes nodes.', array('@nodes' => $sandbox['max'])));
    watchdog('update_price', 'Total update - @total', array('@total' => $sandbox['max']));
  }
}
