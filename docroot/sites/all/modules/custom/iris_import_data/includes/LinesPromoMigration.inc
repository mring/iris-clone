<?php

/**
 * A migration Operations from CSV file.
 */
class LinesPromoMigration extends Migration {

  /**
   * Construct function.
   */
  public function __construct($arguments) {
    $csv_file = isset($arguments['source_file']) ? $arguments['source_file'] : drupal_get_path('module', 'iris_import_data') . '/example/lines.csv';
    $author_id = isset($arguments['author_id']) ? $arguments['author_id'] : 1;
    parent::__construct($arguments);

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'sku' => array(
          'type' => 'int',
          'size' => 'big',
          'not null' => TRUE,
          'description' => 'SKU of Promotion line Product',
        ),
        'operation_number' => array(
          'type' => 'varchar',
          'length' => 100,
          'not null' => TRUE,
          'description' => 'Operation number of Promotion',
        ),
        'activity_sector' => array(
          'type' => 'varchar',
          'length' => 100,
          'not null' => TRUE,
          'description' => 'Activity sector',
        ),
        'slot' => array(
          'type' => 'varchar',
          'length' => 100,
          'not null' => TRUE,
          'description' => 'Slot',
        ),
        'grouping_key' => array(
          'type' => 'varchar',
          'length' => 100,
          'not null' => TRUE,
          'description' => 'Grouping Key of Promotion line',
        ),
        'slot_store' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'description' => 'Slot store',
          'default' => '',
        ),
        'promotion_rule' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'description' => 'Promotion rule',
          'default' => '',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );
    // Create a MigrateSource object, which manages retrieving the input data.
    $this->source = new MigrateSourceCSV($csv_file, $this->csvcolumns(), array(
      'embedded_newlines' => TRUE,
      'track_changes' => TRUE,
      'header_rows' => '1',
      'length' => NULL,
      'enclosure' => NULL,
      'delimiter' => ';',
      'escape' => '\\',
    ));

    $this->destination = new MigrateDestinationNode('promotion_line');

    $this->addFieldMapping('field_promo_line_activity_sector', 'activity_sector')
      ->callbacks(array($this, 'handleActiveSector'))
      ->description('Activity sector');

    $this->addFieldMapping('field_promo_line_activity_sector:source_type')
      ->defaultValue('tid');

    $this->addFieldMapping('field_operation', 'operation_number')
      ->callbacks(array($this, 'handleOperationNumber'))
      ->description('Operation number');

    $this->addFieldMapping('field_promo_sales_organization', 'sales_organization')
      ->callbacks(array($this, 'handleSalesOrg'))
      ->description('Sales organization');

    $this->addFieldMapping('field_promo_sales_organization:source_type')
      ->defaultValue('tid');

    $this->addFieldMapping('field_promo_store', 'store')
      ->callbacks(array($this, 'handleSlotStore'))
      ->description('Store');
    $this->addFieldMapping('field_promo_store:source_type')
      ->defaultValue('tid');

    $this->addFieldMapping('field_promo_slot', 'slot')
      ->description('Slot');
    $this->addFieldMapping('field_promo_slot:ignore_case')
      ->defaultValue(TRUE);

    $this->addFieldMapping('field_slot_stores', 'slot_store')
      ->separator(',')
      ->callbacks(array($this, 'handleSlotStore'))
      ->description('Slot store');

    $this->addFieldMapping('field_slot_stores:source_type')
      ->defaultValue('tid');

    $this->addFieldMapping('field_grouping_key', 'grouping_key')
      ->description('Grouping key');

    $this->addFieldMapping('field_comment', 'comment')
      ->description('Comment');

    $this->addFieldMapping('field_product', 'sku')
      ->description('Product');

    $this->addFieldMapping('field_promotion_rule', 'promotion_rule')
      ->description('Promotion rule');

    $this->addFieldMapping('field_a1_promo_sales_price', 'a1_promo_sales_price')
      ->callbacks(array($this, 'handleFloat'))
      ->description('A1 Promo sales price');
    $this->addFieldMapping('field_a1_discount', 'a1_percent_discount')
      ->callbacks(array($this, 'handleFloat'))
      ->description('A1 % discount');

    $this->addFieldMapping('field_a2_promo_sales_price', 'a2_promo_sales_price')
      ->callbacks(array($this, 'handleFloat'))
      ->description('A2 Promo sales price');
    $this->addFieldMapping('field_a2_discount', 'a2_percent_discount')
      ->callbacks(array($this, 'handleFloat'))
      ->description('A2 % discount');

    $this->addFieldMapping('field_a3_promo_sales_price', 'a3_promo_sales_price')
      ->callbacks(array($this, 'handleFloat'))
      ->description('A3 Promo sales price');
    $this->addFieldMapping('field_a3_discount', 'a3_percent_discount')
      ->callbacks(array($this, 'handleFloat'))
      ->description('A3 % discount');

    $this->addFieldMapping('field_a1_funding', 'a1_funding')
      ->callbacks(array($this, 'handleFloat'))
      ->description('A1 Funding');
    $this->addFieldMapping('field_a2_funding', 'a2_funding')
      ->callbacks(array($this, 'handleFloat'))
      ->description('A2 Funding');
    $this->addFieldMapping('field_a3_funding', 'a3_funding')
      ->callbacks(array($this, 'handleFloat'))
      ->description('A3 Funding');

    $this->addFieldMapping('field_funding_percentage', 'percent_funding')
      ->callbacks(array($this, 'handleFloat'))
      ->description('Funding percentage');

    $this->addFieldMapping('field_funding_currency', 'funding_currency')
      ->description('Funding currency');

    $this->addFieldMapping('field_purchase_organization', 'purchase_organization')
      ->description('Purchase organization');

    $this->addFieldMapping('uid')
      ->defaultValue($author_id);

  }

  /**
   * Csvcolumns function.
   */
  protected function csvcolumns() {
    // Activity sector;Operation number;
    // Sales organization;Store;Slot;Slot store;Grouping key;
    // Comment;SKU;Product description;Promotion rule;
    // A1 promo sales price;A1 % discount;A2 promo sales price;
    // % discount;A3 promo sales price;A3 % discount;A1 funding;
    // A2 funding;A3 funding;% funding;funding currency;funding type;
    // Purchase organization.
    $columns[] = array('operation_number', 'Operation number');
    $columns[] = array('slot', 'Slot');
    $columns[] = array('slot_store', 'Slot store');
    $columns[] = array('grouping_key', 'Grouping key');
    $columns[] = array('activity_sector', 'Activity sector');
    $columns[] = array('sku', 'Sku');
    $columns[] = array('product_description', 'Product description');
    $columns[] = array('sales_organization', 'Sales organization');
    $columns[] = array('store', 'Store');
    $columns[] = array('promotion_rule', 'Promotion rule');
    $columns[] = array('comment', 'Comment');
    // Optional.
    $columns[] = array('a1_promo_sales_price', 'A1 promo sales price');
    // Optional.
    $columns[] = array('a1_percent_discount', 'A1 % discount');
    // optional.
    $columns[] = array('a2_promo_sales_price', 'A2 promo sales price');
    // Optional.
    $columns[] = array('a2_percent_discount', 'A2 % discount');
    // optional.
    $columns[] = array('a3_promo_sales_price', 'A3 promo sales price');
    // optional.
    $columns[] = array('a3_percent_discount', 'A3 % discount');
    $columns[] = array('a1_funding', 'A1 funding');
    $columns[] = array('a2_funding', 'A2 funding');
    $columns[] = array('a3_funding', 'A3 funding');
    $columns[] = array('percent_funding', '% funding');
    $columns[] = array('funding_currency', 'funding currency');
    $columns[] = array('purchase_organization', 'Purchase organization');

    return $columns;
  }

  /**
   * HandleFloat function.
   */
  protected function handleFloat($value) {
    return str_replace(',', '.', $value);
  }

  /**
   * HandleSalesOrg function.
   */
  protected function handleSalesOrg($value) {
    $value = trim($value);
    if ($value == 'N/C') {
      $taxonomy_term = reset(taxonomy_get_term_by_name($value, 'sales_organization'));
      return $taxonomy_term ? $taxonomy_term->tid : FALSE;
    }
    $taxonomy_term = $this->getTermByCode(trim($value), 'sales_organization') ? $this->getTermByCode(trim($value), 'sales_organization') : FALSE;
    return $taxonomy_term ? $taxonomy_term : FALSE;
  }

  /**
   * HandleActiveSector function.
   */
  protected function handleActiveSector($value) {
    $taxonomy_term = $this->getTermByCode(trim($value), 'activity_sector') ? $this->getTermByCode(trim($value), 'activity_sector') : FALSE;
    return $taxonomy_term ? $taxonomy_term : FALSE;
  }

  /**
   * HandleSlotStore function.
   */
  protected function handleSlotStore($value) {
    if (is_array($value)) {
      $value = array_map('trim', $value);
    }
    $taxonomy_term = $this->getTermByCode($value, 'store') ? $this->getTermByCode($value, 'store') : FALSE;
    return $taxonomy_term ? $taxonomy_term : FALSE;
  }

  /**
   * HandleOperationNumber function.
   */
  public function handleOperationNumber($operation_number) {
    return $this->getNodeByTitle($operation_number, 'operation') ? $this->getNodeByTitle($operation_number, 'operation') : FALSE;
  }

  /**
   * PrepareKey function.
   */
  public function prepareKey($source_key, $row) {
    if (!empty($row->slot_store)) {
      $slot_store = array_map('trim', explode(',', $row->slot_store));
      asort($slot_store);
      $row->slot_store = implode(',', $slot_store);
    }

    return parent::prepareKey($source_key, $row);
  }

  /**
   * PrepareRow function.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $return = TRUE;

    foreach ($this->csvcolumns() as $column) {
      if (!drupal_validate_utf8($row->$column[0])) {
        $this->savePrepareMessage(t('@col_name value "@col_value" contain not valid UTF-8 strings in row @rownum', array(
          '@col_name' => $column[1],
          '@col_value' => utf8_encode($row->$column[0]),
          '@rownum' => $row->csvrownum + 1,
        )), MigrationBase::MESSAGE_ERROR);
        $row->$column[0] = utf8_encode($row->$column[0]);
        $return = FALSE;
      }
    }

    foreach ($this->csvcolumns() as $column) {
      switch ($column[0]) {
        case "sku":
          if (!is_numeric($row->$column[0])) {
            $this->savePrepareMessage(t('@col_name value "@col_value" is not integer in row @rownum', array(
              '@col_name' => $column[1],
              '@col_value' => $row->$column[0],
              '@rownum' => $row->csvrownum + 1,
            )), MigrationBase::MESSAGE_ERROR);
            $return = FALSE;
            break;
          }
        case "activity_sector":
        case "operation_number":
        case "sales_organization":
        case "slot":
        case "grouping_key":
        case "promotion_rule":
          if (empty($row->$column[0])) {
            $this->savePrepareMessage(t('@col_name is empty in row @rownum', array(
              '@col_name' => $column[1],
              '@rownum' => $row->csvrownum + 1,
            )), MigrationBase::MESSAGE_ERROR);
            $return = FALSE;
          }
          break;

        default:
          break;
      }
    }

    if (!empty($row->migrate_map_destid1) && $this->isPromotionValidated($row->migrate_map_destid1)) {
      $this->savePrepareMessage(t('Impossible to import the line @rownum with promotion line id @promotion_line, already validated.', array(
        '@promotion_line' => $row->migrate_map_destid1,
        '@rownum' => $row->csvrownum + 1,
      )), MigrationBase::MESSAGE_ERROR);
      $return = FALSE;
    }

    return $return;
  }

  /**
   * Checking is promotion already validated.
   */
  private function isPromotionValidated($promotion_line_nid) {
    $query = db_select('field_data_field_promotion_lines', 'promoline');
    $query->fields('promoline', array('field_promotion_lines_target_id'));
    $query->condition('promoline.field_promotion_lines_target_id', $promotion_line_nid, '=');
    $query->join('field_data_field_reference_validation', 'validation', 'promoline.entity_id=validation.entity_id');
    $query->addField('promoline', 'entity_id', 'promotion_id');
    return $query->execute()->rowCount();
  }

  /**
   * Save messages before import.
   */
  public function savePrepareMessage($message, $level = MigrationBase::MESSAGE_NOTICE) {
    $prepareMessage = new stdClass();
    $prepareMessage->message = $message;
    $prepareMessage->level = $level;
    $prepareMessage->sourceKey = $this->source->getCurrentKey();
    $this->prepareMessages[] = $prepareMessage;
  }

  /**
   * Safe the messages after import.
   */
  protected function import() {
    $return = parent::import();

    if (isset($this->prepareMessages)) {
      foreach ($this->prepareMessages as $message) {
        $this->irisSaveMessage($message->sourceKey, $message->message, $message->level);
      }
    }
    return $return;
  }

  /**
   * Saving logs function.
   */
  public function irisSaveMessage($source_key, $message, $level = Migration::MESSAGE_ERROR) {
    // Source IDs as arguments.
    $count = 1;
    if (is_array($source_key)) {
      foreach ($source_key as $key_value) {
        $fields['sourceid' . $count++] = $key_value ? check_plain(utf8_encode($key_value)) : -1;
      }
      if (!is_numeric($fields['sourceid1'])) {
        $fields['sourceid1'] = -1;
      }
      $fields['level'] = $level;
      $fields['message'] = $message;

      db_insert(Migration::currentMigration()->getMap()->getMessageTable())
        ->fields($fields)
        ->execute();
    }
    else {
      // TODO: What else can we do?
      Migration::displayMessage($message);
    }
  }

  /**
   * Prepare migration function.
   */
  public function prepare($node, stdClass $row) {
    $pr_node_wrapper = entity_metadata_wrapper('node', $node);

    if (!$pr_node_wrapper->field_operation->value()) {
      throw new MigrateException('Operation number not exist');
    }

    if (!$pr_node_wrapper->field_promo_slot->value()) {
      throw new MigrateException('Slot field value is invalid');
    }

    if (!$pr_node_wrapper->field_promo_line_activity_sector->value()) {
      throw new MigrateException('Activity sector field value is invalid');
    }

    if (!$pr_node_wrapper->field_grouping_key->value()) {
      throw new MigrateException('Grouping key field value is invalid');
    }

    if (!$pr_node_wrapper->field_operation->value()) {
      throw new MigrateException('Operation field value is invalid');
    }

    if (!$pr_node_wrapper->field_operation->status->value()) {
      throw new MigrateException('Operation is not published');
    }

    if ($operation_description = $pr_node_wrapper->field_operation->field_operation_description->value()) {
      $node->title = $operation_description . ' ' . $row->grouping_key;
    }
    else {
      throw new MigrateException('Operation description value not exist');
    }

    if (!$promotion_start_date = $pr_node_wrapper->field_operation->field_operation_start_date->value()) {
      throw new MigrateException('Operation start date have empty value');
    }

    if (!empty($row->sku) && !empty($row->sales_organization)) {
      $sales_organization = $row->sales_organization;
      $operation_number_nid = $pr_node_wrapper->field_operation->value() ? $pr_node_wrapper->field_operation->nid->value() : '';
      $date = new DateTime();
      $date->setTimestamp($promotion_start_date);
      $start_date = $date->format('d/m/Y');
      $store = '';
      if ($pr_node_wrapper->field_promo_store->value() && $pr_node_wrapper->field_promo_store->field_taxonomy_code->value()) {
        $store = $pr_node_wrapper->field_promo_store->field_taxonomy_code->value();
      }
      $purchase_organization = '';
      if ($pr_node_wrapper->field_purchase_organization->value()) {
        $purchase_organization = $pr_node_wrapper->field_purchase_organization->value();
      }

      $resulted_nodes = array();
      iris_sap_import_api(trim($row->sku), $operation_number_nid, trim($start_date), trim($sales_organization), $resulted_nodes, $purchase_organization, $store);
      if (isset($resulted_nodes['product_node']) && $resulted_nodes['product_node']) {
        $pr_node_wrapper->field_product->set($resulted_nodes['product_node']);
        $product_node_wrapper = entity_metadata_wrapper('node', $resulted_nodes['product_node']);
      }
      else {
        throw new MigrateException('Product not imported because SKU not found in SAP');
      }

      if (isset($resulted_nodes['price'])) {
        $price_node_wrapper = entity_metadata_wrapper('node', $resulted_nodes['price']);
      }
      else {
        throw new MigrateException('Price not updated/created');
      }

      $sales_org = $this->getTermByCode($row->sales_organization, 'sales_organization');
      $parent_lgs = og_subgroups_parents_load('taxonomy_term', $sales_org, FALSE);
      $rounding_rule = FALSE;
      if (isset($parent_lgs['taxonomy_term'])) {
        $country = taxonomy_term_load(end($parent_lgs['taxonomy_term']));
        if (isset($country->field_rounding_rule[LANGUAGE_NONE][0]['value'])) {
          $rounding_rule = $country->field_rounding_rule[LANGUAGE_NONE][0]['value'];
        }
        else {
          if ($row->sales_organization == 'N/C') {
            $rounding_rule = 1;
          }
          else {
            throw new MigrateException('Rounding rule not available.');
          }
        }
      }
      else {
        if ($row->sales_organization == 'N/C') {
          $rounding_rule = 1;
        }
        else {
          throw new MigrateException('Can\'t get Country from "' . $row->sales_organization . '" Sales Organization.');
        }
      }

      if (!$pr_node_wrapper->field_promotion_rule->value()) {
        throw new MigrateException('Promotion rule not available in system.');
      }

      $product_weight = FALSE;

      $this->productUpdateConvertVolumeWeight($pr_node_wrapper, $product_node_wrapper);

      if (isset($product_node_wrapper) && $product_node_wrapper->field_product_weight->value()) {
        $product_weight = str_replace(array(' KG', ' g', ','), array(
          '',
          '',
          '.',
        ), $product_node_wrapper->field_product_weight->value());
        $product_weight = $product_weight ? $product_weight : 0;
      }
      else {
        throw new MigrateException('Cant get weight of product');
      }

      if (isset($product_node_wrapper) && $product_node_wrapper->field_product_volume->value()) {
        $product_volume = str_replace(array(
          ' DM3',
          ' L',
          ' VE',
          ' ml',
          ',',
        ), array(
          '',
          '',
          '',
          '',
          '.',
        ), $product_node_wrapper->field_product_volume->value());
        $product_volume = $product_volume ? $product_volume : 0;
      }
      else {
        throw new MigrateException('Cant get volume of product');
      }

      if ($activity_sector_code = $pr_node_wrapper->field_promo_line_activity_sector->field_taxonomy_code->value()) {
        if (in_array($activity_sector_code, array(
          'PA',
          'SO',
          'AL',
          'VI',
          'CH',
        ))
        ) {
          $product_weight = $product_volume;
          if (in_array($activity_sector_code, array('PA', 'SO'))) {
            $product_weight = round($product_weight / 100, 2);
          }
        }
        elseif ($activity_sector_code == 'MA') {
          $product_weight = round($product_weight / 100, 2);
        }
      }

      if (!$pr_node_wrapper->field_promotion_rule->field_calculation_method->value()) {
        throw new MigrateException('Calculation method not selected in "' . $pr_node_wrapper->field_promotion_rule->label() . '"');
      }

      for ($zone = 1; $zone <= 3; $zone++) {
        unset($d);
        $sp = is_numeric($price_node_wrapper->{'field_a' . $zone . '_sales_amount'}->value()) ? $price_node_wrapper->{'field_a' . $zone . '_sales_amount'}->value() : FALSE;
        $purchase_price = is_numeric($price_node_wrapper->{'field_purchase_amount'}->value()) ? $price_node_wrapper->{'field_purchase_amount'}->value() : FALSE;

        $promo_sp = $pr_node_wrapper->{'field_a' . $zone . '_promo_sales_price'}->value() ? $pr_node_wrapper->{'field_a' . $zone . '_promo_sales_price'}->value() : FALSE;
        $perc_discount = $pr_node_wrapper->{'field_a' . $zone . '_discount'}->value() ? $pr_node_wrapper->{'field_a' . $zone . '_discount'}->value() : FALSE;

        // Leave funding original for amount if it is in CSV.
        $funding_amount_original = $pr_node_wrapper->{'field_a' . $zone . '_funding'}->value() ? $pr_node_wrapper->{'field_a' . $zone . '_funding'}->value() : FALSE;

        if ($pr_node_wrapper->field_purchase_organization->value()) {
          $pr_node_wrapper->field_funding_type->set('purchase');
          $funding_type = 'purchase';
        }
        else {
          $pr_node_wrapper->field_funding_type->set('sales');
          $funding_type = 'sales';
        }
        $funding_perc = $pr_node_wrapper->field_funding_percentage->value() ? $pr_node_wrapper->field_funding_percentage->value() : FALSE;

        $x = $pr_node_wrapper->field_promotion_rule->field_x->value() ? $pr_node_wrapper->field_promotion_rule->field_x->value() : FALSE;
        $y = $pr_node_wrapper->field_promotion_rule->field_y->value() ? $pr_node_wrapper->field_promotion_rule->field_y->value() : FALSE;

        switch ($pr_node_wrapper->field_promotion_rule->field_calculation_method->value()) {
          case "discount":
            if (!is_numeric($sp)) {
              throw new MigrateException('Sales Price not available');
            }

            if ($perc_discount) {
              $pp = $sp - $sp * $perc_discount;
              switch ($rounding_rule) {
                case 1:
                  // Rounded down to the unit.
                  $pp = floor($pp);
                  break;

                case 2:
                  // Rounded down 10 unit.
                  $pp = floor(floor($pp) / 10) * 10;
                  break;

                case 3:
                  // Rounded down 10 unit - 1.
                  $pp = floor($pp);
                  if (substr($pp, -1, 1) != 9) {
                    $pp = (floor($pp / 10) * 10) - 1;
                  }
                  break;

                case 4:
                  // Rounded down 10 cents.
                  $string_num = sprintf("%.2f", $pp);
                  $pp = (float) (substr($string_num, 0, strpos($string_num, '.'))) + (float) ('0.' . floor(substr($string_num, strpos($string_num, '.') + 1) / 10));
                  break;

                case 5:
                  // Rounded down 10 cents - 1.
                  $pp = number_format($pp, 2, '.', '');
                  if (substr($pp, -1, 1) != 9) {
                    $string_num = sprintf("%.2f", $pp);
                    $pp = (float) (substr($string_num, 0, strpos($string_num, '.'))) + (float) ('0.' . floor(substr($string_num, strpos($string_num, '.') + 1) / 10)) - 0.01;
                  }
                  break;

                default:
                  break;
              }

              $pr_node_wrapper->{'field_a' . $zone . '_calculated_promo_price'}->set($pp);
              $d = $sp - $pp;
              $pr_node_wrapper->{'field_a' . $zone . '_calculated_discount'}->set($d);
              if ($product_weight > 0) {
                $sp_l_kg = round($pp / $product_weight, 2);
                $pr_node_wrapper->{'field_a' . $zone . '_sales_price_l_kg'}->set($sp_l_kg);
              }
              else {
                $pr_node_wrapper->{'field_a' . $zone . '_sales_price_l_kg'}->set(0);
              }
            }

            if (!$funding_amount_original) {
              if ($funding_type && $funding_type == 'purchase' && $purchase_price && $funding_perc && $perc_discount && isset($d)) {
                $pp = $purchase_price - $purchase_price * $perc_discount;
                $d = $purchase_price - $pp;
                $funding_amount = $d * $funding_perc;
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set($funding_amount);
              }
              elseif ($funding_perc && $perc_discount && isset($d)) {
                $funding_amount = $d * $funding_perc;
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set($funding_amount);
              }
              elseif (!$funding_perc) {
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set(0);
              }
              else {
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set(0);
              }
            }
            else {
              $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set(0);
            }
            break;

          case "x_for_p":
            if (!is_numeric($sp)) {
              throw new MigrateException('Sales Price not available');
            }

            if (!$x) {
              throw new MigrateException('X value not available');
            }
            if ($promo_sp) {
              $pp = $promo_sp;
              $pr_node_wrapper->{'field_a' . $zone . '_calculated_promo_price'}->set($pp);
              $d = $sp * $x - $pp;
              $pr_node_wrapper->{'field_a' . $zone . '_calculated_discount'}->set($d);
              if ($product_weight > 0) {
                $sp_l_kg = round($pp / ($x * $product_weight), 2);
                $pr_node_wrapper->{'field_a' . $zone . '_sales_price_l_kg'}->set($sp_l_kg);
              }
              else {
                $pr_node_wrapper->{'field_a' . $zone . '_sales_price_l_kg'}->set(0);
              }
            }

            if (!$funding_amount_original) {
              if ($funding_type && $funding_type == 'purchase' && $funding_perc && isset($d)) {
                if (!is_numeric($purchase_price)) {
                  throw new MigrateException('Purchase price empty');
                }
                $pp = $purchase_price;
                $d = $purchase_price * $x - $pp;
                $funding_amount = $d * $funding_perc;
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set($funding_amount);
              }
              elseif ($funding_perc && isset($d)) {
                $funding_amount = $d * $funding_perc;
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set($funding_amount);
              }
              elseif (!$funding_perc) {
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set(0);
              }
            }
            else {
              $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set(0);
            }
            break;

          case "x_plus_y":

            if (!is_numeric($sp)) {
              throw new MigrateException('Sales Price not available');
            }

            if (!$x) {
              throw new MigrateException('X value not available');
            }

            if (!$y) {
              throw new MigrateException('Y value not available');
            }

            $pp = $sp * $x;
            $pr_node_wrapper->{'field_a' . $zone . '_calculated_promo_price'}->set($pp);
            $d = $sp * $y;
            $pr_node_wrapper->{'field_a' . $zone . '_calculated_discount'}->set($d);
            if ($product_weight > 0) {
              $sp_l_kg = round($pp / (($x + $y) * $product_weight), 2);
              $pr_node_wrapper->{'field_a' . $zone . '_sales_price_l_kg'}->set($sp_l_kg);
            }
            else {
              $pr_node_wrapper->{'field_a' . $zone . '_sales_price_l_kg'}->set(0);
            }

            if (!$funding_amount_original) {
              if ($funding_type && $funding_type == 'purchase' && $funding_perc) {
                if (!is_numeric($purchase_price)) {
                  throw new MigrateException('Purchase price empty');
                }
                $d = $purchase_price * $y;
                $funding_amount = $d * $funding_perc;
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set($funding_amount);
              }
              elseif ($funding_perc) {
                $funding_amount = $d * $funding_perc;
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set($funding_amount);
              }
              elseif (!$funding_perc) {
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set(0);
              }
            }
            else {
              $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set($funding_amount_original);
            }
            break;

          case "buy_x_get_y_percent_off":
            if (!is_numeric($sp)) {
              throw new MigrateException('Sales Price not available');
            }

            if (!$x) {
              throw new MigrateException('X value not available');
            }

            if (!$y) {
              throw new MigrateException('Y value not available');
            }

            $pp = $sp * $x - $sp * $y;
            switch ($rounding_rule) {
              case 1:
                $pp = floor($pp);
                break;

              case 2:
                $pp = floor(floor($pp) / 10) * 10;
                break;

              case 3:
                $pp = (floor(floor($pp) / 10) * 10) - 1;
                break;

              case 4:
                $string_num = sprintf("%.2f", $pp);
                $pp = (float) (substr($string_num, 0, strpos($string_num, '.'))) + (float) ('0.' . floor(substr($string_num, strpos($string_num, '.') + 1) / 10));
                break;

              case 5:
                $string_num = sprintf("%.2f", $pp);
                $pp = (float) (substr($string_num, 0, strpos($string_num, '.'))) + (float) ('0.' . floor(substr($string_num, strpos($string_num, '.') + 1) / 10)) - 0.01;
                break;

              default:
                break;
            }

            $pr_node_wrapper->{'field_a' . $zone . '_calculated_promo_price'}->set($pp);
            $d = $sp * $x - $pp;
            $pr_node_wrapper->{'field_a' . $zone . '_calculated_discount'}->set($d);
            if ($product_weight > 0) {
              $sp_l_kg = round($pp / ($x * $product_weight), 2);
              $pr_node_wrapper->{'field_a' . $zone . '_sales_price_l_kg'}->set($sp_l_kg);
            }
            else {
              $pr_node_wrapper->{'field_a' . $zone . '_sales_price_l_kg'}->set(0);
            }

            if (!$funding_amount_original) {
              if ($funding_type && $funding_type == 'purchase' && $funding_perc) {
                if (!$purchase_price) {
                  throw new MigrateException('Purchase price empty');
                }
                $pp = $purchase_price * $x - $purchase_price * $y;
                $d = $purchase_price * $x - $pp;
                $funding_amount = $d * $funding_perc;
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set($funding_amount);
              }
              elseif ($funding_perc) {
                $funding_amount = $d * $funding_perc;
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set($funding_amount);
              }
              elseif (!$funding_perc) {
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set(0);
              }
            }
            else {
              $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set(0);
            }
            break;

          case "no_calcul":
            if (!is_numeric($sp)) {
              throw new MigrateException('Sales Price not available');
            }
            $pp = $sp;
            $pr_node_wrapper->{'field_a' . $zone . '_calculated_promo_price'}->set($pp);
            $d = 0;
            $pr_node_wrapper->{'field_a' . $zone . '_calculated_discount'}->set($d);
            if ($product_weight > 0) {
              $sp_l_kg = round($pp / $product_weight, 2);
              $pr_node_wrapper->{'field_a' . $zone . '_sales_price_l_kg'}->set($sp_l_kg);
            }
            else {
              $pr_node_wrapper->{'field_a' . $zone . '_sales_price_l_kg'}->set(0);
            }

            if (!$funding_amount_original) {
              if ($funding_type && $funding_type == 'purchase' && $funding_perc) {
                $d = 0;
                $funding_amount = $d * $funding_perc;
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set($funding_amount);
              }
              elseif ($funding_perc) {
                $funding_amount = $d * $funding_perc;
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set($funding_amount);
              }
              elseif (!$funding_perc) {
                $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set(0);
              }
            }
            else {
              $pr_node_wrapper->{'field_a' . $zone . '_funding_amount'}->set(0);
            }
            break;

          default:
            break;
        }
      }

    }
    $node = isset($pr_node_wrapper) ? $pr_node_wrapper->value() : $node;
  }

  /**
   * GetTermByCode migration function.
   */
  public function getTermByCode($code, $vocabulary_name) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'taxonomy_term');
    $query->propertyCondition('vid', taxonomy_vocabulary_machine_name_load($vocabulary_name)->vid);

    if (is_array($code)) {
      $query->fieldCondition('field_taxonomy_code', 'value', $code, 'IN');
    }
    else {
      $query->fieldCondition('field_taxonomy_code', 'value', trim($code), '=');
      $query->range(0, 1);
    }

    $result = $query->execute();

    if (isset($result['taxonomy_term'])) {
      if (is_array($code)) {
        return array_keys($result['taxonomy_term']);
      }

      return key($result['taxonomy_term']);
    }
    else {
      return FALSE;
    }
  }

  /**
   * GetNodeByTitle migration function.
   */
  public function getNodeByTitle($title, $node_type) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', $node_type)
      ->propertyCondition('title', trim($title))
      ->range(0, 1);

    $result = $query->execute();

    if (isset($result['node'])) {
      return key($result['node']);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Complete migration function.
   */
  public function complete($entity, stdClass $row) {

    if (!empty($this->logID)) {
      $this->LogID = $this->logID;
    }

  }

  /**
   * Convert volume weight function.
   */
  public function productUpdateConvertVolumeWeight($wr_promo_node, &$wr_product_node) {
    if ($activity_sector_code = $wr_promo_node->field_promo_line_activity_sector->field_taxonomy_code->value()) {
      if (in_array($activity_sector_code, array(
        'PA',
        'SO',
      )) && isset($wr_product_node) && $wr_product_node->field_product_volume->value()
      ) {
        $product_volume = str_replace(array(' DM3', ' L', ' VE', ','), array(
          '',
          '',
          '',
          '.',
        ), $wr_product_node->field_product_volume->value());
        $product_volume = $product_volume ? $product_volume * 1000 : 0;
        $wr_product_node->field_product_volume->set(str_replace('.', ',', $product_volume) . ' ml');
        $wr_product_node->save();
      }
      elseif (strpos($wr_product_node->field_product_volume->value(), ' VE') !== FALSE) {
        $product_volume = str_replace(array(' VE'), array(
          ' L',
        ), $wr_product_node->field_product_volume->value());

        $wr_product_node->field_product_volume->set($product_volume);
        $wr_product_node->save();
      }

      if (in_array($activity_sector_code, array('MA')) && isset($wr_product_node) && $wr_product_node->field_product_weight->value()) {
        $product_weight = str_replace(array(' KG', ','), array(
          '',
          '.',
        ), $wr_product_node->field_product_weight->value());
        $product_weight = $product_weight ? $product_weight * 1000 : 0;
        $wr_product_node->field_product_weight->set(str_replace('.', ',', $product_weight) . ' g');
        $wr_product_node->save();
      }
    }
  }

  /**
   * PostImport migrate function.
   */
  public function postImport() {
    parent::postImport();

    if (!$queues = advancedqueue_get_queues_info(array('add_edit_promotion' => 'add_edit_promotion'))) {
      return;
    }
    foreach ($queues as $queue_name => $queue_info) {
      $queue = DrupalQueue::get($queue_name);
      while ($item = $queue->claimItem()) {
        advancedqueue_process_item($queue, $queue_name, $queue_info, $item, FALSE);
      }
    }
  }

}
