<?php

/**
 * A migration that is reused for each source CSV file.
 */
class ActivityDomainTaxonomyMigration extends DSCommonMigration {

  /**
   * Construct function.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Stores from CSV file.');

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'activity_domain_code' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Code of organization level',
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    $this->pkcollumn = 'activity_domain_code';

    $this->title_parts = array(
      'activity_domain_code',
      'activity_domain_name',
    );

    $this->addFieldMapping('field_taxonomy_code', 'activity_domain_code')
      ->description('Activity Domain Code');

    $this->destination = new MigrateDestinationTerm('activity_domain', $this->term_options);

    $this->addFieldMapping('activity_domain_code', 'activity_domain_code');

    $this->addFieldMapping('name', 'activity_domain_code')
      ->description('Activity Domain');

    $this->addFieldMapping('description_field', 'activity_domain_name');

    $this->addUnmigratedSources(array(
      'country_code',
      'country_name',
      'company_code',
      'company_name',
      'sales_organization_code',
      'sales_organization_name',
      'store_platform_code',
      'store_platform_name',
      'terminal_code',
      'terminal_name',
      'store_code',
      'store_name',
      'activity_sector_code',
      'activity_sector_name',
    )
    );
  }

  /**
   * CreateStub.
   *
   * @param \Migration $migration
   *   Migration oberct.
   * @param array $source_id
   *   Source array ID.
   *
   * @return array|bool
   *   Indicate if term created or not.
   *
   * @throws \Exception
   */
  protected function createStub(Migration $migration, array $source_id) {
    $term = new stdClass();
    $term->name = t('Stub for @id', array('@id' => $source_id[0]));
    $term->vocabulary_machine_name = $this->destination->getBundle();
    $taxonomy_term = taxonomy_term_save($term);
    if (isset($taxonomy_term->tid)) {
      return array($taxonomy_term->tid);
    }
    else {
      return FALSE;
    }
  }

}
