<?php

/**
 * A migration Operations from CSV file.
 */
class SalesOrganizationTaxonomyMigration extends CommonMigration {

  /**
   * Construct function.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Sales organization from CSV file.');

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'sales_organization_code' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Code of organization level',
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    $this->pkcollumn = 'sales_organization_code';

    $this->title_parts = array(
      'sales_organization_code',
      'sales_organization_name',
    );

    $this->addFieldMapping('field_taxonomy_code', 'sales_organization_code')
      ->description('Company Code');

    $this->destination = new MigrateDestinationTerm('sales_organization', $this->term_options);

    $this->addFieldMapping('sales_organization_code', 'sales_organization_code');

    $this->addFieldMapping('name', 'sales_organization_code')
      ->description('Sales organization');

    $this->addFieldMapping('description_field', 'sales_organization_name');

    $this->addFieldMapping('group_group')
      ->defaultValue(1)
      ->description('Group');

    $this->addFieldMapping('field_company_code', 'company_code')
      ->callbacks(array($this, 'handleParentGroup'))
      ->description(t('ParentTaxonomy'));

    $this->addUnmigratedSources(array(
      'country_code',
      'country_name',
      'company_code',
      'company_name',
      'store_platform_code',
      'store_platform_name',
      'terminal_code',
      'terminal_name',
      'store_code',
      'store_name',
      'activity_domain_code',
      'activity_domain_name',
      'activity_sector_code',
      'activity_sector_name',
    )
    );
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return drupal_strtolower(pathinfo($this->arguments['source_file'],
      PATHINFO_FILENAME));
  }

  /**
   * PrepareRow function.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    if (!$this->handleParentGroup($row->company_code, 'company_code')) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * HandleParentGroup migration function.
   */
  public function handleParentGroup($values, $voc_machine_name = FALSE) {
    return parent::handleParentGroup('company_code', $values);
  }

  /**
   * CreateStub.
   *
   * @param \Migration $migration
   *   Migration oberct.
   * @param array $source_id
   *   Source array ID.
   *
   * @return array|bool
   *   Indicate if term created or not.
   *
   * @throws \Exception
   */
  protected function createStub(Migration $migration, array $source_id) {
    $term = new stdClass();
    $term->name = t('Stub for @id', array('@id' => $source_id[0]));
    $term->vocabulary_machine_name = $this->destination->getBundle();
    $taxonomy_term = taxonomy_term_save($term);
    if (isset($taxonomy_term->tid)) {
      return array($taxonomy_term->tid);
    }
    else {
      return FALSE;
    }
  }

}
