<?php

/**
 * A migration that is reused for each source CSV file.
 */
class CompanyTaxonomyMigration extends CommonMigration {

  /**
   * Construct function.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Companies from CSV file.');

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'company_code' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Code of organization level',
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    $this->pkcollumn = 'company_code';

    $this->title_parts = array(
      'company_code',
      'company_name',
    );

    $this->addFieldMapping('field_taxonomy_code', 'company_code')
      ->description('Company Code');

    $this->destination = new MigrateDestinationTerm('company_code', $this->term_options);

    $this->addFieldMapping('company_code', 'company_code');

    $this->addFieldMapping('name', 'company_code')
      ->description('Company');

    $this->addFieldMapping('description_field', 'company_name');

    $this->addFieldMapping('group_group')
      ->defaultValue(1)
      ->description('Group');

    $this->addFieldMapping('field_country', 'country_code')
      ->callbacks(array($this, 'handleParentGroup'))
      ->description(t('ParentTaxonomy'));

    $this->addUnmigratedSources(array(
      'country_code',
      'country_name',
      'sales_organization_code',
      'sales_organization_name',
      'store_platform_code',
      'store_platform_name',
      'terminal_code',
      'terminal_name',
      'store_code',
      'store_name',
      'activity_domain_code',
      'activity_domain_name',
      'activity_sector_code',
      'activity_sector_name',
    )
    );
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return drupal_strtolower(pathinfo($this->arguments['source_file'],
      PATHINFO_FILENAME));
  }

  /**
   * PrepareRow migration function.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    if (!$this->handleParentGroup($row->country_code, 'country')) {
      watchdog('LGImport', 'Country %country have not found. Location group import ignored line: %row .', array(
        '%country' => $row->country_code,
        '%row' => print_r($row, 1),
      ));
      return FALSE;
    }
    return TRUE;
  }

  /**
   * HandleParentGroup function.
   */
  public function handleParentGroup($values, $voc_machine_name = FALSE) {
    return parent::handleParentGroup('country', $values);
  }

}
