<?php

/**
 * A migration Operations from CSV file.
 */
class TerminalTaxonomyMigration extends CommonMigration {

  /**
   * Construct function.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Terminal from CSV file.');

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'terminal_code' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Code of organization level',
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    $this->pkcollumn = 'terminal_code';

    $this->title_parts = array(
      'terminal_code',
      'terminal_name',
    );

    $this->addFieldMapping('field_taxonomy_code', 'terminal_code')
      ->description('Company Code');

    $this->destination = new MigrateDestinationTerm('terminal', $this->term_options);

    $this->addFieldMapping('terminal_code', 'terminal_code');

    $this->addFieldMapping('name', 'terminal_code')
      ->description('Terminal');

    $this->addFieldMapping('description_field', 'terminal_name');

    $this->addFieldMapping('group_group')
      ->defaultValue(1)
      ->description('Group');

    $this->addFieldMapping('field_store_platform', 'store_platform_code')
      ->callbacks(array($this, 'handleParentGroup'))
      ->description(t('ParentTaxonomy'));

    $this->addUnmigratedSources(array(
      'country_code',
      'country_name',
      'company_code',
      'company_name',
      'sales_organization_code',
      'sales_organization_name',
      'store_platform_code',
      'store_platform_name',
      'store_code',
      'store_name',
      'activity_domain_code',
      'activity_domain_name',
      'activity_sector_code',
      'activity_sector_name',
    )
    );
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return drupal_strtolower(pathinfo($this->arguments['source_file'],
      PATHINFO_FILENAME));
  }

  /**
   * PrepareRow function.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    if (!$this->handleParentGroup($row->store_platform_code, 'store_platform')) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * HandleParentGroup function.
   */
  public function handleParentGroup($values, $voc_machine_name = FALSE) {
    return parent::handleParentGroup('store_platform', $values);
  }

}
