<?php

/**
 * A migration Operations from CSV file.
 */
class OperationsPromoMigration extends Migration {

  /**
   * Construct function.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $sourc_file = & drupal_static('iris_migrate_source_file_operation', FALSE);
    $csv_file = $sourc_file ? $sourc_file : drupal_get_path('module', 'iris_import_data') . '/example/operations.csv';

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'operation_number' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Operation number of Promotion',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );
    // Create a MigrateSource object, which manages retrieving the input data.
    $this->source = new MigrateSourceCSV($csv_file, $this->csvcolumns(), array(
      'embedded_newlines' => TRUE,
      'track_changes' => TRUE,
      'header_rows' => '1',
      'length' => NULL,
      'enclosure' => NULL,
      'delimiter' => ';',
      'escape' => '\\',
    ));

    $this->destination = new MigrateDestinationNode('operation');

    $this->addFieldMapping('title', 'operation_number')
      ->description('Operation number');

    $this->addFieldMapping('field_operation_start_date', 'start_date')
      ->callbacks(array($this, 'handleDate'))
      ->description('Start date');
    $this->addFieldMapping('field_operation_start_date:timezone')
      ->defaultValue('Europe/Paris');

    $this->addFieldMapping('field_operation_end_date', 'end_date')
      ->callbacks(array($this, 'handleDate'))
      ->description('End date');
    $this->addFieldMapping('field_operation_end_date:timezone')
      ->defaultValue('Europe/Paris');

    $this->addFieldMapping('field_operation_description', 'operation_description')
      ->callbacks(array($this, 'handleString'))
      ->description('Operation description');

    $this->addFieldMapping('field_thematic', 'thematic')
      ->callbacks(array($this, 'handleString'))
      ->description('Thematic');

    $this->addFieldMapping('field_thematic:create_term')
      ->defaultValue(TRUE);

    $this->addFieldMapping('uid')
      ->defaultValue(1);

  }

  /**
   * Csvcolumns function.
   */
  protected function csvcolumns() {
    // Operation number;Start date;End date;Operation description;Thematic.
    $columns[0] = array('operation_number', 'Operation number');
    $columns[1] = array('start_date', 'Start date');
    $columns[2] = array('end_date', 'End date');
    $columns[3] = array('operation_description', 'Operation description');
    $columns[4] = array('thematic', 'Thematic');

    return $columns;
  }

  /**
   * HandleString migrate function.
   */
  protected function handleString($value) {
    return trim($value);
  }

  /**
   * HandleDate migrate function.
   */
  protected function handleDate($value, $getTimstamp = FALSE) {
    $date = DateTime::createFromFormat('d/m/Y', $value, new DateTimeZone('Europe/Paris'));
    $errors = DateTime::getLastErrors();
    if ($errors['warning_count'] > 0 || $errors['error_count'] > 0) {
      foreach (array_merge($errors['warnings'], $errors['errors']) as $error_messages) {
        $error_message = t($error_messages . ' (@date)', array('@date' => $value));
        $this->queueMessage($error_message, MigrationBase::RESULT_FAILED);
        $this->displayMessage($error_message);
      }
      $this->saveQueuedMessages();

      return NULL;

    }
    return $getTimstamp ? $date->getTimestamp() : $date->format('Y-m-d 00:00');
  }

  /**
   * PrepareRow function.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    $return = TRUE;
    if (empty($row->operation_number)) {
      drupal_set_message(t('Operation number is empty in row @rownum', array('@rownum' => $row->csvrownum + 1)), 'error');
      $return = FALSE;
    }

    if (empty($row->start_date)) {
      drupal_set_message(t('Start date is empty in row @rownum', array('@rownum' => $row->csvrownum + 1)), 'error');
      $return = FALSE;
    }
    elseif (!$startdate_ts = $this->handleDate($row->start_date, TRUE)) {
      drupal_set_message(t('Start date is incorrect in row @rownum', array('@rownum' => $row->csvrownum + 1)), 'error');
      $return = FALSE;
    }

    if (empty($row->end_date)) {
      drupal_set_message(t('End date is empty in row @rownum', array('@rownum' => $row->csvrownum + 1)), 'error');
      $return = FALSE;
    }
    elseif (!$enddate_ts = $this->handleDate($row->end_date, TRUE)) {
      drupal_set_message(t('End date is incorrect in row @rownum', array('@rownum' => $row->csvrownum + 1)), 'error');
      $return = FALSE;
    }

    if (isset($startdate_ts) && isset($enddate_ts) && $startdate_ts > $enddate_ts) {
      drupal_set_message(t('End date is incorrect (End date before Start date) in row @rownum', array('@rownum' => $row->csvrownum + 1)), 'error');
      $return = FALSE;
    }

    if (empty($row->operation_description)) {
      drupal_set_message(t('Operation description is empty in row @rownum', array('@rownum' => $row->csvrownum + 1)), 'error');
      $return = FALSE;
    }

    if (empty($row->thematic)) {
      drupal_set_message(t('thematic is empty in row @rownum', array('@rownum' => $row->csvrownum + 1)), 'error');
      $return = FALSE;
    }
    return $return;
  }

  /**
   * CreateStub.
   *
   * @param \Migration $migration
   *   Migration objrct.
   * @param array $source_id
   *   Source array ID.
   *
   * @return array|bool
   *   Indicate if term created or not.
   *
   * @throws \Exception
   */
  protected function createStub(Migration $migration, array $source_id) {
    $node = new stdClass();
    $node->title = t('Stub for @id', array('@id' => $source_id[0]));
    $node->type = $this->destination->getBundle();
    $node->uid = 1;
    $node->status = 0;
    node_save($node);
    if (isset($node->nid)) {
      return array($node->nid);
    }
    else {
      return FALSE;
    }
  }

}
