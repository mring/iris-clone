<?php

/**
 * A migration Operations from CSV file.
 */
class StoreTaxonomyMigration extends CommonMigration {

  /**
   * Construct function.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Stores from CSV file.');

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'store_code' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Code of organization level',
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    $this->pkcollumn = 'store_code';

    $this->title_parts = array(
      'store_code',
      'store_name',
    );

    $this->addFieldMapping('field_taxonomy_code', 'store_code')
      ->description('Company Code');

    $this->destination = new MigrateDestinationTerm('store', $this->term_options);

    $this->addFieldMapping('store_code', 'store_code');

    $this->addFieldMapping('name', 'store_code')
      ->description('Terminal');

    $this->addFieldMapping('description_field', 'store_name');

    $this->addFieldMapping('group_group')
      ->defaultValue(1)
      ->description('Group');

    $this->addFieldMapping('field_terminal', 'terminal_code')
      ->callbacks(array($this, 'handleParentGroup'))
      ->description(t('ParentTaxonomy'));

    $this->addFieldMapping('field_activity_domains', 'activity_domain_code')
      // ->separator('|')
      ->callbacks(array($this, 'handleActiveDomain'))
      ->description(t('ActivityDomainTaxonomy'));

    $this->addFieldMapping('field_activity_domains:source_type')
      ->defaultValue('tid');

    $this->addUnmigratedSources(array(
      'country_code',
      'country_name',
      'company_code',
      'company_name',
      'sales_organization_code',
      'sales_organization_name',
      'store_platform_code',
      'store_platform_name',
      'terminal_code',
      'terminal_name',
      'activity_domain_name',
      'activity_sector_code',
      'activity_sector_name',
    )
    );
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return drupal_strtolower(pathinfo($this->arguments['source_file'],
      PATHINFO_FILENAME));
  }

  /**
   * PrepareRow function.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    if (!$this->handleParentGroup($row->terminal_code, 'terminal')) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * HandleParentGroup function.
   */
  public function handleParentGroup($values, $voc_machine_name = FALSE) {
    return parent::handleParentGroup('terminal', $values);
  }

  /**
   * HandleActiveDomain function.
   */
  public function handleActiveDomain($values, $voc_machine_name = FALSE) {
    if (strpos($values, '|')) {
      $values = explode('|', $values);
      $values = array_map('trim', $values);
    }
    return parent::handleParentGroup('activity_domain', $values);
  }

  /**
   * CreateStub.
   *
   * @param \Migration $migration
   *   Migration object.
   * @param array $source_id
   *   Source array ID.
   *
   * @return array|bool
   *   Indicate if term created or not.
   *
   * @throws \Exception
   */
  protected function createStub(Migration $migration, array $source_id) {
    $term = new stdClass();
    $term->name = t('Stub for @id', array('@id' => $source_id[0]));
    $term->vocabulary_machine_name = $this->destination->getBundle();
    $taxonomy_term = taxonomy_term_save($term);
    if (isset($taxonomy_term->tid)) {
      return array($taxonomy_term->tid);
    }
    else {
      return FALSE;
    }
  }

}
