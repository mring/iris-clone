<?php

/**
 * A migration that is reused for each source CSV file.
 */
class CommonMigration extends Migration {

  /**
   * Construct function.
   */
  public function __construct($arguments) {

    if ($arguments['language'] == 'fr') {
      $source_file = 'public://lg_csv_parts/' . $arguments['machine_name'] . '.csv';
    }
    else {
      $source_file = 'public://lg_csv_parts/' . $arguments['machine_name'] . ucfirst($arguments['language']) . '.csv';
    }

    parent::__construct($arguments);

    // Create a MigrateSource object, which manages retrieving the input data.
    $this->source = new MigrateSourceCSV($source_file, $this->csvcolumns($arguments['machine_name']), array(
      'embedded_newlines' => TRUE,
      'track_changes' => TRUE,
      'header_rows' => '1',
    ));
    $this->addFieldMapping('og_user_inheritance')
      ->defaultValue(array(1, 2))
      ->description('User inheritance');

    $this->addFieldMapping('tid', 'existing_tid');

    $this->language = $arguments['language'];
    $this->term_options = MigrateDestinationTerm::options($arguments['language'], 'plain_text');
  }

  /**
   * Csvcolumns function.
   */
  protected function csvcolumns($machine_name = FALSE) {
    // Note: Remember to subtract 1 from column number
    // at http://www.retrosheet.org/gamelogs/glfields.txt
    $columns[0] = array('country_code', 'Country Code');
    $columns[1] = array('country_name', 'Country Name');
    $columns[2] = array('company_code', 'Company Code');
    $columns[3] = array('company_name', 'Company Name');
    $columns[4] = array('sales_organization_code', 'Sales Organization Code');
    $columns[5] = array('sales_organization_name', 'Sales Organization Name');
    $columns[6] = array('store_platform_code', 'Store Platform Code');
    $columns[7] = array('store_platform_name', 'Store Platform Name');
    $columns[8] = array('terminal_code', 'Terminal Code');
    $columns[9] = array('terminal_name', 'Terminal Name');
    $columns[10] = array('store_code', 'Store Code');
    $columns[11] = array('store_name', 'Store Name');
    $columns[12] = array('activity_domain_code', 'Activity domain code');
    $columns[13] = array('activity_domain_name', 'Activity domain name');
    $columns[14] = array('activity_sector_code', 'Activity sector code');
    $columns[15] = array('activity_sector_name', 'Activity sector name');

    $rules = iris_import_data_lg_csv_map();

    foreach ($rules as $rule_name => $rule) {
      if (strpos($machine_name, $rule_name) !== FALSE) {
        foreach (array_keys($columns) as $column_key) {
          if (!in_array($column_key, $rule['map'])) {
            unset($columns[$column_key]);
          }
          else {
            $updated_columns[] = $columns[$column_key];
          }
        }
      }
    }

    return $updated_columns;
  }

  /**
   * HandleParentGroup function.
   */
  public function handleParentGroup($vocabulary, $values = FALSE) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'taxonomy_term')
      ->propertyCondition('vid', taxonomy_vocabulary_machine_name_load($vocabulary)->vid);
    if (is_array($values)) {
      $query->fieldCondition('field_taxonomy_code', 'value', $values, 'IN');
    }
    else {
      $query->fieldCondition('field_taxonomy_code', 'value', $values, '=');
      $query->range(0, 1);
    }

    $result = $query->execute();

    if (isset($result['taxonomy_term'])) {
      if (is_array($values)) {
        return array_keys($result['taxonomy_term']);
      }
      else {
        return key($result['taxonomy_term']);
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * GetOriginalTerm function.
   */
  public function getOriginalTerm($vocabulary, $values = FALSE) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'taxonomy_term')
      ->propertyCondition('vid', taxonomy_vocabulary_machine_name_load($vocabulary)->vid);
    if (is_array($values)) {
      $query->fieldCondition('field_taxonomy_code', 'value', $values, 'IN');
    }
    else {
      $query->fieldCondition('field_taxonomy_code', 'value', $values, '=');
      $query->range(0, 1);
    }

    $result = $query->execute();

    if (isset($result['taxonomy_term'])) {
      if (is_array($values)) {
        return array_keys($result['taxonomy_term']);
      }
      else {
        return key($result['taxonomy_term']);
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return drupal_strtolower(pathinfo($this->arguments['source_file'],
      PATHINFO_FILENAME));
  }

  /**
   * Complete function.
   */
  public function complete($entity, stdClass $row) {
    og_group('taxonomy_term', $entity->tid, array(
      "entity type" => 'user',
      "entity" => user_load(1),
      "membership type" => OG_MEMBERSHIP_TYPE_DEFAULT,
    ));

  }

  /**
   * PrepareRow migration function.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $row->existing_tid = $row->migrate_map_destid1 = $row->migrate_map_destid1 ? $row->migrate_map_destid1 : $this->getOriginalTerm($this->destination->getBundle(), trim($row->{$this->pkcollumn}));

    return TRUE;
  }

  /**
   * Prepare migration function.
   */
  public function prepare(&$entity, $row) {

    if ($original_term = $row->existing_tid) {
      if ($taxonomy_term = taxonomy_term_load($original_term)) {

        if (empty($taxonomy_term->name_field['en'][0]['value']) && $this->language == 'fr') {
          throw new MigrateException('Can\'t create french version without english');
        }

        $entity->name_field = $taxonomy_term->name_field;
        $entity->name_field[$this->language][0]['value'] = $row->{$this->title_parts[0]} . ' - ' . $row->{$this->title_parts[1]};

        $entity->description_field = $taxonomy_term->description_field;
        $entity->description_field[$this->language][0]['value'] = $row->{$this->title_parts[1]};

        $entity->name = $entity->name_field['en'][0]['value'];
        $entity->description = $entity->description_field['en'][0]['value'];
      }
    }
    else {
      if ($this->language == 'fr') {
        throw new MigrateException('Can\'t create french version without english');
      }
      $entity->name_field[$this->language][0]['value'] = $row->{$this->title_parts[0]} . ' - ' . $row->{$this->title_parts[1]};
      $entity->description_field[$this->language][0]['value'] = $row->{$this->title_parts[1]};
      $entity->name = $row->{$this->title_parts[0]} . ' - ' . $row->{$this->title_parts[1]};
      $entity->description = $row->{$this->title_parts[1]};

    }

    $entity->translations = (object) array(
      'original' => 'en',
      'data' => array(
        'en' => array(
          'entity_type' => 'taxonomy_term',
          'entity_id' => $original_term,
          'language' => 'en',
          'source' => '',
          'uid' => '1',
          'status' => '1',
          'translate' => '0',
        ),
        'fr' => array(
          'entity_type' => 'taxonomy_term',
          'entity_id' => $original_term,
          'language' => 'fr',
          'source' => 'en',
          'uid' => '1',
          'status' => '1',
          'translate' => '0',

        ),
      ),
    );
    if ($this->language == 'fr' && $taxonomy_term) {
      entity_save('taxonomy_term', $entity);
    }
  }

}
