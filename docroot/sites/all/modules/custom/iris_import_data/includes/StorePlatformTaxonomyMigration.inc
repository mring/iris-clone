<?php

/**
 * A migration Operations from CSV file.
 */
class StorePlatformTaxonomyMigration extends CommonMigration {

  /**
   * Construct function.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Store Platform from CSV file.');

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'store_platform_code' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Code of organization level',
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    $this->pkcollumn = 'store_platform_code';

    $this->title_parts = array(
      'store_platform_code',
      'store_platform_name',
    );

    $this->addFieldMapping('field_taxonomy_code', 'store_platform_code')
      ->description('Company Code');

    $this->destination = new MigrateDestinationTerm('store_platform', $this->term_options);

    $this->addFieldMapping('store_platform_code', 'store_platform_code');

    $this->addFieldMapping('name', 'store_platform_code')
      ->description('Store platform organization');

    $this->addFieldMapping('description_field', 'store_platform_name');

    $this->addFieldMapping('group_group')
      ->defaultValue(1)
      ->description('Group');

    $this->addFieldMapping('field_sales_organization', 'sales_organization_code')
      ->callbacks(array($this, 'handleParentGroup'))
      ->description(t('ParentTaxonomy'));

    $this->addUnmigratedSources(array(
      'country_code',
      'country_name',
      'company_code',
      'company_name',
      'sales_organization_code',
      'sales_organization_name',
      'terminal_code',
      'terminal_name',
      'store_code',
      'store_name',
      'activity_domain_code',
      'activity_domain_name',
      'activity_sector_code',
      'activity_sector_name',
    )
    );
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return drupal_strtolower(pathinfo($this->arguments['source_file'],
      PATHINFO_FILENAME));
  }

  /**
   * PrepareRow function.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    if (!$this->handleParentGroup($row->sales_organization_code, 'sales_organization')) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * HandleParentGroup function.
   */
  public function handleParentGroup($values, $voc_machine_name = FALSE) {
    return parent::handleParentGroup('sales_organization', $values);
  }

}
