<?php

/**
 * A migration that is reused for each source CSV file.
 */
class ActivitySectorTaxonomyMigration extends DSCommonMigration {

  /**
   * Construct function.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Stores from CSV file.');

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'activity_sector_code' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Code of organization level',
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    $this->pkcollumn = 'activity_sector_code';

    $this->title_parts = array(
      'activity_sector_code',
      'activity_sector_name',
    );

    $this->addFieldMapping('field_taxonomy_code', 'activity_sector_code')
      ->description('Activity Sector Code');

    $this->destination = new MigrateDestinationTerm('activity_sector', $this->term_options);

    $this->addFieldMapping('activity_domain_code', 'activity_sector_code');

    $this->addFieldMapping('name', 'activity_sector_code')
      ->description('Activity Sector');

    $this->addFieldMapping('field_activity_domain', 'activity_domain_code')
      ->callbacks(array($this, 'handleActiveDomain'))
      ->description('Activity Domain');

    $this->addFieldMapping('field_activity_domain:source_type')
      ->defaultValue('tid');

    $this->addFieldMapping('description_field', 'activity_sector_name');

    $this->addUnmigratedSources(array(
      'country_code',
      'country_name',
      'company_code',
      'company_name',
      'sales_organization_code',
      'sales_organization_name',
      'store_platform_code',
      'store_platform_name',
      'terminal_code',
      'terminal_name',
      'store_code',
      'store_name',
      'activity_domain_code',
      'activity_domain_name',
    )
    );
  }

  /**
   * Overwrite handleActiveDomain.
   */
  public function handleActiveDomain($values, $voc_machine_name = FALSE) {
    return parent::handleParentGroup('activity_domain', $values);
  }

  /**
   * CreateStub.
   *
   * @param \Migration $migration
   *   Migration objrct.
   * @param array $source_id
   *   Source array ID.
   *
   * @return array|bool
   *   Indicate if term created or not.
   *
   * @throws \Exception
   */
  protected function createStub(Migration $migration, array $source_id) {
    $term = new stdClass();
    $term->name = t('Stub for @id', array('@id' => $source_id[0]));
    $term->vocabulary_machine_name = $this->destination->getBundle();
    $taxonomy_term = taxonomy_term_save($term);
    if (isset($taxonomy_term->tid)) {
      return array($taxonomy_term->tid);
    }
    else {
      return FALSE;
    }
  }

}
