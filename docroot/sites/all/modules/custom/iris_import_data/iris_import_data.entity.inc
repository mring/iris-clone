<?php

/**
 * @file
 * General entity functions for import.
 */

/**
 * Load entity import.
 */
function iris_import_data_iris_import_get($id = NULL) {
  if (is_null($id)) {
    return FALSE;
  }
  return entity_load_single('iris_import', $id);
}

/**
 * Save import object.
 */
function iris_import_data_iris_import_save($iris_import = NULL) {
  if (is_null($iris_import)) {
    return FALSE;
  }

  if (is_object($iris_import) && isset($iris_import->id)) {

    if ($iris_import->status == IRIS_IMPORT_PROGRESS) {
      migrate_static_registration(array('LinesPromo'));
      $migration = Migration::getInstance('LinesPromo');
      $status = $migration->getStatus();
      if ($status == 1) {
        return FALSE;
      }
    }
    $iris_import->save();
    return TRUE;
  }
  else {
    $entity = entity_create('iris_import', $iris_import);
    $entity->save();
    return $entity;
  }
}
