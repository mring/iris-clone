<?php

/**
 * SapClient class implementation.
 */
class SapClient {
  /**
   * SOAP API URN.
   */
  const SAP_URN = 'urn:sap-com:document:sap:soap:functions:mc-style:ZIRIS_WS_PRODUCT_INFO:ZirisWsGetProductInfoRequest';

  /**
   * Response xml nodes depth.
   */
  const SAP_RESPONSE_DEPTH = 5;

  /**
   * Location of SAP webservice.
   *
   * @var \SoapClient
   */
  private $location;

  /**
   * SOAP API $instance.
   *
   * @var \SoapClient
   */
  private $instance;

  /**
   * Mapping array SAP field -> IRIS field.
   *
   * @var \SoapClient
   */
  private $mapping = array(
    'MATNR' => 'title',
    'PRDHATEXT' => 'field_brand',
    'NAME1' => 'field_supplier',
    'MAKTX' => 'field_product_description',
    'ADDIMAT' => 'field_product_tester_code',
    'VOLUM' => 'field_product_volume',
    'NTGEW' => 'field_product_weight',
    'VKORG' => 'field_price_sales_organization',
    'WERKS' => 'field_price_store',
    'DATABA1' => 'field_a1_validity_start',
    'DATBIA1' => 'field_a1_validity_end',
    'KBETRA1' => 'field_a1_sales_amount',
    'WAERKA1' => 'field_a1_sales_currency',
    'DATABA2' => 'field_a2_validity_start',
    'DATBIA2' => 'field_a2_validity_end',
    'KBETRA2' => 'field_a2_sales_amount',
    'WAERKA2' => 'field_a2_sales_currency',
    'DATABA3' => 'field_a3_validity_start',
    'DATBIA3' => 'field_a3_validity_end',
    'KBETRA3' => 'field_a3_sales_amount',
    'WAERKA3' => 'field_a3_sales_currency',
    'EKORG' => 'field_purchase_organization',
    'KBETR' => 'field_purchase_amount',
    'WAERK' => 'field_purchase_currency',
  );

  /**
   * SapClient constructor. Create connection with SOAP SAP web service.
   */
  public function __construct($location, $login, $pass) {
    $this->location = $location;
    $this->instance = new SoapClient(NULL,
      array(
        'location' => $location,
        'login' => $login,
        'password' => $pass,
        'uri' => self::SAP_URN,
        'style' => SOAP_DOCUMENT,
        'soap_version' => SOAP_1_1,
        'trace' => 1,
        'exceptions' => 0,
      )
    );
  }

  /**
   * Get XML response with product.
   *
   * @param string $sku
   *   Product SKU.
   * @param string $promotion_date
   *   Promotion date.
   * @param string $sales_org
   *   Sales organisation.
   * @param string $purchase_org
   *   Purchase organisation.
   * @param string $shop
   *   Store.
   *
   * @return string
   *   Response string.
   */
  public function getResponse($sku, $promotion_date, $sales_org, $purchase_org = '', $shop = '') {
    $request = $this->createXmlRequest($sku, $promotion_date, $sales_org, $purchase_org, $shop);
    $response = $this->instance->__doRequest($request, $this->location, self::SAP_URN, SOAP_1_1);

    if (is_soap_fault($response)) {
      $error_msg = $this->errorMessage($response->faultcode, $response->faultstring);
      watchdog('iris_sap_import', $error_msg, array(), WATCHDOG_ERROR);
    }

    if (isset($response)) {
      return $response;
    }
    elseif (is_object($this->instance->__soap_fault)) {
      $error_msg = $this->errorMessage($this->instance->__soap_fault->faultcode, $this->instance->__soap_fault->faultstring);
      watchdog('iris_sap_import', $error_msg, array(), WATCHDOG_ERROR);
    }

    return '';
  }

  /**
   * Parse SAP response to array with keys as Iris fields.
   *
   * @param string $response
   *   SAP response.
   *
   * @return array
   *   Array parsed XML.
   */
  public function parseResponse($response) {
    $parser = xml_parser_create();
    xml_parse_into_struct($parser, $response, $values);
    xml_parser_free($parser);

    $attributes = array();

    foreach ($values as $value) {
      if ($value['level'] == self::SAP_RESPONSE_DEPTH && isset($value['tag']) && isset($this->mapping[$value['tag']])) {
        $attributes[$this->mapping[$value['tag']]] = isset($value['value']) ? $value['value'] : '';
      }
    }

    return $attributes;
  }

  /**
   * Create XML request in SAP SOAP format.
   *
   * @param string $sku
   *   Product SKU.
   * @param string $promotion_date
   *   Promotion date.
   * @param string $sales_org
   *   Sales organisation.
   * @param string $purchase_org
   *   Purchese organisation.
   * @param string $shop
   *   Store.
   *
   * @return string
   *   XML string.
   */
  protected function createXmlRequest($sku, $promotion_date, $sales_org, $purchase_org = '', $shop = '') {
    $args = array(
      'ImDate' => $promotion_date,
      'ImEkorg' => $purchase_org,
      'ImMatnr' => $sku,
      'ImVkorg' => $sales_org,
      'ImWerks' => $shop,
    );

    $xml = new DomDocument('1.0', 'UTF-8');
    $root = $xml->createElementNS('http://schemas.xmlsoap.org/soap/envelope/', 'soapenv:Envelope');
    $root->setAttribute('xmlns:urn', 'urn:sap-com:document:sap:soap:functions:mc-style');
    $xml->appendChild($root);
    $header = $xml->createElement('soapenv:Header');
    $root->appendChild($header);
    $body = $xml->createElement('soapenv:Body');
    $root->appendChild($body);
    $method = $xml->createElement('urn:ZirisWsGetProductInfo');
    $body->appendChild($method);

    foreach ($args as $key => $value) {
      $element = $xml->createElement($key, $value);
      $method->appendChild($element);
    }

    return $xml->saveXML();
  }

  /**
   * SOAP Fault error message format.
   *
   * @param string $code
   *   Code number.
   * @param string $string
   *   Error message.
   *
   * @return string
   *   Prepared message.
   */
  protected function errorMessage($code, $string) {
    return "Error -> SAP SOAP Fault - faultcode: {$code}, faultstring: {$string}";
  }

}
