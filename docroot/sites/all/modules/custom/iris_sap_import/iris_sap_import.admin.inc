<?php

/**
 * @file
 * Admin form implementation.
 */

/**
 * Settings form with SAP connection data.
 */
function iris_sap_import_settings_form($form, &$form_state) {
  $form['sap'] = array(
    '#type' => 'fieldset',
    '#title' => t('SAP webservice connection settings'),
  );
  $form['sap']['iris_sap_import_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Import url (location)'),
    '#default_value' => variable_get('iris_sap_import_url', ''),
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['sap']['iris_sap_import_login'] = array(
    '#type' => 'textfield',
    '#title' => t('Login'),
    '#default_value' => variable_get('iris_sap_import_login', ''),
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['sap']['iris_sap_import_pass'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('iris_sap_import_pass', ''),
    '#maxlength' => 15,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
