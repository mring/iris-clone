<?php

/**
 * @file
 * Contains helper functions.
 */

/**
 * Callback for 'iris_multiselect_ajax_callback' MENU_CALLBACK.
 *
 * Changing field $_POST format if multiple values selected.
 */
function iris_dependent_selection_callback() {
  if (isset($_POST['field_activity_domains'])) {
    $domains = explode(',', $_POST['field_activity_domains'][LANGUAGE_NONE][0]);
    if (count($domains) > 1) {
      unset($_POST['field_activity_domains'][LANGUAGE_NONE]);
      $_POST['field_activity_domains'][LANGUAGE_NONE] = $domains;
    }
  }

  list($form, $form_state) = ajax_get_form();
  drupal_process_form($form['#form_id'], $form, $form_state);

  if (!empty($form_state['triggering_element'])) {
    $path = $form_state['triggering_element']['#ajax']['path'];
  }

  if (!isset($_POST['field_activity_domains'])) {
    $form['field_activity_sectors']['#prefix'] = '<div id="dropdown-second-replace" class="element-hidden">';
  }

  if (!empty($path)) {
    return iris_dependent_dropdown_generate_callback($form, $form_state);
  }
}

/**
 * Form field ajax callback.
 */
function iris_dependent_dropdown_generate_callback($form, $form_state) {
  $elements = array(
    $form['field_activity_sectors'],
    $form['field_scope_selection'],
    $form['field_location_group'],
  );
  return $elements;
}

/**
 * Get child vocabularies children.
 *
 * @param array $tids
 *   Array of taxonomy ids.
 *
 * @return array
 *   Associative array of child elements as term_id => term_name.
 */
function iris_dependent_selection_get_child_terms($tids) {
  global $language;

  $query = db_select('field_data_field_activity_domain', 'fdfad');
  $query->join('taxonomy_term_data', 'ttd', 'ttd.tid = fdfad.entity_id');
  $query->join('taxonomy_vocabulary', 'tv', 'tv.vid = ttd.vid');
  $query->join('field_data_name_field', 'nf', 'nf.entity_id = ttd.tid');
  $query->addField('nf', 'entity_id', 'tid');
  $query->addField('nf', 'name_field_value', 'name');
  $query->condition('fdfad.field_activity_domain_tid', $tids, 'IN');
  $query->condition('tv.machine_name', 'activity_sector');
  $query->condition('nf.entity_type', 'taxonomy_term');
  $query->condition('nf.language', $language->language);
  $result = $query->execute()->fetchAllKeyed(0, 1);

  return $result;
}
