<?php

/**
 * @file
 * Content type plugin.
 */

$plugin = array(
  'title' => t('User notifications'),
  'description' => t('Displays user notification in top of page.'),
  'single' => TRUE,
  'content_types' => array('iris_notifications'),
  'render callback' => 'iris_notifications_content_type_render',
  'required context' => new ctools_context_required(t('User'), 'user'),
  'category' => array(t('IRIS'), -9),
);

/**
 * Render callback function.
 */
function iris_notifications_content_type_render($subtype, $conf, $args, $context) {
  $user_id = !empty($context->data->uid) ? $context->data->uid : FALSE;
  if (!$user_id) {
    return;
  }

  $all_notifications_roles = array('manager', 'sales consultant');
  $user = user_load($user_id);

  drupal_add_library('system', 'drupal.ajax');

  $query = db_select('iris_notifications', 'notifications');
  $query->addTag('translatable');
  $query->fields('notifications');
  $query->condition('notifications.uid', $user_id);
  $query->condition('notifications.author_uid', $user_id, '<>');
  if (!array_intersect($user->roles, $all_notifications_roles)) {
    $query->condition('notifications.type', 'comment');
  }
  $query->orderBy('notifications.timestamp', 'DESC');
  $notifications = $query->execute()->fetchAll();

  $notification_links = array();
  foreach ($notifications as $notification) {
    $notification_links[] = l(t($notification->message, unserialize($notification->message_args)), 'notification/read/' . $notification->id, array('html' => TRUE));
  }

  $query = db_select('iris_notifications', 'notifications');
  $query->fields('notifications');
  $query->condition('notifications.uid', $user_id);
  $query->condition('notifications.author_uid', $user_id, '<>');
  if (!array_intersect($user->roles, $all_notifications_roles)) {
    $query->condition('notifications.type', 'comment');
  }
  $query->condition('notifications.is_read', 0);
  $notification_count = $query->execute()->rowCount();

  $block = new stdClass();

  $block->content = array(
    'notifications' => array(),
    'notification_links' => array(),
    'notification_count' => 0,
  );

  if ($notifications) {
    $block->content = array(
      'notifications' => $notifications,
      'notification_links' => $notification_links,
      'notification_count' => $notification_count,
    );
  }
  return $block;
}
