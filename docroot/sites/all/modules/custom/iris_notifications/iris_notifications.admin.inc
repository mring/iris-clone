<?php

/**
 * @file
 * Contains functions for administrative pages.
 */

/**
 * Notifications configuration form.
 */
function iris_notifications_settings_form($form, &$form_state) {
  $form['iris_notifications_deadline'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of days'),
    '#description' => t('After X days do not create new notifications.'),
    '#default_value' => variable_get('iris_notifications_deadline', 2),
    '#element_validate' => array('element_validate_integer_positive'),
    '#size' => 100,
    '#maxlength' => 3,
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
