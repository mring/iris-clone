<?php

/**
 * @file
 * Contains helper functions.
 */

/**
 * Returns data for 'iris_add_term' block.
 */
function iris_add_term_block_data() {
  $add_term_form = drupal_get_form('iris_add_term_block_form');
  return drupal_render($add_term_form);
}

/**
 * Form builder function.
 */
function iris_add_term_block_form($form, $form_state) {
  $form['add_term_fieldset'] = array('#type' => 'fieldset');
  $form['add_term_fieldset']['taxonomies_list'] = array(
    '#type' => 'select',
    '#options' => iris_add_term_block_get_vocabularies_list(),
    '#empty_option' => t('Select vocabulary'),
    '#required' => TRUE,
  );
  $form['add_term_fieldset']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create a taxonomy term'),
  );
  return $form;
}

/**
 * Submit handler for 'iris_add_term_block_form' form.
 */
function iris_add_term_block_form_submit($form, $form_state) {
  $vocabulary_machine_name = $form_state['values']['taxonomies_list'];
  drupal_goto('admin/structure/taxonomy/' . $vocabulary_machine_name . '/add');
}

/**
 * Returns vocabularies list as an associative array.
 */
function iris_add_term_block_get_vocabularies_list() {
  $vocabularies_list = array();
  $vocabularies = taxonomy_vocabulary_get_names();
  foreach ($vocabularies as $vocabulary) {
    $vocabularies_list[$vocabulary->machine_name] = $vocabulary->name;
  }
  ksort($vocabularies_list);
  return $vocabularies_list;
}
