/**
 * @file iris_search_ajax.js
 */
(function ($) {

    /**
     * Defines active item in search list.
     */
    //Drupal.behaviors.irisSearchActiveItem = {
    //    attach: function () {
    //        if (Drupal.settings.iris_search_active) {
    //            var id = Drupal.settings.iris_search_active.entityId;
    //            var links = '#content .news-list';
    //
    //            if (id) {
    //                $(links).find('li.active').removeClass('active');
    //                $(links).find("a[data-entity-id=" + id + "]").parent('li').addClass('active');
    //            }
    //        }
    //    }
    //};

    /**
     * Hide messages on search field change.
     */
    Drupal.behaviors.irisSearchHideErrors = {
        attach: function () {
            $('#edit-query').change(function () {
                var messages = $('.messages');
                messages.hide();
            });
        }
    };

    /**
     * Attaches the AJAX behavior to facet links.
     */
    Drupal.behaviors.irisSearchAjax = {
        attach: function () {
            if (Drupal.settings && Drupal.settings.views && Drupal.settings.views.ajaxViews) {
                var ajax_path = Drupal.settings.views.ajax_path;

                $.each(Drupal.settings.views.ajaxViews, function (i, settings) {
                    var links = '#sidebar .facetapi-facetapi-links';
                    var links_home = '#content-item .home-holder';
                    var links_sort = '#news-sort';

                    var element_settings = {
                        url: ajax_path,
                        submit: settings,
                        setClick: true,
                        event: 'click',
                        selector: links,
                        progress: {
                            type: 'none'
                        }
                    };

                    $(links).find('a:not(".views-processed")').addClass('views-processed').click(function (e) {
                        if ($(this).attr('href')) {
                            var viewData = {};
                            var href = $(this).attr('href');

                            if (typeof Drupal.settings.iris_search_query !== "undefined") {
                                viewData.query = Drupal.settings.iris_search_query;
                            }

                            viewData.type = 'search';
                            viewData.q = Drupal.Views.getPath(href);

                            $.extend(
                                viewData,
                                settings,
                                Drupal.Views.parseQueryString(href)
                            );

                            element_settings.submit = viewData;
                            var ajax = new Drupal.ajax(false, this, element_settings);
                            ajax.eventResponse(ajax, {});

                            History.pushState({}, $(this).text(), getPathFromUrl($(this).attr('href')));
                            updatePrecache(getPathFromUrl($(this).attr('href')));

                            e.preventDefault();
                        }
                    });

                    $(links_home).filter(':not(.views-processed)').each(function () {
                        $(this).addClass('views-processed').find('a').each(function () {
                            if ($(this).attr('href')) {
                                var viewData = {};
                                var href = $(this).attr('href');
                                viewData.type = 'search';
                                viewData.q = Drupal.Views.getPath(href);

                                $.extend(
                                    viewData,
                                    settings,
                                    Drupal.Views.parseQueryString(href)
                                );

                                element_settings.submit = viewData;

                                var ajax = new Drupal.ajax(false, this, element_settings);
                            }
                        }).click(function (e) {
                            if ($(this).attr('href')) {
                                History.pushState({}, $(this).text(), $(this).attr('href'));
                                updatePrecache($(this).attr('href'));
                            }
                        });
                    });

                    var sort_element_settings = {
                        url: ajax_path,
                        submit: settings,
                        selector: links_sort,
                        progress: {
                            type: 'none'
                        }
                    };

                    $(links_sort).find('a:not(".processed")').addClass('processed').click(function (e) {
                        if ($(this).attr('href')) {
                            var viewData = {};
                            viewData.type = 'search';
                            viewData.q = Drupal.Views.getPath(window.location.href);
                            viewData.sort_by = $(this).attr('data-sort-by');

                            $.extend(
                                viewData,
                                settings,
                                Drupal.Views.parseQueryString(window.location.href)
                            );

                            sort_element_settings.submit = viewData;
                            var ajax = new Drupal.ajax(false, this, sort_element_settings);
                            ajax.eventResponse(ajax, {});

                            $(links_sort).find('li.active').removeClass('active');
                            $(this).parent('li').addClass('active');

                            e.preventDefault();
                        }
                    });

                    function getPathFromUrl(url) {
                        return url.split(/[?#]/)[0];
                    }
                });
            }
        }
    };

    /**
     * Attaches the AJAX behavior to node link.
     */
    Drupal.behaviors.irisSearchAjaxNodeLoad = {
        attach: function (context, settings) {
            var links = '#content .news-list';

            $(links).find('a.node-link:not(".processed")').addClass('processed').click(function (e) {
                if ($(this).attr('href')) {

                    var element_settings = {
                        url: Drupal.settings.basePath + 'iris-search/ajax-node',
                        submit: {
                            nid: $(this).attr('data-entity-id'),
                            store: $(this).attr('data-store')
                        },
                        progress: {
                            type: 'none'
                        },
                        selector: links
                    };

                    var ajax = new Drupal.ajax(false, this, element_settings);
                    ajax.eventResponse(ajax, {});

                    History.pushState({}, $(this).text(), $(this).attr('href'));
                    updatePrecache($(this).attr('href'));

                    e.preventDefault();
                }
            });

            $(links).find('a.term-link:not(".processed")').addClass('processed').click(function (e) {
                if ($(this).attr('href')) {

                    var element_settings = {
                        url: '/iris-search/ajax-term',
                        submit: {
                            tid: $(this).attr('data-entity-id'),
                            store: $(this).attr('data-store')
                        },
                        progress: {
                            type: 'none'
                        }
                    };

                    var ajax = new Drupal.ajax(false, this, element_settings);
                    ajax.eventResponse(ajax, {});


                    History.pushState({}, $(this).text(), $(this).attr('href'));
                    updatePrecache($(this).attr('href'));

                    e.preventDefault();
                }
            });
            // Do not close opening filters.
            $('.aside-box').not('.opened').find('.aside-list li').not('.active').addClass('js-slide-hidden');
        }
    };

    /**
     * Attaches the AJAX behavior to node close link.
     */
    Drupal.behaviors.irisSearchAjaxNodeClose = {
        attach: function (context, settings) {
            var links = '#content-item';

            $(links).find('a.close-news:not(".processed")').addClass('processed').click(function (e) {
                if ($(this).attr('href')) {

                    var element_settings = {
                        url: Drupal.settings.basePath + 'iris-search/ajax-close-node',
                        submit: {},
                        progress: {
                            type: 'none'
                        },
                        selector: links
                    };

                    var ajax = new Drupal.ajax(false, this, element_settings);
                    ajax.eventResponse(ajax, {});

                    History.pushState({}, Drupal.t('Home'), Drupal.settings.basePath);
                    updatePrecache(Drupal.settings.basePath);

                    $('#content').find('li.active').removeClass('active');

                    e.preventDefault();
                }
            });
        }
    };

    Drupal.irisGlobalThrobberStart = function () {
        var div = '<div style="display: none;" id="global-throbber"><div class="ajax-progress-throbber"><div class="throbber"><img src="'
            + Drupal.settings.loaderPath + '"></div></div></div>';

        if ($('#global-throbber').length == 0) {
            $('#header').append(div);
            $('#global-throbber').fadeIn("fast");
        }

        $("input[disabled!='disabled']").each(function () {
            if ($(this).css("display") !== "none" && $(this).attr("type") == 'submit') {
                $(this).attr("disabled", true).addClass("global-throbber-disabled").addClass('disabled');
            }
        });
    };

    Drupal.irisGlobalThrobberFinish = function () {
        $("input.global-throbber-disabled").removeAttr("disabled", "disabled").removeClass("global-throbber-disabled").removeClass('disabled');
        $('#global-throbber').remove();
    };


    Drupal.behaviors.irisGlobalThrobber = {
        attach: function (context, settings) {
            $(document).ajaxSend(function (event, request, settings) {
                Drupal.irisGlobalThrobberStart();
            });

            //$(document).ajaxError(function (event, request, settings) {
            //    if (event.target.URL && event.target.URL.indexOf('/ajax') === -1) {
            //        window.location.href = event.target.URL;
            //    }
            //});

            $(document).ajaxComplete(function (event, request, settings) {
                Drupal.irisGlobalThrobberFinish();
            });
        }
    };

    var updatePrecache = function (url) {

    };

    /**
     * Implements fake facet counter.
     */
    Drupal.behaviors.irisSearchFakeFacetCount = {
        attach: function (context, settings) {
            $('#main', context).once('facetcounter', function () {
                $(context).ajaxComplete(function (event, request, settings) {
                    if (typeof request.responseJSON != "undefined" && typeof request.responseJSON.flagStatus != "undefined") {
                        var count = parseInt($('.aside-box a.favorites').find('span.num').text());

                        if (request.responseJSON.flagStatus == "flagged") {
                            count += 1;
                        }
                        if (request.responseJSON.flagStatus == "unflagged") {
                            count -= 1;
                        }

                        $('.aside-box a.favorites').find('span.num').text(count);
                    }
                });
            });
        }
    };


})(jQuery);
