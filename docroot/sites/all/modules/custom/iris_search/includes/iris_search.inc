<?php

/**
 * @file
 * Contains helping functions.
 */

/**
 * Get Merch elements that are referencing merch rubric.
 *
 * @param int $tid
 *   Merchandising rubric id.
 *
 * @return array
 *   Array of merch elements ids.
 */
function iris_search_get_merch_rubric_ancestors($tid) {
  $query = db_select('field_data_field_merch_rubric', 'fdfmr');
  $query->fields('fdfmr', array('entity_id'));
  $query->condition('field_merch_rubric_tid', $tid);
  $merch_elements = $query->execute()->fetchCol();

  return $merch_elements;
}

/**
 * Count Number of merch elements..
 *
 * Count Number of merch elements created or updated for last 7 days
 * in domain for current store.
 */
function iris_search_count_merch_elements($item) {
  // Take full hierarchy related to selected store.
  global $global_store;
  $hierarchy = db_select('iris_org_hierarchy', 'h')
    ->fields('h', array('taxonomy_term_target'))
    ->condition('h.taxonomy_term_source', $global_store)->execute()->fetchCol();

  // Add current store also.
  $hierarchy[] = $global_store;

  // 7 days ago.
  $day_start = strtotime("midnight", time() - 7 * 86400);
  $query = db_select('node', 'n');
  $query->fields('n', array('nid'));
  $query->join('field_data_field_merch_rubric', 'fdfmr', 'fdfmr.entity_id = n.nid');
  $query->join('field_data_field_activity_domain', 'fdfad', 'fdfad.entity_id = fdfmr.field_merch_rubric_tid');
  $query->condition('fdfad.field_activity_domain_tid', $item->tid);
  $query->join('og_membership', 'om', 'om.etid = n.nid');
  $query->condition('om.entity_type', 'node');
  $query->condition('om.field_name', 'field_location_group');
  $query->condition('om.gid', $hierarchy, 'IN');

  $or = db_or()->condition('n.created', $day_start, '>=')
    ->condition('n.changed', $day_start, '>=');

  $query->condition($or);
  $result = $query->execute()->rowCount();

  return $result;
}

/**
 * Count Number of Pages.
 *
 * Count Number of Pages created or updated for last 7 days
 * in page type for current store.
 */
function iris_search_count_pages($item) {
  // Take full hierarchy related to selected store.
  global $global_store;
  $hierarchy = db_select('iris_org_hierarchy', 'h')
    ->fields('h', array('taxonomy_term_target'))
    ->condition('h.taxonomy_term_source', $global_store)->execute()->fetchCol();
  // Add current store also.
  $hierarchy[] = $global_store;

  // 7 days ago.
  $day_start = strtotime("midnight", time() - 7 * 86400);
  $query = db_select('node', 'n');
  $query->fields('n', array('nid'));
  $query->join('field_data_field_page_type', 'fdfpt', 'fdfpt.entity_id = n.nid');
  $query->condition('fdfpt.field_page_type_tid', $item->tid);

  $query->join('og_membership', 'om', 'om.etid = n.nid');
  $query->condition('om.entity_type', 'node');
  $query->condition('om.field_name', 'field_location_group');
  $query->condition('om.gid', $hierarchy, 'IN');

  $or = db_or()->condition('n.created', $day_start, '>=')
    ->condition('n.changed', $day_start, '>=');

  $query->condition($or);
  $result = $query->execute()->rowCount();

  return $result;
}

/**
 * Return merch name.
 */
function iris_search_get_merch_name($item) {
  return $item->name;
}

/**
 * Get last date a Merch element was created in this Merch rubric.
 */
function iris_search_get_last_merch_time($item) {
  $query = db_select('node', 'n');
  $query->fields('n', array('changed'));
  $query->join('field_data_field_merch_rubric', 'fdfmr', 'fdfmr.entity_id = n.nid');
  $query->condition('fdfmr.field_merch_rubric_tid', $item->tid);
  $query->condition('n.type', 'merchandising_element');
  $query->orderBy('changed', 'DESC');

  return $query->execute()->fetchField();
}

/**
 * Get Merch element location group items names.
 */
function iris_search_get_lg_names($item) {
  $names = array();
  $ancestors = iris_search_get_merch_rubric_ancestors($item->tid);

  if ($ancestors) {
    foreach ($ancestors as $ancestor_id) {
      $merch_element = node_load($ancestor_id);
      $emw_merch_element = entity_metadata_wrapper('node', $merch_element);
      if (isset($emw_merch_element->field_location_group)) {
        $merch_element_lg = $emw_merch_element->field_location_group->raw();

        foreach ($merch_element_lg as $group) {
          $group = taxonomy_term_load($group);
          $group_wrapper = entity_metadata_wrapper('taxonomy_term', $group);
          if ($group->vocabulary_machine_name == 'slot') {
            $names[] = $group_wrapper->name->raw();
          }
          else {
            $names[] = $group_wrapper->field_taxonomy_code->raw();
          }
        }
      }
    }
  }
  $names = array_unique($names);

  return $names;
}

/**
 * Get page location group values.
 */
function iris_search_get_page_lg_names($item) {
  $names = array();
  $node = node_load($item->nid);

  if ($node) {
    $emw_node = entity_metadata_wrapper('node', $node);
    $location_group = $emw_node->field_location_group->raw();

    if ($location_group) {
      foreach ($location_group as $term) {
        $group = taxonomy_term_load($term);
        $group_wrapper = entity_metadata_wrapper('taxonomy_term', $group);
        if ($group->vocabulary_machine_name == 'slot') {
          $names[] = $group_wrapper->name->raw();
        }
        else {
          $names[] = $group_wrapper->field_taxonomy_code->raw();
        }
      }
    }
  }

  $names = array_unique($names);

  return $names;
}
