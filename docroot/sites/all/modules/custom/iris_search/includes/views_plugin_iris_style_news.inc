<?php

/**
 * @file
 * Contains the news style plugin.
 */

/**
 * Style plugin to render news list.
 *
 * @ingroup views_style_plugins
 */
class views_plugin_iris_style_news extends views_plugin_style {

  /**
   * Init function.
   */
  public function init(&$view, &$display, $options = NULL) {
    $this->view = &$view;
    $this->display = &$display;

    // Overlay incoming options on top of defaults.
    $this->unpack_options($this->options, isset($options) ? $options : $display->handler->get_option('style_options'));

    if ($this->uses_row_plugin() && $display->handler->get_option('row_plugin')) {
      $this->row_plugin = $display->handler->get_plugin('row');
    }

    $this->options += array(
      'grouping' => array(),
    );

    $this->definition += array(
      'uses grouping' => TRUE,
    );
  }

  /**
   * Render the given style.
   */
  public function options_form(&$form, &$form_state) {
    if ($this->uses_fields() && $this->definition['uses grouping']) {
      $options = array('' => t('- None -'));
      $field_labels = $this->display->handler->get_field_labels(TRUE);
      $options += $field_labels;
      // If there are no fields, we can't group on them.
      if (count($options) > 1) {
        // This is for backward compatibility,
        // when there was just a single select form.
        if (is_string($this->options['grouping'])) {
          $grouping = $this->options['grouping'];
          $this->options['grouping'] = array();
          $this->options['grouping'][0]['field'] = $grouping;
        }
        if (isset($this->options['group_rendered']) && is_string($this->options['group_rendered'])) {
          $this->options['grouping'][0]['rendered'] = $this->options['group_rendered'];
          unset($this->options['group_rendered']);
        }

        $c = count($this->options['grouping']);
        // Add a form for every grouping, plus one.
        for ($i = 0; $i <= $c; $i++) {
          $grouping = !empty($this->options['grouping'][$i]) ? $this->options['grouping'][$i] : array();
          $grouping += array(
            'field'          => '',
            'rendered'       => TRUE,
            'rendered_strip' => FALSE,
          );
          $form['grouping'][$i]['field'] = array(
            '#type'          => 'select',
            '#title'         => t('Grouping field Nr.@number', array('@number' => $i + 1)),
            '#options'       => $options,
            '#default_value' => $grouping['field'],
            '#description'   => t('You may optionally specify a field by which to group the records. Leave blank to not group.'),
          );
          $form['grouping'][$i]['rendered'] = array(
            '#type'          => 'checkbox',
            '#title'         => t('Use rendered output to group rows'),
            '#default_value' => $grouping['rendered'],
            '#description'   => t('If enabled the rendered output of the grouping field is used to group the rows.'),
            '#dependency'    => array(
              'edit-style-options-grouping-' . $i . '-field' => array_keys($field_labels),
            ),
          );
          $form['grouping'][$i]['rendered_strip'] = array(
            '#type'          => 'checkbox',
            '#title'         => t('Remove tags from rendered output'),
            '#default_value' => $grouping['rendered_strip'],
            '#dependency'    => array(
              'edit-style-options-grouping-' . $i . '-field' => array_keys($field_labels),
            ),
          );
        }
      }
    }
  }

  /**
   * Render the display in this style.
   */
  public function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      debug('views_plugin_style_default: Missing row plugin');
      return;
    }

    // Group the rows according to the grouping instructions, if specified.
    $sets = $this->render_grouping(
      $this->view->result,
      $this->options['grouping'],
      TRUE
    );

    return $this->render_grouping_sets($sets);
  }

  /**
   * Group records as needed for rendering.
   *
   * @param array $records
   *   An array of records from the view to group.
   * @param array $groupings
   *   An array of records from the view to group.
   * @param bool $group_rendered
   *   Boolean value whether to use the rendered or the raw field value for
   *   grouping. If set to NULL the return is structured as before
   *   Views 7.x-3.0-rc2. After Views 7.x-3.0 this boolean is only used if
   *   $groupings is an old-style string or if the rendered option is missing
   *   for a grouping instruction.
   *
   * @return array
   *   The grouped record set.
   *   A nested set structure is generated if multiple grouping fields are used.
   *
   * @code
   *   array(
   *     'grouping_field_1:grouping_1' => array(
   *       'group' => 'grouping_field_1:content_1',
   *       'rows' => array(
   *         'grouping_field_2:grouping_a' => array(
   *           'group' => 'grouping_field_2:content_a',
   *           'rows' => array(
   *             $row_index_1 => $row_1,
   *             $row_index_2 => $row_2,
   *             // ...
   *           )
   *         ),
   *       ),
   *     ),
   *     'grouping_field_1:grouping_2' => array(
   *       // ...
   *     ),
   *   )
   * @endcode
   */
  public function render_grouping($records, $groupings = array(), $group_rendered = NULL) {
    // This is for backward compatibility,
    // when $groupings was a string containing.
    // the ID of a single field.
    if (is_string($groupings)) {
      $rendered = $group_rendered === NULL ? TRUE : $group_rendered;
      $groupings = array(array('field' => $groupings, 'rendered' => $rendered));
    }

    $sets = array();
    if ($groupings) {
      foreach ($records as $index => $row) {
        // Iterate through configured grouping fields to determine the
        // hierarchically positioned set where the current row belongs to.
        // While iterating, parent groups, that do not exist yet, are added.
        $set = &$sets;
        foreach ($groupings as $info) {
          $field = $info['field'];
          $grouping = '';
          $group_content = '';

          $grouping = $this->get_field_value($index, $field);

          // Not all field handlers return a scalar value,
          // e.g. views_handler_field_field.
          if (!is_scalar($grouping)) {
            $grouping = md5(serialize($grouping));
          }

          // Create the group if it does not exist yet.
          if (empty($set[$grouping])) {
            $set[$grouping]['group'] = $group_content;
            $set[$grouping]['rows'] = array();
          }

          // Move the set reference into
          // the row set of the group we just determined.
          $set = &$set[$grouping]['rows'];
        }
        // Add the row to the hierarchically positioned
        // row set we just determined.
        $set[$index] = $row;
      }
    }
    else {
      // Create a single group with an empty grouping field.
      $sets[''] = array(
        'group' => '',
        'rows' => $records,
      );
    }

    // If this parameter isn't explicitly set modify the output to be fully
    // backward compatible to code before Views 7.x-3.0-rc2.
    // @TODO Remove this as soon as possible e.g. October 2020
    if ($group_rendered === NULL) {
      $old_style_sets = array();
      foreach ($sets as $group) {
        $old_style_sets[$group['group']] = $group['rows'];
      }
      $sets = $old_style_sets;
    }

    return $sets;
  }

  /**
   * Render the grouping sets.
   *
   * Plugins may override this method if they wish some other way of handling
   * grouping.
   *
   * @param array $sets
   *   Array containing the grouping sets to render.
   * @param int $level
   *   Integer indicating the hierarchical level of the grouping.
   *
   * @return string
   *   Rendered output of given grouping sets.
   */
  public function render_grouping_sets($sets, $level = 0) {
    $output = theme($this->theme_functions(),
      array(
        'view' => $this->view,
        'rows' => $sets,
      )
    );

    return $output;
  }

}
