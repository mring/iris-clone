<?php

/**
 * @file
 * Views preprocess functions.
 */

/**
 * Implementation of template_process for views-view-news.tpl.php.
 */
function template_preprocess_views_view_news(&$vars) {
  global $global_store;
  $vars['data']['global_store'] = isset($global_store) ? $global_store : '';
  $vars['items'] = array();

  if (isset($vars['rows'])) {
    foreach ($vars['rows'] as $id => $row) {
      if (($id > 0) && $promotion = entity_load_single('node', $id)) {
        if (!empty($promotion->field_reference_validation[LANGUAGE_NONE]) && isset($row['rows'])) {
          $promotion_wrapper = entity_metadata_wrapper('node', $promotion);
          $promotion_lines = $promotion_wrapper->field_promotion_lines->value();
          $data['title'] = $promotion->title;
          $data['nid'] = $promotion->nid;
          $data['start_date'] = '';
          $data['end_date'] = '';
          $data['type'] = t('Promotion');
          if (!empty($promotion_lines) && !empty($promotion_lines[0]) && $promotion_wrapper->field_promotion_lines[0]->field_operation->value()) {
            if (isset($promotion_wrapper->field_promotion_lines[0]->field_operation->field_operation_start_date)) {
              $data['start_date'] = strtoupper(date('d M', $promotion_wrapper->field_promotion_lines[0]->field_operation->field_operation_start_date->value()));
            }
            if (isset($promotion_wrapper->field_promotion_lines[0]->field_operation->field_operation_end_date)) {
              $data['end_date'] = strtoupper(date('d M Y', $promotion_wrapper->field_promotion_lines[0]->field_operation->field_operation_end_date->value()));
            }
          }

          $data['class'] = '';
          $data['status_class'] = '';
          $data['date_class'] = '';
          $data['date_status'] = '';

          $vars['items'][] = $data;

          $end = count($row['rows']) - 1;
          foreach ($row['rows'] as $key => $news_item) {
            if ($news_obj = entity_load_single('node', $news_item->entity)) {
              $start = ($key == 0) ? TRUE : FALSE;
              $end = ($key == $end) ? TRUE : FALSE;

              $vars['items'][] = get_news_item_data($news_obj, $global_store, TRUE, $start, $end);
            }
          }
        }
        else {
          foreach ($row['rows'] as $news_item) {
            if ($news_obj = entity_load_single('node', $news_item->entity)) {
              $vars['items'][] = get_news_item_data($news_obj, $global_store);
            }
          }
        }
      }
      elseif (isset($row['rows'])) {
        foreach ($row['rows'] as $news_item) {
          if ($news_obj = entity_load_single('node', $news_item->entity)) {
            $vars['items'][] = get_news_item_data($news_obj, $global_store);
          }
        }
      }
    }
  }
}

/**
 * Get news item data.
 */
function get_news_item_data($news_obj, $global_store, $promo_inherit = FALSE, $start = FALSE, $end = FALSE) {
  $wrapper = entity_metadata_wrapper('node', $news_obj);

  $date_class = '';
  $li_class = '';

  $field_date_start = isset($wrapper->field_date_start) ? $wrapper->field_date_start->value() : 0;
  $field_date_end = isset($wrapper->field_date_end) ? $wrapper->field_date_end->value() : 0;

  $current_time = REQUEST_TIME;

  if ($field_date_start == $field_date_end) {
    $field_date_end = strtotime('+1 day', $field_date_start) - 1;
  }

  if (date('Y', $field_date_start) == date('Y', $field_date_end)) {
    $start_date = strtoupper(format_date($field_date_start, 'news_front'));
  }
  else {
    $start_date = strtoupper(format_date($field_date_start, 'news_front_full'));
  }

  $end_date = $field_date_end ? strtoupper(format_date($field_date_end, 'news_front_full')) : '';

  if ($current_time < $field_date_start) {
    $date_status = t('TO COME');
    $date_class = ' color1';
  }
  elseif ($field_date_end && ($current_time > $field_date_start) && ($current_time < $field_date_end)) {
    $date_status = t('NOW');
    $date_class = ' color2';
    $start_date = '';
  }
  else {
    $li_class = 'completed';
    $date_status = t('DONE');
  }

  $li_class .= $promo_inherit ? ' promo-link' : '';
  $li_class .= $start ? ' start' : '';
  $li_class .= $end ? ' end' : '';

  $type = isset($wrapper->field_news_type) ? $wrapper->field_news_type->value() : '';

  $status_class = 'status new';

  $query = new RelationQuery('node', $news_obj->nid, 0);
  $query->entityCondition('bundle', 'news_article_status');
  $query->fieldCondition('endpoints', 'entity_type', 'taxonomy_term', '=', 1);
  $query->fieldCondition('endpoints', 'entity_id', $global_store, '=', 1);
  $query->fieldCondition('field_news_store_deleted', 'value', 0, '=');
  $query->range(0, 1);
  $relations = $query->execute();

  if ($relations) {
    $relation = relation_load(key($relations), NULL, TRUE);
    $relation_wrapper = entity_metadata_wrapper('relation', $relation);

    if (isset($relation_wrapper->field_news_store_status)) {
      $status_key = $relation_wrapper->field_news_store_status->value();
      $statuses = array('new', 'in-progress', 'dealt');
      $status_class = 'status ' . $statuses[$status_key];
    }
  }

  return array(
    'nid' => $news_obj->nid,
    'title' => $news_obj->title,
    'start_date' => $start_date,
    'end_date' => $end_date,
    'date_status' => $date_status,
    'date_class' => $date_class,
    'status_class' => $status_class,
    'type' => isset($type->name) ? $type->name : '',
    'publish_ago' => iris_core_fo_date_format($news_obj->changed),
    'class' => $li_class,
  );
}
