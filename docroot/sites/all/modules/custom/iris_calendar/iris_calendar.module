<?php

/**
 * @file
 * Contains hooks implementations.
 */

define('IRIS_CALENDAR_CALENDAR_VIEW', 'calendar');
define('IRIS_CALENDAR_CALENDAR_INDEX', 'calendar');

/**
 * Implements hook_views_api().
 */
function iris_calendar_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'iris_calendar') . '/includes',
  );
}

/**
 * Implements hook_views_plugins().
 */
function iris_calendar_views_plugins() {
  $plugins = array(
    'style' => array(
      'iris_calendar' => array(
        'title' => t('IRIS Calendar'),
        'help' => t('Displays data in IRIS calendar style.'),
        'handler' => 'views_plugin_iris_style_calendar',
        'theme' => 'views_view_calendar',
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );

  return $plugins;
}

/**
 * Implements hook_views_api().
 */
function iris_calendar_search_api_views_query_alter(view &$view, SearchApiViewsQuery &$query) {
  if ($view->name == IRIS_CALENDAR_CALENDAR_VIEW) {
    $query_params = drupal_get_query_parameters();

    $start_time = (isset($query_params['date_start']) && !empty($query_params['date_start'])) ? $query_params['date_start'] : date('Y-m', time());

    $dates = iris_calendar_get_current_start_end_date($start_time);

    $start_condition_1 = array('calendar_end_date', $dates['start'], '>=');
    $start_condition_2 = array('calendar_start_date', $dates['end'], '<=');

    $end_condition_1 = array('calendar_end_date', $dates['end'], '<=');
    $end_condition_2 = array('calendar_start_date', $dates['start'], '>=');

    $query->group_operator = 'OR';
    $query->where[1]['conditions'][] = $start_condition_1;
    $query->where[1]['conditions'][] = $start_condition_2;
    $query->where[2]['conditions'][] = $end_condition_1;
    $query->where[2]['conditions'][] = $end_condition_2;
  }
}

/**
 * Returns start and end time for current period.
 */
function iris_calendar_get_current_start_end_date($start_time) {
  $start_time = $start_time . '-01T00:00:00.000001';

  $dates = array('start' => '', 'end' => '');

  $start_date = new DateTime($start_time);
  $start_date->modify('first monday of this month');

  if ($start_date) {
    $dates['start'] = $start_date->getTimestamp();

    $end_date = $start_date->modify('+ 3 month');
    $end_date->modify('first monday of this month');
    // End of sunday.
    $dates['end'] = $end_date->getTimestamp() - 1;

  }

  return $dates;
}

/**
 * Generate period navigation form.
 */
function iris_calendar_form($form, &$form_state) {
  $query_params = drupal_get_query_parameters();
  $cur_time = (isset($query_params['date_start']) && !empty($query_params['date_start'])) ? $query_params['date_start'] : date('Y-m', time());
  $cur_time_arr = explode('-', $cur_time);
  $year_now = date('Y', time());
  $month_now = date('m', time());
  $cur_year = isset($cur_time_arr[0]) ? $cur_time_arr[0] : $year_now;
  $cur_month = isset($cur_time_arr[1]) ? $cur_time_arr[1] : $month_now;

  $months = array();
  for ($m = 1; $m <= 12; $m++) {
    $months[sprintf('%02d', $m)] = t(date('F', mktime(0, 0, 0, $m, 1, $year_now)));
  }

  $years = array();
  for ($y = $year_now - 5; $y <= $year_now + 5; $y++) {
    $years[$y] = $y;
  }

  $form['iris_calendar_month'] = array(
    '#type' => 'select',
    '#options' => $months,
    '#default_value' => $cur_month,
    '#attributes' => array('class' => array('custom-select')),
  );

  $form['iris_calendar_year'] = array(
    '#type' => 'select',
    '#options' => $years,
    '#default_value' => $cur_year,
    '#attributes' => array('class' => array('custom-select')),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('OK'),
    '#prefix' => '<div class="submit">',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Submit callback for iris_calendar_form.
 */
function iris_calendar_form_submit($form, &$form_state) {
  $year = isset($form_state['values']['iris_calendar_year']) ? $form_state['values']['iris_calendar_year'] : date('Y', time());
  $month = isset($form_state['values']['iris_calendar_month']) ? $form_state['values']['iris_calendar_month'] : date('m', time());

  drupal_goto(current_path(), array('query' => array('date_start' => $year . '-' . $month)));
}

/**
 * Implements hook_entity_property_info_alter().
 */
function iris_calendar_entity_property_info_alter(&$info) {
  $info['node']['properties']['calendar_scope_stores'] = array(
    'type' => 'list<taxonomy_term>',
    'label' => t('Calendar scope stores'),
    'sanitized' => TRUE,
    'getter callback' => 'iris_calendar_get_scope_stores',
  );
  $info['node']['properties']['calendar_news_statuses'] = array(
    'type' => 'list<integer>',
    'label' => t('Calendar news relation'),
    'sanitized' => TRUE,
    'getter callback' => 'iris_calendar_get_news_statuses',
  );
  $info['node']['properties']['calendar_universes'] = array(
    'type' => 'list<taxonomy_term>',
    'label' => t('Calendar universes'),
    'sanitized' => TRUE,
    'getter callback' => 'iris_calendar_get_universes',
  );
  $info['node']['properties']['calendar_item_type'] = array(
    'type' => 'integer',
    'label' => t('Calendar item type'),
    'sanitized' => TRUE,
    'getter callback' => 'iris_calendar_get_item_type',
  );
  $info['node']['properties']['calendar_start_date'] = array(
    'type' => 'integer',
    'label' => t('Calendar start date'),
    'sanitized' => TRUE,
    'getter callback' => 'iris_calendar_get_start_date',
  );
  $info['node']['properties']['calendar_end_date'] = array(
    'type' => 'integer',
    'label' => t('Calendar end date'),
    'sanitized' => TRUE,
    'getter callback' => 'iris_calendar_get_end_date',
  );
  $info['node']['properties']['promo_news_count'] = array(
    'type' => 'integer',
    'label' => t('Promotion news count'),
    'sanitized' => TRUE,
    'getter callback' => 'iris_calendar_get_promo_news_count',
  );
  $info['node']['properties']['promo_operation_desc'] = array(
    'type' => 'text',
    'label' => t('Promotion operation description'),
    'sanitized' => TRUE,
    'getter callback' => 'iris_calendar_get_promo_operation_desc',
  );

}

/**
 * Getter callback for Calendar start date.
 */
function iris_calendar_get_start_date($item) {
  $wrapper = entity_metadata_wrapper('node', $item);
  $data = 0;

  if ($item->type == 'news') {
    $data = $wrapper->field_date_start->value();
  }
  elseif ($item->type == 'promotion') {
    $data = iris_calendar_get_promo_operation_field($wrapper, 'field_operation_start_date');
  }

  return $data;
}

/**
 * Getter callback for Calendar end date.
 */
function iris_calendar_get_end_date($item) {
  $wrapper = entity_metadata_wrapper('node', $item);
  $data = 0;

  if ($item->type == 'news') {
    $data = $wrapper->field_date_end->value();
  }
  elseif ($item->type == 'promotion') {
    $data = iris_calendar_get_promo_operation_field($wrapper, 'field_operation_end_date');
  }

  return $data;
}

/**
 * Getter callback for Promotion news count.
 */
function iris_calendar_get_promo_news_count($item) {
  $data = 0;

  if ($item->type == 'promotion') {
    $data = db_select('field_data_field_promotion', 'p')
      ->fields('p', array('entity_id'))
      ->condition('bundle', 'news')
      ->condition('deleted', 0)
      ->condition('field_promotion_target_id', $item->nid)
      ->countQuery()
      ->execute()
      ->fetchField();
  }

  return $data;
}

/**
 * Getter callback for Promotion operation description.
 */
function iris_calendar_get_promo_operation_desc($item) {
  $wrapper = entity_metadata_wrapper('node', $item);
  $data = '';

  if ($item->type == 'promotion') {
    $data = iris_calendar_get_promo_operation_field($wrapper, 'field_operation_description');
  }

  return $data;
}

/**
 * Get promo date field.
 */
function iris_calendar_get_promo_operation_field($wrapper, $field) {
  $promotion_lines = $wrapper->field_promotion_lines->value();
  $promotion_lines_wrapper = entity_metadata_wrapper('node', $promotion_lines[0]);
  $operation = $promotion_lines_wrapper->field_operation->value();
  $operation_wrapper = entity_metadata_wrapper('node', $operation);

  return $operation_wrapper->$field->value();
}

/**
 * Getter callback for Calendar universes.
 */
function iris_calendar_get_universes($item) {
  $wrapper = entity_metadata_wrapper('node', $item);
  $data = array();

  if ($item->type == 'news') {
    $universes = array();
    if (isset($wrapper->field_activity_domains)) {
      $activity_domains = $wrapper->field_activity_domains->value();

      foreach ($activity_domains as $domain) {
        $domain_wrapper = entity_metadata_wrapper('taxonomy_term', $domain);
        $universe_term = $domain_wrapper->field_universe->value();

        if (isset($universe_term->tid) && is_numeric($universe_term->tid)) {
          $universes[] = $universe_term->tid;
        }
      }
    }
    $data = array_unique($universes);
  }
  elseif ($item->type == 'promotion') {
    $activity_sector = $wrapper->field_promo_line_activity_sector->value();
    $activity_sector_wrapper = entity_metadata_wrapper('taxonomy_term', $activity_sector);
    $activity_domain = $activity_sector_wrapper->field_activity_domain->value();
    $activity_domain_wrapper = entity_metadata_wrapper('taxonomy_term', $activity_domain);
    $universe = $activity_domain_wrapper->field_universe->value();
    $data = array($universe->tid);
  }

  return $data;
}

/**
 * Getter callback for Calendar news statuses relation.
 */
function iris_calendar_get_news_statuses($item) {
  $data = array();

  if ($item->type == 'news') {

    $query = new RelationQuery('node', $item->nid, 0);
    $query->entityCondition('bundle', 'news_article_status');
    $query->fieldCondition('field_news_store_deleted', 'value', 0, '=');
    $relations = $query->execute();

    if ($relations) {
      foreach (array_keys($relations) as $rid) {
        $relation = relation_load($rid, NULL, TRUE);
        $relation_wrapper = entity_metadata_wrapper('relation', $relation);
        $status = $relation_wrapper->field_news_store_status->value();
        $store_id = $relation_wrapper->endpoints[1]->getIdentifier();
        $data[$store_id] = $status;
      }
    }
  }
  return $data;
}

/**
 * Getter callback for Calendar scope stores.
 */
function iris_calendar_get_scope_stores($item) {
  $data = array();

  if ($item->type == 'news') {
    $data = iris_search_get_location_group_scope_stores($item);
  }
  elseif ($item->type == 'promotion') {
    $wrapper = entity_metadata_wrapper('node', $item);
    $slot = $wrapper->field_promo_slot->value();

    $slot_wrapper = entity_metadata_wrapper('taxonomy_term', $slot);
    if (isset($slot_wrapper->field_stores)) {
      $field_values = $slot_wrapper->field_stores->value();

      foreach ($field_values as $val) {
        $data[] = $val->tid;
      }
    }

  }

  return $data;
}

/**
 * Getter callback for Calendar item type.
 */
function iris_calendar_get_item_type($item) {
  $data = 0;

  if ($item->type == 'news') {
    $wrapper = entity_metadata_wrapper('node', $item);
    $type = $wrapper->field_news_type->value();

    $data = isset($type->tid) ? $type->tid : 0;
  }

  return $data;
}
