<?php

/**
 * @file
 * Preprocess function for calendar views.
 */

/**
 * Implementation of template_process for views-view-calendar.tpl.php.
 */
function template_preprocess_views_view_calendar(&$vars) {

  global $global_store;
  $vars['grouped_items'] = array();

  foreach ($vars['items'] as $item) {
    if (isset($item->entity)) {
      $node = entity_load_single('node', $item->entity);
      $wrapper = entity_metadata_wrapper('node', $node);
      $id = isset($node->nid) ? $node->nid : 0;
      $title = isset($node->title) ? $node->title : '';

      if ($node->type == 'news') {
        $start_time = isset($wrapper->field_date_start) ? $wrapper->field_date_start->value() : 0;
        $end_time = isset($wrapper->field_date_end) ? $wrapper->field_date_end->value() : 0;
        $type = isset($wrapper->field_news_type) ? $wrapper->field_news_type->raw() : 0;

        $calendar_news_statuses = iris_calendar_get_news_statuses($node);
        $activity_sectors = isset($wrapper->field_activity_sectors) ? $wrapper->field_activity_sectors->value() : FALSE;
        $activity_domains = isset($wrapper->field_activity_domains) ? $wrapper->field_activity_domains->value() : FALSE;

        if (isset($activity_sectors[0]) && $calendar_news_statuses[$global_store] && $calendar_news_statuses[$global_store] == 2) {
          $term = $activity_sectors[0];
        }
        elseif (isset($activity_domains[0])) {
          $term = $activity_domains[0];
        }

        $title .= isset($term->name) ? ' ' . $term->name : '';
        $vars['grouped_items'][$start_time . $end_time]['items'][$id]['title'] = $title ? l($title, 'node/' . $id, array(
          'attributes' => array('target' => '_blank'),
          'html' => TRUE,
        )) : '';
      }
      else {
        $start_time = iris_calendar_get_promo_operation_field($wrapper, 'field_operation_start_date');
        $end_time = iris_calendar_get_promo_operation_field($wrapper, 'field_operation_end_date');
        $type = 0;

        $op_desc = iris_calendar_get_promo_operation_field($wrapper, 'field_operation_description');

        if ($op_desc && isset($wrapper->field_grouping_key)) {
          $title = $wrapper->field_grouping_key->value() . ' ' . $op_desc;
        }
        $vars['grouped_items'][$start_time . $end_time]['items'][$id]['title'] = $title ? l($title, 'node/' . $id, array(
          'attributes' => array('target' => '_blank'),
          'html' => TRUE,
          'query' => array('iris_admin_theme' => ''),
        )) : '';
      }

      $vars['grouped_items'][$start_time . $end_time]['start_time'] = $start_time;
      $vars['grouped_items'][$start_time . $end_time]['end_time'] = $end_time;
      $vars['grouped_items'][$start_time . $end_time]['news_type'] = $type;
      $vars['grouped_items'][$start_time . $end_time]['items'][$id]['title_string'] = $title;
    }
  }

  foreach ($vars['grouped_items'] as &$item) {
    $titles = array();

    foreach ($item['items'] as $key => $row) {
      $titles[$key] = $row['title_string'];
    }
    array_multisort($titles, SORT_ASC, $item['items']);

    unset($titles);
  }
}
