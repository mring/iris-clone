<?php

/**
 * @file
 * Contains the list style plugin.
 */

/**
 * Style plugin.
 *
 * @ingroup views_style_plugins
 */
class views_plugin_iris_style_calendar extends views_plugin_style {

  /**
   * Overrides views_handler::init().
   */
  public function init(&$view, &$display, $options = NULL) {
    $this->view = &$view;
    $this->display = &$display;

    $query_params = drupal_get_query_parameters();
    $start_time = (isset($query_params['date_start']) && !empty($query_params['date_start']))
      ? $query_params['date_start'] : date('Y-m', time());

    $dates = iris_calendar_get_current_start_end_date($start_time);
    $d = $dates['start'];
    $now = time();
    $period = array();
    $class = '';

    $first_week = TRUE;
    $last_week = FALSE;

    while ($d < $dates['end']) {
      $next_week = strtotime('+1 week', $d);
      $end_time = $next_week - 1;

      $year = date('Y', $d);
      $month = date('F', $d);
      $week = date('W', $d);
      $start_date = date('j', $d);
      $end_date = date('j', $end_time);

      if (($now >= $d) && ($now <= $end_time)) {
        $class = 'box current';
      }
      else {
        $class = 'box';
      }

      $period[$year][$month][$week] = array(
        'start_date' => $start_date,
        'end_date'   => $end_date,
        'start_time' => $d,
        'end_time'   => $end_time,
        'class'      => $class,
        'first_week' => $first_week,
        'last_week'  => $last_week,
      );

      $first_week = FALSE;
      $d = $next_week;
    }

    $period[$year][$month][$week]['last_week'] = TRUE;

    $prev_date = date('Y-m', strtotime('-1 month', $dates['start']));
    $next_date = date('Y-m', strtotime('+1 month', $dates['start']));

    $this->view->period_form = drupal_get_form('iris_calendar_form');
    $this->view->prev_date_link = url(current_path(), array('query' => array('date_start' => $prev_date)));
    $this->view->next_date_link = url(current_path(), array('query' => array('date_start' => $next_date)));
    $this->view->period = $period;

    // Overlay incoming options on top of defaults.
    $this->unpack_options($this->options, isset($options) ? $options : $display->handler->get_option('style_options'));

    if ($this->uses_row_plugin() && $display->handler->get_option('row_plugin')) {
      $this->row_plugin = $display->handler->get_plugin('row');
    }

    $this->options += array(
      'grouping' => array(),
    );

    $this->definition += array(
      'uses grouping' => TRUE,
    );
  }

  /**
   * Provide the default form for setting options.
   */
  public function options_form(&$form, &$form_state) {
    if ($this->uses_fields() && $this->definition['uses grouping']) {
      $options = array('' => t('- None -'));
      $field_labels = $this->display->handler->get_field_labels(TRUE);
      $options += $field_labels;
      // If there are no fields, we can't group on them.
      if (count($options) > 1) {
        // This is for backward compatibility,
        // when there was just a single select form.
        if (is_string($this->options['grouping'])) {
          $grouping = $this->options['grouping'];
          $this->options['grouping'] = array();
          $this->options['grouping'][0]['field'] = $grouping;
        }
        if (isset($this->options['group_rendered']) && is_string($this->options['group_rendered'])) {
          $this->options['grouping'][0]['rendered'] = $this->options['group_rendered'];
          unset($this->options['group_rendered']);
        }

        $c = count($this->options['grouping']);
        // Add a form for every grouping, plus one.
        for ($i = 0; $i <= $c; $i++) {
          $grouping = !empty($this->options['grouping'][$i]) ? $this->options['grouping'][$i] : array();
          $grouping += array(
            'field' => '',
            'rendered' => TRUE,
            'rendered_strip' => FALSE,
          );
          $form['grouping'][$i]['field'] = array(
            '#type' => 'select',
            '#title' => t('Grouping field Nr.@number', array('@number' => $i + 1)),
            '#options' => $options,
            '#default_value' => $grouping['field'],
            '#description' => t('You may optionally specify a field by which to group the records. Leave blank to not group.'),
          );
          $form['grouping'][$i]['rendered'] = array(
            '#type' => 'checkbox',
            '#title' => t('Use rendered output to group rows'),
            '#default_value' => $grouping['rendered'],
            '#description' => t('If enabled the rendered output of the grouping field is used to group the rows.'),
            '#dependency' => array(
              'edit-style-options-grouping-' . $i . '-field' => array_keys($field_labels),
            ),
          );
          $form['grouping'][$i]['rendered_strip'] = array(
            '#type' => 'checkbox',
            '#title' => t('Remove tags from rendered output'),
            '#default_value' => $grouping['rendered_strip'],
            '#dependency' => array(
              'edit-style-options-grouping-' . $i . '-field' => array_keys($field_labels),
            ),
          );
        }
      }
    }
  }

  /**
   * Render the display in this style.
   */
  public function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      debug('views_plugin_style_default: Missing row plugin');
      return;
    }

    // Group the rows according to the grouping instructions, if specified.
    $sets = $this->render_grouping(
      $this->view->result,
      $this->options['grouping'],
      TRUE
    );

    return $this->render_grouping_sets($sets);
  }

  /**
   * Group records as needed for rendering.
   *
   * @param array $records
   *   An array of records from the view to group.
   * @param array $groupings
   *   An array of grouping instructions on which fields to group. If empty, the
   *   result set will be given a single group with an empty string as a label.
   * @param bool $group_rendered
   *   Boolean value whether to use the rendered or the raw field value for
   *   grouping. If set to NULL the return is structured as before
   *   Views 7.x-3.0-rc2. After Views 7.x-3.0 this boolean is only used if
   *   $groupings is an old-style string or if the rendered option is missing
   *   for a grouping instruction.
   *
   * @return array
   *   The grouped record set.
   *   A nested set structure is generated if multiple grouping fields are used.
   *
   * @code
   *   array(
   *     'grouping_field_1:grouping_1' => array(
   *       'group' => 'grouping_field_1:content_1',
   *       'rows' => array(
   *         'grouping_field_2:grouping_a' => array(
   *           'group' => 'grouping_field_2:content_a',
   *           'rows' => array(
   *             $row_index_1 => $row_1,
   *             $row_index_2 => $row_2,
   *             // ...
   *           )
   *         ),
   *       ),
   *     ),
   *     'grouping_field_1:grouping_2' => array(
   *       // ...
   *     ),
   *   )
   * @endcode
   */
  public function render_grouping($records, $groupings = array(), $group_rendered = NULL) {
    // This is for backward compatibility,
    // when $groupings was a string containing
    // the ID of a single field.
    if (is_string($groupings)) {
      $rendered = $group_rendered === NULL ? TRUE : $group_rendered;
      $groupings = array(array('field' => $groupings, 'rendered' => $rendered));
    }

    $type_data = array();
    $sets = array();

    if ($groupings) {
      foreach ($records as $index => $row) {
        // Iterate through configured grouping fields to determine the
        // hierarchically positioned set where the current row belongs to.
        // While iterating, parent groups, that do not exist yet, are added.
        $set = &$sets;

        foreach ($groupings as $info) {
          $field = $info['field'];
          $rendered = isset($info['rendered']) ? $info['rendered'] : $group_rendered;
          $rendered_strip = isset($info['rendered_strip']) ? $info['rendered_strip'] : FALSE;
          $grouping = '';
          $group_content = '';
          // Group on the rendered version of the field, not the raw.  That way,
          // we can control any special formatting of the grouping field through
          // the admin or theme layer or anywhere else we'd like.
          if (isset($this->view->field[$field])) {
            if ($field == 'calendar_item_type') {
              $news_type = isset($this->view->field[$field]->wrappers[$index]->field_news_type)
                ? $this->view->field[$field]->wrappers[$index]->field_news_type->raw() : 0;
              $group_content = $news_type ?: 0;
            }
            else {
              $group_content = $this->get_field($index, $field);
            }

            if ($this->view->field[$field]->options['label']) {
              $group_content = $this->view->field[$field]->options['label'] . ': ' . $group_content;
            }
            if ($rendered) {
              $grouping = $group_content;
              if ($rendered_strip) {
                $group_content = $grouping = strip_tags(htmlspecialchars_decode($group_content));
              }
            }
            else {
              $grouping = $this->get_field_value($index, $field);
              // Not all field handlers return a scalar value,
              // e.g. views_handler_field_field.
              if (!is_scalar($grouping)) {
                $grouping = md5(serialize($grouping));
              }
            }
          }

          // Create the group if it does not exist yet.
          if (empty($set[$grouping])) {
            $set[$grouping]['group'] = $group_content;
            $set[$grouping]['rows'] = array();
          }

          $set[$grouping]['class'] = '';
          $set[$grouping]['icon'] = '';

          if ($grouping == 0) {
            $set[$grouping]['group'] = t('Promotions');
            $set[$grouping]['class'] = 'promotions';
            $set[$grouping]['icon'] = 'flag';
          }
          else {
            if (!isset($type_data[$grouping])) {
              $type = taxonomy_term_load($grouping);
              $wrapper = entity_metadata_wrapper('taxonomy_term', $type);

              $type_data[$grouping]['name'] = $type->name;
              $type_data[$grouping]['class'] = $wrapper->field_agenda_color_class->value();
              $type_data[$grouping]['icon'] = $wrapper->field_left_menu_image->value();
            }
          }

          if (isset($type_data[$grouping]['name'])) {
            $set[$grouping]['group'] = $type_data[$grouping]['name'];
          }
          if (isset($type_data[$grouping]['class'])) {
            $set[$grouping]['class'] = $type_data[$grouping]['class'];
          }
          if (isset($type_data[$grouping]['icon'])) {
            $set[$grouping]['icon'] = $type_data[$grouping]['icon'];
          }

          // Move the set reference into the row set of the group
          // we just determined.
          $set = &$set[$grouping]['rows'];
        }
        // Add the row to the hierarchically positioned row set
        // we just determined.
        $set[$index] = $row;
      }
    }
    else {
      // Create a single group with an empty grouping field.
      $sets[''] = array(
        'group' => '',
        'rows'  => $records,
        'class' => '',
        'icon'  => '',
      );
    }

    // If this parameter isn't explicitly set modify the output to be fully
    // backward compatible to code before Views 7.x-3.0-rc2.
    // @TODO Remove this as soon as possible e.g. October 2020
    if ($group_rendered === NULL) {
      $old_style_sets = array();
      foreach ($sets as $group) {
        $old_style_sets[$group['group']] = $group['rows'];
      }
      $sets = $old_style_sets;
    }

    if (isset($sets[0])) {
      $promo = $sets[0];
      unset($sets[0]);
      array_push($sets, $promo);
    }

    return $sets;
  }

  /**
   * Render the grouping sets.
   *
   * Plugins may override this method if they wish some other way of handling
   * grouping.
   *
   * @param array $sets
   *   Containing the grouping sets to render.
   * @param int $level
   *   Indicating the hierarchical level of the grouping.
   *
   * @return string
   *   Rendered output of given grouping sets.
   */
  public function render_grouping_sets($sets, $level = 0) {
    $output = '';

    foreach ($sets as $set) {
      $row = reset($set['rows']);
      // Render as a grouping set.
      if (is_array($row) && isset($row['group'])) {
        $output .= theme(views_theme_functions('views_view_grouping', $this->view, $this->display),
          array(
            'view' => $this->view,
            'grouping' => $this->options['grouping'][$level],
            'grouping_level' => $level,
            'rows' => $set['rows'],
            'title' => $set['group'],
          )
        );
      }
      // Render as a record set.
      else {
        $items = array();

        if ($this->uses_row_plugin()) {
          $items = $set['rows'];
        }

        $output .= theme($this->theme_functions(),
          array(
            'view'           => $this->view,
            'options'        => $this->options,
            'grouping_level' => $level,
            'rows'           => array(),
            'items'          => $items,
            'title'          => $set['group'],
            'class'          => isset($set['class']) ? $set['class'] : '',
            'icon'           => isset($set['icon']) ? $set['icon'] : '',
          )
        );
      }
    }

    unset($this->view->row_index);
    return $output;
  }

}
