<?php

/**
 * Base sort handler that has no options and performs a simple sort.
 *
 * @ingroup views_sort_handlers
 */
class views_handler_slot_stores_sort extends views_handler_sort {

  /**
   * Query function.
   */
  public function query() {

    $this->ensure_my_table();

    if (isset($this->view->query->tables['node_field_data_field_promotion_lines'])) {
      // Collect all promotion stores in string and sort by this string.
      $sub_query = "(SELECT GROUP_CONCAT(sap.field_taxonomy_code_value) "
        . "FROM field_data_field_taxonomy_code AS sap "
        . "LEFT JOIN taxonomy_term_data term ON term.tid = sap.entity_id "
        . "LEFT JOIN og_membership og ON og.gid = term.tid  "
        . "LEFT JOIN field_data_field_promotion_lines pl ON pl.field_promotion_lines_target_id = og.etid  "
        // This is a the obligatory condition mapping the subquery
        // with the outer query.
        . "WHERE pl.field_promotion_lines_target_id = node_field_data_field_promotion_lines.nid "
        . "AND og.state = 1 AND og.field_name = 'field_slot_stores' AND og.entity_type = 'node')";

      $this->query->add_orderby(NULL, $sub_query, $this->options['order'], 'subquery');
    }

  }

}
