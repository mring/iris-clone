<?php

/**
 * @file
 * Contains \views_plugin_pager_infinite_scroll.
 */

/**
 * The plugin to handle the infinite scroll pager.
 *
 * @ingroup views_pager_plugins
 */
class iris_views_plugin_pager_infinite_scroll extends views_plugin_pager_full {

  /**
   * Add nessesary options.
   */
  public function option_definition() {
    $options['items_per_page'] = array('default' => 10);
    $options['offset'] = array('default' => 0);
    $options['content_class'] = array('default' => '');
    return $options;
  }

  /**
   * Options form.
   */
  public function options_form(&$form, &$form_state) {
    $form['items_per_page'] = array(
      '#title'         => t('Items to display'),
      '#type'          => 'textfield',
      '#description'   => t('The number of items to display. Enter 0 for no limit.'),
      '#default_value' => $this->options['items_per_page'],
    );
    $form['offset'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Offset'),
      '#description'   => t('The number of items to skip. For example, if this field is 3, the first 3 items will be skipped and not displayed.'),
      '#default_value' => $this->options['offset'],
    );

    $form['content_class'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Content class'),
      '#required'      => TRUE,
      '#description'   => t('Contents container class.'),
      '#default_value' => isset($this->options['content_class']) ? $this->options['content_class'] : '',
    );
  }

  /**
   * Validation for options form.
   */
  public function options_validate(&$form, &$form_state) {
  }

  /**
   * Summary title.
   */
  public function summary_title() {
    return t('IRIS Infinite Scroll: @parent', ['@parent' => parent::summary_title()]);
  }

  /**
   * Render function.
   */
  public function render($input) {
    $this->view->set_use_ajax(TRUE);
    $pager_theme = views_theme_functions('iris_views_infinite_scroll_pager', $this->view, $this->display);
    $module_path = drupal_get_path('module', 'iris_views_infinite_scroll');
    $settings = array(
      'iris_views_infinite_scroll' => array(
        'img_path'  => url($module_path . '/images/ajax-loader.gif'),
        'container' => isset($this->options['content_class']) ? $this->options['content_class'] : '',
      ),
    );
    drupal_add_js($settings, array('type' => 'setting', 'scope' => JS_DEFAULT));
    drupal_add_js($module_path . '/js/iris-views-infinite-scroll.js', array('scope' => 'footer'));

    return theme($pager_theme);
  }

}
