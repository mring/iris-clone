(function ($) {
    "use strict";

    /**
     * Insert a views infinite scroll view into the document after AJAX.
     *
     * @param {object} $new_view The new view coming from the server.
     */
    $.fn.irisInfiniteScrollInsertView = function ($new_view) {
        var $existing_view = this;
        var $existing_content = $existing_view.find('.view-content').children();
        $new_view.find('.view-content').prepend($existing_content);
        $existing_view.replaceWith($new_view);
        $(document).trigger('infiniteScrollComplete', [$new_view, $existing_content]);
    };

    /**
     * Handle the automatic paging based on the scroll amount.
     */
    Drupal.behaviors.iris_views_infinite_scroll_automatic = {
        attach: function (context, settings) {
            var vis_index = 0;
            var scroll_threshold = 30;
            var settings = settings.iris_views_infinite_scroll;
            var window = '.' + settings.container;
            var loadingImg = '<div class="views_infinite_scroll-ajax-loader"><img src="' + settings.img_path + '" alt="loading..."/></div>';

            var $window = $(window);
            var $contentContainer = $('#content .news-list-container');
            var $scroll = $contentContainer.find('.jcf-scrollbar-handle');

            $('.pager--infinite-scroll-auto', context).once().each(function () {
                var $pager = $(this);
                $pager.find('.pager__item').hide();
                if ($pager.find('.pager__item a').length) {
                    $('.pager__item a', context).once().bind('click', function () {
                        if (!$('.views_infinite_scroll-ajax-loader').length) {
                            $pager.append(loadingImg);
                        }
                    });
                }

                $window.bind('scroll.iris_views_infinite_scroll_' + vis_index, function () {
                    var deviceAgent = navigator.userAgent.toLowerCase();
                    var isTouchDevice = Modernizr.touch ||
                        (deviceAgent.match(/(iphone|ipod|ipad)/) ||
                            deviceAgent.match(/iphone/i) ||
                            deviceAgent.match(/ipad/i) ||
                            deviceAgent.match(/ipod/i));
                    
                    if (isTouchDevice) {
                        var scroll_pos = $('.news-list-scroll').scrollTop() + $('.news-list-scroll').height() + parseInt(scroll_threshold);
                        if ($('.news-list-scroll .view').height() < scroll_pos) {
                            $('.pager__item a').click();
                            $window.unbind('scroll.iris_views_infinite_scroll_' + vis_index);
                        }
                    }
                    if(typeof $scroll.position() !== "undefined" && !isTouchDevice) {
                        var scroll_pos = parseInt($scroll.position().top) + parseInt($scroll.prop('scrollHeight')) + parseInt(scroll_threshold);
                        if ($window.height() < scroll_pos) {
                            $('.pager__item a').click();
                            $window.unbind('scroll.iris_views_infinite_scroll_' + vis_index);
                        }
                    }
                });
                vis_index++;
            });

        }
    };

})(jQuery);
