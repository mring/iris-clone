/**
 * @file iris_comments.js
 */
(function ($) {
  /**
   * Attaches behavior for Shift+Enter.
   */
  Drupal.behaviors.irisCommentsNewLine = {
    attach: function (context, settings) {
      $('.form-comment textarea').keydown(function (e) {
        if (e.which == 13 && !e.shiftKey) {
          e.preventDefault();

          var $form = $(this.form);
          $form.find('[type="submit"]').trigger('click');
        }
      });
    }
  };

  /**
   * Ajax delivery command to close fancybox popup.
   */
  Drupal.ajax.prototype.commands.closePopup = function (ajax, response, status) {
    jQuery.fancybox.close();
    return false;
  };
})(jQuery);
