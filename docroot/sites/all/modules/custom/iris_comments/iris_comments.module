<?php

/**
 * @file
 * Contains hooks implementations.
 */

/**
 * Implements hook_menu().
 */
function iris_comments_menu() {
  $items['iris-cmnt/%/delete'] = array(
    'page callback' => 'iris_comments_ajax_comments_delete',
    'page arguments' => array(1),
    'access callback' => 'iris_core_comment_delete_access_check',
    'access arguments' => array(1),
    'delivery callback' => 'ajax_deliver',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function iris_comments_menu_alter(&$items) {
  if (isset($items['comment/%/delete'])) {
    $items['comment/%/delete']['access callback'] = 'iris_core_comment_delete_access_check';
    $items['comment/%/delete']['access arguments'] = array(1);
  }
}

/**
 * Checks whether user has access to delete comment.
 *
 * @param int $cid
 *   Comment id.
 *
 * @return bool
 *   True if user has access.
 */
function iris_core_comment_delete_access_check($cid) {
  global $user;
  $comment = comment_load($cid);
  $first_condition = user_access('delete any comments');
  $second_condition = $comment->uid == $user->uid && user_access('delete own comments');

  return $first_condition || $second_condition ? TRUE : FALSE;
}

/**
 * Implements hook_permission().
 */
function iris_comments_permission() {
  return array('delete own comments' => array('title' => t('Delete own comments')), 'delete any comments' => array('title' => t('Delete any comments')));
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function iris_comments_form_comment_form_alter(&$form, &$form_state, $form_id) {
  if (!ajax_comments_node_type_active($form['#node']->type)) {
    return;
  }

  $form['#attached']['js'][] = drupal_get_path('module', 'iris_comments') . '/js/iris_comments.js';

  global $user;

  $account = user_load($user->uid);
  $account->node_type = 'comment_node_news';

  $form['able_to_post'] = array(
    '#type' => 'hidden',
    '#value' => array_intersect(array(
      'administrator',
      'webmaster',
      'manager',
    ), $account->roles) ? TRUE : FALSE,
  );

  $reply_class = in_array('ajax-comments-form-reply', $form['#attributes']['class']) ? ' secondary' : '';

  $prefix = '<div class="comment-row' . $reply_class . '">';
  $prefix .= theme('user_picture', array('account' => $account));
  $prefix .= '<div class="txt"><div class="form-comment">';

  $form['wrapper_prefix'] = array('#markup' => $prefix, '#weight' => -1);

  $form['author']['#access'] = FALSE;
  $form['subject']['#access'] = FALSE;
  $form['actions']['submit']['#ajax']['event'] = 'click';
  $form['actions']['#attributes']['class'][] = 'element-invisible';

  $form['comment_body'][LANGUAGE_NONE][0]['#wysiwyg'] = FALSE;
  $form['comment_body'][LANGUAGE_NONE][0]['#resizable'] = FALSE;

  if (isset($form['comment_body'][LANGUAGE_NONE][0]['#base_type'])) {
    $form['comment_body'][LANGUAGE_NONE][0]['#attributes']['placeholder'] = t('Leave a comment');
  }

  if (!empty($form_state['values']['pid'])) {
    $form['comment_body'][LANGUAGE_NONE][0]['#attributes']['placeholder'] = t('Reply...');
  }

  $suffix = '</div></div></div>';

  $form['wrapper_suffix'] = array('#markup' => $suffix, '#weight' => 100);
}

/**
 * Callback for clicking "delete".
 */
function iris_comments_ajax_comments_delete($cid) {
  if (!($comment = comment_load($cid))) {
    return MENU_NOT_FOUND;
  }

  // Need to include comment module admin file for delete form.
  $form_state = array();
  $form_state['build_info']['args'] = array($comment);

  // Load this using form_load_include so it's cached properly and works in the.
  // Ajax callback.
  form_load_include($form_state, 'inc', 'comment', 'comment.admin');

  // Get child comments (replies on this comment)
  $form_state['storage']['cids'] = array();
  $query = db_select('comment', 'c');
  $query->addField('c', 'cid');
  $query->condition('c.nid', $comment->nid)->condition('c.thread', substr($comment->thread, 0, -1) . '.%', 'LIKE')->addTag('node_access');
  if (!user_access('administer comments')) {
    $query->condition('c.status', COMMENT_PUBLISHED);
  }
  $query->orderBy('c.cid', 'ASC');
  $cids = $query->execute()->fetchCol();
  // Save child comments ids if they are exist.
  if (!empty($cids)) {
    $form_state['storage']['cids'] = $cids;
  }

  $id = 'popup-' . $cid;
  $form_build = drupal_build_form('comment_confirm_delete', $form_state);
  $form = '<div class="popup" id="' . $id . '" style="display:none">' . drupal_render($form_build) . '</div>';

  $commands[] = ajax_command_prepend('.comment-wrapper-' . $cid, '<a class="open-popup" href="#' . $id . '"></a>');
  $commands[] = ajax_command_prepend('.comment-wrapper-' . $cid, $form);
  $commands[] = ajax_command_invoke('.open-popup', 'click');

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function iris_comments_form_comment_confirm_delete_alter(&$form, &$form_state, $form_id) {
  if (ajax_comments_node_type_active(substr($form['#comment']->node_type, strlen('comment_node_')))) {
    $cid = $form['cid']['#value'];

    $form['description'] = array('#markup' => '<h3>' . t('Delete this comment?') . '</h3>' . '<p>' . t('This action is irreversible.') . '</p>');
    $form['actions']['#prefix'] = '<div class="btn-line">';
    $form['actions']['#suffix'] = '</div">';
    $form['actions']['submit']['#ajax'] = array(
      'callback' => 'iris_comments_ajax_comments_delete_js',
      'wrapper' => $form['#id'],
      'method' => 'replace',
    );
    $form['actions']['submit']['#value'] = t('Yes');
    $form['actions']['submit']['#attributes']['class'][] = 'ico-yes';
    $form['actions']['cancel']['#title'] = '<span class="ico-not"></span> ' . t('No');
    $form['actions']['cancel']['#options']['html'] = TRUE;
    $form['actions']['cancel']['#attributes']['class'][] = 'not';

    unset($form['actions']['cancel']['#attributes']['onclick']);
    $form['actions']['cancel']['#attributes']['onclick'][] = 'jQuery.fancybox.close(); jQuery("#popup-' . $cid . '").remove(); jQuery(".comment .open-popup").remove(); jQuery(".comment .fancybox-placeholder").remove(); return false;';
  }
}

/**
 * Removes the comment.
 */
function iris_comments_ajax_comments_delete_js($form, &$form_state) {
  $comment = $form['#comment'];

  ajax_comments_remove_status();

  $commands[] = array('command' => 'closePopup');
  $commands[] = ajax_command_remove('.comment-wrapper-' . $comment->cid);
  $commands[] = ajax_command_remove('a#comment-' . $comment->cid);
  $commands[] = ajax_command_remove('.ajax-comments-form-reply');
  $commands[] = ajax_command_replace('#comments .empty-comment-wrapper', '');
  $commands[] = ajax_command_prepend('#comments', '<div class="comment ajax-comment-wrapper clearfix comment-row empty-comment-wrapper"></div>');

  if (!empty($form_state['storage']['cids'])) {
    foreach ($form_state['storage']['cids'] as $cid) {
      $commands[] = ajax_command_remove('.comment-wrapper-' . $cid);
      $commands[] = ajax_command_remove('a#comment-' . $cid);
    }
  }

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Implements hook_ajax_render_alter().
 */
function iris_comments_ajax_render_alter(&$commands) {
  foreach ($commands as &$command) {
    if ($command['command'] == 'ajaxCommentsAddDummyDivAfter' && $command['class'] == 'indented') {
      $command['class'] .= ' ajax-comment-wrapper';
    }
  }
}

/**
 * Implements hook_comment_view_alter().
 */
function iris_comments_comment_view_alter(&$build) {
  $comment = $build['#comment'];
  $node = $build['#node'];

  if (empty($comment->in_preview)) {
    $prefix = '';
    $is_threaded = isset($comment->divs) && variable_get('comment_default_mode_' . $node->type, COMMENT_MODE_THREADED) == COMMENT_MODE_THREADED;

    // Add 'new' anchor if needed.
    if (!empty($comment->first_new)) {
      $prefix .= "<a id=\"new\"></a>\n";
    }

    // Add indentation div or close open divs as needed.
    if ($is_threaded) {
      $prefix .= $comment->divs <= 0 ? str_repeat('</div>', abs($comment->divs)) : "\n" . '<div class="indented ajax-comment-wrapper">';
    }

    // Add anchor for each comment.
    $prefix .= "<a id=\"comment-$comment->cid\"></a>\n";
    $build['#prefix'] = $prefix;

    // Close all open divs.
    if ($is_threaded && !empty($comment->divs_final)) {
      $build['#suffix'] = str_repeat('</div>', $comment->divs_final);
    }
  }
}
