<?php

/**
 * @file
 * Global footer CTools plugin file.
 */

$plugin = array(
  'title'           => t('Node created date'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'node_created_date_content_type_render',
  'edit form'       => 'node_created_date_content_type_edit_form',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Render callback.
 */
function node_created_date_content_type_render($subtype, $conf, $panel_args, $context) {
  $nid = !empty($context->data->nid) ? $context->data->nid : FALSE;
  if (!$nid) {
    return '';
  }
  global $language;

  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';

  if (isset($conf['created_date_format_' . $language->language])) {
    $block->content = format_date($context->data->created, $conf['created_date_format_' . $language->language]);
  }

  return $block;
}

/**
 * Settings form.
 */
function node_created_date_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $languages = language_list();
  $date_formats = array();

  foreach (system_get_date_types() as $key => $format) {
    $date_formats[$key] = $format['title'];
  }

  foreach ($languages as $language) {
    $machine_name = 'created_date_format_' . $language->language;

    $form[$machine_name] = array(
      '#type' => 'select',
      '#title' => t("Date format for @language", array('@language' => $language->language)),
      '#options' => $date_formats,
      '#default_value' => isset($conf[$machine_name]) ? $conf[$machine_name] : '',
    );
  }

  return $form;
}

/**
 * Submit function for node_created_date_content_type_edit_form.
 */
function node_created_date_content_type_edit_form_submit($form, &$form_state) {
  $languages = language_list();
  $keys = array();

  foreach ($languages as $language) {
    $keys[] = 'created_date_format_' . $language->language;
  }

  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}
