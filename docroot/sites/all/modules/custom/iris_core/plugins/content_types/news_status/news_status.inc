<?php

/**
 * @file
 * Global footer CTools plugin file.
 */

$plugin = array(
  'title'           => t('News status'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'news_status_content_type_render',
  'edit form'       => 'news_status_content_type_render_config',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Edit form for config.
 */
function news_status_content_type_render_config($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['read_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Read only'),
    '#default_value' => !empty($conf['read_only']) ? $conf['read_only'] : FALSE,
  );
  return $form;
}

/**
 * Submit function for config form.
 */
function news_status_content_type_render_config_submit($form, &$form_state) {
  $form_state['conf']['read_only'] = $form_state['values']['read_only'] ? $form_state['values']['read_only'] : NULL;
}

/**
 * Render callback.
 */
function news_status_content_type_render($subtype, $conf, $panel_args, $context) {
  $news_nid = !empty($context->data->nid) ? $context->data->nid : FALSE;
  if (!$news_nid) {
    return '';
  }

  global $user;

  drupal_add_library('system', 'drupal.ajax');

  $news_wrapper = entity_metadata_wrapper('node', $context->data);
  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';
  drupal_add_js(path_to_theme() . '/html/js/jquery.fancybox.pack.js');
  drupal_add_js(path_to_theme() . '/html/js/behaviors/fancybox.js');
  drupal_add_js(path_to_theme() . '/html/js/behaviors/news-status.js');

  $store = irish_search_get_current_store($context->data);

  $proper_store = FALSE;
  $user_obj = user_load($user->uid);
  $user_wrapper = entity_metadata_wrapper('user', $user_obj);

  if (isset($user_wrapper->og_user_taxonomy_term)) {
    $user_stores = $user_wrapper->og_user_taxonomy_term->raw();

    if (in_array($store, $user_stores)) {
      $proper_store = TRUE;
    }
  }

  $query = new RelationQuery('node', $news_nid, 0);
  $query->entityCondition('bundle', 'news_article_status');
  $query->fieldCondition('endpoints', 'entity_type', 'taxonomy_term', '=', 1);
  $query->fieldCondition('endpoints', 'entity_id', $store, '=', 1);
  $query->fieldCondition('field_news_store_deleted', 'value', 0, '=');
  $query->range(0, 1);
  $relations = $query->execute();
  if ($relations) {
    $relation = relation_load(key($relations), NULL, TRUE);
    $relation_wrapper = entity_metadata_wrapper('relation', $relation);

    $links = array(
      'new_link' => url('news_status/update/' . $relation_wrapper->getIdentifier() . '/0/nojs'),
      'in_progress_link' => $proper_store ? url('news_status/update/' . $relation_wrapper->getIdentifier() . '/1/nojs') : '#popup2',
      'done_link' => url('news_status/update/' . $relation_wrapper->getIdentifier() . '/2/nojs'),
    );

    $date = '';
    $date_completed = REQUEST_TIME;

    switch ($relation_wrapper->field_news_store_status->value()) {
      case 0:
        if ($action_date = $news_wrapper->field_action_date_deadline->value()) {
          $date = $action_date;
        }
        break;

      case 1:
        if ($action_date = $news_wrapper->field_action_date_deadline->value()) {
          $date = $action_date;
        }
        break;

      case 2:
        $links = array();
        $date_completed = $relation_wrapper->field_news_store_validation_date->value();
        $author = $relation_wrapper->field_news_store_author->field_first_name->value() . ' ' . $relation_wrapper->field_news_store_author->field_last_name->value();
        break;

      default:
        break;
    }

    $block->content = array(
      'rid'            => $relation_wrapper->getIdentifier(),
      'status'         => $relation_wrapper->field_news_store_status->value(),
      'author'         => isset($author) ? $author : FALSE,
      'date'           => $date ? format_date($date, 'pages_front') : FALSE,
      'date_completed' => $date_completed ? format_date($date_completed, 'pages_front') : FALSE,
      'action'         => $news_wrapper->field_action_news->value(),
      'proper_store'   => $proper_store,
    );

    $allowed_roles = array('administrator', 'webmaster', 'manager');

    if (!empty($conf['read_only']) || !array_intersect($allowed_roles, $user->roles)) {
      $links = array();
    }

    $block->content += $links;
  }
  else {
    $block->content['building_message']['gif']['#markup'] = '<div class="news-status-progress-placeholder" data-store="' . $store . '" data-entity-id="' . $news_nid . '">' . theme_image(array(
      'path' => path_to_theme() . '/html/images/plane.gif',
      'width' => NULL,
      'height' => NULL,
      'attributes' => array(),
    )) . '</div>';
    $block->content['building_message']['message']['#markup'] = t('The news is currently being initialized, thank you for your patience.');
  }

  return $block;
}
