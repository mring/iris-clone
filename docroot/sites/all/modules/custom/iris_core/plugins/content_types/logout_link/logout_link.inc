<?php

/**
 * @file
 * Author picture CTools plugin file.
 */

$plugin = array(
  'title'           => t('Logout link'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'logout_link_content_type_render',
);

/**
 * Render callback.
 */
function logout_link_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';

  if (user_is_logged_in()) {
    $link = l('<span class="ico icon-logout"></span>', 'user/logout', array('html' => TRUE));
    $block->content = '<div class="logout">' . $link . '</div>';
  }

  return $block;
}
