<?php

/**
 * @file
 * Author picture CTools plugin file.
 */

$plugin = array(
  'title'           => t('News banner'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'news_banner_content_type_render',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Render callback.
 */
function news_banner_content_type_render($subtype, $conf, $panel_args, $context) {
  $news_nid = !empty($context->data->nid) ? $context->data->nid : FALSE;
  if (!$news_nid) {
    return '';
  }

  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';

  $image = '';
  $news_wrapper = entity_metadata_wrapper('node', $context->data);

  if ($news_wrapper->field_banner_image->value()) {
    $image = $news_wrapper->field_banner_image->file->value();
  }
  else {
    $news_type = $news_wrapper->field_news_type->value();
    $news_type_wrapper = entity_metadata_wrapper('taxonomy_term', $news_type);

    if ($news_type_wrapper->field_default_banner_image->value()) {
      $image = $news_type_wrapper->field_default_banner_image->file->value();
    }
  }

  if ($image) {
    $block->content = theme_image_style(array(
      'style_name' => 'news_banner',
      'path' => $image->uri,
      'width' => NULL,
      'height' => NULL,
    ));
  }

  return $block;
}
