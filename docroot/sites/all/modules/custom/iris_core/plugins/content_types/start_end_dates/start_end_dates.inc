<?php

/**
 * @file
 * Author picture CTools plugin file.
 */

$plugin = array(
  'title'           => t('Start and end dates'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'start_end_dates_content_type_render',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Render callback.
 */
function start_end_dates_content_type_render($subtype, $conf, $panel_args, $context) {
  $news_nid = !empty($context->data->nid) ? $context->data->nid : FALSE;
  if (!$news_nid) {
    return '';
  }

  $setting = array('iris_search_active' => array('entityId' => $news_nid));
  drupal_add_js($setting, 'setting');

  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';
  $end_date_year = '';

  $wrapper = entity_metadata_wrapper('node', $context->data);
  $start_date = $wrapper->field_date_start->value();
  $end_date = $wrapper->field_date_end->value();

  $start_date_out = strtoupper(format_date($start_date, 'news_front'));
  $start_date_year = format_date($start_date, 'custom', 'Y');

  if ($end_date) {
    $end_date_out = strtoupper(format_date($end_date, 'news_front'));
    $end_date_year = format_date($end_date, 'custom', 'Y');
  }

  $output = '';
  $output .= '<span class="date">' . $start_date_out;

  if ($start_date_year != $end_date_year) {
    $output .= ' ' . $start_date_year;
  }

  if ($end_date) {
    $output .= '<span class="icon-arrow-next"></span>' . $end_date_out . ' ' . $end_date_year;
  }

  $output .= '</span>';

  $block->content = $output;

  return $block;
}
