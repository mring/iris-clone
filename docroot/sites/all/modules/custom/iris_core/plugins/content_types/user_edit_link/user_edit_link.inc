<?php

/**
 * @file
 * Author picture CTools plugin file.
 */

$plugin = array(
  'title'           => t('User edit link'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'user_edit_link_content_type_render',
  'required context' => new ctools_context_required(t('User'), 'user'),
);

/**
 * Render callback.
 */
function user_edit_link_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';

  if (isset($context->data->uid) && !empty($context->data->uid)) {
    $link = l('<span class="ico icon-settings"></span>', 'user/' . $context->data->uid . '/edit', array('html' => TRUE));
    $block->content = '<div class="setting">' . $link . '</div>';
  }

  return $block;
}
