<?php

/**
 * @file
 * Global footer CTools plugin file.
 */

$plugin = array(
  'title'           => t('Custom link'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'custom_link_content_type_render',
  'edit form'       => 'custom_link_content_type_edit_form',
);

/**
 * Render callback.
 */
function custom_link_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';

  $title = isset($conf['link_title']) ? t($conf['link_title']) : '';
  $link = isset($conf['link_link']) ? $conf['link_link'] : '';
  $block->content .= isset($conf['link_prefix']) ? $conf['link_prefix'] : '';
  $block->content .= l($title, $link);
  $block->content .= isset($conf['link_suffix']) ? $conf['link_suffix'] : '';

  return $block;
}

/**
 * Settings form.
 */
function custom_link_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['link_prefix'] = array(
    '#type' => 'textarea',
    '#title' => t('Link prefix'),
    '#default_value' => isset($conf['link_prefix']) ? $conf['link_prefix'] : '',
  );
  $form['link_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Link title'),
    '#default_value' => isset($conf['link_title']) ? $conf['link_title'] : '',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['link_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link href'),
    '#default_value' => isset($conf['link_link']) ? $conf['link_link'] : '',
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['link_suffix'] = array(
    '#type' => 'textarea',
    '#title' => t('Link suffix'),
    '#default_value' => isset($conf['link_suffix']) ? $conf['link_suffix'] : '',
  );

  return $form;
}

/**
 * Setting form submit.
 */
function custom_link_content_type_edit_form_submit($form, &$form_state) {
  $keys = array('link_prefix', 'link_suffix', 'link_title', 'link_link');

  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}
