<?php

/**
 * @file
 * Author picture CTools plugin file.
 */

$plugin = array(
  'title'           => t('Home panel'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'home_panel_content_type_render',
);

/**
 * Render callback.
 */
function home_panel_content_type_render($subtype, $conf, $panel_args, $context) {
  global $user;
  $store_default_lang = 'en';

  $block = new stdClass();
  $block->title = NULL;
  $block->content = array();

  $title = t('Hello');
  $image = url(path_to_theme() . '/html/images/img-02.png');

  $current_user = user_load($user->uid);
  $current_user_wrapper = entity_metadata_wrapper('user', $current_user);

  if ($current_user_wrapper->field_first_name->value() && $current_user_wrapper->field_last_name->value()) {
    $username = $current_user_wrapper->field_first_name->value() . ' ' . $current_user_wrapper->field_last_name->value();
  }
  else {
    $username = $current_user->name;
  }

  $stores = array();
  $user_stores = get_user_stores();
  $home_path = variable_get('site_frontpage');

  $i = 1;
  $row = 1;

  foreach ($user_stores as $store) {
    $store_wrapper = entity_metadata_wrapper('taxonomy_term', $store);

    if ($store_wrapper->field_taxonomy_code->value()) {
      $stores[$row][$i]['name'] = $store_wrapper->field_taxonomy_code->value();
    }
    else {
      $stores[$row][$i]['name'] = $store->name_original;
    }

    $stores[$row][$i]['status_new_count'] = isset($store->status_new_count) ? $store->status_new_count : 0;

    $stores[$row][$i]['path'] = '';

    if ($stores[$row][$i]['status_new_count'] != 0) {
      $stores[$row][$i]['path'] = url($home_path . '/'
        . drupal_get_path_alias('taxonomy/term/' . $store->tid, $store_default_lang) . '-' . $store->tid . '/news_status/' . $store->tid . ':0');
    }

    if ($i == 3) {
      $i = 1;
      $row++;
    }
    else {
      $i++;
    }
  }

  $block->content = theme('iris_home_pane', array(
    'title'    => $title,
    'image'    => $image,
    'username' => $username,
    'stores'   => $stores,
  ));

  return $block;
}
