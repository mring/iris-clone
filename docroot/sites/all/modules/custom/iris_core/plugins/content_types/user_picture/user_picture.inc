<?php

/**
 * @file
 * Author picture CTools plugin file.
 */

$plugin = array(
  'title'           => t('User picture'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'user_picture_content_type_render',
  'required context' => new ctools_context_required(t('User'), 'user'),
);

/**
 * Render callback.
 */
function user_picture_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';

  if (isset($context->data->uid) && !empty($context->data->uid)) {
    $user_wrapper = entity_metadata_wrapper('user', $context->data);
    $prefix = '<div class="user"><div class="photo">';
    $suffix = '</div></div>';
    $img = '';

    if ($user_picture_val = $user_wrapper->field_user_picture->value()) {
      if (isset($user_picture_val['uri']) && !empty($user_picture_val['uri'])) {
        $style = 'user_picture';
        $img_url = image_style_url($style, $user_picture_val['uri']);
        $img = '<img src="' . $img_url . '" alt="">';
      }
    }
    else {
      $img = '<a><span class="ico icon-user3"></span></a>';

    }

    $block->content = $prefix . $img . $suffix;
  }

  return $block;
}
