<?php

/**
 * @file
 * Global footer CTools plugin file.
 */

$plugin = array(
  'title' => t('Assign news to user'),
  'single' => TRUE,
  'category' => t('IRIS'),
  'render callback' => 'news_user_assign_content_type_render',
  'edit form' => 'news_user_assign_content_type_config',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Edit form for config.
 */
function news_user_assign_content_type_config($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['read_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Read only'),
    '#default_value' => !empty($conf['read_only']) ? $conf['read_only'] : FALSE,
  );
  return $form;
}

/**
 * Submit function for config form.
 */
function news_user_assign_content_type_config_submit($form, &$form_state) {
  $form_state['conf']['read_only'] = $form_state['values']['read_only'] ? $form_state['values']['read_only'] : NULL;
}

/**
 * Render callback.
 */
function news_user_assign_content_type_render($subtype, $conf, $panel_args, $context) {
  $news_nid = !empty($context->data->nid) ? $context->data->nid : FALSE;
  if (!$news_nid) {
    return '';
  }

  drupal_add_library('system', 'drupal.ajax');

  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';
  if (!empty($conf['read_only'])) {
    $block->content['read_only'] = TRUE;
  }
  $store = irish_search_get_current_store($context->data);
  $query = new RelationQuery('node', $news_nid, 0);
  $query->entityCondition('bundle', 'news_article_status');
  $query->fieldCondition('endpoints', 'entity_type', 'taxonomy_term', '=', 1);
  $query->fieldCondition('endpoints', 'entity_id', $store, '=', 1);
  $query->fieldCondition('field_news_store_deleted', 'value', 0, '=');
  $query->range(0, 1);
  $relations = $query->execute();
  if ($relations) {
    $rid = key($relations);
    drupal_add_library('system', 'ui.autocomplete');
    drupal_add_js(array('news_user_assign' => array('rid' => $rid, 'nid' => $store)), 'setting');
    drupal_add_js(path_to_theme() . '/html/js/behaviors/news-user-assign.js');

    $block->content['rid'] = $rid;
    $relation = relation_load(key($relations), NULL, TRUE);
    $relation_wrapper = entity_metadata_wrapper('relation', $relation);

    $block->content['assigned_users'] = array();
    foreach ($relation_wrapper->field_news_store_assigned_users as $assigned_user) {
      $block->content['assigned_users'][$assigned_user->getIdentifier()] = $assigned_user->field_first_name->value() . ' ' . $assigned_user->field_last_name->value();
    }

  }
  else {
    $block->content['building_message'] = t('User assigning widget not available yet. Assigning News to stores in progress. Could take up to 5 minutes.');
  }

  return $block;
}
