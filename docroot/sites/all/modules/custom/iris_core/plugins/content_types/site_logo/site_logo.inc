<?php

/**
 * @file
 * Global footer CTools plugin file.
 */

$plugin = array(
  'title'           => t('Site logo'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'site_logo_content_type_render',
  'edit form'       => 'site_logos_content_type_edit_form',
);

/**
 * Render callback.
 */
function site_logo_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';

  if (isset($conf['site_logo']) && !empty($conf['site_logo'])) {
    $logo_path = url($conf['site_logo'], array('absolute' => TRUE));
    $logo_img = '<img src="' . $logo_path . '" alt="">';

    $block->content = '<div class="logo">';
    $block->content .= l($logo_img, '<front>', array('html' => TRUE));
    $block->content .= '</div>';
  }

  return $block;
}

/**
 * Settings form.
 */
function site_logos_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['site_logo'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo'),
    '#default_value' => isset($conf['site_logo']) ? $conf['site_logo'] : '',
    '#description' => t('Path to site logo'),
  );

  return $form;
}

/**
 * Settings form submit function.
 */
function site_logos_content_type_edit_form_submit($form, &$form_state) {
  foreach (array('site_logo') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}
