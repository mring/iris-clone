<?php

/**
 * @file
 * Author picture CTools plugin file.
 */

$plugin = array(
  'title'           => t('User editor link'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'user_editor_link_content_type_render',
  'required context' => new ctools_context_required(t('User'), 'user'),
);

/**
 * Render callback.
 */
function user_editor_link_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';

  if (isset($context->data->uid) && !empty($context->data->uid) && user_access('access news dashboard', $context->data)) {
    $link = l('<span class="ico icon-edit"></span>', '/admin/news/news-dashboard', array('html' => TRUE));
    $block->content = '<div class="edit">' . $link . '</div>';
  }

  return $block;
}
