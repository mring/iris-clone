<?php

/**
 * @file
 * Author picture CTools plugin file.
 */

$plugin = array(
  'title'           => t('Operation start and end dates'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'operation_start_end_dates_content_type_render',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Render callback.
 */
function operation_start_end_dates_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';
  $end_date_year = '';

  $wrapper = entity_metadata_wrapper('node', $context->data);
  $promotion_lines = $wrapper->field_promotion_lines->value();
  if (empty($promotion_lines[0]) || !$wrapper->field_promotion_lines[0]->field_operation->value()) {
    return '';
  }
  $start_date = $wrapper->field_promotion_lines[0]->field_operation->field_operation_start_date->value();
  $end_date = $wrapper->field_promotion_lines[0]->field_operation->field_operation_end_date->value();

  $start_date_out = strtoupper(format_date($start_date, 'news_front'));
  $start_date_year = format_date($start_date, 'custom', 'Y');

  if ($end_date) {
    $end_date_out = strtoupper(format_date($end_date, 'news_front'));
    $end_date_year = format_date($end_date, 'custom', 'Y');
  }

  $output = '';
  $output .= '<span class="date">' . $start_date_out;

  if ($start_date_year != $end_date_year) {
    $output .= ' ' . $end_date_year;
  }

  if ($end_date) {
    $output .= '<span class="icon-arrow-next"></span>' . $end_date_out . ' ' . $end_date_year;
  }

  $output .= '</span>';

  $block->content = $output;

  return $block;
}
