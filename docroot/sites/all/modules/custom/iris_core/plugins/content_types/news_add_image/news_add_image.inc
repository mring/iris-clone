<?php

/**
 * @file
 * Global footer CTools plugin file.
 */

$plugin = array(
  'title' => t('Attach a picture to news'),
  'single' => TRUE,
  'category' => t('IRIS'),
  'render callback' => 'news_add_image_content_type_render',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Render callback.
 */
function news_add_image_content_type_render($subtype, $conf, $panel_args, $context) {
  $news_nid = !empty($context->data->nid) ? $context->data->nid : FALSE;

  if (!$news_nid) {
    return '';
  }

  global $user;

  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';
  $block->content['read_only'] = FALSE;

  if (array_intersect(
    array(
      'generic contributor',
      'operation planning responsible',
      'operation contributor',
      'operation upload and validation',
      'merchandising contributor',
      'operation responsible',
    ), $user->roles)) {
    $block->content['read_only'] = TRUE;
  }

  $store = irish_search_get_current_store($context->data);
  $query = new RelationQuery('node', $news_nid, 0);
  $query->entityCondition('bundle', 'news_article_status');
  $query->fieldCondition('endpoints', 'entity_type', 'taxonomy_term', '=', 1);
  $query->fieldCondition('endpoints', 'entity_id', $store, '=', 1);
  $query->fieldCondition('field_news_store_deleted', 'value', 0, '=');
  $query->range(0, 1);
  $relations = $query->execute();
  if ($relations) {
    $rid = key($relations);

    $block->content['rid'] = $rid;
    $relation = relation_load(key($relations), NULL, TRUE);
    $relation_wrapper = entity_metadata_wrapper('relation', $relation);

    $block->content['remove_link_classes'] = array('delete-link', 'use-ajax');
    $image_html = '';

    if ($relation_wrapper->field_news_store_image->value()) {
      $image = $relation_wrapper->field_news_store_image->file->value();

      if (isset($image->uid) && ($image->uid != $user->uid)) {
        $block->content['remove_link_classes'][] = 'hidden';
      }

      $image_html = theme_image_style(
        array(
          'style_name' => 'news_status_image',
          'path' => $image->uri,
          'width' => NULL,
          'height' => NULL,
        )
      );
    }
    else {
      $block->content['remove_link_classes'][] = 'hidden';
    }

    $block->content['news_status_image'] = $image_html;
    $block->content['news_status_value'] = $relation_wrapper->field_news_store_status->value();

  }
  else {
    $block->content['building_message'] = t('Adding photo widget not available yet. Assigning News to stores in progress. Could take up to 5 minutes.');
  }

  return $block;
}
