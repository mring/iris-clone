(function ($) {
  /**
   * Hides "All" filters on tablets.
   */
  Drupal.behaviors.irisCoreHideTabletAllLink = {
    attach: function (context, settings) {
      var $links = $('#sidebar');

      $links.find('a.facetapi-active').each(function() {
        var $this = $(this);
        if ($this.attr('data-value') == 'all') {
          $this.closest('.aside-box').addClass('no-visible-tablet-bar');
        } else {
          $this.closest('.aside-box').removeClass('no-visible-tablet-bar');
        }
      });
    }
  };

  /**
   * Shows "Add another item" when URL is present in Link field.
   */
  Drupal.behaviors.irisCoreAddAnotherItem = {
    attach: function (context, settings) {
      if (typeof settings.ajaxPageState.theme != "undefined" && settings.ajaxPageState.theme != "iris_theme") {
        var $input_element = $('.field-widget-link-field .link-field-url .form-text');
        var $change_element = $('.field-widget-link-field .form-item .field-add-more-submit');

        $input_element.live('input', function() {
          if ($input_element.val() != '') {
            $change_element.css('display', 'block');
          } else {
            $change_element.css('display', 'none');
          }
        });
      }
    }
  };

  Drupal.behaviors.AjaxIrisCoreImagefieldCrop = {
    attach: function (context, settings) {
      if ($('.cropbox', context).length == 0) {
        // no cropbox, probably an image upload (http://drupal.org/node/366296)
        return;
      }

      // get the id attribute for multiple image support
      var self_id = jQuery('.cropbox').attr('id');
      var id = self_id.substring(0, self_id.indexOf('-cropbox'));
      var imagefield = settings.imagefield_crop[id];
      var ratio = imagefield.box.ratio;

      if ($(window).width() <= imagefield.preview.width * 2) {
        settings.imagefield_crop[id].preview.height = imagefield.preview.height / ratio;
        settings.imagefield_crop[id].preview.width = imagefield.preview.width / ratio;

        settings.imagefield_crop[id].box.box_height = imagefield.box.box_height / ratio;
        settings.imagefield_crop[id].box.box_width = imagefield.box.box_width / ratio;

        $('.jcrop-preview-wrapper').css({
          width: imagefield.preview.width + 'px',
          height: imagefield.preview.height + 'px'
        });
      }
    }
  };

})(jQuery);
