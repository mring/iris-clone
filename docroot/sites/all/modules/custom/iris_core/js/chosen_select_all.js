(function ($) {
  Drupal.behaviors.chosenSelect = {
    attach: function (context, settings) {
      if (typeof settings.ajaxPageState.theme != "undefined" && settings.ajaxPageState.theme != "iris_theme") {
        // Add select all option to optgroups in chosen
        $(function() {
          $('.chosen-results .group-result').live('click', function() {
            // Get unselected items in this group
            var unselected = $(this).nextUntil('.group-result').not('.result-selected');

            if(unselected.length) {
              // Select all items in this group
              unselected.trigger('mouseup');
            }
          });
          $('.chosen-results .group-option').live('click', function() {
            $(this).closest('.form-type-select').find('select').trigger("change");
          });
        });
      }
    }
  }
})(jQuery);
