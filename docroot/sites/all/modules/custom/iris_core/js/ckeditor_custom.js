(function ($) {
  Drupal.behaviors.irisCkeditor = {
    attach: function (context, settings) {
      // Alter Ocupload button.
      if (typeof CKEDITOR != "undefined") {
        CKEDITOR.on('instanceReady', function() {
          $("a.cke_button__ocupload").attr("title", Drupal.t('Upload an image'));
          $('.cke_button__ocupload_icon').css('background-position', '0 0').css('background-image', 'url(/sites/all/modules/contrib/ckeditor/images/buttons/image.png)').css('background-repeat','no-repeat');
        });
      }


    }
  }
})(jQuery);
