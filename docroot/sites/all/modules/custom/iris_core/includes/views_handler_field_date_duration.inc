<?php

/**
 * @file
 * Definition of views_handler_field_custom.
 */

/**
 * A handler to provide timestamp as duration.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_date_duration extends views_handler_field {

  /**
   * Add necessary setting for options form.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['date_granularity'] = array('default' => 2);

    return $options;
  }

  /**
   * Provide the form for setting options.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['date_granularity'] = array(
      '#title' => t('Granularity'),
      '#type' => 'textfield',
      '#element_validate' => array('element_validate_integer_positive'),
      '#default_value' => $this->options['date_granularity'] ?: 2,
      '#description' => t('Choose granularity.'),
      '#required' => TRUE,
    );
  }

  /**
   * Options form submit.
   */
  public function options_form_submit($form, &$form_state) {
    $form_state['values']['options']['date_granularity'] = array_filter($form_state['values']['options']['date_granularity']);
  }

  /**
   * Render this display.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if ($value) {
      $granularity = !empty($this->options['granularity']) ? $this->options['granularity'] : 2;
      return format_interval($value, $granularity);
    }
    return '';
  }

}
