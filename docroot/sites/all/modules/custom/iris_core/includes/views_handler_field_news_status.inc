<?php

/**
 * @file
 * Handler for news status percentage field.
 */

/**
 * Field handler to present the path to the node.
 *
 * @ingroup views_handler_field_news_status
 */
class views_handler_field_news_status extends views_handler_field {

  /**
   * Add necessary setting for options form.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['store_status'] = array('default' => 2);
    $options['prefix'] = array('default' => '', 'translatable' => TRUE);
    $options['suffix'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  /**
   * Overrides views_handler_field::construct().
   */
  public function construct() {
    parent::construct();

  }

  /**
   * Provide the form for setting options.
   */
  public function options_form(&$form, &$form_state) {
    $form['store_status'] = array(
      '#type' => 'select',
      '#title' => t('Store status'),
      '#options' => array(
        0 => t('New'),
        1 => t('In progress'),
        2 => t('Done'),
      ),
      '#default_value' => $this->options['store_status'],
    );

    $form['prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Prefix'),
      '#default_value' => $this->options['prefix'],
      '#description' => t('Text to put before the value text.'),
    );
    $form['suffix'] = array(
      '#type' => 'textfield',
      '#title' => t('Suffix'),
      '#default_value' => $this->options['suffix'],
      '#description' => t('Text to put after the value text.'),
    );
    parent::options_form($form, $form_state);
  }

  /**
   * Loads additional fields.
   */
  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Render this display.
   */
  public function render($values) {

    if ($this->handler_type == 'header') {
      $nid = isset($this->view->argument['nid']->value[0]) ? $this->view->argument['nid']->value[0] : FALSE;

      if (!$nid) {
        return '';
      }
    }
    else {
      $nid = !empty($values->nid) ? $values->nid : FALSE;

      if (!$nid) {
        return '';
      }
    }

    $query = new RelationQuery('node', $nid, 0);
    $query->entityCondition('bundle', 'news_article_status');
    $query->fieldCondition('field_news_store_deleted', 'value', 0);
    $query->fieldCondition('field_linked_with_domains', 'value', 1);
    $relations = count($query->execute());

    $query->fieldCondition('field_news_store_status', 'value', $this->options['store_status']);
    $relations_done = count($query->execute());

    $calculated_value = 0;
    $in_percent = ($relations / 100) != 0 ? ($relations / 100) : FALSE;
    if ($in_percent) {
      $calculated_value = round($relations_done / $in_percent);
    }

    return $this->sanitize_value($this->options['prefix'], 'xss') . ' ' . $calculated_value . '% ' . $this->sanitize_value($this->options['suffix'], 'xss') . '<br/>';
  }

}
