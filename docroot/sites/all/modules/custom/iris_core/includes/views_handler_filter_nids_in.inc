<?php

/**
 * Handler for NIDS filter.
 */
class views_handler_filter_nids_in extends views_handler_filter_in_operator {

  /**
   * Options form subform for setting options.
   */
  public function value_form(&$form, &$form_state) {

    $default_value = is_array($this->value) ? implode(',', $this->value) : $this->value;
    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Node Ids'),
      '#description' => t('Enter a comma separated list of node ids.'),
      '#default_value' => $default_value,
    );

    if (!empty($form_state['exposed']) && !isset($form_state['input'][$this->options['expose']['identifier']])) {
      $form_state['input'][$this->options['expose']['identifier']] = $default_value;
    }
  }

  /**
   * Submit the options form.
   */
  public function value_submit($form, &$form_state) {
    // Prevent array filter from removing our anonymous user.
  }

  /**
   * Override to do nothing.
   */
  public function get_value_options() {}

  /**
   * Validate the options form.
   */
  public function value_validate($form, &$form_state) {
    $values = drupal_explode_tags($form_state['values']['options']['value']);

    if ($values) {
      $form_state['values']['options']['value'] = $values;
      $this->value = $values;
    }
  }

  /**
   * Check accept.
   *
   * Check to see if input from the exposed filters should change
   * the behavior of this filter.
   */
  public function accept_exposed_input($input) {
    return TRUE;
  }

  /**
   * Validate function for expose.
   */
  public function exposed_validate(&$form, &$form_state) {
    if (empty($this->options['exposed'])) {
      return;
    }

    if (empty($this->options['expose']['identifier'])) {
      return;
    }

    $identifier = $this->options['expose']['identifier'];
    $values = $form_state['input'][$identifier];

    if ($values) {
      $this->validated_exposed_input = $values;
      $this->value = $values;
    }
  }

  /**
   * Op_simple function.
   */
  public function op_simple() {
    if (!is_array($this->value)) {
      return;
    }
    $this->ensure_my_table();

    // We use array_values() because the checkboxes keep keys and that can cause
    // array addition problems.
    $this->query->add_where($this->options['group'], "$this->table_alias.$this->real_field", array_values($this->value), $this->operator);
  }

  /**
   * Op_empty function.
   */
  public function op_empty() {
    $this->ensure_my_table();
    if ($this->operator == 'empty') {
      $operator = "IS NULL";
    }
    else {
      $operator = "IS NOT NULL";
    }

    $this->query->add_where($this->options['group'], "$this->table_alias.$this->real_field", NULL, $operator);
  }

}
