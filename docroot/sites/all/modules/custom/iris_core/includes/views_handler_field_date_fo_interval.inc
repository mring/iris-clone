<?php

/**
 * @file
 * Definition of views_handler_field_custom.
 */

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_date_fo_interval extends views_handler_field {

  /**
   * Do nothing in query.
   */
  public function query() {
    // Do nothing.
  }

  /**
   * Add necessary setting for options form.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['date_field'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Provide the form for setting options.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      $date_fields[$field] = $handler->ui_name();
    }

    $form['date_field'] = array(
      '#title' => t('Date field'),
      '#type' => 'select',
      '#options' => $date_fields,
      '#default_value' => $this->options['date_field'],
      '#multiple' => FALSE,
      '#description' => t('Select date field.'),
      '#required' => TRUE,
    );
  }

  /**
   * Submit functuin for options form.
   */
  public function options_form_submit($form, &$form_state) {
    $form_state['values']['options']['date_field'] = array_filter($form_state['values']['options']['date_field']);
  }

  /**
   * Render this display.
   */
  public function render($values) {
    if (!empty($values->_entity_properties[$this->options['date_field']])) {
      return iris_core_fo_date_format($values->_entity_properties[$this->options['date_field']]);
    }
    return '';
  }

}
