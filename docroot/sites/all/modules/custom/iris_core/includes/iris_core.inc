<?php

/**
 * @file
 * Contains helping functions.
 */

/**
 * Get promotion lines that referencing operation.
 *
 * @param int $nid
 *   Operation nid.
 *
 * @return array
 *   Result of db_select.
 */
function iris_core_get_operation_parent($nid) {
  $query = db_select('node', 'n');
  $query->join('field_data_field_operation', 'fdfo', 'fdfo.entity_id = n.nid');
  $query->condition('n.type', 'promotion_line');
  $query->condition('fdfo.field_operation_target_id', $nid);
  $query->fields('n', array('nid', 'title'));
  $result = $query->execute()->fetchAllKeyed(0, 1);

  return $result;
}

/**
 * Access callback for user/%user/edit page.
 *
 * @param object $user_edited
 *   User being edited.
 *
 * @return bool
 *   Boolean value whenether access is allowed.
 */
function iris_core_user_edit_access($user_edited) {
  global $user;

  $administrators_condition = array_intersect(array('administrator', 'webmaster'), $user->roles);
  $uid_condition = $user->uid == $user_edited->uid;

  if ($administrators_condition || $uid_condition) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Action handler to archive news or promotions.
 */
function iris_core_archive_node_action(&$node, $context) {
  if (isset($node->node_archived) && ($node->type == 'promotion' || $node->type == 'news')) {
    $node->node_archived = 1;
  }
}

/**
 * Action handler to un-archive news or promotions.
 */
function iris_core_unarchive_node_action(&$node, $context) {
  if (isset($node->node_archived) && ($node->type == 'promotion' || $node->type == 'news')) {
    $node->node_archived = 0;
  }
}

/**
 * Hiding some form fields.
 */
function iris_core_user_form_after_build($form, &$form_state) {
  if (isset($form['account']['mail']['htmlmail_plaintext'])) {
    $form['account']['mail']['htmlmail_plaintext']['#access'] = FALSE;
  }

  if (isset($form['l10n_client'])) {
    $form['l10n_client']['#access'] = FALSE;
  }

  return $form;
}

/**
 * Converts timestamp to FO datetime format.
 *
 * @param int $value
 *   Timestamp.
 *
 * @return string
 *   Interval or Date.
 */
function iris_core_fo_date_format($value) {
  if (is_numeric($value)) {
    $interval = REQUEST_TIME - $value;
    $min = 60;
    $hr = $min * 60;
    $day = $hr * 24;
    $sixteen_days = $day * 16;

    if ($interval >= $sixteen_days) {
      return format_date($value, 'pages_front');
    }
    elseif ($interval < $min) {
      return t('@datemin', array('@date' => 1));
    }
    elseif ($interval < $hr) {
      return t('@datemin', array('@date' => floor($interval / $min + 1)));
    }
    elseif ($interval < $day) {
      return t('@dateh', array('@date' => floor($interval / $hr + 1)));
    }
    else {
      return t('@dated', array('@date' => floor($interval / $day + 1)));
    }
  }

  return '';
}
