<?php

/**
 * @file
 * Definition of views_handler_field_custom.
 */

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_date_diff extends views_handler_field {

  /**
   * Do nothing in query.
   */
  public function query() {
    // Do nothing.
  }

  /**
   * Add necessary setting for options form.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['date_fields_end'] = array('default' => FALSE);
    $options['date_fields_start'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Provide the form for setting options.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      if (empty($handler->definition['is date'])) {
        continue;
      }
      $date_fields[$field] = $handler->ui_name();
    }

    $form['date_fields_end'] = array(
      '#title' => t('Date field End'),
      '#type' => 'select',
      '#options' => $date_fields,
      '#default_value' => $this->options['date_fields_end'],
      '#multiple' => FALSE,
      '#description' => t('Select end date field.'),
      '#required' => TRUE,
    );

    $form['date_fields_start'] = array(
      '#title' => t('Date field Start'),
      '#type' => 'select',
      '#options' => $date_fields,
      '#default_value' => $this->options['date_fields_start'],
      '#multiple' => FALSE,
      '#description' => t('Select start date field.'),
      '#required' => TRUE,
    );
  }

  /**
   * Submit funtion for options form.
   */
  public function options_form_submit($form, &$form_state) {
    $form_state['values']['options']['date_fields_end'] = array_filter($form_state['values']['options']['date_fields_end']);
    $form_state['values']['options']['date_fields_start'] = array_filter($form_state['values']['options']['date_fields_start']);
  }

  /**
   * Render this display.
   */
  public function render($values) {
    if (empty($values->{$this->options['date_fields_start']}) && empty($values->{$this->options['date_fields_end']})
      && !empty($values->{'field_' . $this->options['date_fields_end']}) && !empty($values->{'field_' . $this->options['date_fields_start']})) {
      $start_date = new DateObject($values->{'field_' . $this->options['date_fields_start']}[0]['raw']['value']);
      $end_date = new DateObject(str_replace('00:00:00', '23:59:59', $values->{'field_' . $this->options['date_fields_end']}[0]['raw']['value']));
      $duration = round($start_date->difference($end_date, 'hours')) / 24;
      return $duration;
    }
    return '';
  }

}
