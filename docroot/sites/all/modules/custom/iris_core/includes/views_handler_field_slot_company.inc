<?php

/**
 * @file
 * Definition of views_handler_field_slot_company.
 */

/**
 * A handler to provide company code description field.
 *
 * A handler to provide company code description field accoding
 * to slot in promotion export.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_slot_company extends views_handler_field {

  /**
   * Add necessary setting for options form.
   */
  public function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * Provide the form for setting options.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['attention'] = array(
      '#markup' => t('Companies description field based on field "field_promo_slot (node_field_data_field_promo_slot_tid)'),
      '#weight' => -105,
    );
  }

  /**
   * Do nothing for query.
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * Render this display.
   */
  public function render($data) {

    $slot_company = array();
    $type = 'taxonomy_term';

    if ($tid = $data->taxonomy_term_data_field_data_field_promo_slot_tid) {
      $companies = db_select('iris_org_hierarchy', 'h')
        ->fields('h', array('taxonomy_term_target'))
        ->condition('h.taxonomy_term_source', $tid)
        ->condition('h.vocabulary_name_source', 'slot')
        ->condition('h.vocabulary_name_target', 'company_code')
        ->execute()
        ->fetchCol();

      $terms = entity_load($type, array_unique($companies));
      foreach ($terms as $term) {
        $wrapper = entity_metadata_wrapper($type, $term);
        if (isset($wrapper->field_taxonomy_code) && ($company_name = $wrapper->field_taxonomy_code->value())) {
          $slot_company[] = $company_name;
        }
      }
    }

    return $slot_company ? implode(', ', $slot_company) : '';
  }

}
