<?php

/**
 * @file
 * Definition of views_handler_field_location_groups.
 */

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_location_groups extends views_handler_field {

  /**
   * Add some required fields needed on render().
   */
  public function construct() {
    parent::construct();
    $this->additional_fields['nid'] = array(
      'table' => 'node',
      'field' => 'nid',
    );
  }

  /**
   * Loads additional fields.
   */
  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Add necessary setting for options form.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['separator'] = array('default' => '|');
    return $options;
  }

  /**
   * Provide the form for setting options.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['separator'] = array(
      '#type' => 'textfield',
      '#title' => t('Separator'),
      '#default_value' => $this->options['separator'],
    );
  }

  /**
   * Submit options form.
   */
  public function options_form_submit($form, &$form_state) {
    $form_state['values']['options']['separator'] = array_filter($form_state['values']['options']['separator']);
  }

  /**
   * Render this display.
   */
  public function render($values) {
    $type = 'taxonomy_term';
    $groups_arr = array();
    $separator = isset($this->options['separator']) ? $this->options['separator'] : '|';
    $nid = $this->get_value($values, 'nid');

    if ($nid) {
      $node = entity_load_single('node', $nid);
      $groups = og_get_entity_groups('node', $node, array(OG_STATE_ACTIVE), 'field_location_group');

      if (isset($groups[$type])) {
        foreach ($groups[$type] as $group_id) {
          $term = entity_load_single($type, $group_id);
          $wrapper = entity_metadata_wrapper($type, $term);

          if (isset($wrapper->field_taxonomy_code) && ($code = $wrapper->field_taxonomy_code->value())) {
            $groups_arr[] = $code;
          }
          else {
            $groups_arr[] = $term->name;
          }
        }
      }
    }

    return implode($separator, $groups_arr);
  }

}
