<?php

/**
 * @file
 * Definition of views_handler_field_slot_so_currency.
 */

/**
 * A handler to provide SO currency accoding to slot in promotion export.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_canal_export_name extends views_handler_field {

  /**
   * Do not any any addition settings.
   */
  public function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * Provide the default form for setting options.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['attention'] = array(
      '#markup' => t('Used only for canal export'),
      '#weight' => -105,
    );
  }

  /**
   * Do nothing in query.
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * Render this display.
   */
  public function render($data) {
    $canal_name = '';
    $exposed_filter = $this->view->get_exposed_input();

    $canal = isset($exposed_filter['canal']) ? $exposed_filter['canal'] : '';
    if (isset($exposed_filter['export_id'])) {
      $iris_export = iris_core_iris_export_get($exposed_filter['export_id']);
      if ($iris_export) {
        $export_query = unserialize($iris_export->query);
        $canal = isset($export_query['canal']) ? $export_query['canal'] : '';
      }
    }

    if ($canal && is_numeric($canal)) {
      $canal_term = taxonomy_term_load($canal);
      if ($canal_term) {
        $canal_name = $canal_term->name;
      }
    }

    return $canal_name;
  }

}
