<?php

/**
 * @file
 * Definition of views_handler_field_slot_so_currency.
 */

/**
 * A handler to provide sales organisation currency.
 *
 * A handler to provide sales organisation currency
 * accoding to slot in promotion export.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_slot_so_currency extends views_handler_field {

  /**
   * Add necessary setting for options form.
   */
  public function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * Provide the form for setting options.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['attention'] = array(
      '#markup' => t('SO currency based on field "field_promo_slot (node_field_data_field_promo_slot_tid)"'),
      '#weight' => -105,
    );
  }

  /**
   * Do nothing for query.
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * Render this display.
   */
  public function render($data) {

    $slot_currency = array();
    $type = 'taxonomy_term';

    if ($tid = $data->taxonomy_term_data_field_data_field_promo_slot_tid) {

      $sales_org = db_select('iris_org_hierarchy', 'h')
        ->fields('h', array('taxonomy_term_target'))
        ->condition('h.taxonomy_term_source', $tid)
        ->condition('h.vocabulary_name_source', 'slot')
        ->condition('h.vocabulary_name_target', 'sales_organization')
        ->execute()
        ->fetchCol();

      $terms = entity_load($type, array_unique($sales_org));
      foreach ($terms as $term) {
        $wrapper = entity_metadata_wrapper($type, $term);
        if (isset($wrapper->field_currency) && ($currency = $wrapper->field_currency->value())) {
          $options = list_allowed_values(field_info_field('field_currency'));
          if (!in_array($options[$currency], $slot_currency)) {
            $slot_currency[] = $options[$currency];
          }
        }
      }
    }
    return $slot_currency ? implode(', ', $slot_currency) : '';
  }

}
