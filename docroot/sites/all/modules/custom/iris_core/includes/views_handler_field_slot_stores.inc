<?php

/**
 * @file
 * Definition of views_handler_field_slot_stores.
 */

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_slot_stores extends views_handler_field {

  /**
   * Add necessary setting for options form.
   */
  public function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * Provide the form for setting options.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['attention'] = array(
      '#markup' => t('Based on field "Promo Line Nid for Slot stores (node_field_data_field_promotion_lines_nid)"!!!'),
      '#weight' => -105,
    );
  }

  /**
   * Do nothing for query.
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * Render this display.
   */
  public function render($data) {
    $slot_stores = array();

    $type = 'taxonomy_term';

    if ($nid = $data->node_field_data_field_promotion_lines_nid) {
      $node = entity_load_single('node', $nid);
      $groups = og_get_entity_groups('node', $node, array(OG_STATE_ACTIVE), 'field_slot_stores');

      if (isset($groups[$type])) {
        foreach ($groups[$type] as $group_id) {
          $term = entity_load_single($type, $group_id);
          $wrapper = entity_metadata_wrapper($type, $term);

          if (isset($wrapper->field_taxonomy_code) && ($code = $wrapper->field_taxonomy_code->value())) {
            $slot_stores[] = $code;
          }
          else {
            $slot_stores[] = $term->name;
          }
        }
      }
    }

    return $slot_stores ? implode(', ', $slot_stores) : '';
  }

}
