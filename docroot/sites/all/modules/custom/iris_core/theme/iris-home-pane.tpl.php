<?php

/**
 * @file
 * Template for home pane.
 */
?>
<div id="content-item" class="news-content-frame">
  <div class="home-holder">
    <div class="head">
      <img src="<?php print $image; ?>" alt="">
      <div class="txt">
        <h2><?php print $title; ?>,</h2>
        <div class="frame">
          <span class="name"><?php print $username; ?></span>
        </div>
      </div>
    </div>
    <?php foreach ($stores as $rows): ?>
      <div class="store-row">
        <ul class="store">
          <?php foreach ($rows as $row): ?>
            <li>
              <div class="head-store">
                <span><?php print t('Store'); ?></span>
                <strong><?php print $row['name']; ?></strong>
              </div>
              <a<?php print $row['path'] ? ' href="' . $row['path'] . '"' : ''; ?>>
                <div class="frame">
                  <div class="ico"><span class="ico icon-info"></span></div>
                  <div class="text">
                    <strong><?php print $row['status_new_count']; ?></strong>
                    <span><?php print t('Unread news'); ?></span>
                  </div>
                </div>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    <?php endforeach; ?>
  </div>
</div>
