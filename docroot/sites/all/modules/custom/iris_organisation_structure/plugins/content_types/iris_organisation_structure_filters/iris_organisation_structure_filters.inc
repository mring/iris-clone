<?php

/**
 * @file
 *
 * Organisation structure filters CTools plugin.
 */

$plugin = array(
  'title'           => t('Organisation structure filters'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'iris_organisation_structure_filters_type_render',
);

/**
 * Render callback.
 */
function iris_organisation_structure_filters_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->module = 'iris_organisation_structure';
  $block->delta  = 'iris_organisation_structure_filters';
  $block->title = '';
  $block->content = '';

  $form_id = 'iris_organisation_structure_filters_form';

  $form_state = array(
    'ctools edit form plugin' => TRUE, // Mark this form as from plugin.
    'want form' => TRUE,
  );

  $form = drupal_build_form($form_id, $form_state);
  $block->content = $form;

  return $block;
}

/**
 * Organisation structure form.
 */
function iris_organisation_structure_filters_form() {
  $form = array();
  $params = drupal_get_query_parameters();

  $form['filters'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'exposed-filters'
      )
    )
  );

  // Location group filter.
  $field_instance = field_info_instance('node', 'field_location_group', 'news');
  $field = field_info_field_by_id($field_instance['field_id']);
  $lg_options = entityreference_options_list($field, $field_instance);

  $form['filters']['lg'] = array(
    '#title'         => t('Location group'),
    '#type'          => 'select',
    '#options'       => $lg_options,
    '#default_value' => isset($params['lg']) ? $params['lg'] : '',
    '#chosen'        => TRUE,
  );

  // Vocabulary filter.
  $loc_group_structure_keys = array(
    'country',
    'company_code',
    'sales_organization',
    'store_platform',
    'terminal',
    'store',
    'slot'
  );

  $loc_group_structure = array();
  foreach ($loc_group_structure_keys as $value) {
    $loc_group_structure[$value] = taxonomy_vocabulary_machine_name_load($value)->name;
  }

  $form['filters']['vocabulary'] = array(
    '#title'         => t('Vocabulary'),
    '#type'          => 'select',
    '#options'       => $loc_group_structure,
    '#default_value' => isset($params['vocabulary']) ? $params['vocabulary'] : '',
  );

  // Activity domain filter.
  $vocabulary = taxonomy_vocabulary_machine_name_load('activity_domain');
  $activity_domain_tree = taxonomy_get_tree($vocabulary->vid);

  $activity_domain_terms['_none'] = t('- None -');
  foreach ($activity_domain_tree as $activity_domain) {
    $activity_domain_terms[$activity_domain->tid] = $activity_domain->name;
  }

  $form['filters']['activity_domain'] = array(
    '#title'         => t('Activity domain'),
    '#type'          => 'select',
    '#options'       => $activity_domain_terms,
    '#default_value' => isset($params['activity_domain']) ? $params['activity_domain'] : '',
    '#chosen'        => TRUE,
  );

  $form['filters']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Apply'),
    '#attributes' => array(
      'id' => 'edit-submit-news-filters',
    ),
  );

  $form['filters']['reset'] = array(
    '#type'  => 'submit',
    '#value' => t('Reset'),
  );

  return $form;
}

/**
 * Submit handler for iris_organisation_structure_filters_form().
 */
function iris_organisation_structure_filters_form_submit(&$form, $form_state) {
  $query = array();

  if ($form_state['values']['op'] == t('Reset')) {
    drupal_goto(current_path());
  }
  else {
    if ($form_state['values']['lg']) {
      $query['lg'] = $form_state['values']['lg'];
    }
    if ($form_state['values']['vocabulary']) {
      $query['vocabulary'] = $form_state['values']['vocabulary'];
    }
    if ($form_state['values']['activity_domain']) {
      $query['activity_domain'] = $form_state['values']['activity_domain'];
    }

    drupal_goto($_GET['q'], array('query' => array($query)));
  }
}
