<?php

/**
 * @file
 *
 * Organisation structure panel CTools plugin.
 */

$plugin = array(
  'title'           => t('Organisation structure panel'),
  'single'          => TRUE,
  'category'        => t('IRIS'),
  'render callback' => 'iris_organisation_structure_panel_type_render',
);

/**
 * Render callback.
 */
function iris_organisation_structure_panel_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->module = 'iris_organisation_structure';
  $block->delta  = 'iris_organisation_structure_panel';
  $block->title = '';
  $block->content = '';

  $block->content .= theme('table', array(
    'header' => array(
      t('Taxonomy'),
      t('Name'),
      t('Actions'),
    ),
    'rows' => iris_organisation_structure_build_tree(),
    'empty' => t('No results found.')
  ));

  return $block;
}

/**
 * Helper function to build organisation structure.
 */
function iris_organisation_structure_build_tree() {
  $params = drupal_get_query_parameters();
  $location_group = isset($params['lg']) ? taxonomy_term_load($params['lg']) : FALSE;
  $vocabulary = isset($params['vocabulary']) ? taxonomy_vocabulary_machine_name_load($params['vocabulary']) : FALSE;
  $activity_domain = isset($params['activity_domain']) && $params['activity_domain'] != '_none' ? $params['activity_domain'] : FALSE;

  if ($location_group && $vocabulary) {
    $query = db_select('field_data_field_activity_domains', 'ad');
    $query->addField('ad', 'entity_id', 'store');
    $query->condition('ad.entity_type', 'taxonomy_term');
    $query->condition('ad.bundle', 'store');
    $query->condition('ad.field_activity_domains_tid', $activity_domain);
    $query->distinct();
    $results = $query->execute()->fetchCol('store');

    $loc_group = array();
    $stores = taxonomy_term_load_multiple($results);
    if ($stores) {
      foreach ($stores as $store) {
        $related_taxonomies = og_subgroups_all_load('taxonomy_term', $store->tid, FALSE);
        $related_terms = taxonomy_term_load_multiple($related_taxonomies['taxonomy_term']);
        foreach ($related_terms as $related_term) {
          if ($related_term->vocabulary_machine_name == $location_group->vocabulary_machine_name &&
            $related_term->tid == $location_group->tid) {
            foreach ($related_terms as $related) {
              if ($related && $related->vocabulary_machine_name == $vocabulary->machine_name) {
                $loc_group[$related->tid] = array(
                  taxonomy_vocabulary_machine_name_load($related->vocabulary_machine_name)->name,
                  $related->name,
                  l('edit', "taxonomy/term/{$related->tid}/edit"),
                );
              }
            }
            if ($store && $store->vocabulary_machine_name == $vocabulary->machine_name) {
              $loc_group[$store->tid] = array(
                taxonomy_vocabulary_machine_name_load($store->vocabulary_machine_name)->name,
                $store->name,
                l('edit', "taxonomy/term/{$store->tid}/edit"),
              );
            }
            break;
          }
        }
      }
    }
    else {
      $related_taxonomies = og_subgroups_all_load('taxonomy_term', $location_group->tid, FALSE);
      $related_terms = taxonomy_term_load_multiple($related_taxonomies['taxonomy_term']);
      foreach ($related_terms as $related_term) {
        if ($related_term && $related_term->vocabulary_machine_name == $vocabulary->machine_name) {
          $loc_group[$related_term->tid] = array(
            taxonomy_vocabulary_machine_name_load($related_term->vocabulary_machine_name)->name,
            $related_term->name,
            l('edit', "taxonomy/term/{$related_term->tid}/edit"),
          );
        }
      }
    }

    return $loc_group;
  }
}
