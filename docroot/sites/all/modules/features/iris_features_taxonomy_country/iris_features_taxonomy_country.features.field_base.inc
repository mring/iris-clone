<?php
/**
 * @file
 * iris_features_taxonomy_country.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function iris_features_taxonomy_country_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_currency'.
  $field_bases['field_currency'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_currency',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'eur' => 'EUR',
        'xpf' => 'XPF',
        'pln' => 'PLN',
        'czk' => 'CZK',
        'hrk' => 'HRK',
        'gbp' => 'GBP',
        'aud' => 'AUD',
        'chf' => 'CHF',
        'cny' => 'CNY',
        'hkd' => 'HKD',
        'myr' => 'MYR',
        'nzd' => 'NZD',
        'sgd' => 'SGD',
        'sar' => 'SAR',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_rounding_rule'.
  $field_bases['field_rounding_rule'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_rounding_rule',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        1 => 'Rounded down to the unit',
        2 => 'Rounded down 10 unit',
        3 => 'Rounded down 10 unit - 1',
        4 => 'Rounded down 10 cents',
        5 => 'Rounded down 10 cents - 1',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'group_group'.
  $field_bases['group_group'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'group_group',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'Not a group',
        1 => 'Group',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  return $field_bases;
}
