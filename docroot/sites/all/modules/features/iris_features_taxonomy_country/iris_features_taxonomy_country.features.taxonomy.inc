<?php
/**
 * @file
 * iris_features_taxonomy_country.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_country_taxonomy_default_vocabularies() {
  return array(
    'country' => array(
      'name' => 'Country',
      'machine_name' => 'country',
      'description' => 'The country is part of the location group (1st level) and used to define the countries where Lagardere Travel Retail are located',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -6,
    ),
  );
}
