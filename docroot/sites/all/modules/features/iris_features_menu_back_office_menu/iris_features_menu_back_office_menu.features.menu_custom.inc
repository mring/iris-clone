<?php
/**
 * @file
 * iris_features_menu_back_office_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function iris_features_menu_back_office_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-back-office-menu.
  $menus['menu-back-office-menu'] = array(
    'menu_name' => 'menu-back-office-menu',
    'title' => 'Back-office menu',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Back-office menu');

  return $menus;
}
