<?php
/**
 * @file
 * iris_features_menu_back_office_menu.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function iris_features_menu_back_office_menu_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'admin_menu_source_settings';
  $strongarm->value = array(
    1 => array(
      'source' => '',
    ),
    2 => array(
      'source' => '',
    ),
    3 => array(
      'source' => '',
    ),
    4 => array(
      'source' => 'menu-back-office-menu',
    ),
    6 => array(
      'source' => 'menu-back-office-menu',
    ),
    12 => array(
      'source' => 'menu-back-office-menu',
    ),
    7 => array(
      'source' => 'menu-back-office-menu',
    ),
    9 => array(
      'source' => 'menu-back-office-menu',
    ),
    10 => array(
      'source' => 'menu-back-office-menu',
    ),
    11 => array(
      'source' => 'menu-back-office-menu',
    ),
    8 => array(
      'source' => 'menu-back-office-menu',
    ),
    13 => array(
      'source' => 'menu-back-office-menu',
    ),
  );
  $export['admin_menu_source_settings'] = $strongarm;

  return $export;
}
