<?php
/**
 * @file
 * iris_features_themekey_rules.features.inc
 */

/**
 * Implements hook_themekey_features_rule_chain().
 */
function iris_features_themekey_rules_themekey_features_rule_chain() {
if (!defined('THEMEKEY_PAGECACHE_UNSUPPORTED')) {
    define('THEMEKEY_PAGECACHE_UNSUPPORTED', 0);
    define('THEMEKEY_PAGECACHE_SUPPORTED', 1);
    define('THEMEKEY_PAGECACHE_TIMEBASED', 2);
  }
$rules = array(
  0 => array(
    'rule' => array(
      'property' => 'node:type',
      'operator' => '=',
      'value' => 'product',
      'theme' => 'ThemeKeyAdminTheme',
      'enabled' => 1,
      'wildcards' => 'a:0:{}',
      'module' => 'iris_features_themekey_rules',
    ),
    'string' => '"node:type = product >>> ThemeKeyAdminTheme"',
    'childs' => array(),
  ),
  1 => array(
    'rule' => array(
      'property' => 'node:referer',
      'operator' => '*',
      'value' => 'admin/',
      'theme' => 'ThemeKeyAdminTheme',
      'enabled' => 1,
      'wildcards' => 'a:0:{}',
      'module' => 'iris_features_themekey_rules',
    ),
    'string' => '"node:referer * admin/ >>> ThemeKeyAdminTheme"',
    'childs' => array(),
  ),
  2 => array(
    'rule' => array(
      'property' => 'system:post',
      'operator' => '*',
      'value' => 'form_id=iris_core_export_request',
      'theme' => 'ThemeKeyAdminTheme',
      'enabled' => 1,
      'wildcards' => 'a:0:{}',
      'module' => 'iris_features_themekey_rules',
    ),
    'string' => '"system:post * form_id=iris_core_export_request >>> ThemeKeyAdminTheme"',
    'childs' => array(),
  ),
  3 => array(
    'rule' => array(
      'property' => 'system:query_param',
      'operator' => '*',
      'value' => 'iris_admin_theme',
      'theme' => 'ThemeKeyAdminTheme',
      'enabled' => 1,
      'wildcards' => 'a:0:{}',
      'module' => 'iris_features_themekey_rules',
    ),
    'string' => '"system:query_param * iris_admin_theme >>> ThemeKeyAdminTheme"',
    'childs' => array(),
  ),
);

return $rules;
}
