<?php
/**
 * @file
 * iris_features_taxonomy_sales_organization.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_taxonomy_sales_organization_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
