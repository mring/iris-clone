<?php
/**
 * @file
 * iris_features_taxonomy_sales_organization.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_sales_organization_taxonomy_default_vocabularies() {
  return array(
    'sales_organization' => array(
      'name' => 'Sales organization',
      'machine_name' => 'sales_organization',
      'description' => 'The sales organization is part of the location group (3rd level). The taxonomy is only populated via the import of the location ',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -2,
    ),
  );
}
