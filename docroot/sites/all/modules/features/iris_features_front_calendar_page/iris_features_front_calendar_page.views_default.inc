<?php
/**
 * @file
 * iris_features_front_calendar_page.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function iris_features_front_calendar_page_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'calendar';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_calendar';
  $view->human_name = 'Calendar';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Calendar';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'iris_calendar';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'calendar_item_type',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'There is no news in the selected interval.';
  /* Relationship: Indexed Node: News theme */
  $handler->display->display_options['relationships']['field_news_type']['id'] = 'field_news_type';
  $handler->display->display_options['relationships']['field_news_type']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['relationships']['field_news_type']['field'] = 'field_news_type';
  /* Field: Indexed Node: Node ID */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  /* Field: Indexed Node: Calendar item type */
  $handler->display->display_options['fields']['calendar_item_type']['id'] = 'calendar_item_type';
  $handler->display->display_options['fields']['calendar_item_type']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['fields']['calendar_item_type']['field'] = 'calendar_item_type';
  $handler->display->display_options['fields']['calendar_item_type']['label'] = '';
  $handler->display->display_options['fields']['calendar_item_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['calendar_item_type']['element_type'] = '0';
  $handler->display->display_options['fields']['calendar_item_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['calendar_item_type']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['calendar_item_type']['separator'] = '';
  $handler->display->display_options['fields']['calendar_item_type']['link_to_entity'] = 0;
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: Indexed Node: Calendar start date */
  $handler->display->display_options['fields']['calendar_start_date']['id'] = 'calendar_start_date';
  $handler->display->display_options['fields']['calendar_start_date']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['fields']['calendar_start_date']['field'] = 'calendar_start_date';
  $handler->display->display_options['fields']['calendar_start_date']['separator'] = '';
  $handler->display->display_options['fields']['calendar_start_date']['link_to_entity'] = 0;
  /* Field: Indexed Node: Calendar end date */
  $handler->display->display_options['fields']['calendar_end_date']['id'] = 'calendar_end_date';
  $handler->display->display_options['fields']['calendar_end_date']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['fields']['calendar_end_date']['field'] = 'calendar_end_date';
  $handler->display->display_options['fields']['calendar_end_date']['separator'] = '';
  $handler->display->display_options['fields']['calendar_end_date']['link_to_entity'] = 0;
  /* Field: Taxonomy term: Agenda color class */
  $handler->display->display_options['fields']['field_agenda_color_class']['id'] = 'field_agenda_color_class';
  $handler->display->display_options['fields']['field_agenda_color_class']['table'] = 'entity_taxonomy_term';
  $handler->display->display_options['fields']['field_agenda_color_class']['field'] = 'field_agenda_color_class';
  $handler->display->display_options['fields']['field_agenda_color_class']['relationship'] = 'field_news_type';
  $handler->display->display_options['fields']['field_agenda_color_class']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_agenda_color_class']['type'] = 'list_key';
  /* Field: Indexed Node: Grouping key */
  $handler->display->display_options['fields']['field_grouping_key']['id'] = 'field_grouping_key';
  $handler->display->display_options['fields']['field_grouping_key']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['fields']['field_grouping_key']['field'] = 'field_grouping_key';
  $handler->display->display_options['fields']['field_grouping_key']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_grouping_key']['type'] = 'text_plain';
  /* Field: Indexed Node: Promotion news count */
  $handler->display->display_options['fields']['promo_news_count']['id'] = 'promo_news_count';
  $handler->display->display_options['fields']['promo_news_count']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['fields']['promo_news_count']['field'] = 'promo_news_count';
  $handler->display->display_options['fields']['promo_news_count']['exclude'] = TRUE;
  $handler->display->display_options['fields']['promo_news_count']['separator'] = '';
  $handler->display->display_options['fields']['promo_news_count']['link_to_entity'] = 0;
  /* Field: Indexed Node: Promotion operation description */
  $handler->display->display_options['fields']['promo_operation_desc']['id'] = 'promo_operation_desc';
  $handler->display->display_options['fields']['promo_operation_desc']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['fields']['promo_operation_desc']['field'] = 'promo_operation_desc';
  $handler->display->display_options['fields']['promo_operation_desc']['exclude'] = TRUE;
  $handler->display->display_options['fields']['promo_operation_desc']['link_to_entity'] = 0;
  /* Field: Indexed Node: Activity domain */
  $handler->display->display_options['fields']['field_activity_domains']['id'] = 'field_activity_domains';
  $handler->display->display_options['fields']['field_activity_domains']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['fields']['field_activity_domains']['field'] = 'field_activity_domains';
  $handler->display->display_options['fields']['field_activity_domains']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_activity_domains']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_activity_domains']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_activity_domains']['bypass_access'] = 0;
  /* Field: Indexed Node: Activity sector */
  $handler->display->display_options['fields']['field_activity_sectors']['id'] = 'field_activity_sectors';
  $handler->display->display_options['fields']['field_activity_sectors']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['fields']['field_activity_sectors']['field'] = 'field_activity_sectors';
  $handler->display->display_options['fields']['field_activity_sectors']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_activity_sectors']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_activity_sectors']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_activity_sectors']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_activity_sectors']['bypass_access'] = 0;
  /* Field: Indexed Node: Calendar news relation */
  $handler->display->display_options['fields']['calendar_news_statuses']['id'] = 'calendar_news_statuses';
  $handler->display->display_options['fields']['calendar_news_statuses']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['fields']['calendar_news_statuses']['field'] = 'calendar_news_statuses';
  $handler->display->display_options['fields']['calendar_news_statuses']['label'] = '';
  $handler->display->display_options['fields']['calendar_news_statuses']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['calendar_news_statuses']['list']['mode'] = 'list';
  $handler->display->display_options['fields']['calendar_news_statuses']['link_to_entity'] = 1;
  /* Sort criterion: Indexed Node: Calendar start date */
  $handler->display->display_options['sorts']['calendar_start_date']['id'] = 'calendar_start_date';
  $handler->display->display_options['sorts']['calendar_start_date']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['sorts']['calendar_start_date']['field'] = 'calendar_start_date';
  /* Contextual filter: Indexed Node: Calendar start date */
  $handler->display->display_options['arguments']['calendar_start_date']['id'] = 'calendar_start_date';
  $handler->display->display_options['arguments']['calendar_start_date']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['arguments']['calendar_start_date']['field'] = 'calendar_start_date';
  $handler->display->display_options['arguments']['calendar_start_date']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['calendar_start_date']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['calendar_start_date']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['calendar_start_date']['not'] = 0;
  /* Filter criterion: Indexed Node: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'search_api_index_calendar';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = array(
    1 => '1',
  );

  /* Display: Calendar pane */
  $handler = $view->new_display('panel_pane', 'Calendar pane', 'calendar_pane');
  $translatables['calendar'] = array(
    t('Master'),
    t('Calendar'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('There is no news in the selected interval.'),
    t('News theme'),
    t('Node ID'),
    t('.'),
    t(','),
    t('Calendar start date'),
    t('Calendar end date'),
    t('Agenda color class'),
    t('Grouping key'),
    t('Promotion news count'),
    t('Promotion operation description'),
    t('Activity domain'),
    t('Activity sector'),
    t('All'),
    t('Calendar pane'),
    t('View panes'),
  );
  $export['calendar'] = $view;

  return $export;
}
