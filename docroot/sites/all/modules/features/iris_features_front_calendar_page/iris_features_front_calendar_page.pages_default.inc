<?php
/**
 * @file
 * iris_features_front_calendar_page.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_front_calendar_page_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'calendar';
  $page->task = 'page';
  $page->admin_title = 'Calendar';
  $page->admin_description = '';
  $page->path = 'calendar';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_calendar__panel';
  $handler->task = 'page';
  $handler->subtask = 'calendar';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Calendar',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'calendar_2_cols';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'calendar_sidebar' => NULL,
      'calendar_content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Calendar';
  $display->uuid = 'fe1ad8de-d38d-4e59-9813-3a9d4675dc7f';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_calendar__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-70cd8119-fd1b-4f74-b033-f47c5188c00f';
  $pane->panel = 'calendar_content';
  $pane->type = 'views_panes';
  $pane->subtype = 'calendar-calendar_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '70cd8119-fd1b-4f74-b033-f47c5188c00f';
  $display->content['new-70cd8119-fd1b-4f74-b033-f47c5188c00f'] = $pane;
  $display->panels['calendar_content'][0] = 'new-70cd8119-fd1b-4f74-b033-f47c5188c00f';
  $pane = new stdClass();
  $pane->pid = 'new-7489b7d3-c273-4c4e-84e0-73c80d91d891';
  $pane->panel = 'calendar_sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-Swpz6qNIZOtQuxFsvoB34vv2N2IVpnMB';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Universes',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7489b7d3-c273-4c4e-84e0-73c80d91d891';
  $display->content['new-7489b7d3-c273-4c4e-84e0-73c80d91d891'] = $pane;
  $display->panels['calendar_sidebar'][0] = 'new-7489b7d3-c273-4c4e-84e0-73c80d91d891';
  $pane = new stdClass();
  $pane->pid = 'new-0d33291f-096d-4857-81b5-3b0cb30b5818';
  $pane->panel = 'calendar_sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-4bgIfXWCTmj6ZdTy1uHxHzt7UT16byNL';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Stores',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '0d33291f-096d-4857-81b5-3b0cb30b5818';
  $display->content['new-0d33291f-096d-4857-81b5-3b0cb30b5818'] = $pane;
  $display->panels['calendar_sidebar'][1] = 'new-0d33291f-096d-4857-81b5-3b0cb30b5818';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['calendar'] = $page;

  return $pages;

}
