<?php
/**
 * @file
 * iris_features_page_news_add_edit_form.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function iris_features_page_news_add_edit_form_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_edit__panel_context_14d5378f-bb5a-46e5-b5a4-f6e8b0b3ceeb';
  $handler->task = 'node_edit';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'News content type',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'news' => 'news',
            ),
          ),
          'context' => 'argument_node_edit_1',
          'not' => FALSE,
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'bottom' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
    'top' => array(
      'style' => '-1',
    ),
    'left' => array(
      'style' => '-1',
    ),
    'right' => array(
      'style' => '-1',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '81de7cea-2659-452e-b63d-ed14778e5d0d';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'node_edit__panel_context_14d5378f-bb5a-46e5-b5a4-f6e8b0b3ceeb';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-80a437ef-96cc-43aa-bc8a-71d2b259899e';
  $pane->panel = 'bottom';
  $pane->type = 'entity_form_field';
  $pane->subtype = 'node:group_step_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => '',
    'formatter' => '',
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '80a437ef-96cc-43aa-bc8a-71d2b259899e';
  $display->content['new-80a437ef-96cc-43aa-bc8a-71d2b259899e'] = $pane;
  $display->panels['bottom'][0] = 'new-80a437ef-96cc-43aa-bc8a-71d2b259899e';
  $pane = new stdClass();
  $pane->pid = 'new-37a349a4-6dcf-47a1-8641-51467ecec4a7';
  $pane->panel = 'bottom';
  $pane->type = 'entity_form_field';
  $pane->subtype = 'node:group_step_3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => '',
    'formatter' => '',
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '37a349a4-6dcf-47a1-8641-51467ecec4a7';
  $display->content['new-37a349a4-6dcf-47a1-8641-51467ecec4a7'] = $pane;
  $display->panels['bottom'][1] = 'new-37a349a4-6dcf-47a1-8641-51467ecec4a7';
  $pane = new stdClass();
  $pane->pid = 'new-f5898345-5f0d-461a-adec-d1884d6eb763';
  $pane->panel = 'bottom';
  $pane->type = 'entity_form_field';
  $pane->subtype = 'node:group_step_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => '',
    'formatter' => '',
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'f5898345-5f0d-461a-adec-d1884d6eb763';
  $display->content['new-f5898345-5f0d-461a-adec-d1884d6eb763'] = $pane;
  $display->panels['bottom'][2] = 'new-f5898345-5f0d-461a-adec-d1884d6eb763';
  $pane = new stdClass();
  $pane->pid = 'new-24b40643-1d76-420a-b5f2-48e452ffa84a';
  $pane->panel = 'bottom';
  $pane->type = 'form';
  $pane->subtype = 'form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '24b40643-1d76-420a-b5f2-48e452ffa84a';
  $display->content['new-24b40643-1d76-420a-b5f2-48e452ffa84a'] = $pane;
  $display->panels['bottom'][3] = 'new-24b40643-1d76-420a-b5f2-48e452ffa84a';
  $pane = new stdClass();
  $pane->pid = 'new-34d61dd2-8b46-4d4b-8900-5475cafdcec5';
  $pane->panel = 'bottom';
  $pane->type = 'node_form_buttons';
  $pane->subtype = 'node_form_buttons';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '34d61dd2-8b46-4d4b-8900-5475cafdcec5';
  $display->content['new-34d61dd2-8b46-4d4b-8900-5475cafdcec5'] = $pane;
  $display->panels['bottom'][4] = 'new-34d61dd2-8b46-4d4b-8900-5475cafdcec5';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_edit__panel_context_14d5378f-bb5a-46e5-b5a4-f6e8b0b3ceeb'] = $handler;

  return $export;
}
