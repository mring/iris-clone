<?php
/**
 * @file
 * Contains helping functions.
 */

/**
 * Hiding some form fields.
 */
function iris_features_page_taxonomies_dashboard_hide_fields($form, &$form_state) {
  global $user;
  $fields_to_hide = array(
    'path',
    'relations',
    'og_user_inheritance',
  );
  if (isset($form['translation'])) {
    if (!in_array('administrator', $user->roles)) {
      $form['translation']['#access'] = FALSE;
    }
  }

  foreach ($fields_to_hide as $field) {
    if (isset($form[$field])) {
      $form[$field]['#attributes']['style'][] = 'display: none';
    }
  }

  return $form;
}
