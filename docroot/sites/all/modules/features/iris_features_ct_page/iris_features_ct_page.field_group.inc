<?php
/**
 * @file
 * iris_features_ct_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function iris_features_ct_page_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_step_1|node|page|form';
  $field_group->group_name = 'group_step_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Step 1: Playbook content',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_files',
      2 => 'field_links',
      3 => 'field_page_banner_image',
      4 => 'field_page_type',
      5 => 'title_field',
      6 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Step 1: Playbook content',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-step-1 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_step_1|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_step_2|node|page|form';
  $field_group->group_name = 'group_step_2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Step 2 : Playbook Visibility scope',
    'weight' => '1',
    'children' => array(
      0 => 'field_location_group',
      1 => 'field_scope_selection',
      2 => 'field_activity_domains',
      3 => 'field_activity_sectors',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Step 2 : Playbook Visibility scope',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-step-2 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_step_2|node|page|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Step 1: Playbook content');
  t('Step 2 : Playbook Visibility scope');

  return $field_groups;
}
