<?php
/**
 * @file
 * iris_features_ct_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_ct_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function iris_features_ct_page_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Playbook'),
      'base' => 'node_content',
      'description' => t('The generic pages are used to display static content not related to a news (e.g. Legal terms). They are not affected to a location group so every user can access them in the front-office.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
