<?php
/**
 * @file
 * iris_features_ct_promotion_line.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_ct_promotion_line_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function iris_features_ct_promotion_line_node_info() {
  $items = array(
    'promotion_line' => array(
      'name' => t('Promotion line'),
      'base' => 'node_content',
      'description' => t('A promotion line is imported from a promotion import file. Each line correspond to an entry in the CSV and is also related to a Product SKU.
All the fields described in this section are populated automatically by the promotion import.'),
      'has_title' => '1',
      'title_label' => t('Promotion name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
