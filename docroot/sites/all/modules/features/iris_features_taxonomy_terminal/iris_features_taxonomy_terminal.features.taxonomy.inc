<?php
/**
 * @file
 * iris_features_taxonomy_terminal.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_terminal_taxonomy_default_vocabularies() {
  return array(
    'terminal' => array(
      'name' => 'Terminal',
      'machine_name' => 'terminal',
      'description' => 'The terminal is part of the location group (5th level). The taxonomy is only populated via the import of the location group ',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 3,
    ),
  );
}
