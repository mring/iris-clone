<?php
/**
 * @file
 * iris_features_general_layout.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function iris_features_general_layout_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template__panel_context_c588c6cb-ccba-47d2-aea4-1580a382f32b';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'General',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'access' => array(
      'logic' => 'and',
      'plugins' => array(),
    ),
  );
  $display = new panels_display();
  $display->layout = 'site_general';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'general_sidebar' => NULL,
      'general_navigation' => NULL,
      'general_header' => NULL,
      'general_content' => NULL,
      'general_main_nav' => NULL,
      'general_account_nav' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'b0b37ba3-ffb6-418b-a37b-9827cce33477';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1f4a1650-7e22-4106-b605-88ca0ab8cd30';
    $pane->panel = 'general_account_nav';
    $pane->type = 'user_picture';
    $pane->subtype = 'user_picture';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1f4a1650-7e22-4106-b605-88ca0ab8cd30';
    $display->content['new-1f4a1650-7e22-4106-b605-88ca0ab8cd30'] = $pane;
    $display->panels['general_account_nav'][0] = 'new-1f4a1650-7e22-4106-b605-88ca0ab8cd30';
    $pane = new stdClass();
    $pane->pid = 'new-30d660cf-ea51-4abf-b14b-cf48c1844263';
    $pane->panel = 'general_account_nav';
    $pane->type = 'logout_link';
    $pane->subtype = 'logout_link';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '30d660cf-ea51-4abf-b14b-cf48c1844263';
    $display->content['new-30d660cf-ea51-4abf-b14b-cf48c1844263'] = $pane;
    $display->panels['general_account_nav'][1] = 'new-30d660cf-ea51-4abf-b14b-cf48c1844263';
    $pane = new stdClass();
    $pane->pid = 'new-42831dbe-e2de-425a-a43b-50c5a4616245';
    $pane->panel = 'general_account_nav';
    $pane->type = 'user_editor_link';
    $pane->subtype = 'user_editor_link';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '42831dbe-e2de-425a-a43b-50c5a4616245';
    $display->content['new-42831dbe-e2de-425a-a43b-50c5a4616245'] = $pane;
    $display->panels['general_account_nav'][2] = 'new-42831dbe-e2de-425a-a43b-50c5a4616245';
    $pane = new stdClass();
    $pane->pid = 'new-3ac088a3-c5b7-4b06-a177-3c1cc1208a96';
    $pane->panel = 'general_account_nav';
    $pane->type = 'user_edit_link';
    $pane->subtype = 'user_edit_link';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '3ac088a3-c5b7-4b06-a177-3c1cc1208a96';
    $display->content['new-3ac088a3-c5b7-4b06-a177-3c1cc1208a96'] = $pane;
    $display->panels['general_account_nav'][3] = 'new-3ac088a3-c5b7-4b06-a177-3c1cc1208a96';
    $pane = new stdClass();
    $pane->pid = 'new-4bc08c4e-95c2-417b-b138-fc084a08a813';
    $pane->panel = 'general_account_nav';
    $pane->type = 'notifications';
    $pane->subtype = 'notifications';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 2,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '4bc08c4e-95c2-417b-b138-fc084a08a813';
    $display->content['new-4bc08c4e-95c2-417b-b138-fc084a08a813'] = $pane;
    $display->panels['general_account_nav'][4] = 'new-4bc08c4e-95c2-417b-b138-fc084a08a813';
    $pane = new stdClass();
    $pane->pid = 'new-ea7e5210-e232-4165-917d-81ce38c18b17';
    $pane->panel = 'general_content';
    $pane->type = 'page_messages';
    $pane->subtype = 'page_messages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ea7e5210-e232-4165-917d-81ce38c18b17';
    $display->content['new-ea7e5210-e232-4165-917d-81ce38c18b17'] = $pane;
    $display->panels['general_content'][0] = 'new-ea7e5210-e232-4165-917d-81ce38c18b17';
    $pane = new stdClass();
    $pane->pid = 'new-b35a13d8-d9d9-4850-808a-12c5c5e4a585';
    $pane->panel = 'general_content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'b35a13d8-d9d9-4850-808a-12c5c5e4a585';
    $display->content['new-b35a13d8-d9d9-4850-808a-12c5c5e4a585'] = $pane;
    $display->panels['general_content'][1] = 'new-b35a13d8-d9d9-4850-808a-12c5c5e4a585';
    $pane = new stdClass();
    $pane->pid = 'new-3fcdcfa5-a8fc-43d0-a3a4-71fa05065860';
    $pane->panel = 'general_header';
    $pane->type = 'site_logo';
    $pane->subtype = 'site_logo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'site_logo' => 'sites/all/themes/iris_theme/html/images/logo.png',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array(
      'method' => 'simple',
      'settings' => array(
        'lifetime' => '28800',
        'granularity' => 'context',
      ),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3fcdcfa5-a8fc-43d0-a3a4-71fa05065860';
    $display->content['new-3fcdcfa5-a8fc-43d0-a3a4-71fa05065860'] = $pane;
    $display->panels['general_header'][0] = 'new-3fcdcfa5-a8fc-43d0-a3a4-71fa05065860';
    $pane = new stdClass();
    $pane->pid = 'new-c362c77d-8e9d-4235-84ae-cd19dc6a56a4';
    $pane->panel = 'general_header';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 1,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '  <a href="#" class="mobile-menu">
    <span></span>
  </a>',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array(
      'method' => 'simple',
      'settings' => array(
        'lifetime' => '28800',
        'granularity' => 'context',
      ),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c362c77d-8e9d-4235-84ae-cd19dc6a56a4';
    $display->content['new-c362c77d-8e9d-4235-84ae-cd19dc6a56a4'] = $pane;
    $display->panels['general_header'][1] = 'new-c362c77d-8e9d-4235-84ae-cd19dc6a56a4';
    $pane = new stdClass();
    $pane->pid = 'new-df3ee105-5d3e-4945-92c4-cb44e822733d';
    $pane->panel = 'general_main_nav';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 2,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array(
      'method' => '0',
      'settings' => array(),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'df3ee105-5d3e-4945-92c4-cb44e822733d';
    $display->content['new-df3ee105-5d3e-4945-92c4-cb44e822733d'] = $pane;
    $display->panels['general_main_nav'][0] = 'new-df3ee105-5d3e-4945-92c4-cb44e822733d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['site_template__panel_context_c588c6cb-ccba-47d2-aea4-1580a382f32b'] = $handler;

  return $export;
}
