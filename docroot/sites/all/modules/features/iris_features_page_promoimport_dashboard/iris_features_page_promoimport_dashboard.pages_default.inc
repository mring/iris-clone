<?php
/**
 * @file
 * iris_features_page_promoimport_dashboard.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_promoimport_dashboard_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'asynchronous_import';
  $page->task = 'page';
  $page->admin_title = 'Asynchronous import interface';
  $page->admin_description = '';
  $page->path = 'admin/promotions/promotion-import-interface';
  $page->access = array(
    'plugins' => array(
      1 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'import promotions',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_asynchronous_import__panel_context_0e2494ba-4753-476f-9a3e-1d8c9d4ae2c9';
  $handler->task = 'page';
  $handler->subtask = 'asynchronous_import';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Promotions import',
    'panels_breadcrumbs_paths' => 'admin/promotions/promotion-import-interface',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Promotions import';
  $display->uuid = '94746d2d-526c-4a56-9afa-db4554968ae5';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b9727ee6-51a5-4b42-ae23-c6ea62eb9a9a';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'iris_import_data-promotion_line_upload';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b9727ee6-51a5-4b42-ae23-c6ea62eb9a9a';
    $display->content['new-b9727ee6-51a5-4b42-ae23-c6ea62eb9a9a'] = $pane;
    $display->panels['middle'][0] = 'new-b9727ee6-51a5-4b42-ae23-c6ea62eb9a9a';
    $pane = new stdClass();
    $pane->pid = 'new-33032a1e-2a74-4887-8efd-e17e45a26c11';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'promotions_import-asynchronous_import_table';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '33032a1e-2a74-4887-8efd-e17e45a26c11';
    $display->content['new-33032a1e-2a74-4887-8efd-e17e45a26c11'] = $pane;
    $display->panels['middle'][1] = 'new-33032a1e-2a74-4887-8efd-e17e45a26c11';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-33032a1e-2a74-4887-8efd-e17e45a26c11';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['asynchronous_import'] = $page;

  return $pages;

}
