<?php
/**
 * @file
 * iris_features_page_canal_promotions_dashboard.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function iris_features_page_canal_promotions_dashboard_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'promotions_canal_dashboard';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Promotions canal dashboard';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access promotions canal dashboard';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'nid' => 'nid',
    'promo_canals' => 'promo_canals',
    'field_operation_number' => 'field_operation_number',
    'name' => 'name',
    'field_operation_start_date' => 'field_operation_start_date',
    'field_operation_end_date' => 'field_operation_end_date',
    'field_operation_description' => 'field_operation_description',
    'field_thematic' => 'field_thematic',
    'field_reference_validation' => 'field_reference_validation',
    'nothing' => 'nothing',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'promo_canals' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_operation_number' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_operation_start_date' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_operation_end_date' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_operation_description' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_thematic' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_reference_validation' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No promotions found';
  $handler->display->display_options['empty']['area']['format'] = 'plain_text';
  /* Relationship: SLOT */
  $handler->display->display_options['relationships']['field_promo_slot_tid']['id'] = 'field_promo_slot_tid';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['table'] = 'field_data_field_promo_slot';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['field'] = 'field_promo_slot_tid';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['ui_name'] = 'SLOT';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['label'] = 'SLOT';
  /* Relationship: SECTOR */
  $handler->display->display_options['relationships']['field_promo_line_activity_sector_tid']['id'] = 'field_promo_line_activity_sector_tid';
  $handler->display->display_options['relationships']['field_promo_line_activity_sector_tid']['table'] = 'field_data_field_promo_line_activity_sector';
  $handler->display->display_options['relationships']['field_promo_line_activity_sector_tid']['field'] = 'field_promo_line_activity_sector_tid';
  $handler->display->display_options['relationships']['field_promo_line_activity_sector_tid']['ui_name'] = 'SECTOR';
  $handler->display->display_options['relationships']['field_promo_line_activity_sector_tid']['label'] = 'SECTOR';
  /* Relationship: DOMAIN */
  $handler->display->display_options['relationships']['field_activity_domain_tid']['id'] = 'field_activity_domain_tid';
  $handler->display->display_options['relationships']['field_activity_domain_tid']['table'] = 'field_data_field_activity_domain';
  $handler->display->display_options['relationships']['field_activity_domain_tid']['field'] = 'field_activity_domain_tid';
  $handler->display->display_options['relationships']['field_activity_domain_tid']['relationship'] = 'field_promo_line_activity_sector_tid';
  $handler->display->display_options['relationships']['field_activity_domain_tid']['ui_name'] = 'DOMAIN';
  $handler->display->display_options['relationships']['field_activity_domain_tid']['label'] = 'DOMAIN';
  /* Relationship: PROMO_LINES */
  $handler->display->display_options['relationships']['field_promotion_lines_target_id']['id'] = 'field_promotion_lines_target_id';
  $handler->display->display_options['relationships']['field_promotion_lines_target_id']['table'] = 'field_data_field_promotion_lines';
  $handler->display->display_options['relationships']['field_promotion_lines_target_id']['field'] = 'field_promotion_lines_target_id';
  $handler->display->display_options['relationships']['field_promotion_lines_target_id']['ui_name'] = 'PROMO_LINES';
  $handler->display->display_options['relationships']['field_promotion_lines_target_id']['label'] = 'PROMO_LINES';
  /* Relationship: OPERATION */
  $handler->display->display_options['relationships']['field_operation_target_id']['id'] = 'field_operation_target_id';
  $handler->display->display_options['relationships']['field_operation_target_id']['table'] = 'field_data_field_operation';
  $handler->display->display_options['relationships']['field_operation_target_id']['field'] = 'field_operation_target_id';
  $handler->display->display_options['relationships']['field_operation_target_id']['relationship'] = 'field_promotion_lines_target_id';
  $handler->display->display_options['relationships']['field_operation_target_id']['ui_name'] = 'OPERATION';
  $handler->display->display_options['relationships']['field_operation_target_id']['label'] = 'OPERATION';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Content: Promotion canals */
  $handler->display->display_options['fields']['promo_canals']['id'] = 'promo_canals';
  $handler->display->display_options['fields']['promo_canals']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['promo_canals']['field'] = 'promo_canals';
  $handler->display->display_options['fields']['promo_canals']['label'] = 'Canal';
  $handler->display->display_options['fields']['promo_canals']['link_to_entity'] = 0;
  /* Field: Content: Operation number */
  $handler->display->display_options['fields']['field_operation_number']['id'] = 'field_operation_number';
  $handler->display->display_options['fields']['field_operation_number']['table'] = 'field_data_field_operation_number';
  $handler->display->display_options['fields']['field_operation_number']['field'] = 'field_operation_number';
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'field_promo_line_activity_sector_tid';
  $handler->display->display_options['fields']['name']['label'] = 'Activity sector';
  /* Field: Content: Start date */
  $handler->display->display_options['fields']['field_operation_start_date']['id'] = 'field_operation_start_date';
  $handler->display->display_options['fields']['field_operation_start_date']['table'] = 'field_data_field_operation_start_date';
  $handler->display->display_options['fields']['field_operation_start_date']['field'] = 'field_operation_start_date';
  $handler->display->display_options['fields']['field_operation_start_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: End date */
  $handler->display->display_options['fields']['field_operation_end_date']['id'] = 'field_operation_end_date';
  $handler->display->display_options['fields']['field_operation_end_date']['table'] = 'field_data_field_operation_end_date';
  $handler->display->display_options['fields']['field_operation_end_date']['field'] = 'field_operation_end_date';
  $handler->display->display_options['fields']['field_operation_end_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Operation description */
  $handler->display->display_options['fields']['field_operation_description']['id'] = 'field_operation_description';
  $handler->display->display_options['fields']['field_operation_description']['table'] = 'field_data_field_operation_description';
  $handler->display->display_options['fields']['field_operation_description']['field'] = 'field_operation_description';
  $handler->display->display_options['fields']['field_operation_description']['relationship'] = 'field_operation_target_id';
  $handler->display->display_options['fields']['field_operation_description']['type'] = 'text_plain';
  /* Field: Content: Thematic */
  $handler->display->display_options['fields']['field_thematic']['id'] = 'field_thematic';
  $handler->display->display_options['fields']['field_thematic']['table'] = 'field_data_field_thematic';
  $handler->display->display_options['fields']['field_thematic']['field'] = 'field_thematic';
  $handler->display->display_options['fields']['field_thematic']['relationship'] = 'field_operation_target_id';
  $handler->display->display_options['fields']['field_thematic']['label'] = 'Operation thematic';
  $handler->display->display_options['fields']['field_thematic']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Validation */
  $handler->display->display_options['fields']['field_reference_validation']['id'] = 'field_reference_validation';
  $handler->display->display_options['fields']['field_reference_validation']['table'] = 'field_data_field_reference_validation';
  $handler->display->display_options['fields']['field_reference_validation']['field'] = 'field_reference_validation';
  $handler->display->display_options['fields']['field_reference_validation']['label'] = 'Validation date';
  $handler->display->display_options['fields']['field_reference_validation']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Operations';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'View';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'node/[nid]?iris_admin_theme';
  /* Sort criterion: Content: Start date (field_archive_startdate) */
  $handler->display->display_options['sorts']['field_archive_startdate_value']['id'] = 'field_archive_startdate_value';
  $handler->display->display_options['sorts']['field_archive_startdate_value']['table'] = 'field_data_field_archive_startdate';
  $handler->display->display_options['sorts']['field_archive_startdate_value']['field'] = 'field_archive_startdate_value';
  /* Sort criterion: Content: Operation number (field_operation_number) */
  $handler->display->display_options['sorts']['field_operation_number_value']['id'] = 'field_operation_number_value';
  $handler->display->display_options['sorts']['field_operation_number_value']['table'] = 'field_data_field_operation_number';
  $handler->display->display_options['sorts']['field_operation_number_value']['field'] = 'field_operation_number_value';
  /* Sort criterion: Taxonomy term: Name */
  $handler->display->display_options['sorts']['name']['id'] = 'name';
  $handler->display->display_options['sorts']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['name']['field'] = 'name';
  $handler->display->display_options['sorts']['name']['relationship'] = 'field_promo_line_activity_sector_tid';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'promotion' => 'promotion',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Operation number (field_operation_number) */
  $handler->display->display_options['filters']['field_operation_number_value']['id'] = 'field_operation_number_value';
  $handler->display->display_options['filters']['field_operation_number_value']['table'] = 'field_data_field_operation_number';
  $handler->display->display_options['filters']['field_operation_number_value']['field'] = 'field_operation_number_value';
  $handler->display->display_options['filters']['field_operation_number_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_operation_number_value']['group'] = 1;
  $handler->display->display_options['filters']['field_operation_number_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_operation_number_value']['expose']['operator_id'] = 'field_operation_number_value_op';
  $handler->display->display_options['filters']['field_operation_number_value']['expose']['label'] = 'Operation number';
  $handler->display->display_options['filters']['field_operation_number_value']['expose']['operator'] = 'field_operation_number_value_op';
  $handler->display->display_options['filters']['field_operation_number_value']['expose']['identifier'] = 'field_operation_number_value';
  $handler->display->display_options['filters']['field_operation_number_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  /* Filter criterion: Content: Nid */
  $handler->display->display_options['filters']['nid_1']['id'] = 'nid_1';
  $handler->display->display_options['filters']['nid_1']['table'] = 'node';
  $handler->display->display_options['filters']['nid_1']['field'] = 'nid';
  $handler->display->display_options['filters']['nid_1']['relationship'] = 'field_promotion_lines_target_id';
  $handler->display->display_options['filters']['nid_1']['group'] = 1;
  $handler->display->display_options['filters']['nid_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['nid_1']['expose']['operator_id'] = 'nid_1_op';
  $handler->display->display_options['filters']['nid_1']['expose']['label'] = 'Canal';
  $handler->display->display_options['filters']['nid_1']['expose']['operator'] = 'nid_1_op';
  $handler->display->display_options['filters']['nid_1']['expose']['identifier'] = 'canal';
  $handler->display->display_options['filters']['nid_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  /* Filter criterion: Date: Date (node) */
  $handler->display->display_options['filters']['date_filter']['id'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter']['table'] = 'node';
  $handler->display->display_options['filters']['date_filter']['field'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter']['operator'] = '>=';
  $handler->display->display_options['filters']['date_filter']['group'] = 1;
  $handler->display->display_options['filters']['date_filter']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date_filter']['expose']['operator_id'] = 'date_filter_op';
  $handler->display->display_options['filters']['date_filter']['expose']['label'] = 'Start date';
  $handler->display->display_options['filters']['date_filter']['expose']['operator'] = 'date_filter_op';
  $handler->display->display_options['filters']['date_filter']['expose']['identifier'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  $handler->display->display_options['filters']['date_filter']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['date_filter']['date_fields'] = array(
    'field_data_field_operation_start_date.field_operation_start_date_value' => 'field_data_field_operation_start_date.field_operation_start_date_value',
  );
  /* Filter criterion: Date: Date (node) */
  $handler->display->display_options['filters']['date_filter_1']['id'] = 'date_filter_1';
  $handler->display->display_options['filters']['date_filter_1']['table'] = 'node';
  $handler->display->display_options['filters']['date_filter_1']['field'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter_1']['operator'] = '<=';
  $handler->display->display_options['filters']['date_filter_1']['group'] = 1;
  $handler->display->display_options['filters']['date_filter_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date_filter_1']['expose']['operator_id'] = 'date_filter_1_op';
  $handler->display->display_options['filters']['date_filter_1']['expose']['label'] = 'End date';
  $handler->display->display_options['filters']['date_filter_1']['expose']['operator'] = 'date_filter_1_op';
  $handler->display->display_options['filters']['date_filter_1']['expose']['identifier'] = 'date_filter_1';
  $handler->display->display_options['filters']['date_filter_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  $handler->display->display_options['filters']['date_filter_1']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['date_filter_1']['date_fields'] = array(
    'field_data_field_operation_end_date.field_operation_end_date_value' => 'field_data_field_operation_end_date.field_operation_end_date_value',
  );
  /* Filter criterion: Content: Activity sector (field_promo_line_activity_sector) */
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['id'] = 'field_promo_line_activity_sector_tid';
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['table'] = 'field_data_field_promo_line_activity_sector';
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['field'] = 'field_promo_line_activity_sector_tid';
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['expose']['operator_id'] = 'field_promo_line_activity_sector_tid_op';
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['expose']['label'] = 'Activity sector';
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['expose']['operator'] = 'field_promo_line_activity_sector_tid_op';
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['expose']['identifier'] = 'field_promo_line_activity_sector_tid';
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_promo_line_activity_sector_tid']['vocabulary'] = 'activity_sector';
  /* Filter criterion: Date: Date (node) */
  $handler->display->display_options['filters']['date_filter_2']['id'] = 'date_filter_2';
  $handler->display->display_options['filters']['date_filter_2']['table'] = 'node';
  $handler->display->display_options['filters']['date_filter_2']['field'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter_2']['operator'] = '>=';
  $handler->display->display_options['filters']['date_filter_2']['group'] = 1;
  $handler->display->display_options['filters']['date_filter_2']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date_filter_2']['expose']['operator_id'] = 'date_filter_2_op';
  $handler->display->display_options['filters']['date_filter_2']['expose']['label'] = 'Validation start date';
  $handler->display->display_options['filters']['date_filter_2']['expose']['operator'] = 'date_filter_2_op';
  $handler->display->display_options['filters']['date_filter_2']['expose']['identifier'] = 'date_filter_2';
  $handler->display->display_options['filters']['date_filter_2']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  $handler->display->display_options['filters']['date_filter_2']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['date_filter_2']['date_fields'] = array(
    'field_data_field_reference_validation.field_reference_validation_value' => 'field_data_field_reference_validation.field_reference_validation_value',
  );
  /* Filter criterion: Date: Date (node) */
  $handler->display->display_options['filters']['date_filter_3']['id'] = 'date_filter_3';
  $handler->display->display_options['filters']['date_filter_3']['table'] = 'node';
  $handler->display->display_options['filters']['date_filter_3']['field'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter_3']['operator'] = '<=';
  $handler->display->display_options['filters']['date_filter_3']['group'] = 1;
  $handler->display->display_options['filters']['date_filter_3']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date_filter_3']['expose']['operator_id'] = 'date_filter_3_op';
  $handler->display->display_options['filters']['date_filter_3']['expose']['label'] = 'Validation end date';
  $handler->display->display_options['filters']['date_filter_3']['expose']['operator'] = 'date_filter_3_op';
  $handler->display->display_options['filters']['date_filter_3']['expose']['identifier'] = 'date_filter_3';
  $handler->display->display_options['filters']['date_filter_3']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  $handler->display->display_options['filters']['date_filter_3']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['date_filter_3']['date_fields'] = array(
    'field_data_field_reference_validation.field_reference_validation_value' => 'field_data_field_reference_validation.field_reference_validation_value',
  );
  /* Filter criterion: Content: Thematic (field_thematic) */
  $handler->display->display_options['filters']['field_thematic_tid']['id'] = 'field_thematic_tid';
  $handler->display->display_options['filters']['field_thematic_tid']['table'] = 'field_data_field_thematic';
  $handler->display->display_options['filters']['field_thematic_tid']['field'] = 'field_thematic_tid';
  $handler->display->display_options['filters']['field_thematic_tid']['relationship'] = 'field_operation_target_id';
  $handler->display->display_options['filters']['field_thematic_tid']['value'] = '';
  $handler->display->display_options['filters']['field_thematic_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_thematic_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_thematic_tid']['expose']['operator_id'] = 'field_thematic_tid_op';
  $handler->display->display_options['filters']['field_thematic_tid']['expose']['label'] = 'Thematic';
  $handler->display->display_options['filters']['field_thematic_tid']['expose']['operator'] = 'field_thematic_tid_op';
  $handler->display->display_options['filters']['field_thematic_tid']['expose']['identifier'] = 'field_thematic_tid';
  $handler->display->display_options['filters']['field_thematic_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  $handler->display->display_options['filters']['field_thematic_tid']['vocabulary'] = 'thematic';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'canal_dashboard');
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Attachment */
  $handler = $view->new_display('attachment', 'Attachment', 'promotion_ids');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: SLOT */
  $handler->display->display_options['relationships']['field_promo_slot_tid']['id'] = 'field_promo_slot_tid';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['table'] = 'field_data_field_promo_slot';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['field'] = 'field_promo_slot_tid';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['ui_name'] = 'SLOT';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['label'] = 'SLOT';
  /* Relationship: SECTOR */
  $handler->display->display_options['relationships']['field_promo_line_activity_sector_tid']['id'] = 'field_promo_line_activity_sector_tid';
  $handler->display->display_options['relationships']['field_promo_line_activity_sector_tid']['table'] = 'field_data_field_promo_line_activity_sector';
  $handler->display->display_options['relationships']['field_promo_line_activity_sector_tid']['field'] = 'field_promo_line_activity_sector_tid';
  $handler->display->display_options['relationships']['field_promo_line_activity_sector_tid']['ui_name'] = 'SECTOR';
  $handler->display->display_options['relationships']['field_promo_line_activity_sector_tid']['label'] = 'SECTOR';
  /* Relationship: DOMAIN */
  $handler->display->display_options['relationships']['field_activity_domain_tid']['id'] = 'field_activity_domain_tid';
  $handler->display->display_options['relationships']['field_activity_domain_tid']['table'] = 'field_data_field_activity_domain';
  $handler->display->display_options['relationships']['field_activity_domain_tid']['field'] = 'field_activity_domain_tid';
  $handler->display->display_options['relationships']['field_activity_domain_tid']['relationship'] = 'field_promo_line_activity_sector_tid';
  $handler->display->display_options['relationships']['field_activity_domain_tid']['ui_name'] = 'DOMAIN';
  $handler->display->display_options['relationships']['field_activity_domain_tid']['label'] = 'DOMAIN';
  /* Relationship: PROMO_LINES */
  $handler->display->display_options['relationships']['field_promotion_lines_target_id']['id'] = 'field_promotion_lines_target_id';
  $handler->display->display_options['relationships']['field_promotion_lines_target_id']['table'] = 'field_data_field_promotion_lines';
  $handler->display->display_options['relationships']['field_promotion_lines_target_id']['field'] = 'field_promotion_lines_target_id';
  $handler->display->display_options['relationships']['field_promotion_lines_target_id']['ui_name'] = 'PROMO_LINES';
  $handler->display->display_options['relationships']['field_promotion_lines_target_id']['label'] = 'PROMO_LINES';
  /* Relationship: OPERATION */
  $handler->display->display_options['relationships']['field_operation_target_id']['id'] = 'field_operation_target_id';
  $handler->display->display_options['relationships']['field_operation_target_id']['table'] = 'field_data_field_operation';
  $handler->display->display_options['relationships']['field_operation_target_id']['field'] = 'field_operation_target_id';
  $handler->display->display_options['relationships']['field_operation_target_id']['relationship'] = 'field_promotion_lines_target_id';
  $handler->display->display_options['relationships']['field_operation_target_id']['ui_name'] = 'OPERATION';
  $handler->display->display_options['relationships']['field_operation_target_id']['label'] = 'OPERATION';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid_1']['id'] = 'nid_1';
  $handler->display->display_options['fields']['nid_1']['table'] = 'node';
  $handler->display->display_options['fields']['nid_1']['field'] = 'nid';
  $handler->display->display_options['fields']['nid_1']['relationship'] = 'field_promotion_lines_target_id';
  $handler->display->display_options['fields']['nid_1']['exclude'] = TRUE;
  /* Field: Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  $handler->display->display_options['fields']['tid']['relationship'] = 'field_promo_slot_tid';
  /* Field: Global: Slot so currency */
  $handler->display->display_options['fields']['slot_so_currency']['id'] = 'slot_so_currency';
  $handler->display->display_options['fields']['slot_so_currency']['table'] = 'views';
  $handler->display->display_options['fields']['slot_so_currency']['field'] = 'slot_so_currency';
  $handler->display->display_options['inherit_exposed_filters'] = TRUE;
  $translatables['promotions_canal_dashboard'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('No promotions found'),
    t('SLOT'),
    t('SECTOR'),
    t('DOMAIN'),
    t('PROMO_LINES'),
    t('OPERATION'),
    t('Nid'),
    t('Canal'),
    t('Operation number'),
    t('Activity sector'),
    t('Start date'),
    t('End date'),
    t('Operation description'),
    t('Operation thematic'),
    t('Validation date'),
    t('Operations'),
    t('View'),
    t('Validation start date'),
    t('Validation end date'),
    t('Thematic'),
    t('Content pane'),
    t('View panes'),
    t('Attachment'),
    t('Title'),
    t('Term ID'),
    t('.'),
    t(','),
    t('Slot so currency'),
  );
  $export['promotions_canal_dashboard'] = $view;

  return $export;
}
