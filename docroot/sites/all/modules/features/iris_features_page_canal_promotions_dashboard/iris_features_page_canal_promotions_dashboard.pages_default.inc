<?php
/**
 * @file
 * iris_features_page_canal_promotions_dashboard.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_canal_promotions_dashboard_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'promotion_canal_dashboard';
  $page->task = 'page';
  $page->admin_title = 'Promotion canal dashboard';
  $page->admin_description = '';
  $page->path = 'admin/promotions/promotions-canal-dashboard';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'access promotions canal dashboard',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_promotion_canal_dashboard__panel';
  $handler->task = 'page';
  $handler->subtask = 'promotion_canal_dashboard';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Promotions canal dashboard',
    'panels_breadcrumbs_paths' => '/admin/promotions/promotions-canal-dashboard',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Promotions canal dashboard';
  $display->uuid = 'a7814df0-b064-450c-ab79-38aeac9d89d9';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_promotion_canal_dashboard__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f6ffd8ff-6521-459c-bcdc-d6ffaead6459';
  $pane->panel = 'center';
  $pane->type = 'block';
  $pane->subtype = 'iris_core-request_export_canal';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f6ffd8ff-6521-459c-bcdc-d6ffaead6459';
  $display->content['new-f6ffd8ff-6521-459c-bcdc-d6ffaead6459'] = $pane;
  $display->panels['center'][0] = 'new-f6ffd8ff-6521-459c-bcdc-d6ffaead6459';
  $pane = new stdClass();
  $pane->pid = 'new-cfad2aad-a9fe-4ac2-b055-285fbaf04ca1';
  $pane->panel = 'center';
  $pane->type = 'views_panes';
  $pane->subtype = 'promotions_canal_dashboard-canal_dashboard';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'cfad2aad-a9fe-4ac2-b055-285fbaf04ca1';
  $display->content['new-cfad2aad-a9fe-4ac2-b055-285fbaf04ca1'] = $pane;
  $display->panels['center'][1] = 'new-cfad2aad-a9fe-4ac2-b055-285fbaf04ca1';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['promotion_canal_dashboard'] = $page;

  return $pages;

}
