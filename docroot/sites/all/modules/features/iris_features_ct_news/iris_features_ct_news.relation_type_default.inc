<?php
/**
 * @file
 * iris_features_ct_news.relation_type_default.inc
 */

/**
 * Implements hook_relation_default_relation_types().
 */
function iris_features_ct_news_relation_default_relation_types() {
  $export = array();

  $relation_type = new stdClass();
  $relation_type->disabled = FALSE; /* Edit this to true to make a default relation_type disabled initially */
  $relation_type->api_version = 1;
  $relation_type->relation_type = 'news_article_status';
  $relation_type->label = 'News article status';
  $relation_type->reverse_label = 'Store';
  $relation_type->directional = 1;
  $relation_type->transitive = 0;
  $relation_type->r_unique = 1;
  $relation_type->min_arity = 2;
  $relation_type->max_arity = 2;
  $relation_type->source_bundles = array(
    0 => 'node:news',
  );
  $relation_type->target_bundles = array(
    0 => 'taxonomy_term:store',
  );
  $export['news_article_status'] = $relation_type;

  return $export;
}
