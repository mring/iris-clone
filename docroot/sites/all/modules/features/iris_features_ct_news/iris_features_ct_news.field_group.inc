<?php
/**
 * @file
 * iris_features_ct_news.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function iris_features_ct_news_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_3_left|node|news|form';
  $field_group->group_name = 'group_3_left';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_step_3';
  $field_group->data = array(
    'label' => 'Step 2 left',
    'weight' => '28',
    'children' => array(
      0 => 'field_date_end',
      1 => 'field_date_start',
      2 => 'field_news_type',
      3 => 'title',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Step 2 left',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'news-step-left',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_3_left|node|news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_3_right|node|news|form';
  $field_group->group_name = 'group_3_right';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_step_3';
  $field_group->data = array(
    'label' => 'Step 2 right',
    'weight' => '36',
    'children' => array(
      0 => 'field_banner_image',
      1 => 'field_promotion',
      2 => 'field_files',
      3 => 'field_links',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Step 2 right',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'news-step-right',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_3_right|node|news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_step_1|node|news|form';
  $field_group->group_name = 'group_step_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Step 1 : News type',
    'weight' => '1',
    'children' => array(
      0 => 'field_action_date_deadline',
      1 => 'field_action_news',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Step 1 : News type',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-step-1 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_step_1|node|news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_step_2|node|news|form';
  $field_group->group_name = 'group_step_2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Step 3 : Scope visibility',
    'weight' => '3',
    'children' => array(
      0 => 'field_activity_domains',
      1 => 'field_activity_sectors',
      2 => 'field_location_group',
      3 => 'field_scope_selection',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Step 3 : Scope visibility',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-step-2 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_step_2|node|news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_step_3|node|news|form';
  $field_group->group_name = 'group_step_3';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Step 2 : News content',
    'weight' => '2',
    'children' => array(
      0 => 'body',
      1 => 'group_3_left',
      2 => 'group_3_right',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Step 2 : News content',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_step_3|node|news|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Step 1 : News type');
  t('Step 2 : News content');
  t('Step 2 left');
  t('Step 2 right');
  t('Step 3 : Scope visibility');

  return $field_groups;
}
