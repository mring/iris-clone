<?php
/**
 * @file
 * iris_features_ct_news.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function iris_features_ct_news_default_rules_configuration() {
  $items = array();
  $items['rules_answer_to_comment'] = entity_import('rules_config', '{ "rules_answer_to_comment" : {
      "LABEL" : "Answer to comment",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "IRIS Notifications" ],
      "REQUIRES" : [ "rules", "iris_notifications", "comment" ],
      "ON" : { "comment_insert--comment_node_news" : { "bundle" : "comment_node_news" } },
      "IF" : [
        { "NOT data_is_empty" : { "data" : [ "comment:parent:author" ] } },
        { "NOT data_is" : { "data" : [ "comment:parent:author" ], "value" : [ "comment:author" ] } }
      ],
      "DO" : [
        { "iris_notifications_rules_action_add_notification_queue_item" : {
            "type" : "comment_answer",
            "news_article" : [ "comment:node:nid" ],
            "news_article_comment" : [ "comment:cid" ],
            "news_article_parent_comment" : [ "comment:parent:cid" ]
          }
        }
      ]
    }
  }');
  $items['rules_commenting_news'] = entity_import('rules_config', '{ "rules_commenting_news" : {
      "LABEL" : "Commenting News",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "IRIS Notifications" ],
      "REQUIRES" : [ "rules", "iris_notifications", "comment" ],
      "ON" : { "comment_insert--comment_node_news" : { "bundle" : "comment_node_news" } },
      "IF" : [ { "data_is_empty" : { "data" : [ "comment:parent:cid" ] } } ],
      "DO" : [
        { "iris_notifications_rules_action_add_notification_queue_item" : {
            "type" : "comment_news_article",
            "news_article" : [ "comment:node:nid" ],
            "news_article_comment" : [ "comment:cid" ]
          }
        }
      ]
    }
  }');
  $items['rules_updating_user_assigning_to_news'] = entity_import('rules_config', '{ "rules_updating_user_assigning_to_news" : {
      "LABEL" : "Updating User Assigning to News",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "IRIS Notifications" ],
      "REQUIRES" : [ "rules", "iris_notifications", "relation" ],
      "ON" : { "relation_update" : [] },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "relation" ],
            "type" : "relation",
            "bundle" : { "value" : { "news_article_status" : "news_article_status" } }
          }
        },
        { "entity_has_field" : { "entity" : [ "relation" ], "field" : "field_news_store_assigned_users" } },
        { "NOT data_is" : {
            "data" : [ "relation:field-news-store-assigned-users" ],
            "value" : [ "relation-unchanged:field-news-store-assigned-users" ]
          }
        }
      ],
      "DO" : [
        { "iris_notifications_rules_action_add_notification_queue_item" : {
            "type" : "news_article_assign",
            "news_article_relation" : [ "relation:rid" ],
            "news_article_change_author" : [ "site:current-user:uid" ]
          }
        }
      ]
    }
  }');
  return $items;
}
