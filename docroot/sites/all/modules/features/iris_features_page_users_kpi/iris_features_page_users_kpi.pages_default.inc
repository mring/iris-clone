<?php
/**
 * @file
 * iris_features_page_users_kpi.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_users_kpi_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'users_kpi';
  $page->task = 'page';
  $page->admin_title = 'Users KPI';
  $page->admin_description = '';
  $page->path = 'admin/users-kpi';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'view users kpi dashboard',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_users_kpi__panel';
  $handler->task = 'page';
  $handler->subtask = 'users_kpi';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 2,
          1 => 1,
          2 => 'main-row',
          3 => 3,
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(),
        'parent' => 'main',
        'class' => '',
        'hide_empty' => 0,
      ),
      2 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'top',
        ),
        'parent' => 'main',
        'class' => '',
        'hide_empty' => 0,
      ),
      'top' => array(
        'type' => 'region',
        'title' => 'Top',
        'width' => 100,
        'width_type' => '%',
        'parent' => '2',
        'class' => '',
        'hide_empty' => 0,
      ),
      3 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'row',
          1 => 'right',
        ),
        'parent' => 'main',
        'class' => 'Row',
        'hide_empty' => 0,
      ),
      'row' => array(
        'type' => 'region',
        'title' => 'Left',
        'width' => '60.02080020315784',
        'width_type' => '%',
        'parent' => '3',
        'class' => '',
        'hide_empty' => 0,
      ),
      'right' => array(
        'type' => 'region',
        'title' => 'Right',
        'width' => '39.97919979684216',
        'width_type' => '%',
        'parent' => '3',
        'class' => '',
        'hide_empty' => 0,
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'top' => NULL,
      'row' => NULL,
      'right' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Users KPI';
  $display->uuid = 'e70cefa7-f8c1-4b64-991f-158163a5268f';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-568786bb-d2c9-40b3-b4f9-ed5e51cdd6ce';
    $pane->panel = 'middle';
    $pane->type = 'iris_kpi_users_filters';
    $pane->subtype = 'iris_kpi_users_filters';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '568786bb-d2c9-40b3-b4f9-ed5e51cdd6ce';
    $display->content['new-568786bb-d2c9-40b3-b4f9-ed5e51cdd6ce'] = $pane;
    $display->panels['middle'][0] = 'new-568786bb-d2c9-40b3-b4f9-ed5e51cdd6ce';
    $pane = new stdClass();
    $pane->pid = 'new-f14ceee5-f9e4-4336-a0aa-e09cea595fdc';
    $pane->panel = 'middle';
    $pane->type = 'iris_kpi_users_total';
    $pane->subtype = 'iris_kpi_users_total';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'f14ceee5-f9e4-4336-a0aa-e09cea595fdc';
    $display->content['new-f14ceee5-f9e4-4336-a0aa-e09cea595fdc'] = $pane;
    $display->panels['middle'][1] = 'new-f14ceee5-f9e4-4336-a0aa-e09cea595fdc';
    $pane = new stdClass();
    $pane->pid = 'new-da82ed87-24ae-422b-bf40-eedfb24e101f';
    $pane->panel = 'middle';
    $pane->type = 'iris_kpi_users_per_role';
    $pane->subtype = 'iris_kpi_users_per_role';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'da82ed87-24ae-422b-bf40-eedfb24e101f';
    $display->content['new-da82ed87-24ae-422b-bf40-eedfb24e101f'] = $pane;
    $display->panels['middle'][2] = 'new-da82ed87-24ae-422b-bf40-eedfb24e101f';
    $pane = new stdClass();
    $pane->pid = 'new-8f4114b5-67e9-4d4a-a0b9-669b76a2701e';
    $pane->panel = 'middle';
    $pane->type = 'iris_kpi_users_per_company';
    $pane->subtype = 'iris_kpi_users_per_company';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '8f4114b5-67e9-4d4a-a0b9-669b76a2701e';
    $display->content['new-8f4114b5-67e9-4d4a-a0b9-669b76a2701e'] = $pane;
    $display->panels['middle'][3] = 'new-8f4114b5-67e9-4d4a-a0b9-669b76a2701e';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['users_kpi'] = $page;

  return $pages;

}
