<?php
/**
 * @file
 * iris_features_user_settings.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function iris_features_user_settings_user_default_roles() {
  $roles = array();

  // Exported role: Canal contributor.
  $roles['Canal contributor'] = array(
    'name' => 'Canal contributor',
    'weight' => 11,
  );

  // Exported role: generic contributor.
  $roles['generic contributor'] = array(
    'name' => 'generic contributor',
    'weight' => 6,
  );

  // Exported role: kpi supervisor.
  $roles['kpi supervisor'] = array(
    'name' => 'kpi supervisor',
    'weight' => 13,
  );

  // Exported role: manager.
  $roles['manager'] = array(
    'name' => 'manager',
    'weight' => 4,
  );

  // Exported role: merchandising contributor.
  $roles['merchandising contributor'] = array(
    'name' => 'merchandising contributor',
    'weight' => 10,
  );

  // Exported role: news responsible.
  $roles['news responsible'] = array(
    'name' => 'news responsible',
    'weight' => 11,
  );

  // Exported role: operation contributor.
  $roles['operation contributor'] = array(
    'name' => 'operation contributor',
    'weight' => 8,
  );

  // Exported role: operation planning responsible.
  $roles['operation planning responsible'] = array(
    'name' => 'operation planning responsible',
    'weight' => 7,
  );

  // Exported role: operation responsible.
  $roles['operation responsible'] = array(
    'name' => 'operation responsible',
    'weight' => 12,
  );

  // Exported role: operation upload and validation.
  $roles['operation upload and validation'] = array(
    'name' => 'operation upload and validation',
    'weight' => 9,
  );

  // Exported role: sales consultant.
  $roles['sales consultant'] = array(
    'name' => 'sales consultant',
    'weight' => 5,
  );

  // Exported role: webmaster.
  $roles['webmaster'] = array(
    'name' => 'webmaster',
    'weight' => 3,
  );

  return $roles;
}
