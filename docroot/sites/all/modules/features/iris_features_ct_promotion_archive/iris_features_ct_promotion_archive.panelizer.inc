<?php
/**
 * @file
 * iris_features_ct_promotion_archive.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function iris_features_ct_promotion_archive_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'promotion_archive';
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->name = 'node:promotion_archive:default';
  $panelizer->css_id = '';
  $panelizer->css_class = '';
  $panelizer->css = '';
  $panelizer->no_blocks = FALSE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'ddafda1d-59f0-4eb1-aa45-863102e59f86';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'node:promotion_archive:default';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-5b6eb35d-5364-4b30-a47b-ec5a80c2a58f';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'promotion_lines_archive_list-promotion_lines_archive_list_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5b6eb35d-5364-4b30-a47b-ec5a80c2a58f';
  $display->content['new-5b6eb35d-5364-4b30-a47b-ec5a80c2a58f'] = $pane;
  $display->panels['middle'][0] = 'new-5b6eb35d-5364-4b30-a47b-ec5a80c2a58f';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:promotion_archive:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'promotion_archive';
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->name = 'node:promotion_archive:default:default';
  $panelizer->css_id = '';
  $panelizer->css_class = '';
  $panelizer->css = '';
  $panelizer->no_blocks = FALSE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '865fe861-b592-4c91-9be2-ded492dfc1c6';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'node:promotion_archive:default:default';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-32164419-9aa1-428c-8b17-4a61d0f97db6';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'promotion_lines_archive_list-promotion_lines_archive_list_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '32164419-9aa1-428c-8b17-4a61d0f97db6';
  $display->content['new-32164419-9aa1-428c-8b17-4a61d0f97db6'] = $pane;
  $display->panels['middle'][0] = 'new-32164419-9aa1-428c-8b17-4a61d0f97db6';
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:promotion_archive:default:default'] = $panelizer;

  return $export;
}
