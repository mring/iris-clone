<?php
/**
 * @file
 * iris_features_ct_promotion_archive.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function iris_features_ct_promotion_archive_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'promotion_lines_archive_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Promotion lines archive list';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access promotions archive dashboard';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_archive_brand' => 'field_archive_brand',
    'field_archive_channel' => 'field_archive_channel',
    'nid' => 'nid',
    'field_archive_brand_1' => 'field_archive_brand_1',
    'field_archive_category' => 'field_archive_category',
    'field_archive_comment' => 'field_archive_comment',
    'field_archive_country' => 'field_archive_country',
    'field_archive_currency' => 'field_archive_currency',
    'field_archive_description' => 'field_archive_description',
    'field_archive_delivery' => 'field_archive_delivery',
    'field_archive_duration' => 'field_archive_duration',
    'field_archive_enddate' => 'field_archive_enddate',
    'field_archive_initiator' => 'field_archive_initiator',
    'field_archive_lkg' => 'field_archive_lkg',
    'field_archive_promorule' => 'field_archive_promorule',
    'field_archive_ref_validation' => 'field_archive_ref_validation',
    'field_archive_roundingrule' => 'field_archive_roundingrule',
    'field_archive_sku' => 'field_archive_sku',
    'field_archive_slot' => 'field_archive_slot',
    'field_archive_startdate' => 'field_archive_startdate',
    'field_archive_supplier' => 'field_archive_supplier',
    'field_archive_testercode' => 'field_archive_testercode',
    'field_archive_thematic' => 'field_archive_thematic',
    'field_archive_universe' => 'field_archive_universe',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_brand' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_channel' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_brand_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_category' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_comment' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_country' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_currency' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_description' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_delivery' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_duration' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_enddate' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_initiator' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_lkg' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_promorule' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_ref_validation' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_roundingrule' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_sku' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_slot' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_startdate' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_supplier' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_testercode' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_thematic' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_universe' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_field_archive_promo_lines_node']['id'] = 'reverse_field_archive_promo_lines_node';
  $handler->display->display_options['relationships']['reverse_field_archive_promo_lines_node']['table'] = 'node';
  $handler->display->display_options['relationships']['reverse_field_archive_promo_lines_node']['field'] = 'reverse_field_archive_promo_lines_node';
  $handler->display->display_options['relationships']['reverse_field_archive_promo_lines_node']['label'] = 'Archive promotion';
  $handler->display->display_options['relationships']['reverse_field_archive_promo_lines_node']['required'] = TRUE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['relationship'] = 'reverse_field_archive_promo_lines_node';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Content: Reference validation */
  $handler->display->display_options['fields']['field_archive_ref_validation']['id'] = 'field_archive_ref_validation';
  $handler->display->display_options['fields']['field_archive_ref_validation']['table'] = 'field_data_field_archive_ref_validation';
  $handler->display->display_options['fields']['field_archive_ref_validation']['field'] = 'field_archive_ref_validation';
  $handler->display->display_options['fields']['field_archive_ref_validation']['settings'] = array(
    'format_type' => 'short_without_time_fr',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Country */
  $handler->display->display_options['fields']['field_archive_country']['id'] = 'field_archive_country';
  $handler->display->display_options['fields']['field_archive_country']['table'] = 'field_data_field_archive_country';
  $handler->display->display_options['fields']['field_archive_country']['field'] = 'field_archive_country';
  /* Field: Content: Universe */
  $handler->display->display_options['fields']['field_archive_universe']['id'] = 'field_archive_universe';
  $handler->display->display_options['fields']['field_archive_universe']['table'] = 'field_data_field_archive_universe';
  $handler->display->display_options['fields']['field_archive_universe']['field'] = 'field_archive_universe';
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_archive_category']['id'] = 'field_archive_category';
  $handler->display->display_options['fields']['field_archive_category']['table'] = 'field_data_field_archive_category';
  $handler->display->display_options['fields']['field_archive_category']['field'] = 'field_archive_category';
  /* Field: Content: Start date */
  $handler->display->display_options['fields']['field_archive_startdate']['id'] = 'field_archive_startdate';
  $handler->display->display_options['fields']['field_archive_startdate']['table'] = 'field_data_field_archive_startdate';
  $handler->display->display_options['fields']['field_archive_startdate']['field'] = 'field_archive_startdate';
  $handler->display->display_options['fields']['field_archive_startdate']['settings'] = array(
    'format_type' => 'short_without_time_fr',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: End date */
  $handler->display->display_options['fields']['field_archive_enddate']['id'] = 'field_archive_enddate';
  $handler->display->display_options['fields']['field_archive_enddate']['table'] = 'field_data_field_archive_enddate';
  $handler->display->display_options['fields']['field_archive_enddate']['field'] = 'field_archive_enddate';
  $handler->display->display_options['fields']['field_archive_enddate']['settings'] = array(
    'format_type' => 'short_without_time_fr',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Duration */
  $handler->display->display_options['fields']['field_archive_duration']['id'] = 'field_archive_duration';
  $handler->display->display_options['fields']['field_archive_duration']['table'] = 'field_data_field_archive_duration';
  $handler->display->display_options['fields']['field_archive_duration']['field'] = 'field_archive_duration';
  /* Field: Content: Initiator */
  $handler->display->display_options['fields']['field_archive_initiator']['id'] = 'field_archive_initiator';
  $handler->display->display_options['fields']['field_archive_initiator']['table'] = 'field_data_field_archive_initiator';
  $handler->display->display_options['fields']['field_archive_initiator']['field'] = 'field_archive_initiator';
  /* Field: Content: Thematic */
  $handler->display->display_options['fields']['field_archive_thematic']['id'] = 'field_archive_thematic';
  $handler->display->display_options['fields']['field_archive_thematic']['table'] = 'field_data_field_archive_thematic';
  $handler->display->display_options['fields']['field_archive_thematic']['field'] = 'field_archive_thematic';
  /* Field: Content: Slot */
  $handler->display->display_options['fields']['field_archive_slot']['id'] = 'field_archive_slot';
  $handler->display->display_options['fields']['field_archive_slot']['table'] = 'field_data_field_archive_slot';
  $handler->display->display_options['fields']['field_archive_slot']['field'] = 'field_archive_slot';
  /* Field: Content: Supplier */
  $handler->display->display_options['fields']['field_archive_supplier']['id'] = 'field_archive_supplier';
  $handler->display->display_options['fields']['field_archive_supplier']['table'] = 'field_data_field_archive_supplier';
  $handler->display->display_options['fields']['field_archive_supplier']['field'] = 'field_archive_supplier';
  /* Field: Content: Brand */
  $handler->display->display_options['fields']['field_archive_brand_1']['id'] = 'field_archive_brand_1';
  $handler->display->display_options['fields']['field_archive_brand_1']['table'] = 'field_data_field_archive_brand';
  $handler->display->display_options['fields']['field_archive_brand_1']['field'] = 'field_archive_brand';
  /* Field: Content: Comment */
  $handler->display->display_options['fields']['field_archive_comment']['id'] = 'field_archive_comment';
  $handler->display->display_options['fields']['field_archive_comment']['table'] = 'field_data_field_archive_comment';
  $handler->display->display_options['fields']['field_archive_comment']['field'] = 'field_archive_comment';
  /* Field: Content: Direct delivery */
  $handler->display->display_options['fields']['field_archive_delivery']['id'] = 'field_archive_delivery';
  $handler->display->display_options['fields']['field_archive_delivery']['table'] = 'field_data_field_archive_delivery';
  $handler->display->display_options['fields']['field_archive_delivery']['field'] = 'field_archive_delivery';
  /* Field: Content: SAP SKU */
  $handler->display->display_options['fields']['field_archive_sku']['id'] = 'field_archive_sku';
  $handler->display->display_options['fields']['field_archive_sku']['table'] = 'field_data_field_archive_sku';
  $handler->display->display_options['fields']['field_archive_sku']['field'] = 'field_archive_sku';
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_archive_description']['id'] = 'field_archive_description';
  $handler->display->display_options['fields']['field_archive_description']['table'] = 'field_data_field_archive_description';
  $handler->display->display_options['fields']['field_archive_description']['field'] = 'field_archive_description';
  /* Field: Content: Tester code */
  $handler->display->display_options['fields']['field_archive_testercode']['id'] = 'field_archive_testercode';
  $handler->display->display_options['fields']['field_archive_testercode']['table'] = 'field_data_field_archive_testercode';
  $handler->display->display_options['fields']['field_archive_testercode']['field'] = 'field_archive_testercode';
  /* Field: Content: LKg */
  $handler->display->display_options['fields']['field_archive_lkg']['id'] = 'field_archive_lkg';
  $handler->display->display_options['fields']['field_archive_lkg']['table'] = 'field_data_field_archive_lkg';
  $handler->display->display_options['fields']['field_archive_lkg']['field'] = 'field_archive_lkg';
  /* Field: Content: Promotion rule */
  $handler->display->display_options['fields']['field_archive_promorule']['id'] = 'field_archive_promorule';
  $handler->display->display_options['fields']['field_archive_promorule']['table'] = 'field_data_field_archive_promorule';
  $handler->display->display_options['fields']['field_archive_promorule']['field'] = 'field_archive_promorule';
  /* Field: Content: Currency */
  $handler->display->display_options['fields']['field_archive_currency']['id'] = 'field_archive_currency';
  $handler->display->display_options['fields']['field_archive_currency']['table'] = 'field_data_field_archive_currency';
  $handler->display->display_options['fields']['field_archive_currency']['field'] = 'field_archive_currency';
  /* Field: Content: Rounding rule */
  $handler->display->display_options['fields']['field_archive_roundingrule']['id'] = 'field_archive_roundingrule';
  $handler->display->display_options['fields']['field_archive_roundingrule']['table'] = 'field_data_field_archive_roundingrule';
  $handler->display->display_options['fields']['field_archive_roundingrule']['field'] = 'field_archive_roundingrule';
  /* Field: Content: Sales price A1 */
  $handler->display->display_options['fields']['field_archive_salesprice_a1']['id'] = 'field_archive_salesprice_a1';
  $handler->display->display_options['fields']['field_archive_salesprice_a1']['table'] = 'field_data_field_archive_salesprice_a1';
  $handler->display->display_options['fields']['field_archive_salesprice_a1']['field'] = 'field_archive_salesprice_a1';
  /* Field: Content: Promo sales prices A1 */
  $handler->display->display_options['fields']['field_archive_promosalesprice_a1']['id'] = 'field_archive_promosalesprice_a1';
  $handler->display->display_options['fields']['field_archive_promosalesprice_a1']['table'] = 'field_data_field_archive_promosalesprice_a1';
  $handler->display->display_options['fields']['field_archive_promosalesprice_a1']['field'] = 'field_archive_promosalesprice_a1';
  /* Field: Content: Discount amount A1 */
  $handler->display->display_options['fields']['field_archive_discount_amount_a1']['id'] = 'field_archive_discount_amount_a1';
  $handler->display->display_options['fields']['field_archive_discount_amount_a1']['table'] = 'field_data_field_archive_discount_amount_a1';
  $handler->display->display_options['fields']['field_archive_discount_amount_a1']['field'] = 'field_archive_discount_amount_a1';
  /* Field: Content: Percentage discount A1 */
  $handler->display->display_options['fields']['field_archive_perc_discount_a1']['id'] = 'field_archive_perc_discount_a1';
  $handler->display->display_options['fields']['field_archive_perc_discount_a1']['table'] = 'field_data_field_archive_perc_discount_a1';
  $handler->display->display_options['fields']['field_archive_perc_discount_a1']['field'] = 'field_archive_perc_discount_a1';
  /* Field: Content: Sales price A2 */
  $handler->display->display_options['fields']['field_archive_salesprice_a2']['id'] = 'field_archive_salesprice_a2';
  $handler->display->display_options['fields']['field_archive_salesprice_a2']['table'] = 'field_data_field_archive_salesprice_a2';
  $handler->display->display_options['fields']['field_archive_salesprice_a2']['field'] = 'field_archive_salesprice_a2';
  /* Field: Content: Promo sales prices A2 */
  $handler->display->display_options['fields']['field_archive_promosalesprice_a2']['id'] = 'field_archive_promosalesprice_a2';
  $handler->display->display_options['fields']['field_archive_promosalesprice_a2']['table'] = 'field_data_field_archive_promosalesprice_a2';
  $handler->display->display_options['fields']['field_archive_promosalesprice_a2']['field'] = 'field_archive_promosalesprice_a2';
  /* Field: Content: Discount amount A2 */
  $handler->display->display_options['fields']['field_archive_discount_amount_a2']['id'] = 'field_archive_discount_amount_a2';
  $handler->display->display_options['fields']['field_archive_discount_amount_a2']['table'] = 'field_data_field_archive_discount_amount_a2';
  $handler->display->display_options['fields']['field_archive_discount_amount_a2']['field'] = 'field_archive_discount_amount_a2';
  /* Field: Content: Percentage discount A2 */
  $handler->display->display_options['fields']['field_archive_perc_discount_a2']['id'] = 'field_archive_perc_discount_a2';
  $handler->display->display_options['fields']['field_archive_perc_discount_a2']['table'] = 'field_data_field_archive_perc_discount_a2';
  $handler->display->display_options['fields']['field_archive_perc_discount_a2']['field'] = 'field_archive_perc_discount_a2';
  /* Field: Content: Sales price LKg A1 */
  $handler->display->display_options['fields']['field_archive_salespricelkg_a1']['id'] = 'field_archive_salespricelkg_a1';
  $handler->display->display_options['fields']['field_archive_salespricelkg_a1']['table'] = 'field_data_field_archive_salespricelkg_a1';
  $handler->display->display_options['fields']['field_archive_salespricelkg_a1']['field'] = 'field_archive_salespricelkg_a1';
  /* Field: Content: Sales price LKg A2 */
  $handler->display->display_options['fields']['field_archive_salespricelkg_a2']['id'] = 'field_archive_salespricelkg_a2';
  $handler->display->display_options['fields']['field_archive_salespricelkg_a2']['table'] = 'field_data_field_archive_salespricelkg_a2';
  $handler->display->display_options['fields']['field_archive_salespricelkg_a2']['field'] = 'field_archive_salespricelkg_a2';
  /* Field: Content: Ref amount A1 */
  $handler->display->display_options['fields']['field_archive_refamount_a1']['id'] = 'field_archive_refamount_a1';
  $handler->display->display_options['fields']['field_archive_refamount_a1']['table'] = 'field_data_field_archive_refamount_a1';
  $handler->display->display_options['fields']['field_archive_refamount_a1']['field'] = 'field_archive_refamount_a1';
  /* Field: Content: Ref amount A2 */
  $handler->display->display_options['fields']['field_archive_refamount_a2']['id'] = 'field_archive_refamount_a2';
  $handler->display->display_options['fields']['field_archive_refamount_a2']['table'] = 'field_data_field_archive_refamount_a2';
  $handler->display->display_options['fields']['field_archive_refamount_a2']['field'] = 'field_archive_refamount_a2';
  /* Field: Content: Channel */
  $handler->display->display_options['fields']['field_archive_channel']['id'] = 'field_archive_channel';
  $handler->display->display_options['fields']['field_archive_channel']['table'] = 'field_data_field_archive_channel';
  $handler->display->display_options['fields']['field_archive_channel']['field'] = 'field_archive_channel';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'reverse_field_archive_promo_lines_node';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'promotion_line_archive' => 'promotion_line_archive',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'promotion_lines_archive_list_pane');
  $handler->display->display_options['argument_input'] = array(
    'nid' => array(
      'type' => 'none',
      'context' => 'entity:node.feed-nid',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Content: Nid',
    ),
  );
  $handler->display->display_options['inherit_panels_path'] = '1';
  $translatables['promotion_lines_archive_list'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Archive promotion'),
    t('Nid'),
    t('Reference validation'),
    t('Country'),
    t('Universe'),
    t('Category'),
    t('Start date'),
    t('End date'),
    t('Duration'),
    t('Initiator'),
    t('Thematic'),
    t('Slot'),
    t('Supplier'),
    t('Brand'),
    t('Comment'),
    t('Direct delivery'),
    t('SAP SKU'),
    t('Description'),
    t('Tester code'),
    t('LKg'),
    t('Promotion rule'),
    t('Currency'),
    t('Rounding rule'),
    t('Sales price A1'),
    t('Promo sales prices A1'),
    t('Discount amount A1'),
    t('Percentage discount A1'),
    t('Sales price A2'),
    t('Promo sales prices A2'),
    t('Discount amount A2'),
    t('Percentage discount A2'),
    t('Sales price LKg A1'),
    t('Sales price LKg A2'),
    t('Ref amount A1'),
    t('Ref amount A2'),
    t('Channel'),
    t('All'),
    t('Content pane'),
    t('View panes'),
  );
  $export['promotion_lines_archive_list'] = $view;

  return $export;
}
