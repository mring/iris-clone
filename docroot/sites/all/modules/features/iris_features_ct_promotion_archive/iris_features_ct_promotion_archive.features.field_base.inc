<?php
/**
 * @file
 * iris_features_ct_promotion_archive.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function iris_features_ct_promotion_archive_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_archive_promo_lines'.
  $field_bases['field_archive_promo_lines'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_archive_promo_lines',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'unique_field_behavior' => array(
            'status' => 0,
          ),
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'promotion_line_archive' => 'promotion_line_archive',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
