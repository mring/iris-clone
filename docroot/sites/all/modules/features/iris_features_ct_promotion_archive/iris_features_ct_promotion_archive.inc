<?php
/**
 * @file
 * Contains helping functions.
 */

/**
 * Checks whether promotion already exists.
 *
 * @param string $groupment
 *   From 'Promotion line archive': Slot+Country+Universe+Category+Thematic+Start date+End date+Supplier+Reference validation
.
 */
function iris_features_ct_promotion_archive_existing_promotion_archive($groupment) {
  return db_select('field_data_field_groupment', 'fdfg')
    ->fields('fdfg', array('entity_id'))
    ->condition('field_groupment_value', $groupment)
    ->execute()
    ->fetchField();
}

/**
 * Create groupment for promotion line archive.
 *
 * @param object $node
 *   Promotion line archive node.
 *
 * @throws Exception
 *   If any of groupment item is missing.
 *
 * @return array
 *   Array containing concatenated groupment string and used for this items.
 */
function iris_features_ct_promotion_archive_create_groupment($node) {
  try {
    $emw_node = entity_metadata_wrapper('node', $node);

    // Promotion line archive values.
    $slot = $emw_node->field_archive_slot->value();
    $country = $emw_node->field_archive_country->value();
    $universe = $emw_node->field_archive_universe->value();
    $category = $emw_node->field_archive_category->value();
    $thematic = $emw_node->field_archive_thematic->value();
    $start_date = $emw_node->field_archive_startdate->value();
    $end_date = $emw_node->field_archive_enddate->value();
    $supplier = $emw_node->field_archive_supplier->value();
    $ref_valid = $emw_node->field_archive_ref_validation->value();

    // Array to create groupment.
    $groupment_items = array(
      'slot'       => $slot,
      'country'    => $country,
      'universe'   => $universe,
      'category'   => $category,
      'thematic'   => $thematic,
      'start_date' => $start_date,
      'end_date'   => $end_date,
      'supplier'   => $supplier,
      'ref_valid'  => $ref_valid,
    );

    foreach ($groupment_items as $key => $value) {
      if (!isset($value)) {
        throw new Exception(t('Value !value is missing', array('!value' => $key)));
      }
    }

    $groupment = implode('_', $groupment_items);

    return array($groupment, $groupment_items);
  }
  catch (EntityMetadataWrapperException $exc) {
    watchdog(
      'iris_features_ct_promotion_archive',
      'EntityMetadataWrapper exception in %function() <pre>@trace</pre>',
      array('%function' => __FUNCTION__, '@trace' => $exc->getTraceAsString()),
      WATCHDOG_ERROR
    );
  };
}

/**
 * Creates new promotion archive if doesn't exists or adds promotion line archive to old one.
 *
 * @param array $item
 *   Array containing necessary data.
 */
function iris_features_ct_promotion_archive_add_edit_promotion_archive($item) {
  global $user;
  $groupment = $item['groupment'];
  $groupment_items = $item['groupment_items'];
  $node = $item['node'];

  $promotion_id = iris_features_ct_promotion_archive_existing_promotion_archive($groupment);

  try {
    if ($promotion_id) {
      // Add promotion line archive to an existing promotion.
      $promotion = node_load($promotion_id);
      $emw_promotion = entity_metadata_wrapper('node', $promotion);
      $emw_promotion->field_archive_promo_lines[] = $node->nid;
    }
    else {
      // Create new promotion archive.
      $promotion = entity_create('node', array('type' => 'promotion_archive'));
      $promotion->uid = $user->uid;
      $promotion->title = $node->title;

      $emw_promotion = entity_metadata_wrapper('node', $promotion);
      $emw_promotion->field_archive_promo_lines->set(array($node->nid));
      $emw_promotion->field_archive_slot = $groupment_items['slot'];
      $emw_promotion->field_archive_country = $groupment_items['country'];
      $emw_promotion->field_archive_universe = $groupment_items['universe'];
      $emw_promotion->field_archive_category = $groupment_items['category'];
      $emw_promotion->field_archive_thematic = $groupment_items['thematic'];
      $emw_promotion->field_archive_startdate = $groupment_items['start_date'];
      $emw_promotion->field_archive_enddate = $groupment_items['end_date'];
      $emw_promotion->field_archive_supplier = $groupment_items['supplier'];
      $emw_promotion->field_archive_ref_validation = $groupment_items['ref_valid'];
      $emw_promotion->field_groupment = $groupment;
    }
    $emw_promotion->save();
  }
  catch (EntityMetadataWrapperException $exc) {
    watchdog(
      'iris_features_ct_promotion_archive',
      'EntityMetadataWrapper exception in %function() <pre>@trace</pre>',
      array('%function' => __FUNCTION__, '@trace' => $exc->getTraceAsString()),
      WATCHDOG_ERROR
    );
  }
}

/**
 * Removes promotion line from promotion after groupment change.
 *
 * @param array $item
 *   Array containing necessary data.
 */
function iris_features_ct_promotion_archive_rm_from_original_promotion_archive($item) {
  $original_groupment = $item['original_groupment'];
  $original_node = $item['original_node'];
  $original_promotion_id = iris_features_ct_promotion_archive_existing_promotion_archive($original_groupment);
  $original_promotion = node_load($original_promotion_id);

  try {
    $emw_original_promotion = entity_metadata_wrapper('node', $original_promotion);
    $promotion_lines_ids = $emw_original_promotion->field_archive_promo_lines->raw();

    foreach ($promotion_lines_ids as $delta => $promotion_line_id) {
      if ($promotion_line_id == $original_node->nid) {
        $emw_original_promotion->field_promotion_lines[$delta]->set(NULL);
        unset($promotion_lines_ids[$delta]);
        break;
      }
    }

    if (!empty($promotion_lines_ids)) {
      $emw_original_promotion->save();
    }
    else {
      $emw_original_promotion->delete();
    }
  }
  catch (EntityMetadataWrapperException $exc) {
    watchdog(
      'iris_features_ct_promotion_archive',
      'EntityMetadataWrapper exception in %function() <pre>@trace</pre>',
      array('%function' => __FUNCTION__, '@trace' => $exc->getTraceAsString()),
      WATCHDOG_ERROR
    );
  }
}
