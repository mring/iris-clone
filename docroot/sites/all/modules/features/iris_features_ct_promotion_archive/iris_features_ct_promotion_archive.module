<?php
/**
 * @file
 * Code for the IRIS Features: CT Promotion archive feature.
 */

include_once 'iris_features_ct_promotion_archive.features.inc';
include_once 'iris_features_ct_promotion_archive.inc';

/**
 * Implements hook_node_insert().
 */
function iris_features_ct_promotion_archive_node_insert($node) {
  if ($node->type == 'promotion_line_archive') {
    list($groupment, $groupment_items) = iris_features_ct_promotion_archive_create_groupment($node);

    $queue_add_edit_promotion_archive = DrupalQueue::get('add_edit_promotion_archive');
    $item = array(
      'groupment' => $groupment,
      'groupment_items' => $groupment_items,
      'node' => $node,
    );
    $queue_add_edit_promotion_archive->createItem($item);
  }
}

/**
 * Implements hook_node_update().
 */
function iris_features_ct_promotion_archive_node_update($node) {
  if ($node->type == 'promotion_line_archive') {
    $original_node = $node->original;
    list($original_groupment, $original_groupment_items) = iris_features_ct_promotion_archive_create_groupment($original_node);
    list($groupment, $groupment_items) = iris_features_ct_promotion_archive_create_groupment($node);

    if ($original_groupment !== $groupment) {
      // Remove promotion line from original promotion.
      $queue_rm_from_original_promotion_archive = DrupalQueue::get('rm_from_original_promotion_archive');
      $item = array(
        'original_groupment' => $original_groupment,
        'original_node' => $original_node,
      );
      $queue_rm_from_original_promotion_archive->createItem($item);

      // Create new promotion or add to an existing.
      $queue_add_edit_promotion_archive = DrupalQueue::get('add_edit_promotion_archive');
      $item = array(
        'groupment' => $groupment,
        'groupment_items' => $groupment_items,
        'node' => $node,
      );
      $queue_add_edit_promotion_archive->createItem($item);
    }
  }
}

/**
 * Implements hook_node_delete().
 */
function iris_features_ct_promotion_archive_node_delete($node) {
  if ($node->type == 'promotion_line_archive') {
    list($groupment, $groupment_items) = iris_features_ct_promotion_archive_create_groupment($node);
    $queue_rm_from_original_promotion_archive = DrupalQueue::get('rm_from_original_promotion_archive');
    $item = array(
      'original_groupment' => $groupment,
      'original_node' => $node,
    );
    $queue_rm_from_original_promotion_archive->createItem($item);
  }
}

/**
 * Implements hook_cron_queue_info().
 */
function iris_features_ct_promotion_archive_cron_queue_info() {
  $queues['add_edit_promotion_archive'] = array(
    'worker callback' => 'iris_features_ct_promotion_archive_add_edit_promotion_archive',
    'time' => 60,
  );
  $queues['rm_from_original_promotion_archive'] = array(
    'worker callback' => 'iris_features_ct_promotion_archive_rm_from_original_promotion_archive',
    'time' => 60,
  );

  return $queues;
}
