<?php
/**
 * @file
 * iris_features_ct_promotion_archive.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_ct_promotion_archive_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function iris_features_ct_promotion_archive_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function iris_features_ct_promotion_archive_node_info() {
  $items = array(
    'promotion_archive' => array(
      'name' => t('Promotion archive'),
      'base' => 'node_content',
      'description' => t('The promotions archive are a group of promotion lines archive.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_themekey_features_rule_chain().
 */
function iris_features_ct_promotion_archive_themekey_features_rule_chain() {
if (!defined('THEMEKEY_PAGECACHE_UNSUPPORTED')) {
    define('THEMEKEY_PAGECACHE_UNSUPPORTED', 0);
    define('THEMEKEY_PAGECACHE_SUPPORTED', 1);
    define('THEMEKEY_PAGECACHE_TIMEBASED', 2);
  }
$rules = array(
  0 => array(
    'rule' => array(
      'property' => 'node:type',
      'operator' => '=',
      'value' => 'promotion_archive',
      'theme' => 'ThemeKeyAdminTheme',
      'enabled' => 1,
      'wildcards' => 'a:0:{}',
      'module' => 'iris_features_ct_promotion_archive',
    ),
    'string' => '"node:type = promotion_archive >>> ThemeKeyAdminTheme"',
    'childs' => array(),
  ),
);

return $rules;
}
