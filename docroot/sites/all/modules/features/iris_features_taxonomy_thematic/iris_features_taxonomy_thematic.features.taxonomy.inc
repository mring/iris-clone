<?php
/**
 * @file
 * iris_features_taxonomy_thematic.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_thematic_taxonomy_default_vocabularies() {
  return array(
    'thematic' => array(
      'name' => '​Thematic',
      'machine_name' => 'thematic',
      'description' => 'A thematic is a promotion category. The taxonomy is populated during the promotion import process.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 4,
    ),
  );
}
