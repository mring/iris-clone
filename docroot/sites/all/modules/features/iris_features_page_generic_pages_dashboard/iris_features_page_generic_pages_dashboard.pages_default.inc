<?php
/**
 * @file
 * iris_features_page_generic_pages_dashboard.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_generic_pages_dashboard_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'pages_dashboard';
  $page->task = 'page';
  $page->admin_title = 'Pages dashboard';
  $page->admin_description = '';
  $page->path = 'admin/pages/pages-dashboard';
  $page->access = array(
    'logic' => 'and',
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'access pages dashboard',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_pages_dashboard__panel';
  $handler->task = 'page';
  $handler->subtask = 'pages_dashboard';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Generic pages dashboard',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Generic pages dashboard',
    'panels_breadcrumbs_paths' => 'admin/pages/pages-dashboard',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'left',
          1 => 'right',
        ),
        'parent' => 'main',
        'class' => '',
        'hide_empty' => 0,
      ),
      'left' => array(
        'type' => 'region',
        'title' => 'Left',
        'width' => 50,
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
        'hide_empty' => 0,
      ),
      'right' => array(
        'type' => 'region',
        'title' => 'Right',
        'width' => 50,
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
        'hide_empty' => 0,
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
    'left' => array(
      'style' => '-1',
    ),
  );
  $display->cache = array();
  $display->title = 'Generic pages dashboard';
  $display->uuid = 'e5c7587d-6c2c-4779-bad7-c9bbce0f4114';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-7ae35461-f32f-4a26-8a92-3638b94bb336';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'pages_dashboard-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7ae35461-f32f-4a26-8a92-3638b94bb336';
    $display->content['new-7ae35461-f32f-4a26-8a92-3638b94bb336'] = $pane;
    $display->panels['center'][0] = 'new-7ae35461-f32f-4a26-8a92-3638b94bb336';
    $pane = new stdClass();
    $pane->pid = 'new-dd018d2b-3c3a-456a-b65c-2f70ddc693c8';
    $pane->panel = 'right';
    $pane->type = 'node_created_date';
    $pane->subtype = 'node_created_date';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link_prefix' => '<ul class="action-links"><li>
',
      'link_suffix' => '</li></ul>',
      'link_title' => 'Create a Playbook file',
      'link_link' => 'node/add/page',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'dd018d2b-3c3a-456a-b65c-2f70ddc693c8';
    $display->content['new-dd018d2b-3c3a-456a-b65c-2f70ddc693c8'] = $pane;
    $display->panels['right'][0] = 'new-dd018d2b-3c3a-456a-b65c-2f70ddc693c8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-7ae35461-f32f-4a26-8a92-3638b94bb336';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['pages_dashboard'] = $page;

  return $pages;

}
