<?php
/**
 * @file
 * iris_features_page_merchandising_dashboard.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_merchandising_dashboard_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'merchandising_dashboard';
  $page->task = 'page';
  $page->admin_title = 'Merchandising dashboard';
  $page->admin_description = 'The Merchandising dashboard lists all the Merchandising element created.';
  $page->path = 'admin/merchandising/merchandising-dashboard';
  $page->access = array(
    'logic' => 'and',
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'access merch dashboard',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_merchandising_dashboard__panel';
  $handler->task = 'page';
  $handler->subtask = 'merchandising_dashboard';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'access' => array(
      'logic' => 'and',
    ),
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Merchandising dashboard',
    'panels_breadcrumbs_paths' => 'admin/merchandising/merchandising-dashboard',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Merchandising dashboard';
  $display->uuid = '4f65db0f-8f0e-4edf-b0cc-9f5c347cd69a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a65f6c87-b6a2-49e2-8228-bfdb4ccdaa10';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'merchandasing_dashboard-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a65f6c87-b6a2-49e2-8228-bfdb4ccdaa10';
    $display->content['new-a65f6c87-b6a2-49e2-8228-bfdb4ccdaa10'] = $pane;
    $display->panels['center'][0] = 'new-a65f6c87-b6a2-49e2-8228-bfdb4ccdaa10';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-a65f6c87-b6a2-49e2-8228-bfdb4ccdaa10';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['merchandising_dashboard'] = $page;

  return $pages;

}
