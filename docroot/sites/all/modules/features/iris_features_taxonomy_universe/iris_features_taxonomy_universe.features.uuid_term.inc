<?php
/**
 * @file
 * iris_features_taxonomy_universe.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function iris_features_taxonomy_universe_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'ATGC',
    'description' => '<p><strong>Alcohol Tobacco Gastronomy Confectionery</strong></p>
',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '85a338be-8cca-4ad9-b0ca-b11706b1355e',
    'vocabulary_machine_name' => 'universe',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'PC',
    'description' => '<p><strong>Perfume Cosmetic</strong></p>
',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '8e17e650-0430-4aa3-81b7-5c702ac9d185',
    'vocabulary_machine_name' => 'universe',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'MODE',
    'description' => '<p><strong>Mode</strong></p>
',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '8fb1c726-e6e9-41e6-a477-5590fb674500',
    'vocabulary_machine_name' => 'universe',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'SPE',
    'description' => '<p><strong>Specialties</strong></p>
',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'b898961e-673a-423f-b61d-5fa49ddb8987',
    'vocabulary_machine_name' => 'universe',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
