<?php
/**
 * @file
 * iris_features_taxonomy_universe.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_universe_taxonomy_default_vocabularies() {
  return array(
    'universe' => array(
      'name' => '​Universe',
      'machine_name' => 'universe',
      'description' => 'The universe is used to classify a news.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 5,
    ),
  );
}
