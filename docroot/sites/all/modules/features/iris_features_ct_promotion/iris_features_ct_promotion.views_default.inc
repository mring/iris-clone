<?php
/**
 * @file
 * iris_features_ct_promotion.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function iris_features_ct_promotion_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'stores_from_promotion';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Stores from Promotion';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Stores';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: SLOT */
  $handler->display->display_options['relationships']['field_promo_slot_tid']['id'] = 'field_promo_slot_tid';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['table'] = 'field_data_field_promo_slot';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['field'] = 'field_promo_slot_tid';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['ui_name'] = 'SLOT';
  $handler->display->display_options['relationships']['field_promo_slot_tid']['label'] = 'SLOT';
  /* Relationship: OG MEMBERSHIP */
  $handler->display->display_options['relationships']['og_membership_rel']['id'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['relationships']['og_membership_rel']['field'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['relationship'] = 'field_promo_slot_tid';
  $handler->display->display_options['relationships']['og_membership_rel']['ui_name'] = 'OG MEMBERSHIP';
  $handler->display->display_options['relationships']['og_membership_rel']['label'] = 'OG MEMBERSHIP';
  /* Relationship: GROUP */
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group']['id'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group']['table'] = 'og_membership';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group']['field'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group']['relationship'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group']['ui_name'] = 'GROUP';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group']['label'] = 'GROUP';
  /* Field: Taxonomy term: Code SAP */
  $handler->display->display_options['fields']['field_taxonomy_code']['id'] = 'field_taxonomy_code';
  $handler->display->display_options['fields']['field_taxonomy_code']['table'] = 'field_data_field_taxonomy_code';
  $handler->display->display_options['fields']['field_taxonomy_code']['field'] = 'field_taxonomy_code';
  $handler->display->display_options['fields']['field_taxonomy_code']['relationship'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['fields']['field_taxonomy_code']['group_type'] = 'group_concat_dis';
  $handler->display->display_options['fields']['field_taxonomy_code']['label'] = '';
  $handler->display->display_options['fields']['field_taxonomy_code']['element_type'] = '0';
  $handler->display->display_options['fields']['field_taxonomy_code']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_taxonomy_code']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_taxonomy_code']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_taxonomy_code']['type'] = 'text_plain';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['nid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['nid']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['nid']['validate_options']['types'] = array(
    'promotion' => 'promotion',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'promotion_stores_pane');
  $handler->display->display_options['pane_title'] = 'STORES';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 0;
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'nid' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Content: Nid',
    ),
  );
  $translatables['stores_from_promotion'] = array(
    t('Master'),
    t('Stores'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('SLOT'),
    t('OG MEMBERSHIP'),
    t('GROUP'),
    t('All'),
    t('Content pane'),
    t('STORES'),
    t('View panes'),
  );
  $export['stores_from_promotion'] = $view;

  return $export;
}
