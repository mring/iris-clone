<?php
/**
 * @file
 * iris_features_ct_promotion.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_ct_promotion_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function iris_features_ct_promotion_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function iris_features_ct_promotion_node_info() {
  $items = array(
    'promotion' => array(
      'name' => t('Promotion'),
      'base' => 'node_content',
      'description' => t('The promotions are a group of promotion lines. They are generated automatically at the end of the promotion lines import.'),
      'has_title' => '1',
      'title_label' => t('Promotion title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
