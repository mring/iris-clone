<?php
/**
 * @file
 * iris_features_settings.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function iris_features_settings_default_rules_configuration() {
  $items = array();
  $items['rules_add_consultant_role'] = entity_import('rules_config', '{ "rules_add_consultant_role" : {
      "LABEL" : "Add consultant role",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_insert" : [] },
      "DO" : [
        { "user_add_role" : { "account" : [ "account" ], "roles" : { "value" : { "12" : "12" } } } }
      ]
    }
  }');
  $items['rules_anonnymous_redirect'] = entity_import('rules_config', '{ "rules_anonnymous_redirect" : {
      "LABEL" : "Anonymous redirect",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "init" : [] },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "1" : "1" } }
          }
        },
        { "NOT text_matches" : { "text" : [ "site:current-page:url" ], "match" : "user\\/login" } }
      ],
      "DO" : [ { "redirect" : { "url" : "user\\/login", "destination" : "1" } } ]
    }
  }');
  return $items;
}
