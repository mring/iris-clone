<?php
/**
 * @file
 * iris_features_settings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_settings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_custom_pub_defaults().
 */
function iris_features_settings_custom_pub_defaults() {
  $options = array();
  // Exported option: node_archived
  $options['node_archived'] = array(
    'type' => 'node_archived',
    'name' => t('Archived'),
    'node_types' => array(
      'news' => t('News'),
      'promotion' => t('Promotion'),
    ),
  );

  return $options;
}

/**
 * Implements hook_fe_date_custom_date_formats().
 */
function iris_features_settings_fe_date_custom_date_formats() {
  $custom_date_formats = array();
  $custom_date_formats['d M'] = 'd M';
  $custom_date_formats['d M Y'] = 'd M Y';
  $custom_date_formats['d/m/Y'] = 'd/m/Y';
  $custom_date_formats['m/d/Y'] = 'm/d/Y';
  return $custom_date_formats;
}

/**
 * Implements hook_date_format_types().
 */
function iris_features_settings_date_format_types() {
  $format_types = array();
  // Exported date format type: news_front
  $format_types['news_front'] = 'News front';
  // Exported date format type: news_front_full
  $format_types['news_front_full'] = 'News front full';
  // Exported date format type: short_without_time
  $format_types['short_without_time'] = 'Short without time';
  // Exported date format type: short_without_time_fr
  $format_types['short_without_time_fr'] = 'Short without time FR';
  return $format_types;
}

/**
 * Implements hook_fe_date_locale_date_format().
 */
function iris_features_settings_fe_date_locale_date_format() {
  $locale_date_formats = array();

  // Exported format: long::en
  $locale_date_formats['long::en'] = array(
    'type' => 'long',
    'format' => 'l, F j, Y - H:i',
    'locales' => array(
      0 => 'en',
    ),
  );
  // Exported format: long::fr
  $locale_date_formats['long::fr'] = array(
    'type' => 'long',
    'format' => 'l, F j, Y - H:i',
    'locales' => array(
      0 => 'fr',
    ),
  );
  // Exported format: medium::en
  $locale_date_formats['medium::en'] = array(
    'type' => 'medium',
    'format' => 'D, d/m/Y - H:i',
    'locales' => array(
      0 => 'en',
    ),
  );
  // Exported format: medium::fr
  $locale_date_formats['medium::fr'] = array(
    'type' => 'medium',
    'format' => 'D, d/m/Y - H:i',
    'locales' => array(
      0 => 'fr',
    ),
  );
  // Exported format: short::en
  $locale_date_formats['short::en'] = array(
    'type' => 'short',
    'format' => 'd/m/Y - H:i',
    'locales' => array(
      0 => 'en',
    ),
  );
  // Exported format: short::fr
  $locale_date_formats['short::fr'] = array(
    'type' => 'short',
    'format' => 'd/m/Y - H:i',
    'locales' => array(
      0 => 'fr',
    ),
  );
  return $locale_date_formats;
}

/**
 * Implements hook_image_default_styles().
 */
function iris_features_settings_image_default_styles() {
  $styles = array();

  // Exported image style: news_banner.
  $styles['news_banner'] = array(
    'label' => 'News banner (width 780)',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 780,
          'height' => 352,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: user_picture.
  $styles['user_picture'] = array(
    'label' => 'user_picture_36x36',
    'effects' => array(
      5 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 36,
          'height' => 36,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
