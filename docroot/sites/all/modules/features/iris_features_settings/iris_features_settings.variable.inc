<?php
/**
 * @file
 * iris_features_settings.variable.inc
 */

/**
 * Implements hook_variable_realm_default_variables().
 */
function iris_features_settings_variable_realm_default_variables() {
$realm_variables = array();
  $realm_variables['language']['en'] = array(
  'chosen_no_results_text' => 'No results match',
  'chosen_placeholder_text_multiple' => 'Choose some options',
  'chosen_placeholder_text_single' => 'Choose an option',
);
  $realm_variables['language']['fr'] = array(
  'chosen_no_results_text' => 'Aucun résultat ne correspond',
  'chosen_placeholder_text_multiple' => 'Sélectionnez une ou plusieurs options',
  'chosen_placeholder_text_single' => 'Sélectionnez une option',
);

return $realm_variables;
}
