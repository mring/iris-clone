(function ($) {
    Drupal.behaviors.date_start_autofill = {
        attach: function (context, settings) {
            $('#edit-field-action-date-deadline-und-0-value-datepicker-popup-0').change(function () {
                var value = $(this).val();
                $('#edit-field-date-start-und-0-value-datepicker-popup-0').val(value);
            });
        }
    };
})(jQuery);