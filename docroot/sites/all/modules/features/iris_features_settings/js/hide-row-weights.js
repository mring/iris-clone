var customTableDrag = customTableDrag || {};

(function ($) {
    Drupal.behaviors.customTableDrag = {
        attach: function (context, settings) {
            //turn off service worker in admin area
            if (Drupal.settings.ajaxPageState.theme && Drupal.settings.ajaxPageState.theme != 'iris_theme') {
                if ('serviceWorker' in navigator && Drupal.settings.user_js_uid > 0) {
                    navigator.serviceWorker.getRegistrations().then(function(registrations) {
                        for(i in registrations) {
                            registrations[i].unregister();
                        } })
                }


                }
            customTableDrag.hideShowRowWeights();
        }
    };

    /**
     * Remove "Hide/Show row weights" toggle in tabledrag.
     */
    customTableDrag.hideShowRowWeights = function () {
        if (Drupal.tableDrag) {

            var initColumns = Drupal.tableDrag.prototype.initColumns;
            Drupal.tableDrag.prototype.initColumns = function () {
                initColumns.call(this);

                $('a.tabledrag-toggle-weight').unbind('click')
                    .parents('div.tabledrag-toggle-weight-wrapper')
                    .remove();
            };
        }
    };
})(jQuery);