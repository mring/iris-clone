<?php
/**
 * @file
 * iris_features_taxonomy_store.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function iris_features_taxonomy_store_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_store_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_store_pattern'] = $strongarm;

  return $export;
}
