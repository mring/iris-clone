<?php
/**
 * @file
 * iris_features_taxonomy_store.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_store_taxonomy_default_vocabularies() {
  return array(
    'store' => array(
      'name' => 'Store',
      'machine_name' => 'store',
      'description' => 'Each user from the intranet needs to be affected to one or several stores in order to determine the context of his news feed.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
