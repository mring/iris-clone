<?php
/**
 * @file
 * iris_features_taxonomy_activity_domain.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_activity_domain_taxonomy_default_vocabularies() {
  return array(
    'activity_domain' => array(
      'name' => 'Activity domain',
      'machine_name' => 'activity_domain',
      'description' => 'The activity domain is used to classify a news.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -10,
    ),
  );
}
