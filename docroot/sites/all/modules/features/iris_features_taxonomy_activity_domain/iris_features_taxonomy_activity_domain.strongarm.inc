<?php
/**
 * @file
 * iris_features_taxonomy_activity_domain.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function iris_features_taxonomy_activity_domain_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_universe_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_universe_pattern'] = $strongarm;

  return $export;
}
