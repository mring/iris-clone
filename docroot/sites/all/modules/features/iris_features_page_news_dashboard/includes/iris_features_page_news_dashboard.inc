<?php
/**
 * @file
 * Contains helping functions.
 */

/**
 * Validates exposed date filter.
 */
function _iris_validate_exposed_date_filter($form, &$form_state) {
  _iris_core_validate_range_dates($form_state, 'date_from', 'date_to');
}
