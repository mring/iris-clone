<?php
/**
 * @file
 * iris_features_favorites_flag.features.inc
 */

/**
 * Implements hook_flag_default_flags().
 */
function iris_features_favorites_flag_flag_default_flags() {
  $flags = array();
  // Exported flag: "Favorite".
  $flags['favorite'] = array(
    'entity_type' => 'node',
    'title' => 'Favorite',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'off',
    'flag_long' => 'Add to favorites',
    'flag_message' => '',
    'unflag_short' => 'on',
    'unflag_long' => 'Remove from favorites',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'diff_standard' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'module' => 'iris_features_favorites_flag',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}
