<?php
/**
 * @file
 * iris_features_page_promotions_archive_dashboard.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_promotions_archive_dashboard_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'promotions_archive_dashboard';
  $page->task = 'page';
  $page->admin_title = 'Promotions archive dashboard';
  $page->admin_description = '';
  $page->path = 'admin/promotions/promotions-archive-dashboard';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'access promotions archive dashboard',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_promotions_archive_dashboard__panel';
  $handler->task = 'page';
  $handler->subtask = 'promotions_archive_dashboard';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Promotions archive',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Wiki Boutiques archive (files upload between 01/01/2015 and 05/10/2016)';
  $display->uuid = '005376d5-428d-44d5-9417-1400814e07ac';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-7a64e7f2-2ee4-4d1e-b123-7eb6ef1e7e63';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'promotions_archive_dashboard-promotions_archive_dashboard_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7a64e7f2-2ee4-4d1e-b123-7eb6ef1e7e63';
    $display->content['new-7a64e7f2-2ee4-4d1e-b123-7eb6ef1e7e63'] = $pane;
    $display->panels['center'][0] = 'new-7a64e7f2-2ee4-4d1e-b123-7eb6ef1e7e63';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-7a64e7f2-2ee4-4d1e-b123-7eb6ef1e7e63';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['promotions_archive_dashboard'] = $page;

  return $pages;

}
