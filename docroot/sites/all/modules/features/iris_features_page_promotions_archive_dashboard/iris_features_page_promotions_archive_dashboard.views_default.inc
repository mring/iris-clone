<?php
/**
 * @file
 * iris_features_page_promotions_archive_dashboard.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function iris_features_page_promotions_archive_dashboard_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'promotions_archive_dashboard';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Promotions archive dashboard';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access promotions archive dashboard';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_archive_slot' => 'field_archive_slot',
    'field_archive_country' => 'field_archive_country',
    'field_archive_universe' => 'field_archive_universe',
    'field_archive_category' => 'field_archive_category',
    'field_archive_thematic' => 'field_archive_thematic',
    'field_archive_startdate' => 'field_archive_startdate',
    'field_archive_enddate' => 'field_archive_enddate',
    'field_archive_supplier' => 'field_archive_supplier',
    'field_archive_ref_validation' => 'field_archive_ref_validation',
    'view_node' => 'view_node',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_archive_slot' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_country' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_universe' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_category' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_thematic' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_startdate' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_enddate' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_supplier' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_archive_ref_validation' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'view_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: Slot */
  $handler->display->display_options['fields']['field_archive_slot']['id'] = 'field_archive_slot';
  $handler->display->display_options['fields']['field_archive_slot']['table'] = 'field_data_field_archive_slot';
  $handler->display->display_options['fields']['field_archive_slot']['field'] = 'field_archive_slot';
  /* Field: Content: Country */
  $handler->display->display_options['fields']['field_archive_country']['id'] = 'field_archive_country';
  $handler->display->display_options['fields']['field_archive_country']['table'] = 'field_data_field_archive_country';
  $handler->display->display_options['fields']['field_archive_country']['field'] = 'field_archive_country';
  /* Field: Content: Universe */
  $handler->display->display_options['fields']['field_archive_universe']['id'] = 'field_archive_universe';
  $handler->display->display_options['fields']['field_archive_universe']['table'] = 'field_data_field_archive_universe';
  $handler->display->display_options['fields']['field_archive_universe']['field'] = 'field_archive_universe';
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_archive_category']['id'] = 'field_archive_category';
  $handler->display->display_options['fields']['field_archive_category']['table'] = 'field_data_field_archive_category';
  $handler->display->display_options['fields']['field_archive_category']['field'] = 'field_archive_category';
  /* Field: Content: Thematic */
  $handler->display->display_options['fields']['field_archive_thematic']['id'] = 'field_archive_thematic';
  $handler->display->display_options['fields']['field_archive_thematic']['table'] = 'field_data_field_archive_thematic';
  $handler->display->display_options['fields']['field_archive_thematic']['field'] = 'field_archive_thematic';
  /* Field: Content: Start date */
  $handler->display->display_options['fields']['field_archive_startdate']['id'] = 'field_archive_startdate';
  $handler->display->display_options['fields']['field_archive_startdate']['table'] = 'field_data_field_archive_startdate';
  $handler->display->display_options['fields']['field_archive_startdate']['field'] = 'field_archive_startdate';
  $handler->display->display_options['fields']['field_archive_startdate']['settings'] = array(
    'format_type' => 'short_without_time_fr',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: End date */
  $handler->display->display_options['fields']['field_archive_enddate']['id'] = 'field_archive_enddate';
  $handler->display->display_options['fields']['field_archive_enddate']['table'] = 'field_data_field_archive_enddate';
  $handler->display->display_options['fields']['field_archive_enddate']['field'] = 'field_archive_enddate';
  $handler->display->display_options['fields']['field_archive_enddate']['settings'] = array(
    'format_type' => 'short_without_time_fr',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Supplier */
  $handler->display->display_options['fields']['field_archive_supplier']['id'] = 'field_archive_supplier';
  $handler->display->display_options['fields']['field_archive_supplier']['table'] = 'field_data_field_archive_supplier';
  $handler->display->display_options['fields']['field_archive_supplier']['field'] = 'field_archive_supplier';
  /* Field: Content: Reference validation */
  $handler->display->display_options['fields']['field_archive_ref_validation']['id'] = 'field_archive_ref_validation';
  $handler->display->display_options['fields']['field_archive_ref_validation']['table'] = 'field_data_field_archive_ref_validation';
  $handler->display->display_options['fields']['field_archive_ref_validation']['field'] = 'field_archive_ref_validation';
  $handler->display->display_options['fields']['field_archive_ref_validation']['label'] = 'Validation date';
  $handler->display->display_options['fields']['field_archive_ref_validation']['settings'] = array(
    'format_type' => 'short_without_time_fr',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = 'Action';
  $handler->display->display_options['fields']['view_node']['text'] = 'View';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'promotion_archive' => 'promotion_archive',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Slot (field_archive_slot) */
  $handler->display->display_options['filters']['field_archive_slot_value']['id'] = 'field_archive_slot_value';
  $handler->display->display_options['filters']['field_archive_slot_value']['table'] = 'field_data_field_archive_slot';
  $handler->display->display_options['filters']['field_archive_slot_value']['field'] = 'field_archive_slot_value';
  $handler->display->display_options['filters']['field_archive_slot_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_archive_slot_value']['group'] = 1;
  $handler->display->display_options['filters']['field_archive_slot_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_archive_slot_value']['expose']['operator_id'] = 'field_archive_slot_value_op';
  $handler->display->display_options['filters']['field_archive_slot_value']['expose']['label'] = 'Slot';
  $handler->display->display_options['filters']['field_archive_slot_value']['expose']['operator'] = 'field_archive_slot_value_op';
  $handler->display->display_options['filters']['field_archive_slot_value']['expose']['identifier'] = 'slot';
  $handler->display->display_options['filters']['field_archive_slot_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  /* Filter criterion: Content: Country (field_archive_country) */
  $handler->display->display_options['filters']['field_archive_country_value']['id'] = 'field_archive_country_value';
  $handler->display->display_options['filters']['field_archive_country_value']['table'] = 'field_data_field_archive_country';
  $handler->display->display_options['filters']['field_archive_country_value']['field'] = 'field_archive_country_value';
  $handler->display->display_options['filters']['field_archive_country_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_archive_country_value']['group'] = 1;
  $handler->display->display_options['filters']['field_archive_country_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_archive_country_value']['expose']['operator_id'] = 'field_archive_country_value_op';
  $handler->display->display_options['filters']['field_archive_country_value']['expose']['label'] = 'Country';
  $handler->display->display_options['filters']['field_archive_country_value']['expose']['operator'] = 'field_archive_country_value_op';
  $handler->display->display_options['filters']['field_archive_country_value']['expose']['identifier'] = 'country';
  $handler->display->display_options['filters']['field_archive_country_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  /* Filter criterion: Content: Universe (field_archive_universe) */
  $handler->display->display_options['filters']['field_archive_universe_value']['id'] = 'field_archive_universe_value';
  $handler->display->display_options['filters']['field_archive_universe_value']['table'] = 'field_data_field_archive_universe';
  $handler->display->display_options['filters']['field_archive_universe_value']['field'] = 'field_archive_universe_value';
  $handler->display->display_options['filters']['field_archive_universe_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_archive_universe_value']['group'] = 1;
  $handler->display->display_options['filters']['field_archive_universe_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_archive_universe_value']['expose']['operator_id'] = 'field_archive_universe_value_op';
  $handler->display->display_options['filters']['field_archive_universe_value']['expose']['label'] = 'Universe';
  $handler->display->display_options['filters']['field_archive_universe_value']['expose']['operator'] = 'field_archive_universe_value_op';
  $handler->display->display_options['filters']['field_archive_universe_value']['expose']['identifier'] = 'universe';
  $handler->display->display_options['filters']['field_archive_universe_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  /* Filter criterion: Content: Category (field_archive_category) */
  $handler->display->display_options['filters']['field_archive_category_value']['id'] = 'field_archive_category_value';
  $handler->display->display_options['filters']['field_archive_category_value']['table'] = 'field_data_field_archive_category';
  $handler->display->display_options['filters']['field_archive_category_value']['field'] = 'field_archive_category_value';
  $handler->display->display_options['filters']['field_archive_category_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_archive_category_value']['group'] = 1;
  $handler->display->display_options['filters']['field_archive_category_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_archive_category_value']['expose']['operator_id'] = 'field_archive_category_value_op';
  $handler->display->display_options['filters']['field_archive_category_value']['expose']['label'] = 'Category';
  $handler->display->display_options['filters']['field_archive_category_value']['expose']['operator'] = 'field_archive_category_value_op';
  $handler->display->display_options['filters']['field_archive_category_value']['expose']['identifier'] = 'category';
  $handler->display->display_options['filters']['field_archive_category_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  /* Filter criterion: Content: Thematic (field_archive_thematic) */
  $handler->display->display_options['filters']['field_archive_thematic_value']['id'] = 'field_archive_thematic_value';
  $handler->display->display_options['filters']['field_archive_thematic_value']['table'] = 'field_data_field_archive_thematic';
  $handler->display->display_options['filters']['field_archive_thematic_value']['field'] = 'field_archive_thematic_value';
  $handler->display->display_options['filters']['field_archive_thematic_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_archive_thematic_value']['group'] = 1;
  $handler->display->display_options['filters']['field_archive_thematic_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_archive_thematic_value']['expose']['operator_id'] = 'field_archive_thematic_value_op';
  $handler->display->display_options['filters']['field_archive_thematic_value']['expose']['label'] = 'Thematic';
  $handler->display->display_options['filters']['field_archive_thematic_value']['expose']['operator'] = 'field_archive_thematic_value_op';
  $handler->display->display_options['filters']['field_archive_thematic_value']['expose']['identifier'] = 'thematic';
  $handler->display->display_options['filters']['field_archive_thematic_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  /* Filter criterion: Content: Start date (field_archive_startdate) */
  $handler->display->display_options['filters']['field_archive_startdate_value']['id'] = 'field_archive_startdate_value';
  $handler->display->display_options['filters']['field_archive_startdate_value']['table'] = 'field_data_field_archive_startdate';
  $handler->display->display_options['filters']['field_archive_startdate_value']['field'] = 'field_archive_startdate_value';
  $handler->display->display_options['filters']['field_archive_startdate_value']['group'] = 1;
  $handler->display->display_options['filters']['field_archive_startdate_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_archive_startdate_value']['expose']['operator_id'] = 'field_archive_startdate_value_op';
  $handler->display->display_options['filters']['field_archive_startdate_value']['expose']['label'] = 'Start date';
  $handler->display->display_options['filters']['field_archive_startdate_value']['expose']['operator'] = 'field_archive_startdate_value_op';
  $handler->display->display_options['filters']['field_archive_startdate_value']['expose']['identifier'] = 'start_date';
  $handler->display->display_options['filters']['field_archive_startdate_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  $handler->display->display_options['filters']['field_archive_startdate_value']['form_type'] = 'date_popup';
  /* Filter criterion: Content: End date (field_archive_enddate) */
  $handler->display->display_options['filters']['field_archive_enddate_value']['id'] = 'field_archive_enddate_value';
  $handler->display->display_options['filters']['field_archive_enddate_value']['table'] = 'field_data_field_archive_enddate';
  $handler->display->display_options['filters']['field_archive_enddate_value']['field'] = 'field_archive_enddate_value';
  $handler->display->display_options['filters']['field_archive_enddate_value']['group'] = 1;
  $handler->display->display_options['filters']['field_archive_enddate_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_archive_enddate_value']['expose']['operator_id'] = 'field_archive_enddate_value_op';
  $handler->display->display_options['filters']['field_archive_enddate_value']['expose']['label'] = 'End date';
  $handler->display->display_options['filters']['field_archive_enddate_value']['expose']['operator'] = 'field_archive_enddate_value_op';
  $handler->display->display_options['filters']['field_archive_enddate_value']['expose']['identifier'] = 'end_date';
  $handler->display->display_options['filters']['field_archive_enddate_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  $handler->display->display_options['filters']['field_archive_enddate_value']['form_type'] = 'date_popup';
  /* Filter criterion: Content: Supplier (field_archive_supplier) */
  $handler->display->display_options['filters']['field_archive_supplier_value']['id'] = 'field_archive_supplier_value';
  $handler->display->display_options['filters']['field_archive_supplier_value']['table'] = 'field_data_field_archive_supplier';
  $handler->display->display_options['filters']['field_archive_supplier_value']['field'] = 'field_archive_supplier_value';
  $handler->display->display_options['filters']['field_archive_supplier_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_archive_supplier_value']['group'] = 1;
  $handler->display->display_options['filters']['field_archive_supplier_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_archive_supplier_value']['expose']['operator_id'] = 'field_archive_supplier_value_op';
  $handler->display->display_options['filters']['field_archive_supplier_value']['expose']['label'] = 'Supplier';
  $handler->display->display_options['filters']['field_archive_supplier_value']['expose']['operator'] = 'field_archive_supplier_value_op';
  $handler->display->display_options['filters']['field_archive_supplier_value']['expose']['identifier'] = 'supplier';
  $handler->display->display_options['filters']['field_archive_supplier_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  /* Filter criterion: Content: Reference validation (field_archive_ref_validation) */
  $handler->display->display_options['filters']['field_archive_ref_validation_value']['id'] = 'field_archive_ref_validation_value';
  $handler->display->display_options['filters']['field_archive_ref_validation_value']['table'] = 'field_data_field_archive_ref_validation';
  $handler->display->display_options['filters']['field_archive_ref_validation_value']['field'] = 'field_archive_ref_validation_value';
  $handler->display->display_options['filters']['field_archive_ref_validation_value']['group'] = 1;
  $handler->display->display_options['filters']['field_archive_ref_validation_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_archive_ref_validation_value']['expose']['operator_id'] = 'field_archive_ref_validation_value_op';
  $handler->display->display_options['filters']['field_archive_ref_validation_value']['expose']['label'] = 'Validation date';
  $handler->display->display_options['filters']['field_archive_ref_validation_value']['expose']['operator'] = 'field_archive_ref_validation_value_op';
  $handler->display->display_options['filters']['field_archive_ref_validation_value']['expose']['identifier'] = 'field_archive_ref_validation_value';
  $handler->display->display_options['filters']['field_archive_ref_validation_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
    12 => 0,
    7 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    8 => 0,
  );
  $handler->display->display_options['filters']['field_archive_ref_validation_value']['form_type'] = 'date_popup';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'promotions_archive_dashboard_pane');
  $handler->display->display_options['inherit_panels_path'] = '1';
  $translatables['promotions_archive_dashboard'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Slot'),
    t('Country'),
    t('Universe'),
    t('Category'),
    t('Thematic'),
    t('Start date'),
    t('End date'),
    t('Supplier'),
    t('Validation date'),
    t('Action'),
    t('View'),
    t('Content pane'),
    t('View panes'),
  );
  $export['promotions_archive_dashboard'] = $view;

  return $export;
}
