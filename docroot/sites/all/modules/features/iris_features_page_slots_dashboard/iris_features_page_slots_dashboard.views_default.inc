<?php
/**
 * @file
 * iris_features_page_slots_dashboard.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function iris_features_page_slots_dashboard_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'slots_dashboard';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Slots dashboard';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access slots dashboard';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'description_field' => 'description_field',
    'field_stores' => 'field_stores',
    'name_1' => 'name_1',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'description_field' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_stores' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No slots found.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Relationship: OG membership: Group Taxonomy term from OG membership */
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group']['id'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group']['table'] = 'og_membership';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group']['field'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group']['label'] = 'Stores';
  /* Relationship: OG membership: Group Taxonomy term from OG membership */
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_1']['id'] = 'og_membership_related_taxonomy_term_group_1';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_1']['table'] = 'og_membership';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_1']['field'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_1']['relationship'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_1']['label'] = 'Terminal';
  /* Relationship: OG membership: Group Taxonomy term from OG membership */
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_2']['id'] = 'og_membership_related_taxonomy_term_group_2';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_2']['table'] = 'og_membership';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_2']['field'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_2']['relationship'] = 'og_membership_related_taxonomy_term_group_1';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_2']['label'] = 'Store platform';
  /* Relationship: OG membership: Group Taxonomy term from OG membership */
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_3']['id'] = 'og_membership_related_taxonomy_term_group_3';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_3']['table'] = 'og_membership';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_3']['field'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_3']['relationship'] = 'og_membership_related_taxonomy_term_group_2';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_3']['label'] = 'Sales organization';
  /* Relationship: OG membership: Group Taxonomy term from OG membership */
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_4']['id'] = 'og_membership_related_taxonomy_term_group_4';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_4']['table'] = 'og_membership';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_4']['field'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_4']['relationship'] = 'og_membership_related_taxonomy_term_group_3';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_4']['label'] = 'Company code';
  /* Relationship: OG membership: Group Taxonomy term from OG membership */
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_5']['id'] = 'og_membership_related_taxonomy_term_group_5';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_5']['table'] = 'og_membership';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_5']['field'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_5']['relationship'] = 'og_membership_related_taxonomy_term_group_4';
  $handler->display->display_options['relationships']['og_membership_related_taxonomy_term_group_5']['label'] = 'Country';
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = 'Slot code';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  /* Field: Taxonomy term: Description */
  $handler->display->display_options['fields']['description_field']['id'] = 'description_field';
  $handler->display->display_options['fields']['description_field']['table'] = 'field_data_description_field';
  $handler->display->display_options['fields']['description_field']['field'] = 'description_field';
  $handler->display->display_options['fields']['description_field']['label'] = 'Slot full name';
  $handler->display->display_options['fields']['description_field']['type'] = 'text_plain';
  $handler->display->display_options['fields']['description_field']['link_to_entity'] = 0;
  /* Field: Taxonomy term: Code */
  $handler->display->display_options['fields']['field_taxonomy_code']['id'] = 'field_taxonomy_code';
  $handler->display->display_options['fields']['field_taxonomy_code']['table'] = 'field_data_field_taxonomy_code';
  $handler->display->display_options['fields']['field_taxonomy_code']['field'] = 'field_taxonomy_code';
  $handler->display->display_options['fields']['field_taxonomy_code']['relationship'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['fields']['field_taxonomy_code']['group_type'] = 'group_concat_dis';
  $handler->display->display_options['fields']['field_taxonomy_code']['label'] = 'Stores';
  $handler->display->display_options['fields']['field_taxonomy_code']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Description */
  $handler->display->display_options['fields']['description_field_1']['id'] = 'description_field_1';
  $handler->display->display_options['fields']['description_field_1']['table'] = 'field_data_description_field';
  $handler->display->display_options['fields']['description_field_1']['field'] = 'description_field';
  $handler->display->display_options['fields']['description_field_1']['relationship'] = 'og_membership_related_taxonomy_term_group_5';
  $handler->display->display_options['fields']['description_field_1']['label'] = 'Country';
  $handler->display->display_options['fields']['description_field_1']['link_to_entity'] = 0;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'slot' => 'slot',
  );
  $handler->display->display_options['filters']['machine_name']['group'] = 1;
  /* Filter criterion: Taxonomy term: Name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['operator'] = 'contains';
  $handler->display->display_options['filters']['name']['group'] = 1;
  $handler->display->display_options['filters']['name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['label'] = 'Slot code';
  $handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Taxonomy term: Term */
  $handler->display->display_options['filters']['tid']['id'] = 'tid';
  $handler->display->display_options['filters']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['filters']['tid']['field'] = 'tid';
  $handler->display->display_options['filters']['tid']['relationship'] = 'og_membership_related_taxonomy_term_group';
  $handler->display->display_options['filters']['tid']['value'] = '';
  $handler->display->display_options['filters']['tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['tid']['expose']['operator_id'] = 'tid_op';
  $handler->display->display_options['filters']['tid']['expose']['label'] = 'Store';
  $handler->display->display_options['filters']['tid']['expose']['operator'] = 'tid_op';
  $handler->display->display_options['filters']['tid']['expose']['identifier'] = 'tid';
  $handler->display->display_options['filters']['tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['tid']['vocabulary'] = 'store';
  /* Filter criterion: Taxonomy term: Term */
  $handler->display->display_options['filters']['tid_1']['id'] = 'tid_1';
  $handler->display->display_options['filters']['tid_1']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['filters']['tid_1']['field'] = 'tid';
  $handler->display->display_options['filters']['tid_1']['relationship'] = 'og_membership_related_taxonomy_term_group_5';
  $handler->display->display_options['filters']['tid_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['tid_1']['expose']['operator_id'] = 'tid_1_op';
  $handler->display->display_options['filters']['tid_1']['expose']['label'] = 'Country';
  $handler->display->display_options['filters']['tid_1']['expose']['operator'] = 'tid_1_op';
  $handler->display->display_options['filters']['tid_1']['expose']['identifier'] = 'tid_1';
  $handler->display->display_options['filters']['tid_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['tid_1']['type'] = 'select';
  $handler->display->display_options['filters']['tid_1']['vocabulary'] = 'country';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['inherit_panels_path'] = '1';
  $translatables['slots_dashboard'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('No slots found.'),
    t('Stores'),
    t('Terminal'),
    t('Store platform'),
    t('Sales organization'),
    t('Company code'),
    t('Country'),
    t('Slot code'),
    t('Slot full name'),
    t('Store'),
    t('Content pane'),
    t('View panes'),
  );
  $export['slots_dashboard'] = $view;

  return $export;
}
