<?php
/**
 * @file
 * iris_features_taxonomy_canal.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_canal_taxonomy_default_vocabularies() {
  return array(
    'canal' => array(
      'name' => 'Canal',
      'machine_name' => 'canal',
      'description' => 'The canal it a selection of stores. User for canal promotion exports',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
