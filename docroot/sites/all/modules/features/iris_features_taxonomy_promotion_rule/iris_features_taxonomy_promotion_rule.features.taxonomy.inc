<?php
/**
 * @file
 * iris_features_taxonomy_promotion_rule.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_promotion_rule_taxonomy_default_vocabularies() {
  return array(
    'promotion_rule' => array(
      'name' => 'Promotion rule',
      'machine_name' => 'promotion_rule',
      'description' => 'A promotion is used rule during the promotion import process to calculate the discounted product price',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -3,
    ),
  );
}
