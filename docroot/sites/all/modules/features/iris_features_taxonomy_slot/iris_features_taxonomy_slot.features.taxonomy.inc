<?php
/**
 * @file
 * iris_features_taxonomy_slot.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_slot_taxonomy_default_vocabularies() {
  return array(
    'slot' => array(
      'name' => 'Slot',
      'machine_name' => 'slot',
      'description' => 'The slot it a selection of stores. It can be used for the scope of visibility as if multiple stores were selected.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -1,
    ),
  );
}
