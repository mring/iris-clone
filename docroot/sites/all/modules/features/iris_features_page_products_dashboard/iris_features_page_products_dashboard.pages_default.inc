<?php
/**
 * @file
 * iris_features_page_products_dashboard.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_products_dashboard_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'products_dashboard';
  $page->task = 'page';
  $page->admin_title = 'Products dashboard';
  $page->admin_description = '';
  $page->path = 'admin/products/products-dashboard';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'access products dashboard',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_products_dashboard__panel';
  $handler->task = 'page';
  $handler->subtask = 'products_dashboard';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Products dashboard',
    'panels_breadcrumbs_paths' => 'admin/products/products-dashboard',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Products dashboard';
  $display->uuid = '4b5a0329-1831-4e43-96d4-7cc85663e70b';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1fd5fc47-b302-42ad-a70a-dceb68803fd6';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'products_dashboard-products_dashboard_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1fd5fc47-b302-42ad-a70a-dceb68803fd6';
    $display->content['new-1fd5fc47-b302-42ad-a70a-dceb68803fd6'] = $pane;
    $display->panels['center'][0] = 'new-1fd5fc47-b302-42ad-a70a-dceb68803fd6';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['products_dashboard'] = $page;

  return $pages;

}
