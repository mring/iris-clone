<?php
/**
 * @file
 * iris_features_taxonomy_store_platform.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function iris_features_taxonomy_store_platform_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_sales_organization'.
  $field_bases['field_sales_organization'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_sales_organization',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'handler' => 'og',
      'handler_settings' => array(
        'behaviors' => array(
          'og_behavior' => array(
            'status' => TRUE,
          ),
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'membership_type' => 'og_membership_type_default',
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'sales_organization' => 'sales_organization',
        ),
      ),
      'handler_submit' => 'Change handler',
      'target_type' => 'taxonomy_term',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
