<?php
/**
 * @file
 * iris_features_taxonomy_store_platform.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_store_platform_taxonomy_default_vocabularies() {
  return array(
    'store_platform' => array(
      'name' => 'Store platform',
      'machine_name' => 'store_platform',
      'description' => 'The store platform is part of the location group (4th level). The taxonomy is only populated via the import of the location grou',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 1,
    ),
  );
}
