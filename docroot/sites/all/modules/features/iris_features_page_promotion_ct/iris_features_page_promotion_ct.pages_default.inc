<?php
/**
 * @file
 * iris_features_page_promotion_ct.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function iris_features_page_promotion_ct_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__panel_context_7e099e8f-1b5c-4552-b783-902e49895d32';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(
      0 => array(
        'identifier' => 'Node from Node (on Node: Promotion lines [field_promotion_lines])',
        'keyword' => 'node_2',
        'name' => 'entity_from_field:field_promotion_lines-node-node',
        'delta' => '',
        'context' => 'argument_entity_id:node_1',
        'id' => 1,
      ),
      1 => array(
        'identifier' => 'Operation',
        'keyword' => 'node_3',
        'name' => 'entity_from_field:field_operation-node-node',
        'delta' => 0,
        'context' => 'relationship_entity_from_field:field_promotion_lines-node-node_1',
        'id' => 1,
      ),
      2 => array(
        'identifier' => 'Slot',
        'keyword' => 'taxonomy_term',
        'name' => 'entity_from_field:field_promo_slot-node-taxonomy_term',
        'delta' => 0,
        'context' => 'argument_entity_id:node_1',
        'id' => 1,
      ),
      3 => array(
        'identifier' => 'Stores',
        'keyword' => 'taxonomy_term_2',
        'name' => 'entity_from_field:field_stores-taxonomy_term-taxonomy_term',
        'delta' => '0',
        'context' => 'relationship_entity_from_field:field_promo_slot-node-taxonomy_term_1',
        'id' => 1,
      ),
      4 => array(
        'identifier' => 'Terminals',
        'keyword' => 'taxonomy_term_3',
        'name' => 'entity_from_field:field_terminal-taxonomy_term-taxonomy_term',
        'delta' => 0,
        'context' => 'relationship_entity_from_field:field_stores-taxonomy_term-taxonomy_term_1',
        'id' => 1,
      ),
      5 => array(
        'identifier' => 'Store platforms',
        'keyword' => 'taxonomy_term_4',
        'name' => 'entity_from_field:field_store_platform-taxonomy_term-taxonomy_term',
        'delta' => 0,
        'context' => 'relationship_entity_from_field:field_terminal-taxonomy_term-taxonomy_term_1',
        'id' => 1,
      ),
      6 => array(
        'identifier' => 'Sales organizations',
        'keyword' => 'taxonomy_term_5',
        'name' => 'entity_from_field:field_sales_organization-taxonomy_term-taxonomy_term',
        'delta' => 0,
        'context' => 'relationship_entity_from_field:field_store_platform-taxonomy_term-taxonomy_term_1',
        'id' => 1,
      ),
      7 => array(
        'identifier' => 'Company codes',
        'keyword' => 'taxonomy_term_6',
        'name' => 'entity_from_field:field_company_code-taxonomy_term-taxonomy_term',
        'delta' => 0,
        'context' => 'relationship_entity_from_field:field_sales_organization-taxonomy_term-taxonomy_term_1',
        'id' => 1,
      ),
      8 => array(
        'identifier' => 'Countries',
        'keyword' => 'taxonomy_term_7',
        'name' => 'entity_from_field:field_country-taxonomy_term-taxonomy_term',
        'delta' => 0,
        'context' => 'relationship_entity_from_field:field_company_code-taxonomy_term-taxonomy_term_1',
        'id' => 1,
      ),
    ),
    'name' => '',
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'promotion' => 'promotion',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
          1 => 1,
          2 => 7,
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
          1 => 'right',
          2 => '3_col',
          3 => '4_col',
          4 => '5_col',
          5 => '6_col',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => '1 Col',
        'width' => '15.998513578161882',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
        'hide_empty' => 0,
      ),
      'right' => array(
        'type' => 'region',
        'title' => '2 Col',
        'width' => '15.585013621619854',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
        'hide_empty' => 0,
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center_',
        ),
        'parent' => 'main',
        'class' => '',
        'hide_empty' => 0,
      ),
      'center_' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
        'hide_empty' => 0,
      ),
      '3_col' => array(
        'type' => 'region',
        'title' => '3 Col',
        'width' => '15.997554972054703',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
        'hide_empty' => 0,
      ),
      '4_col' => array(
        'type' => 'region',
        'title' => '4 Col',
        'width' => '15.961428248389465',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
        'hide_empty' => 0,
      ),
      '5_col' => array(
        'type' => 'region',
        'title' => '5 Col',
        'width' => 18.228744789887003,
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
        'hide_empty' => 0,
      ),
      '6_col' => array(
        'type' => 'region',
        'title' => '6 Col',
        'width' => 18.228744789887003,
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
        'hide_empty' => 0,
      ),
      7 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'left_bottom',
          1 => 'right_bottom',
        ),
        'parent' => 'main',
        'class' => '',
        'hide_empty' => 0,
      ),
      'left_bottom' => array(
        'type' => 'region',
        'title' => 'Left Bottom',
        'width' => 50,
        'width_type' => '%',
        'parent' => '7',
        'class' => '',
        'hide_empty' => 0,
      ),
      'right_bottom' => array(
        'type' => 'region',
        'title' => 'Right Bottom',
        'width' => 50,
        'width_type' => '%',
        'parent' => '7',
        'class' => '',
        'hide_empty' => 0,
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'right' => NULL,
      'center_' => NULL,
      '3_col' => NULL,
      '4_col' => NULL,
      '5_col' => NULL,
      '6_col' => NULL,
      'left_bottom' => NULL,
      'right_bottom' => NULL,
    ),
    'center' => array(
      'style' => '-1',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '6cdd04c8-2283-461a-9c23-91a8c3a6d569';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-73231e1c-41e4-4870-9da6-6dba8d1850cd';
    $pane->panel = '3_col';
    $pane->type = 'term_name';
    $pane->subtype = 'term_name';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'none',
      'id' => '',
      'class' => '',
      'context' => 'relationship_entity_from_field:field_country-taxonomy_term-taxonomy_term_1',
      'override_title' => 1,
      'override_title_text' => 'Country :',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '73231e1c-41e4-4870-9da6-6dba8d1850cd';
    $display->content['new-73231e1c-41e4-4870-9da6-6dba8d1850cd'] = $pane;
    $display->panels['3_col'][0] = 'new-73231e1c-41e4-4870-9da6-6dba8d1850cd';
    $pane = new stdClass();
    $pane->pid = 'new-587843a3-84e0-4d74-ae14-6c9381c8f01b';
    $pane->panel = '3_col';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_promo_slot';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'taxonomy_term_reference_plain',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => 'Slot :',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '587843a3-84e0-4d74-ae14-6c9381c8f01b';
    $display->content['new-587843a3-84e0-4d74-ae14-6c9381c8f01b'] = $pane;
    $display->panels['3_col'][1] = 'new-587843a3-84e0-4d74-ae14-6c9381c8f01b';
    $pane = new stdClass();
    $pane->pid = 'new-ef35a224-d558-46ee-b73c-5429e4b59d64';
    $pane->panel = '4_col';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_operation_start_date';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'date_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'format_type' => 'short',
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_to' => '',
        'fromto' => 'both',
        'show_remaining_days' => 0,
      ),
      'context' => 'relationship_entity_from_field:field_operation-node-node_1',
      'override_title' => 1,
      'override_title_text' => 'Start date :',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ef35a224-d558-46ee-b73c-5429e4b59d64';
    $display->content['new-ef35a224-d558-46ee-b73c-5429e4b59d64'] = $pane;
    $display->panels['4_col'][0] = 'new-ef35a224-d558-46ee-b73c-5429e4b59d64';
    $pane = new stdClass();
    $pane->pid = 'new-58b4df4f-f517-4c4a-aa97-e854e22da332';
    $pane->panel = '4_col';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_operation_end_date';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'date_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'format_type' => 'short',
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_to' => '',
        'fromto' => 'both',
        'show_remaining_days' => 0,
      ),
      'context' => 'relationship_entity_from_field:field_operation-node-node_1',
      'override_title' => 1,
      'override_title_text' => 'End date :',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '58b4df4f-f517-4c4a-aa97-e854e22da332';
    $display->content['new-58b4df4f-f517-4c4a-aa97-e854e22da332'] = $pane;
    $display->panels['4_col'][1] = 'new-58b4df4f-f517-4c4a-aa97-e854e22da332';
    $pane = new stdClass();
    $pane->pid = 'new-9cd14af6-8897-4afe-8d76-7a56b58e9f0b';
    $pane->panel = '5_col';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_reference_validation';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'date_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'format_type' => 'short',
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_to' => '',
        'fromto' => 'both',
        'show_remaining_days' => 0,
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => 'Validation :',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9cd14af6-8897-4afe-8d76-7a56b58e9f0b';
    $display->content['new-9cd14af6-8897-4afe-8d76-7a56b58e9f0b'] = $pane;
    $display->panels['5_col'][0] = 'new-9cd14af6-8897-4afe-8d76-7a56b58e9f0b';
    $pane = new stdClass();
    $pane->pid = 'new-d111a2af-f28c-46c7-add6-4cd82666c6a5';
    $pane->panel = '6_col';
    $pane->type = 'block';
    $pane->subtype = 'iris_core-request_export';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd111a2af-f28c-46c7-add6-4cd82666c6a5';
    $display->content['new-d111a2af-f28c-46c7-add6-4cd82666c6a5'] = $pane;
    $display->panels['6_col'][0] = 'new-d111a2af-f28c-46c7-add6-4cd82666c6a5';
    $pane = new stdClass();
    $pane->pid = 'new-934303f4-fc9a-4b6b-9256-35943e1b0192';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_operation_number';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_plain',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'title_style' => 'h2',
        'title_link' => '',
        'title_class' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => 'Operation number :',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '934303f4-fc9a-4b6b-9256-35943e1b0192';
    $display->content['new-934303f4-fc9a-4b6b-9256-35943e1b0192'] = $pane;
    $display->panels['center'][0] = 'new-934303f4-fc9a-4b6b-9256-35943e1b0192';
    $pane = new stdClass();
    $pane->pid = 'new-e89bf4dc-52c5-4cad-84bc-55e96b726c63';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_operation_description';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'relationship_entity_from_field:field_operation-node-node_1',
      'override_title' => 1,
      'override_title_text' => 'Operation description :',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'e89bf4dc-52c5-4cad-84bc-55e96b726c63';
    $display->content['new-e89bf4dc-52c5-4cad-84bc-55e96b726c63'] = $pane;
    $display->panels['center'][1] = 'new-e89bf4dc-52c5-4cad-84bc-55e96b726c63';
    $pane = new stdClass();
    $pane->pid = 'new-f765c24c-b4e9-48c3-8e88-759f49d7d35d';
    $pane->panel = 'center_';
    $pane->type = 'views_panes';
    $pane->subtype = 'promotions_promo_lines-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:node_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f765c24c-b4e9-48c3-8e88-759f49d7d35d';
    $display->content['new-f765c24c-b4e9-48c3-8e88-759f49d7d35d'] = $pane;
    $display->panels['center_'][0] = 'new-f765c24c-b4e9-48c3-8e88-759f49d7d35d';
    $pane = new stdClass();
    $pane->pid = 'new-d78ad50c-34f0-4288-91c0-b0f2f3078788';
    $pane->panel = 'left_bottom';
    $pane->type = 'views_panes';
    $pane->subtype = 'news_related_to_promotion-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd78ad50c-34f0-4288-91c0-b0f2f3078788';
    $display->content['new-d78ad50c-34f0-4288-91c0-b0f2f3078788'] = $pane;
    $display->panels['left_bottom'][0] = 'new-d78ad50c-34f0-4288-91c0-b0f2f3078788';
    $pane = new stdClass();
    $pane->pid = 'new-5c143006-c1cc-4df1-8b66-bd524c45fa2c';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_promo_line_activity_sector';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'taxonomy_term_reference_plain',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => 'Activity sector :',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5c143006-c1cc-4df1-8b66-bd524c45fa2c';
    $display->content['new-5c143006-c1cc-4df1-8b66-bd524c45fa2c'] = $pane;
    $display->panels['right'][0] = 'new-5c143006-c1cc-4df1-8b66-bd524c45fa2c';
    $pane = new stdClass();
    $pane->pid = 'new-d7efef4d-0218-45f2-b074-fd99ad9524ae';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_grouping_key';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'title_style' => '_none',
        'title_link' => '',
        'title_class' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => 'Grouping key :',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'd7efef4d-0218-45f2-b074-fd99ad9524ae';
    $display->content['new-d7efef4d-0218-45f2-b074-fd99ad9524ae'] = $pane;
    $display->panels['right'][1] = 'new-d7efef4d-0218-45f2-b074-fd99ad9524ae';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view__panel_context_7e099e8f-1b5c-4552-b783-902e49895d32'] = $handler;

  return $export;
}
