<?php
/**
 * @file
 * iris_features_page_promotion_ct.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_page_promotion_ct_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function iris_features_page_promotion_ct_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
