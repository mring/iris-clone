<?php
/**
 * @file
 * iris_features_taxonomy_page_type.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_page_type_taxonomy_default_vocabularies() {
  return array(
    'page_type' => array(
      'name' => 'Page type',
      'machine_name' => 'page_type',
      'description' => 'Page type taxonomy is used to define the page type of a page content type.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -4,
    ),
  );
}
