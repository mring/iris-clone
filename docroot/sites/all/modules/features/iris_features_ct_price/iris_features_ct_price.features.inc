<?php
/**
 * @file
 * iris_features_ct_price.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_ct_price_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function iris_features_ct_price_node_info() {
  $items = array(
    'price' => array(
      'name' => t('Price'),
      'base' => 'node_content',
      'description' => t('The prices are related to a product and a company code. Different prices can related to the same product. Each price has a validity period defined by a date start and end.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
