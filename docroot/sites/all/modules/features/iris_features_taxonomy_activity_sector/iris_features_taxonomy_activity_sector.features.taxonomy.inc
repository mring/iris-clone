<?php
/**
 * @file
 * iris_features_taxonomy_activity_sector.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_activity_sector_taxonomy_default_vocabularies() {
  return array(
    'activity_sector' => array(
      'name' => 'Activity sector',
      'machine_name' => 'activity_sector',
      'description' => 'The activity sector is used to classify a news. It’s related to an activity domain',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -9,
    ),
  );
}
