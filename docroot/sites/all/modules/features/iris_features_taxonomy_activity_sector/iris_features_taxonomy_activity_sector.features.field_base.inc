<?php
/**
 * @file
 * iris_features_taxonomy_activity_sector.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function iris_features_taxonomy_activity_sector_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_activity_domain'.
  $field_bases['field_activity_domain'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_activity_domain',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'activity_domain',
          'parent' => 0,
        ),
      ),
      'entity_translation_sync' => FALSE,
      'options_list_callback' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
