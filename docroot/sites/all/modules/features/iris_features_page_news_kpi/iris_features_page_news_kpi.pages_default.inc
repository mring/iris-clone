<?php
/**
 * @file
 * iris_features_page_news_kpi.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_news_kpi_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'news_kpi';
  $page->task = 'page';
  $page->admin_title = 'News KPI';
  $page->admin_description = '';
  $page->path = 'admin/news-kpi';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'view news kpi dashboard',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_news_kpi__panel';
  $handler->task = 'page';
  $handler->subtask = 'news_kpi';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'News KPI';
  $display->uuid = '83c61662-606e-4d21-a438-c09ca898371f';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_news_kpi__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-a586b42d-3560-43a6-8085-2275cfa1d41d';
  $pane->panel = 'center';
  $pane->type = 'iris_kpi_news_filters';
  $pane->subtype = 'iris_kpi_news_filters';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a586b42d-3560-43a6-8085-2275cfa1d41d';
  $display->content['new-a586b42d-3560-43a6-8085-2275cfa1d41d'] = $pane;
  $display->panels['center'][0] = 'new-a586b42d-3560-43a6-8085-2275cfa1d41d';
  $pane = new stdClass();
  $pane->pid = 'new-58de6ae3-7997-46b8-8286-b325a24ebde9';
  $pane->panel = 'center';
  $pane->type = 'iris_kpi_news_total';
  $pane->subtype = 'iris_kpi_news_total';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '58de6ae3-7997-46b8-8286-b325a24ebde9';
  $display->content['new-58de6ae3-7997-46b8-8286-b325a24ebde9'] = $pane;
  $display->panels['center'][1] = 'new-58de6ae3-7997-46b8-8286-b325a24ebde9';
  $pane = new stdClass();
  $pane->pid = 'new-a7a38782-a982-456f-b1cf-f1d455acdef8';
  $pane->panel = 'center';
  $pane->type = 'iris_kpi_news_created_per_month';
  $pane->subtype = 'iris_kpi_news_created_per_month';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'a7a38782-a982-456f-b1cf-f1d455acdef8';
  $display->content['new-a7a38782-a982-456f-b1cf-f1d455acdef8'] = $pane;
  $display->panels['center'][2] = 'new-a7a38782-a982-456f-b1cf-f1d455acdef8';
  $pane = new stdClass();
  $pane->pid = 'new-13f28603-93c5-4fac-8448-a40a7288804e';
  $pane->panel = 'center';
  $pane->type = 'iris_kpi_news_per_universe';
  $pane->subtype = 'iris_kpi_news_per_universe';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '13f28603-93c5-4fac-8448-a40a7288804e';
  $display->content['new-13f28603-93c5-4fac-8448-a40a7288804e'] = $pane;
  $display->panels['center'][3] = 'new-13f28603-93c5-4fac-8448-a40a7288804e';
  $pane = new stdClass();
  $pane->pid = 'new-43dd5fb7-0bc5-4819-909f-2575b39fb665';
  $pane->panel = 'center';
  $pane->type = 'iris_kpi_news_per_type';
  $pane->subtype = 'iris_kpi_news_per_type';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '43dd5fb7-0bc5-4819-909f-2575b39fb665';
  $display->content['new-43dd5fb7-0bc5-4819-909f-2575b39fb665'] = $pane;
  $display->panels['center'][4] = 'new-43dd5fb7-0bc5-4819-909f-2575b39fb665';
  $pane = new stdClass();
  $pane->pid = 'new-e8085b5b-4522-4994-b028-0a20660538b7';
  $pane->panel = 'center';
  $pane->type = 'iris_kpi_news_done_per_store';
  $pane->subtype = 'iris_kpi_news_done_per_store';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = 'e8085b5b-4522-4994-b028-0a20660538b7';
  $display->content['new-e8085b5b-4522-4994-b028-0a20660538b7'] = $pane;
  $display->panels['center'][5] = 'new-e8085b5b-4522-4994-b028-0a20660538b7';
  $pane = new stdClass();
  $pane->pid = 'new-3b4b513a-98ab-4d65-b6a6-fec6898d99a0';
  $pane->panel = 'center';
  $pane->type = 'iris_kpi_news_done_per_user';
  $pane->subtype = 'iris_kpi_news_done_per_user';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'type' => 'highest',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 6;
  $pane->locks = array();
  $pane->uuid = '3b4b513a-98ab-4d65-b6a6-fec6898d99a0';
  $display->content['new-3b4b513a-98ab-4d65-b6a6-fec6898d99a0'] = $pane;
  $display->panels['center'][6] = 'new-3b4b513a-98ab-4d65-b6a6-fec6898d99a0';
  $pane = new stdClass();
  $pane->pid = 'new-57439cb5-16ef-4d01-b706-9877a594cdf2';
  $pane->panel = 'center';
  $pane->type = 'iris_kpi_news_done_per_user';
  $pane->subtype = 'iris_kpi_news_done_per_user';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'type' => 'lowest',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 7;
  $pane->locks = array();
  $pane->uuid = '57439cb5-16ef-4d01-b706-9877a594cdf2';
  $display->content['new-57439cb5-16ef-4d01-b706-9877a594cdf2'] = $pane;
  $display->panels['center'][7] = 'new-57439cb5-16ef-4d01-b706-9877a594cdf2';
  $pane = new stdClass();
  $pane->pid = 'new-95b4d344-5c06-450a-bc92-148667f8dcb2';
  $pane->panel = 'center';
  $pane->type = 'iris_kpi_news_done_per_store_p';
  $pane->subtype = 'iris_kpi_news_done_per_store_p';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'type' => 'highest',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 8;
  $pane->locks = array();
  $pane->uuid = '95b4d344-5c06-450a-bc92-148667f8dcb2';
  $display->content['new-95b4d344-5c06-450a-bc92-148667f8dcb2'] = $pane;
  $display->panels['center'][8] = 'new-95b4d344-5c06-450a-bc92-148667f8dcb2';
  $pane = new stdClass();
  $pane->pid = 'new-7165411f-9245-41c8-9a00-9b092ee92900';
  $pane->panel = 'center';
  $pane->type = 'iris_kpi_news_done_per_univers_p';
  $pane->subtype = 'iris_kpi_news_done_per_univers_p';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 9;
  $pane->locks = array();
  $pane->uuid = '7165411f-9245-41c8-9a00-9b092ee92900';
  $display->content['new-7165411f-9245-41c8-9a00-9b092ee92900'] = $pane;
  $display->panels['center'][9] = 'new-7165411f-9245-41c8-9a00-9b092ee92900';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['news_kpi'] = $page;

  return $pages;

}
