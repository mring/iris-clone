<?php
/**
 * @file
 * iris_features_page_operations_dashboard.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_operations_dashboard_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'operations_dashboard';
  $page->task = 'page';
  $page->admin_title = 'Operations dashboard';
  $page->admin_description = '';
  $page->path = 'admin/promotions/operations-dashboard';
  $page->access = array(
    'logic' => 'and',
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'access operations dashboard',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_operations_dashboard__panel';
  $handler->task = 'page';
  $handler->subtask = 'operations_dashboard';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Operations dashboard',
    'panels_breadcrumbs_paths' => 'admin/promotions/operations-dashboard',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'left' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Promotions planning';
  $display->uuid = 'cc304382-3fa9-4439-8dd5-37e5b10324f6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-6b4a6e1e-38bb-4672-a984-23c9fb4f7382';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'iris_import_data-operation_upload';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
              1 => 4,
              2 => 9,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6b4a6e1e-38bb-4672-a984-23c9fb4f7382';
    $display->content['new-6b4a6e1e-38bb-4672-a984-23c9fb4f7382'] = $pane;
    $display->panels['center'][0] = 'new-6b4a6e1e-38bb-4672-a984-23c9fb4f7382';
    $pane = new stdClass();
    $pane->pid = 'new-338fe9f7-a6b7-43cc-a538-ed1baad19f0c';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'operations_dashboard-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '338fe9f7-a6b7-43cc-a538-ed1baad19f0c';
    $display->content['new-338fe9f7-a6b7-43cc-a538-ed1baad19f0c'] = $pane;
    $display->panels['center'][1] = 'new-338fe9f7-a6b7-43cc-a538-ed1baad19f0c';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-338fe9f7-a6b7-43cc-a538-ed1baad19f0c';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['operations_dashboard'] = $page;

  return $pages;

}
