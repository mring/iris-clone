<?php
/**
 * @file
 * iris_features_ct_promotion_line_archive.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_ct_promotion_line_archive_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function iris_features_ct_promotion_line_archive_node_info() {
  $items = array(
    'promotion_line_archive' => array(
      'name' => t('Promotion line archive'),
      'base' => 'node_content',
      'description' => t('The promotion line archive is a flat structure to migrate the promotion lines from the old system (XWiki).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
