<?php
/**
 * @file
 * iris_features_ct_promotion_line_archive.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function iris_features_ct_promotion_line_archive_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_promotion_line_archive';
  $strongarm->value = 0;
  $export['comment_anonymous_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_promotion_line_archive';
  $strongarm->value = 0;
  $export['comment_default_mode_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_promotion_line_archive';
  $strongarm->value = '50';
  $export['comment_default_per_page_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_promotion_line_archive';
  $strongarm->value = 1;
  $export['comment_form_location_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_promotion_line_archive';
  $strongarm->value = '0';
  $export['comment_preview_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_promotion_line_archive';
  $strongarm->value = '1';
  $export['comment_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_promotion_line_archive';
  $strongarm->value = 1;
  $export['comment_subject_field_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__promotion_line_archive';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '-5',
        ),
        'path' => array(
          'weight' => '30',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_promotion_line_archive';
  $strongarm->value = '0';
  $export['language_content_type_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_promotion_line_archive';
  $strongarm->value = array();
  $export['menu_options_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_promotion_line_archive';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_promotion_line_archive';
  $strongarm->value = array(
    0 => 'status',
    1 => 'revision',
  );
  $export['node_options_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_promotion_line_archive';
  $strongarm->value = '0';
  $export['node_preview_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_promotion_line_archive';
  $strongarm->value = 0;
  $export['node_submitted_promotion_line_archive'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_promotion_line_archive_pattern';
  $strongarm->value = 'promotion_line_archive/[node:nid]';
  $export['pathauto_node_promotion_line_archive_pattern'] = $strongarm;

  return $export;
}
