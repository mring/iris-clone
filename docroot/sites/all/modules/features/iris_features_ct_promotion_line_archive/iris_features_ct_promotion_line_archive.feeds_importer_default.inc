<?php
/**
 * @file
 * iris_features_ct_promotion_line_archive.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function iris_features_ct_promotion_line_archive_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'promotion_lines_archive_importer';
  $feeds_importer->config = array(
    'name' => 'Promotion lines archive importer',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'direct' => 0,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'encoding' => 'UTF-8',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Valid. ref',
            'target' => 'field_archive_ref_validation:start',
            'unique' => FALSE,
            'language' => 'und',
          ),
          1 => array(
            'source' => 'Pays',
            'target' => 'field_archive_country',
            'unique' => FALSE,
            'language' => 'und',
          ),
          2 => array(
            'source' => 'Univers',
            'target' => 'field_archive_universe',
            'unique' => FALSE,
            'language' => 'und',
          ),
          3 => array(
            'source' => 'Catégorie',
            'target' => 'field_archive_category',
            'unique' => FALSE,
            'language' => 'und',
          ),
          4 => array(
            'source' => 'Date de début',
            'target' => 'field_archive_startdate:start',
            'unique' => FALSE,
            'language' => 'und',
          ),
          5 => array(
            'source' => 'Date de fin',
            'target' => 'field_archive_enddate:start',
            'unique' => FALSE,
            'language' => 'und',
          ),
          6 => array(
            'source' => 'Durée',
            'target' => 'field_archive_duration',
            'unique' => FALSE,
            'language' => 'und',
          ),
          7 => array(
            'source' => 'Initiateur',
            'target' => 'field_archive_initiator',
            'unique' => FALSE,
            'language' => 'und',
          ),
          8 => array(
            'source' => 'Thématique',
            'target' => 'field_archive_thematic',
            'unique' => FALSE,
            'language' => 'und',
          ),
          9 => array(
            'source' => 'Slot',
            'target' => 'field_archive_slot',
            'unique' => FALSE,
            'language' => 'und',
          ),
          10 => array(
            'source' => 'Fournisseur',
            'target' => 'field_archive_supplier',
            'unique' => FALSE,
            'language' => 'und',
          ),
          11 => array(
            'source' => 'Marque',
            'target' => 'field_archive_brand',
            'unique' => FALSE,
            'language' => 'und',
          ),
          12 => array(
            'source' => 'Commentaire',
            'target' => 'field_archive_comment',
            'unique' => FALSE,
            'language' => 'und',
          ),
          13 => array(
            'source' => 'Livraison directe',
            'target' => 'field_archive_delivery',
            'unique' => FALSE,
            'language' => 'und',
          ),
          14 => array(
            'source' => 'SAP SKU',
            'target' => 'field_archive_sku',
            'unique' => FALSE,
            'language' => 'und',
          ),
          15 => array(
            'source' => 'Description',
            'target' => 'field_archive_description',
            'unique' => FALSE,
            'language' => 'und',
          ),
          16 => array(
            'source' => 'Code Testeur',
            'target' => 'field_archive_testercode',
            'unique' => FALSE,
            'language' => 'und',
          ),
          17 => array(
            'source' => 'LKg',
            'target' => 'field_archive_lkg',
            'unique' => FALSE,
            'language' => 'und',
          ),
          18 => array(
            'source' => 'Mécanique',
            'target' => 'field_archive_promorule',
            'unique' => FALSE,
            'language' => 'und',
          ),
          19 => array(
            'source' => 'Devise',
            'target' => 'field_archive_currency',
            'unique' => FALSE,
            'language' => 'und',
          ),
          20 => array(
            'source' => 'Arrondi',
            'target' => 'field_archive_roundingrule',
            'unique' => FALSE,
            'language' => 'und',
          ),
          21 => array(
            'source' => 'PV DF',
            'target' => 'field_archive_salesprice_a1',
            'unique' => FALSE,
            'language' => 'und',
          ),
          22 => array(
            'source' => 'PV Promo DF',
            'target' => 'field_archive_promosalesprice_a1',
            'unique' => FALSE,
            'language' => 'und',
          ),
          23 => array(
            'source' => 'Montant remise DF',
            'target' => 'field_archive_discount_amount_a1',
            'unique' => FALSE,
            'language' => 'und',
          ),
          24 => array(
            'source' => '% remise DF',
            'target' => 'field_archive_perc_discount_a1',
            'unique' => FALSE,
            'language' => 'und',
          ),
          25 => array(
            'source' => 'PVDP',
            'target' => 'field_archive_salesprice_a2',
            'unique' => FALSE,
            'language' => 'und',
          ),
          26 => array(
            'source' => 'PV Promo DP',
            'target' => 'field_archive_promosalesprice_a2',
            'unique' => FALSE,
            'language' => 'und',
          ),
          27 => array(
            'source' => 'Montant remise DP',
            'target' => 'field_archive_discount_amount_a2',
            'unique' => FALSE,
            'language' => 'und',
          ),
          28 => array(
            'source' => '% remise DP',
            'target' => 'field_archive_perc_discount_a2',
            'unique' => FALSE,
            'language' => 'und',
          ),
          29 => array(
            'source' => 'PVLKgDF',
            'target' => 'field_archive_salespricelkg_a1',
            'unique' => FALSE,
            'language' => 'und',
          ),
          30 => array(
            'source' => 'PVLKgDP',
            'target' => 'field_archive_salespricelkg_a2',
            'unique' => FALSE,
            'language' => 'und',
          ),
          31 => array(
            'source' => 'Montant ref DF',
            'target' => 'field_archive_refamount_a1',
            'unique' => FALSE,
            'language' => 'und',
          ),
          32 => array(
            'source' => 'Montant ref DP',
            'target' => 'field_archive_refamount_a2',
            'unique' => FALSE,
            'language' => 'und',
          ),
          33 => array(
            'source' => 'Canal',
            'target' => 'field_archive_channel',
            'unique' => FALSE,
            'language' => 'und',
          ),
        ),
        'insert_new' => '1',
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'promotion_line_archive',
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['promotion_lines_archive_importer'] = $feeds_importer;

  return $export;
}
