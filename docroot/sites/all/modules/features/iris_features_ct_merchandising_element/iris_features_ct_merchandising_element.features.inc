<?php
/**
 * @file
 * iris_features_ct_merchandising_element.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_ct_merchandising_element_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function iris_features_ct_merchandising_element_node_info() {
  $items = array(
    'merchandising_element' => array(
      'name' => t('Merchandising element'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Description'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
