<?php
/**
 * @file
 * iris_features_pages_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_pages_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function iris_features_pages_search_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_date_format_types().
 */
function iris_features_pages_search_date_format_types() {
  $format_types = array();
  // Exported date format type: pages_front
  $format_types['pages_front'] = 'Pages front';
  return $format_types;
}

/**
 * Implements hook_fe_date_locale_date_format().
 */
function iris_features_pages_search_fe_date_locale_date_format() {
  $locale_date_formats = array();

  // Exported format: pages_front::en
  $locale_date_formats['pages_front::en'] = array(
    'type' => 'pages_front',
    'format' => 'd/m/Y',
    'locales' => array(
      0 => 'en',
    ),
  );
  // Exported format: pages_front::fr
  $locale_date_formats['pages_front::fr'] = array(
    'type' => 'pages_front',
    'format' => 'd/m/Y',
    'locales' => array(
      0 => 'fr',
    ),
  );
  return $locale_date_formats;
}

/**
 * Implements hook_default_search_api_index().
 */
function iris_features_pages_search_default_search_api_index() {
  $items = array();
  $items['pages'] = entity_import('search_api_index', '{
    "name" : "Pages",
    "machine_name" : "pages",
    "description" : "Generic pages index",
    "server" : "solr_dev",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "page" ] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "changed" : { "type" : "date" },
        "field_files:description" : { "type" : "list\\u003Ctext\\u003E" },
        "field_files:file" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "file" },
        "field_links:title" : { "type" : "list\\u003Ctext\\u003E" },
        "field_links:url" : { "type" : "list\\u003Curi\\u003E" },
        "field_location_group" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_page_type" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "nid" : { "type" : "integer" },
        "page_location_group" : { "type" : "list\\u003Cstring\\u003E" },
        "page_scope_stores" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "integer" },
        "title_field" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_language_control" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : { "title_field" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title_field" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 0,
          "weight" : "15",
          "settings" : { "fields" : { "title_field" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title_field" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title_field" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}
