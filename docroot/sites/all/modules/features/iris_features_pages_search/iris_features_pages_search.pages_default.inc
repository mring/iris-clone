<?php
/**
 * @file
 * iris_features_pages_search.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_pages_search_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'pages';
  $page->task = 'page';
  $page->admin_title = 'Pages';
  $page->admin_description = '';
  $page->path = 'pages';
  $page->access = array(
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Pages',
    'name' => 'main-menu',
    'weight' => '14',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_pages__panel';
  $handler->task = 'page';
  $handler->subtask = 'pages';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Pages search',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'access' => array(
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'merch_3_cols';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'merch_sidebar' => NULL,
      'merch_list' => NULL,
      'merch_content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '797bbb24-30c2-4149-826f-f55b1b0d809e';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_pages__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7867e3ff-06eb-4286-b58b-718d15b4bfea';
  $pane->panel = 'merch_content';
  $pane->type = 'views_panes';
  $pane->subtype = 'pages-pages_search_item';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7867e3ff-06eb-4286-b58b-718d15b4bfea';
  $display->content['new-7867e3ff-06eb-4286-b58b-718d15b4bfea'] = $pane;
  $display->panels['merch_content'][0] = 'new-7867e3ff-06eb-4286-b58b-718d15b4bfea';
  $pane = new stdClass();
  $pane->pid = 'new-6cf8c708-b492-480d-b08d-166258b8b3ce';
  $pane->panel = 'merch_list';
  $pane->type = 'views_panes';
  $pane->subtype = 'pages-pages_search';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '6cf8c708-b492-480d-b08d-166258b8b3ce';
  $display->content['new-6cf8c708-b492-480d-b08d-166258b8b3ce'] = $pane;
  $display->panels['merch_list'][0] = 'new-6cf8c708-b492-480d-b08d-166258b8b3ce';
  $pane = new stdClass();
  $pane->pid = 'new-5687969f-6d62-4171-8f5e-cda18a9c40f7';
  $pane->panel = 'merch_sidebar';
  $pane->type = 'block';
  $pane->subtype = 'views--exp-pages-pages_search';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'inherit_path' => 1,
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5687969f-6d62-4171-8f5e-cda18a9c40f7';
  $display->content['new-5687969f-6d62-4171-8f5e-cda18a9c40f7'] = $pane;
  $display->panels['merch_sidebar'][0] = 'new-5687969f-6d62-4171-8f5e-cda18a9c40f7';
  $pane = new stdClass();
  $pane->pid = 'new-92d3a6ea-de6d-4270-bae6-a4baf69d7115';
  $pane->panel = 'merch_sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-qsAzNrNhd10qCPTc1CEzaeGnznrsPKUi';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Folder',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '92d3a6ea-de6d-4270-bae6-a4baf69d7115';
  $display->content['new-92d3a6ea-de6d-4270-bae6-a4baf69d7115'] = $pane;
  $display->panels['merch_sidebar'][1] = 'new-92d3a6ea-de6d-4270-bae6-a4baf69d7115';
  $pane = new stdClass();
  $pane->pid = 'new-f07a6172-08ec-482d-ae54-460a0552a613';
  $pane->panel = 'merch_sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-THb8mBd7brLreOMvc2ssQp00eCsd2K31';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Stores',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'f07a6172-08ec-482d-ae54-460a0552a613';
  $display->content['new-f07a6172-08ec-482d-ae54-460a0552a613'] = $pane;
  $display->panels['merch_sidebar'][2] = 'new-f07a6172-08ec-482d-ae54-460a0552a613';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-5687969f-6d62-4171-8f5e-cda18a9c40f7';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['pages'] = $page;

  return $pages;

}
