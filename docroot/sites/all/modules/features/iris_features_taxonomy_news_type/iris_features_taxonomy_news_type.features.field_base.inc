<?php
/**
 * @file
 * iris_features_taxonomy_news_type.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function iris_features_taxonomy_news_type_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_agenda_color_class'.
  $field_bases['field_agenda_color_class'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_agenda_color_class',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'theme-red' => 'theme-red',
        'theme-yellow' => 'theme-yellow',
        'theme-blue' => 'theme-blue',
        'theme-lilac' => 'theme-lilac',
        'theme-green' => 'theme-green',
        'theme-light-blue' => 'theme-light-blue',
        'theme-light-red' => 'theme-light-red',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_default_banner_image'.
  $field_bases['field_default_banner_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_default_banner_image',
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'entity_translation_sync' => array(
        0 => 'fid',
      ),
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_left_menu'.
  $field_bases['field_left_menu'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_left_menu',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'Do not display in the left menu filters',
        1 => 'Display in the left menu filters',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  // Exported field_base: 'field_left_menu_image'.
  $field_bases['field_left_menu_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_left_menu_image',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'list' => 'list',
        'info' => 'info',
        'label' => 'label',
        'flag' => 'flag',
        'car' => 'car',
        'basket' => 'basket',
        'star2' => 'star',
        'glass' => 'glass',
        'pc' => 'pc',
        'mode' => 'mode',
        'processor' => 'processor',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
