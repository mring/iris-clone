<?php
/**
 * @file
 * iris_features_taxonomy_news_type.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_news_type_taxonomy_default_vocabularies() {
  return array(
    'news_type' => array(
      'name' => 'News type',
      'machine_name' => 'news_type',
      'description' => 'Used to define the type of the news created.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -5,
    ),
  );
}
