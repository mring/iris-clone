<?php
/**
 * @file
 * iris_features_merch_search.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function iris_features_merch_search_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'taxonomy_term';
  $panelizer->panelizer_key = 'merchandising_rubric';
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->name = 'taxonomy_term:merchandising_rubric:default';
  $panelizer->css_id = '';
  $panelizer->css_class = '';
  $panelizer->css = '';
  $panelizer->no_blocks = FALSE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'merch_item_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'merch_sidebar' => NULL,
      'merch_list' => NULL,
      'merch_item_title' => NULL,
      'merch_item_image' => NULL,
      'merch_item_links' => NULL,
      'merch_item_stores' => NULL,
      'merch_item_docs' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '45c77075-f469-4af1-b092-387dd512c78d';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'taxonomy_term:merchandising_rubric:default';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-4101b681-8b07-45f0-8724-0f307fb3abe2';
  $pane->panel = 'merch_item_docs';
  $pane->type = 'merch_rubric_docs';
  $pane->subtype = 'merch_rubric_docs';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'merch_rubric_docs_type' => 'files',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '4101b681-8b07-45f0-8724-0f307fb3abe2';
  $display->content['new-4101b681-8b07-45f0-8724-0f307fb3abe2'] = $pane;
  $display->panels['merch_item_docs'][0] = 'new-4101b681-8b07-45f0-8724-0f307fb3abe2';
  $pane = new stdClass();
  $pane->pid = 'new-aec4c8f7-17e7-4011-aec5-690f18f21dfe';
  $pane->panel = 'merch_item_image';
  $pane->type = 'entity_field';
  $pane->subtype = 'taxonomy_term:field_merch_banner';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_link' => '',
      'image_style' => 'news_banner',
    ),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'aec4c8f7-17e7-4011-aec5-690f18f21dfe';
  $display->content['new-aec4c8f7-17e7-4011-aec5-690f18f21dfe'] = $pane;
  $display->panels['merch_item_image'][0] = 'new-aec4c8f7-17e7-4011-aec5-690f18f21dfe';
  $pane = new stdClass();
  $pane->pid = 'new-d3908fdf-0fac-447f-b784-6bcfe958e101';
  $pane->panel = 'merch_item_links';
  $pane->type = 'merch_rubric_docs';
  $pane->subtype = 'merch_rubric_docs';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'merch_rubric_docs_type' => 'links',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd3908fdf-0fac-447f-b784-6bcfe958e101';
  $display->content['new-d3908fdf-0fac-447f-b784-6bcfe958e101'] = $pane;
  $display->panels['merch_item_links'][0] = 'new-d3908fdf-0fac-447f-b784-6bcfe958e101';
  $pane = new stdClass();
  $pane->pid = 'new-323f01e2-7cd2-45f9-bd25-bd8c275043e1';
  $pane->panel = 'merch_item_stores';
  $pane->type = 'views_panes';
  $pane->subtype = 'merch_stores-merch_stores';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'arguments' => array(
      'tid' => '%taxonomy_term:tid',
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '323f01e2-7cd2-45f9-bd25-bd8c275043e1';
  $display->content['new-323f01e2-7cd2-45f9-bd25-bd8c275043e1'] = $pane;
  $display->panels['merch_item_stores'][0] = 'new-323f01e2-7cd2-45f9-bd25-bd8c275043e1';
  $pane = new stdClass();
  $pane->pid = 'new-fa33da45-c18c-4a02-92c3-79a9115495e6';
  $pane->panel = 'merch_item_title';
  $pane->type = 'term_name';
  $pane->subtype = 'term_name';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 0,
    'markup' => 'h2',
    'id' => '',
    'class' => '',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fa33da45-c18c-4a02-92c3-79a9115495e6';
  $display->content['new-fa33da45-c18c-4a02-92c3-79a9115495e6'] = $pane;
  $display->panels['merch_item_title'][0] = 'new-fa33da45-c18c-4a02-92c3-79a9115495e6';
  $pane = new stdClass();
  $pane->pid = 'new-93ef170d-0ed6-40fa-815e-c6451b93439c';
  $pane->panel = 'merch_list';
  $pane->type = 'views_panes';
  $pane->subtype = 'merchandising-merch_search';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '93ef170d-0ed6-40fa-815e-c6451b93439c';
  $display->content['new-93ef170d-0ed6-40fa-815e-c6451b93439c'] = $pane;
  $display->panels['merch_list'][0] = 'new-93ef170d-0ed6-40fa-815e-c6451b93439c';
  $pane = new stdClass();
  $pane->pid = 'new-2031e8f7-e00a-45fb-8e89-2c3e096c93b7';
  $pane->panel = 'merch_sidebar';
  $pane->type = 'block';
  $pane->subtype = 'views--exp-merchandising-merch_search';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'inherit_path' => 1,
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2031e8f7-e00a-45fb-8e89-2c3e096c93b7';
  $display->content['new-2031e8f7-e00a-45fb-8e89-2c3e096c93b7'] = $pane;
  $display->panels['merch_sidebar'][0] = 'new-2031e8f7-e00a-45fb-8e89-2c3e096c93b7';
  $pane = new stdClass();
  $pane->pid = 'new-20b05a9b-ea17-4fdd-bbd6-10dc2f04f71b';
  $pane->panel = 'merch_sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-iiNqrU0Yewr0sN189Bzl1Egsy2SnmQlf';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Merchandising category',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '20b05a9b-ea17-4fdd-bbd6-10dc2f04f71b';
  $display->content['new-20b05a9b-ea17-4fdd-bbd6-10dc2f04f71b'] = $pane;
  $display->panels['merch_sidebar'][1] = 'new-20b05a9b-ea17-4fdd-bbd6-10dc2f04f71b';
  $pane = new stdClass();
  $pane->pid = 'new-7006be73-0ac5-4127-9907-8797dee8e0a3';
  $pane->panel = 'merch_sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-nhdnPVLZJP2boLtmHDAVrZwE1ys2v44r';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Stores',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '7006be73-0ac5-4127-9907-8797dee8e0a3';
  $display->content['new-7006be73-0ac5-4127-9907-8797dee8e0a3'] = $pane;
  $display->panels['merch_sidebar'][2] = 'new-7006be73-0ac5-4127-9907-8797dee8e0a3';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['taxonomy_term:merchandising_rubric:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'taxonomy_term';
  $panelizer->panelizer_key = 'merchandising_rubric';
  $panelizer->access = array();
  $panelizer->view_mode = 'indexed_result';
  $panelizer->name = 'taxonomy_term:merchandising_rubric:default:indexed_result';
  $panelizer->css_id = '';
  $panelizer->css_class = '';
  $panelizer->css = '';
  $panelizer->no_blocks = FALSE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'merch_item';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'merch_item_title' => NULL,
      'merch_item_image' => NULL,
      'merch_item_links' => NULL,
      'merch_item_stores' => NULL,
      'merch_item_docs' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'c50c6966-0a23-4e44-956f-66e23ae6c941';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'taxonomy_term:merchandising_rubric:default:indexed_result';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-2da99a11-7c9b-43fa-80b5-895ea1b8806f';
  $pane->panel = 'merch_item_docs';
  $pane->type = 'merch_rubric_docs';
  $pane->subtype = 'merch_rubric_docs';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'merch_rubric_docs_type' => 'files',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2da99a11-7c9b-43fa-80b5-895ea1b8806f';
  $display->content['new-2da99a11-7c9b-43fa-80b5-895ea1b8806f'] = $pane;
  $display->panels['merch_item_docs'][0] = 'new-2da99a11-7c9b-43fa-80b5-895ea1b8806f';
  $pane = new stdClass();
  $pane->pid = 'new-07401a1e-0b6e-4258-819d-a6827cced2e7';
  $pane->panel = 'merch_item_image';
  $pane->type = 'entity_field';
  $pane->subtype = 'taxonomy_term:field_merch_banner';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'news_banner',
      'image_link' => '',
    ),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '07401a1e-0b6e-4258-819d-a6827cced2e7';
  $display->content['new-07401a1e-0b6e-4258-819d-a6827cced2e7'] = $pane;
  $display->panels['merch_item_image'][0] = 'new-07401a1e-0b6e-4258-819d-a6827cced2e7';
  $pane = new stdClass();
  $pane->pid = 'new-93c05d6b-e680-4e65-a636-2236c984222c';
  $pane->panel = 'merch_item_links';
  $pane->type = 'merch_rubric_docs';
  $pane->subtype = 'merch_rubric_docs';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'merch_rubric_docs_type' => 'links',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '93c05d6b-e680-4e65-a636-2236c984222c';
  $display->content['new-93c05d6b-e680-4e65-a636-2236c984222c'] = $pane;
  $display->panels['merch_item_links'][0] = 'new-93c05d6b-e680-4e65-a636-2236c984222c';
  $pane = new stdClass();
  $pane->pid = 'new-0b9dc66c-d3fc-4350-86b0-97ef82bbdcff';
  $pane->panel = 'merch_item_stores';
  $pane->type = 'views_panes';
  $pane->subtype = 'merch_stores-merch_stores';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'arguments' => array(
      'tid' => '%taxonomy_term:tid',
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '0b9dc66c-d3fc-4350-86b0-97ef82bbdcff';
  $display->content['new-0b9dc66c-d3fc-4350-86b0-97ef82bbdcff'] = $pane;
  $display->panels['merch_item_stores'][0] = 'new-0b9dc66c-d3fc-4350-86b0-97ef82bbdcff';
  $pane = new stdClass();
  $pane->pid = 'new-5aa1f02d-f7cd-405f-a565-36de0599e827';
  $pane->panel = 'merch_item_title';
  $pane->type = 'term_name';
  $pane->subtype = 'term_name';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 0,
    'markup' => 'h2',
    'id' => '',
    'class' => '',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5aa1f02d-f7cd-405f-a565-36de0599e827';
  $display->content['new-5aa1f02d-f7cd-405f-a565-36de0599e827'] = $pane;
  $display->panels['merch_item_title'][0] = 'new-5aa1f02d-f7cd-405f-a565-36de0599e827';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['taxonomy_term:merchandising_rubric:default:indexed_result'] = $panelizer;

  return $export;
}
