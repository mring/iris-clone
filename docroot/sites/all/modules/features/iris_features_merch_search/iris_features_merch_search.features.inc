<?php
/**
 * @file
 * iris_features_merch_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_merch_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function iris_features_merch_search_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function iris_features_merch_search_default_search_api_index() {
  $items = array();
  $items['merchandising'] = entity_import('search_api_index', '{
    "name" : "Merchandising",
    "machine_name" : "merchandising",
    "description" : null,
    "server" : "solr_dev",
    "item_type" : "taxonomy_term",
    "options" : {
      "datasource" : { "bundles" : [ "merchandising_rubric" ] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "field_activity_domain" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_files:description" : { "type" : "list\\u003Ctext\\u003E" },
        "field_files:file" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "file" },
        "field_links:title" : { "type" : "list\\u003Ctext\\u003E" },
        "field_links:url" : { "type" : "list\\u003Curi\\u003E" },
        "merch_name" : { "type" : "string" },
        "merch_scope_stores" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "name" : { "type" : "text" },
        "name_field" : { "type" : "text" },
        "parents_all" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "search_api_language" : { "type" : "string" },
        "tid" : { "type" : "integer" }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}
