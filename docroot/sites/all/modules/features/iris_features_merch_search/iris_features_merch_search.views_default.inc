<?php
/**
 * @file
 * iris_features_merch_search.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function iris_features_merch_search_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'merch_stores';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Merch stores';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Taxonomy term: Merch location group */
  $handler->display->display_options['fields']['merch_location_group']['id'] = 'merch_location_group';
  $handler->display->display_options['fields']['merch_location_group']['table'] = 'views_entity_taxonomy_term';
  $handler->display->display_options['fields']['merch_location_group']['field'] = 'merch_location_group';
  $handler->display->display_options['fields']['merch_location_group']['label'] = 'CONCERNED STORES';
  $handler->display->display_options['fields']['merch_location_group']['element_type'] = '0';
  $handler->display->display_options['fields']['merch_location_group']['element_label_type'] = 'span';
  $handler->display->display_options['fields']['merch_location_group']['element_label_class'] = 'ttl';
  $handler->display->display_options['fields']['merch_location_group']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['merch_location_group']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['merch_location_group']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['merch_location_group']['list']['mode'] = 'list';
  $handler->display->display_options['fields']['merch_location_group']['link_to_entity'] = 0;
  /* Contextual filter: Taxonomy term: Term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'merchandising_rubric' => 'merchandising_rubric',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'merch_stores');
  $handler->display->display_options['argument_input'] = array(
    'tid' => array(
      'type' => 'user',
      'context' => 'entity:comment.author',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Taxonomy term: Term ID',
    ),
  );
  $translatables['merch_stores'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('CONCERNED STORES'),
    t('All'),
    t('Content pane'),
    t('View panes'),
  );
  $export['merch_stores'] = $view;

  $view = new view();
  $view->name = 'merchandising';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_merchandising';
  $view->human_name = 'Merchandising';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Merch search';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'iris_infinite_scroll';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['content_class'] = 'news-list-scroll';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No results.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Relationship: Indexed Taxonomy term: Activity domain */
  $handler->display->display_options['relationships']['field_activity_domain']['id'] = 'field_activity_domain';
  $handler->display->display_options['relationships']['field_activity_domain']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['relationships']['field_activity_domain']['field'] = 'field_activity_domain';
  /* Field: Indexed Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['element_type'] = '0';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_entity'] = 0;
  /* Field: Indexed Taxonomy term: Activity domain */
  $handler->display->display_options['fields']['field_activity_domain']['id'] = 'field_activity_domain';
  $handler->display->display_options['fields']['field_activity_domain']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['field_activity_domain']['field'] = 'field_activity_domain';
  $handler->display->display_options['fields']['field_activity_domain']['label'] = '';
  $handler->display->display_options['fields']['field_activity_domain']['element_type'] = '0';
  $handler->display->display_options['fields']['field_activity_domain']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_activity_domain']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_activity_domain']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_activity_domain']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_activity_domain']['bypass_access'] = 0;
  /* Field: Indexed Taxonomy term: Merch tracker */
  $handler->display->display_options['fields']['merch_tracker']['id'] = 'merch_tracker';
  $handler->display->display_options['fields']['merch_tracker']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['merch_tracker']['field'] = 'merch_tracker';
  $handler->display->display_options['fields']['merch_tracker']['label'] = '';
  $handler->display->display_options['fields']['merch_tracker']['element_type'] = '0';
  $handler->display->display_options['fields']['merch_tracker']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['merch_tracker']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['merch_tracker']['link_to_entity'] = 0;
  /* Field: Indexed Taxonomy term: Thumbnail */
  $handler->display->display_options['fields']['field_merch_thumbnail']['id'] = 'field_merch_thumbnail';
  $handler->display->display_options['fields']['field_merch_thumbnail']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['field_merch_thumbnail']['field'] = 'field_merch_thumbnail';
  $handler->display->display_options['fields']['field_merch_thumbnail']['label'] = '';
  $handler->display->display_options['fields']['field_merch_thumbnail']['element_type'] = '0';
  $handler->display->display_options['fields']['field_merch_thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_merch_thumbnail']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_merch_thumbnail']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_merch_thumbnail']['settings'] = array(
    'image_style' => '16x16',
    'image_link' => '',
  );
  /* Field: Indexed Taxonomy term: Merch rubric Location group scope stores */
  $handler->display->display_options['fields']['merch_scope_stores']['id'] = 'merch_scope_stores';
  $handler->display->display_options['fields']['merch_scope_stores']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['merch_scope_stores']['field'] = 'merch_scope_stores';
  /* Sort criterion: Indexed Taxonomy term: Name as string */
  $handler->display->display_options['sorts']['merch_name']['id'] = 'merch_name';
  $handler->display->display_options['sorts']['merch_name']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['sorts']['merch_name']['field'] = 'merch_name';
  /* Contextual filter: Search: Fulltext search */
  $handler->display->display_options['arguments']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['search_api_views_fulltext']['not'] = 0;
  $handler->display->display_options['arguments']['search_api_views_fulltext']['conjunction'] = 'OR';
  /* Contextual filter: Indexed Taxonomy term: Term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['tid']['not'] = 0;
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['operator'] = 'OR';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'query';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['min_length'] = '3';

  /* Display: Merch list */
  $handler = $view->new_display('panel_pane', 'Merch list', 'merch_search');
  $handler->display->display_options['defaults']['use_ajax'] = FALSE;
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'iris_infinite_scroll';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['content_class'] = 'news-list-scroll';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['class'] = 'news-list';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Indexed Taxonomy term: Activity domain */
  $handler->display->display_options['relationships']['field_activity_domain']['id'] = 'field_activity_domain';
  $handler->display->display_options['relationships']['field_activity_domain']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['relationships']['field_activity_domain']['field'] = 'field_activity_domain';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Indexed Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['element_type'] = '0';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_entity'] = 0;
  /* Field: Taxonomy term: Designation */
  $handler->display->display_options['fields']['description_field']['id'] = 'description_field';
  $handler->display->display_options['fields']['description_field']['table'] = 'entity_taxonomy_term';
  $handler->display->display_options['fields']['description_field']['field'] = 'description_field';
  $handler->display->display_options['fields']['description_field']['relationship'] = 'field_activity_domain';
  $handler->display->display_options['fields']['description_field']['label'] = '';
  $handler->display->display_options['fields']['description_field']['element_type'] = '0';
  $handler->display->display_options['fields']['description_field']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['description_field']['element_default_classes'] = FALSE;
  /* Field: Indexed Taxonomy term: Merch tracker */
  $handler->display->display_options['fields']['merch_tracker']['id'] = 'merch_tracker';
  $handler->display->display_options['fields']['merch_tracker']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['merch_tracker']['field'] = 'merch_tracker';
  $handler->display->display_options['fields']['merch_tracker']['label'] = '';
  $handler->display->display_options['fields']['merch_tracker']['exclude'] = TRUE;
  $handler->display->display_options['fields']['merch_tracker']['element_type'] = '0';
  $handler->display->display_options['fields']['merch_tracker']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['merch_tracker']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['merch_tracker']['link_to_entity'] = 0;
  /* Field: Indexed Taxonomy term: Thumbnail */
  $handler->display->display_options['fields']['field_merch_thumbnail']['id'] = 'field_merch_thumbnail';
  $handler->display->display_options['fields']['field_merch_thumbnail']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['field_merch_thumbnail']['field'] = 'field_merch_thumbnail';
  $handler->display->display_options['fields']['field_merch_thumbnail']['label'] = '';
  $handler->display->display_options['fields']['field_merch_thumbnail']['element_type'] = '0';
  $handler->display->display_options['fields']['field_merch_thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_merch_thumbnail']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_merch_thumbnail']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_merch_thumbnail']['settings'] = array(
    'image_style' => '16x16',
    'image_link' => '',
  );
  /* Field: Global: Date interval */
  $handler->display->display_options['fields']['date_interval']['id'] = 'date_interval';
  $handler->display->display_options['fields']['date_interval']['table'] = 'views';
  $handler->display->display_options['fields']['date_interval']['field'] = 'date_interval';
  $handler->display->display_options['fields']['date_interval']['date_field'] = 'merch_tracker';
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Merch item */
  $handler = $view->new_display('panel_pane', 'Merch item', 'merch_item_pane');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Merch item';
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'indexed_result';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Indexed Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['element_type'] = '0';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_entity'] = 0;
  /* Field: Indexed Taxonomy term: Banner image */
  $handler->display->display_options['fields']['field_merch_banner']['id'] = 'field_merch_banner';
  $handler->display->display_options['fields']['field_merch_banner']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['field_merch_banner']['field'] = 'field_merch_banner';
  $handler->display->display_options['fields']['field_merch_banner']['label'] = '';
  $handler->display->display_options['fields']['field_merch_banner']['element_type'] = '0';
  $handler->display->display_options['fields']['field_merch_banner']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_merch_banner']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_merch_banner']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_merch_banner']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Indexed Taxonomy term: Files */
  $handler->display->display_options['fields']['field_files']['id'] = 'field_files';
  $handler->display->display_options['fields']['field_files']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['field_files']['field'] = 'field_files';
  $handler->display->display_options['fields']['field_files']['label'] = 'Documents';
  $handler->display->display_options['fields']['field_files']['element_type'] = '0';
  $handler->display->display_options['fields']['field_files']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_files']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_files']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_files']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_files']['separator'] = '';
  /* Field: Indexed Taxonomy term: Links */
  $handler->display->display_options['fields']['field_links']['id'] = 'field_links';
  $handler->display->display_options['fields']['field_links']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['field_links']['field'] = 'field_links';
  $handler->display->display_options['fields']['field_links']['element_type'] = '0';
  $handler->display->display_options['fields']['field_links']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_links']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_links']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_links']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_links']['separator'] = ' ';
  /* Field: Indexed Taxonomy term: Merch rubric Location group scope stores */
  $handler->display->display_options['fields']['merch_scope_stores']['id'] = 'merch_scope_stores';
  $handler->display->display_options['fields']['merch_scope_stores']['table'] = 'search_api_index_merchandising';
  $handler->display->display_options['fields']['merch_scope_stores']['field'] = 'merch_scope_stores';
  $handler->display->display_options['fields']['merch_scope_stores']['label'] = 'Stores';
  $handler->display->display_options['fields']['merch_scope_stores']['element_type'] = '0';
  $handler->display->display_options['fields']['merch_scope_stores']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['merch_scope_stores']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['merch_scope_stores']['list']['mode'] = 'list';
  $handler->display->display_options['fields']['merch_scope_stores']['list']['separator'] = ' ';
  $handler->display->display_options['fields']['merch_scope_stores']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['merch_scope_stores']['view_mode'] = 'full';
  $handler->display->display_options['fields']['merch_scope_stores']['bypass_access'] = 0;
  $translatables['merchandising'] = array(
    t('Master'),
    t('Merch search'),
    t('more'),
    t('Search'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('No results.'),
    t('Activity domain'),
    t('Merch rubric Location group scope stores'),
    t('All'),
    t('Merch list'),
    t('Date interval'),
    t('View panes'),
    t('Merch item'),
    t('Documents'),
    t('Links'),
    t('Stores'),
  );
  $export['merchandising'] = $view;

  return $export;
}
