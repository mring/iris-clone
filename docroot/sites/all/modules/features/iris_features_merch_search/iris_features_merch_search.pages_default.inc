<?php
/**
 * @file
 * iris_features_merch_search.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_merch_search_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'merchandising';
  $page->task = 'page';
  $page->admin_title = 'Merchandising';
  $page->admin_description = 'Merch search';
  $page->path = 'merchandising';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Merch',
    'name' => 'main-menu',
    'weight' => '10',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_merchandising__panel';
  $handler->task = 'page';
  $handler->subtask = 'merchandising';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'access' => array(
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'merch_3_cols';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'news_sidebar' => NULL,
      'news_list' => NULL,
      'news_content' => NULL,
      'merch_sidebar' => NULL,
      'merch_list' => NULL,
      'merch_content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'a9cb72cd-b6da-4766-8a2b-b2ac32c59f88';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_merchandising__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-05ac6ffd-d245-432a-9073-43f6893a7632';
  $pane->panel = 'merch_content';
  $pane->type = 'views_panes';
  $pane->subtype = 'merchandising-merch_item_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '05ac6ffd-d245-432a-9073-43f6893a7632';
  $display->content['new-05ac6ffd-d245-432a-9073-43f6893a7632'] = $pane;
  $display->panels['merch_content'][0] = 'new-05ac6ffd-d245-432a-9073-43f6893a7632';
  $pane = new stdClass();
  $pane->pid = 'new-5f88e0a3-9b91-4ce4-a9e0-b8eacaf9e9bf';
  $pane->panel = 'merch_list';
  $pane->type = 'views_panes';
  $pane->subtype = 'merchandising-merch_search';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5f88e0a3-9b91-4ce4-a9e0-b8eacaf9e9bf';
  $display->content['new-5f88e0a3-9b91-4ce4-a9e0-b8eacaf9e9bf'] = $pane;
  $display->panels['merch_list'][0] = 'new-5f88e0a3-9b91-4ce4-a9e0-b8eacaf9e9bf';
  $pane = new stdClass();
  $pane->pid = 'new-3ec6e3cb-d071-4e5b-b267-92f03bc3c39f';
  $pane->panel = 'merch_sidebar';
  $pane->type = 'block';
  $pane->subtype = 'views--exp-merchandising-merch_search';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'inherit_path' => 1,
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '3ec6e3cb-d071-4e5b-b267-92f03bc3c39f';
  $display->content['new-3ec6e3cb-d071-4e5b-b267-92f03bc3c39f'] = $pane;
  $display->panels['merch_sidebar'][0] = 'new-3ec6e3cb-d071-4e5b-b267-92f03bc3c39f';
  $pane = new stdClass();
  $pane->pid = 'new-cf496338-8a4d-479f-848a-e969a029d099';
  $pane->panel = 'merch_sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-iiNqrU0Yewr0sN189Bzl1Egsy2SnmQlf';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Merchandising category',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'cf496338-8a4d-479f-848a-e969a029d099';
  $display->content['new-cf496338-8a4d-479f-848a-e969a029d099'] = $pane;
  $display->panels['merch_sidebar'][1] = 'new-cf496338-8a4d-479f-848a-e969a029d099';
  $pane = new stdClass();
  $pane->pid = 'new-4f48bc22-88a4-45c4-a9dd-1ed99de64044';
  $pane->panel = 'merch_sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-nhdnPVLZJP2boLtmHDAVrZwE1ys2v44r';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Stores',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '4f48bc22-88a4-45c4-a9dd-1ed99de64044';
  $display->content['new-4f48bc22-88a4-45c4-a9dd-1ed99de64044'] = $pane;
  $display->panels['merch_sidebar'][2] = 'new-4f48bc22-88a4-45c4-a9dd-1ed99de64044';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-3ec6e3cb-d071-4e5b-b267-92f03bc3c39f';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['merchandising'] = $page;

  return $pages;

}
