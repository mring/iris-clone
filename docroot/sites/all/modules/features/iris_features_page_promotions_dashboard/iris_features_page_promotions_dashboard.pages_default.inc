<?php
/**
 * @file
 * iris_features_page_promotions_dashboard.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_promotions_dashboard_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'promotions_dashboard';
  $page->task = 'page';
  $page->admin_title = 'Promotions dashboard';
  $page->admin_description = '';
  $page->path = 'admin/promotions/promotions-dashboard';
  $page->access = array(
    'logic' => 'and',
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'access promotions dashboard',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_promotions_dashboard__panel';
  $handler->task = 'page';
  $handler->subtask = 'promotions_dashboard';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Promotions dashboard',
    'panels_breadcrumbs_paths' => 'admin/promotions/promotions-dashboard',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'center_' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Promotions dashboard';
  $display->uuid = 'f174626e-27c8-4e9e-a0b7-019a4301e12a';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_promotions_dashboard__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1fb44506-cd07-4aab-a41c-c86d5ebc7cc7';
  $pane->panel = 'center';
  $pane->type = 'block';
  $pane->subtype = 'iris_core-request_export';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1fb44506-cd07-4aab-a41c-c86d5ebc7cc7';
  $display->content['new-1fb44506-cd07-4aab-a41c-c86d5ebc7cc7'] = $pane;
  $display->panels['center'][0] = 'new-1fb44506-cd07-4aab-a41c-c86d5ebc7cc7';
  $pane = new stdClass();
  $pane->pid = 'new-65b5eae9-52e3-4669-bf05-6870b6bef015';
  $pane->panel = 'center';
  $pane->type = 'views_panes';
  $pane->subtype = 'promotions_dashboard-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '65b5eae9-52e3-4669-bf05-6870b6bef015';
  $display->content['new-65b5eae9-52e3-4669-bf05-6870b6bef015'] = $pane;
  $display->panels['center'][1] = 'new-65b5eae9-52e3-4669-bf05-6870b6bef015';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['promotions_dashboard'] = $page;

  return $pages;

}
