<?php
/**
 * @file
 * Contains helper functions.
 */

/**
 * Action handler to validate promotions reference.
 */
function iris_features_page_promotions_dashboard_validate_reference(&$node, $context) {
  global $user;
  $admin_roles_condition = array_intersect(array('webmaster', 'administrator'), $user->roles);
  $nonadmins_condition = array_intersect(
      array(
        'operation planning responsible',
        'operation contributor',
        'operation upload and validation',
        'operation responsible',
      ), $user->roles);

  if ($node->type == 'promotion' && ($admin_roles_condition || $nonadmins_condition)) {
    $date = new DateTime();
    $node->field_reference_validation[LANGUAGE_NONE][0]['value'] = $date->format('Y-m-d');
    $node->field_reference_validation[LANGUAGE_NONE][0]['timezone'] = $date->getTimezone();
    $node->field_reference_validation[LANGUAGE_NONE][0]['timezone_db'] = $date->getTimezone();
    $node->field_reference_validation[LANGUAGE_NONE][0]['date_type'] = 'datetime';
    $node->status = NODE_PUBLISHED;
  }
}

/**
 * Action handler to invalidate promotions reference.
 */
function iris_features_page_promotions_dashboard_invalidate_reference(&$node, $context) {
  global $user;

  if ($node->type == 'promotion' && array_intersect(
      array(
        'administrator',
        'webmaster',
        'operation planning responsible',
        'operation contributor',
        'operation responsible',
      ), $user->roles)) {
    $node->field_reference_validation = array();
    $node->status = NODE_NOT_PUBLISHED;
  }
}
