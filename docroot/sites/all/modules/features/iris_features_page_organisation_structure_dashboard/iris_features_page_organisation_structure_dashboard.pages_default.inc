<?php
/**
 * @file
 * iris_features_page_organisation_structure_dashboard.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_organisation_structure_dashboard_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'organisation_structure';
  $page->task = 'page';
  $page->admin_title = 'Organisation structure';
  $page->admin_description = '';
  $page->path = 'admin/taxonomy/organisation-structure';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_organisation_structure__panneau';
  $handler->task = 'page';
  $handler->subtask = 'organisation_structure';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Organisation structure dashboard',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panneau',
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Organisation structure dashboard';
  $display->uuid = '67d5941e-26ef-4ecc-a648-e7de5e0bb1d2';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_organisation_structure__panneau';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-611d4c3a-c493-4900-923c-04f8d3bbdcd9';
  $pane->panel = 'center';
  $pane->type = 'iris_organisation_structure_filters';
  $pane->subtype = 'iris_organisation_structure_filters';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '611d4c3a-c493-4900-923c-04f8d3bbdcd9';
  $display->content['new-611d4c3a-c493-4900-923c-04f8d3bbdcd9'] = $pane;
  $display->panels['center'][0] = 'new-611d4c3a-c493-4900-923c-04f8d3bbdcd9';
  $pane = new stdClass();
  $pane->pid = 'new-6dafd2f2-64e7-4720-b252-458eb6ae128d';
  $pane->panel = 'center';
  $pane->type = 'iris_organisation_structure_panel';
  $pane->subtype = 'iris_organisation_structure_panel';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '6dafd2f2-64e7-4720-b252-458eb6ae128d';
  $display->content['new-6dafd2f2-64e7-4720-b252-458eb6ae128d'] = $pane;
  $display->panels['center'][1] = 'new-6dafd2f2-64e7-4720-b252-458eb6ae128d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['organisation_structure'] = $page;

  return $pages;

}
