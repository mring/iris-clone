<?php
/**
 * @file
 * iris_features_taxonomy_supplier.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_supplier_taxonomy_default_vocabularies() {
  return array(
    'supplier' => array(
      'name' => '​Supplier',
      'machine_name' => 'supplier',
      'description' => 'Suppliers are generated during the SAP product import (See product entity section). They are referenced by a product.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 2,
    ),
  );
}
