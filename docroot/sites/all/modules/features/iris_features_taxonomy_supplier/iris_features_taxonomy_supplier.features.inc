<?php
/**
 * @file
 * iris_features_taxonomy_supplier.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_taxonomy_supplier_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
