<?php
/**
 * @file
 * iris_features_ct_product.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_ct_product_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function iris_features_ct_product_node_info() {
  $items = array(
    'product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => t('The products are imported automatically via a SAP web service (See SAP documentation) during the promotions import. If a SKU is not found during the promotion import, it’s imported from SAP. In case the SKU already exists in Drupal, it’s updated to match the last version from SAP.'),
      'has_title' => '1',
      'title_label' => t('SKU'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
