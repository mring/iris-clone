<?php
/**
 * @file
 * iris_features_taxonomy_company_code.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function iris_features_taxonomy_company_code_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_company_code_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_company_code_pattern'] = $strongarm;

  return $export;
}
