<?php
/**
 * @file
 * iris_features_taxonomy_company_code.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_company_code_taxonomy_default_vocabularies() {
  return array(
    'company_code' => array(
      'name' => 'Company code',
      'machine_name' => 'company_code',
      'description' => 'The company code is part of the location group (2nd level). The taxonomy is only populated via the import of the location group',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -7,
    ),
  );
}
