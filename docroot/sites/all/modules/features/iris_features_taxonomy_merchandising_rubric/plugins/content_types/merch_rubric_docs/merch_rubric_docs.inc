<?php

/**
 * @file
 *
 * Merch rubric's documents CTools plugin file.
 */

$plugin = array(
  'title'            => t("Merch rubric's documents"),
  'single'           => TRUE,
  'category'         => t('IRIS'),
  'render callback'  => 'merch_rubric_docs_content_type_render',
  'edit form'        => 'merch_rubric_docs_content_type_edit_form',
  'required context' => new ctools_context_required(t('Term'), array(
    'taxonomy_term',
    'terms'
  )),
);

/**
 * Render callback.
 */
function merch_rubric_docs_content_type_render($subtype, $conf, $panel_args, $context) {
  $tid = !empty($context->data->tid) ? $context->data->tid : FALSE;
  if (!$tid) {
    return;
  }

  if (isset($conf['merch_rubric_docs_type'])) {
    $field = 'field_' . $conf['merch_rubric_docs_type'];
  }

  $store = irish_search_get_current_store(new stdClass());

  $block = new stdClass();
  $block->title = NULL;
  $block->content = '';
  $docs = array();

  $merch_elements = _merch_rubric_get_store_merch_elements($context->data, $store);

  foreach ($merch_elements as $element) {
    $wrapper = entity_metadata_wrapper('node', $element);
    if (isset($wrapper->field_files)) {
      if (isset($wrapper->{$field})) {
        $el_docs = $wrapper->{$field}->value();
        $docs = array_merge($docs, $el_docs);
      }
    }
  }

  if (count($docs) > 0) {
    $block->content .= '<ul class="links-list">';

    foreach ($docs as $doc) {
      if ($conf['merch_rubric_docs_type'] == 'files') {
        $block->content .= theme('file_link', array('file' => (object) $doc));
      }
      else {
        $block->content .= theme('link_formatter_link_default', array('element' => $doc));
      }
    }

    $block->content .= '</ul>';
  }

  return $block;
}

/**
 * Settings form.
 */
function merch_rubric_docs_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $types = array(
    'files' => t('Files'),
    'links' => t('Links')
  );

  $form['merch_rubric_docs_type'] = array(
    '#type'          => 'select',
    '#title'         => t('Type of documents'),
    '#options'       => $types,
    '#default_value' => isset($conf['merch_rubric_docs_type']) ? $conf['merch_rubric_docs_type'] : '',
  );

  return $form;
}

/**
 * Settings form submit.
 */
function merch_rubric_docs_content_type_edit_form_submit($form, &$form_state) {
  $keys = array('merch_rubric_docs_type');

  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Get all merch rubrics elements.
 */
function _merch_rubric_get_store_merch_elements($term, $store) {
  $ancestors = iris_search_get_merch_rubric_ancestors($term->tid);
  $merch_elements = array();

  if ($ancestors) {
    foreach ($ancestors as $ancestor_id) {
      $merch_element = node_load($ancestor_id);
        //check if merch element is publish
        //do not display documents for unpublish merch
        if($merch_element->status){
        $merch_element_stores = iris_search_get_location_group_scope_stores($merch_element);

        if (in_array($store, $merch_element_stores) || !$store) {
          $merch_elements[] = $merch_element;
        }
      }
    }
  }

  return $merch_elements;
}
