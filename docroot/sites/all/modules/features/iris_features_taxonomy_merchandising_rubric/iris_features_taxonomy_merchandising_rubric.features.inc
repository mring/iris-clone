<?php
/**
 * @file
 * iris_features_taxonomy_merchandising_rubric.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_taxonomy_merchandising_rubric_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function iris_features_taxonomy_merchandising_rubric_image_default_styles() {
  $styles = array();

  // Exported image style: 16x16.
  $styles['16x16'] = array(
    'label' => '40x40',
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 40,
          'height' => 40,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
