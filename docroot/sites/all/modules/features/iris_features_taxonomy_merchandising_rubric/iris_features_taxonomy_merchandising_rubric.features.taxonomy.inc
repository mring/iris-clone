<?php
/**
 * @file
 * iris_features_taxonomy_merchandising_rubric.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function iris_features_taxonomy_merchandising_rubric_taxonomy_default_vocabularies() {
  return array(
    'merchandising_rubric' => array(
      'name' => 'Merchandising rubric',
      'machine_name' => 'merchandising_rubric',
      'description' => 'The Merchandising rubric taxonomy is used to classify the Merchandising element on the Merchandising page.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
