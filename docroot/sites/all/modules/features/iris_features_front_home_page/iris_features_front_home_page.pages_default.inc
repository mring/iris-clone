<?php
/**
 * @file
 * iris_features_front_home_page.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_front_home_page_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'home_page';
  $page->task = 'page';
  $page->admin_title = 'Home page';
  $page->admin_description = '';
  $page->path = 'home';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_home_page__panel';
  $handler->task = 'page';
  $handler->subtask = 'home_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Home',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'news_3_cols';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'middle' => NULL,
      'right' => NULL,
      'news_sidebar' => NULL,
      'news_list' => NULL,
      'news_content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Home';
  $display->uuid = '8a0d3e04-1e4c-4f8f-a40d-fada8dd7bdc7';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-7f11f90e-b001-4f37-83aa-301fa1e9f923';
    $pane->panel = 'news_content';
    $pane->type = 'home_panel';
    $pane->subtype = 'home_panel';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7f11f90e-b001-4f37-83aa-301fa1e9f923';
    $display->content['new-7f11f90e-b001-4f37-83aa-301fa1e9f923'] = $pane;
    $display->panels['news_content'][0] = 'new-7f11f90e-b001-4f37-83aa-301fa1e9f923';
    $pane = new stdClass();
    $pane->pid = 'new-5fa56181-5244-4c98-8dbb-0caab3fb81a6';
    $pane->panel = 'news_list';
    $pane->type = 'views_panes';
    $pane->subtype = 'news_search-news_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array(
      'method' => '0',
      'settings' => array(),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5fa56181-5244-4c98-8dbb-0caab3fb81a6';
    $display->content['new-5fa56181-5244-4c98-8dbb-0caab3fb81a6'] = $pane;
    $display->panels['news_list'][0] = 'new-5fa56181-5244-4c98-8dbb-0caab3fb81a6';
    $pane = new stdClass();
    $pane->pid = 'new-94690edf-0eea-451f-bc56-4566aa443788';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'views--exp-news_search-news_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'inherit_path' => 1,
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '94690edf-0eea-451f-bc56-4566aa443788';
    $display->content['new-94690edf-0eea-451f-bc56-4566aa443788'] = $pane;
    $display->panels['news_sidebar'][0] = 'new-94690edf-0eea-451f-bc56-4566aa443788';
    $pane = new stdClass();
    $pane->pid = 'new-13fe62d7-d28a-497a-b03d-95fb7a6a061e';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-m0RbYLiBmeDC98s6ICzg1R4b09oUeUkH';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'My news',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '13fe62d7-d28a-497a-b03d-95fb7a6a061e';
    $display->content['new-13fe62d7-d28a-497a-b03d-95fb7a6a061e'] = $pane;
    $display->panels['news_sidebar'][1] = 'new-13fe62d7-d28a-497a-b03d-95fb7a6a061e';
    $pane = new stdClass();
    $pane->pid = 'new-0d751287-8641-4bff-95a9-cf425852227d';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-x3joQCSFyH9PfSNalcOuSISHe2XSZJO1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Themes',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '0d751287-8641-4bff-95a9-cf425852227d';
    $display->content['new-0d751287-8641-4bff-95a9-cf425852227d'] = $pane;
    $display->panels['news_sidebar'][2] = 'new-0d751287-8641-4bff-95a9-cf425852227d';
    $pane = new stdClass();
    $pane->pid = 'new-a3b5c00b-b1d2-4eef-b165-86990df5d2c6';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-5eet10bAhFk6qdYI9x9DWiXyEUWg4C5U';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Stores',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'a3b5c00b-b1d2-4eef-b165-86990df5d2c6';
    $display->content['new-a3b5c00b-b1d2-4eef-b165-86990df5d2c6'] = $pane;
    $display->panels['news_sidebar'][3] = 'new-a3b5c00b-b1d2-4eef-b165-86990df5d2c6';
    $pane = new stdClass();
    $pane->pid = 'new-de3993f5-323e-4191-866f-779f4527ff15';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-1EQhlKXYYYZJiHvPCCBJz3w3Y3R1NInX';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Universe',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'de3993f5-323e-4191-866f-779f4527ff15';
    $display->content['new-de3993f5-323e-4191-866f-779f4527ff15'] = $pane;
    $display->panels['news_sidebar'][4] = 'new-de3993f5-323e-4191-866f-779f4527ff15';
    $pane = new stdClass();
    $pane->pid = 'new-633943e3-d3b2-4df8-a653-73b7cf458b57';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-C79HN4NUBVPtGuXLhndN0xUhEQBmFKxu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'News status',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '633943e3-d3b2-4df8-a653-73b7cf458b57';
    $display->content['new-633943e3-d3b2-4df8-a653-73b7cf458b57'] = $pane;
    $display->panels['news_sidebar'][5] = 'new-633943e3-d3b2-4df8-a653-73b7cf458b57';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['home_page'] = $page;

  return $pages;

}
