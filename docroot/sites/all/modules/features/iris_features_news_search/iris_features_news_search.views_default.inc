<?php
/**
 * @file
 * iris_features_news_search.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function iris_features_news_search_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'news_search';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_news';
  $view->human_name = 'News search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'News search';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'iris_infinite_scroll';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['content_class'] = 'news-list-scroll';
  $handler->display->display_options['style_plugin'] = 'iris_news';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_promotion',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_news';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_type'] = '0';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: Indexed Node: News theme */
  $handler->display->display_options['fields']['field_news_type']['id'] = 'field_news_type';
  $handler->display->display_options['fields']['field_news_type']['table'] = 'search_api_index_news';
  $handler->display->display_options['fields']['field_news_type']['field'] = 'field_news_type';
  $handler->display->display_options['fields']['field_news_type']['label'] = '';
  $handler->display->display_options['fields']['field_news_type']['element_type'] = '0';
  $handler->display->display_options['fields']['field_news_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_news_type']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_news_type']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_news_type']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_news_type']['bypass_access'] = 0;
  /* Field: Link to a promotion: Node ID (indexed) */
  $handler->display->display_options['fields']['field_promotion_nid']['id'] = 'field_promotion_nid';
  $handler->display->display_options['fields']['field_promotion_nid']['table'] = 'search_api_index_news';
  $handler->display->display_options['fields']['field_promotion_nid']['field'] = 'field_promotion_nid';
  $handler->display->display_options['fields']['field_promotion_nid']['label'] = '';
  $handler->display->display_options['fields']['field_promotion_nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_promotion_nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_promotion_nid']['separator'] = '';
  $handler->display->display_options['fields']['field_promotion_nid']['link_to_entity'] = 0;
  /* Sort criterion: Indexed Node: Date created */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'search_api_index_news';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['sorts']['created']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['created']['expose']['label'] = 'VIEW STANDARD';
  /* Sort criterion: Indexed Node: Action date deadline */
  $handler->display->display_options['sorts']['field_action_date_deadline']['id'] = 'field_action_date_deadline';
  $handler->display->display_options['sorts']['field_action_date_deadline']['table'] = 'search_api_index_news';
  $handler->display->display_options['sorts']['field_action_date_deadline']['field'] = 'field_action_date_deadline';
  $handler->display->display_options['sorts']['field_action_date_deadline']['order'] = 'DESC';
  $handler->display->display_options['sorts']['field_action_date_deadline']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_action_date_deadline']['expose']['label'] = 'VIEW DEADLINE';
  /* Contextual filter: Search: Fulltext search */
  $handler->display->display_options['arguments']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['table'] = 'search_api_index_news';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['search_api_views_fulltext']['not'] = 0;
  $handler->display->display_options['arguments']['search_api_views_fulltext']['conjunction'] = 'OR';
  /* Filter criterion: Indexed Node: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'search_api_index_news';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = array(
    1 => '1',
  );
  $handler->display->display_options['filters']['status']['group'] = 1;
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_news';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['operator'] = 'OR';
  $handler->display->display_options['filters']['search_api_views_fulltext']['group'] = 1;
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Search';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'query';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['min_length'] = '3';

  /* Display: News list pane */
  $handler = $view->new_display('panel_pane', 'News list pane', 'news_pane');
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No results.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Indexed Node: Location group scope stores */
  $handler->display->display_options['fields']['scope_stores']['id'] = 'scope_stores';
  $handler->display->display_options['fields']['scope_stores']['table'] = 'search_api_index_news';
  $handler->display->display_options['fields']['scope_stores']['field'] = 'scope_stores';
  $handler->display->display_options['fields']['scope_stores']['label'] = '';
  $handler->display->display_options['fields']['scope_stores']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['scope_stores']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['scope_stores']['view_mode'] = 'full';
  $handler->display->display_options['fields']['scope_stores']['bypass_access'] = 0;
  /* Field: Indexed Node: Date start */
  $handler->display->display_options['fields']['field_date_start']['id'] = 'field_date_start';
  $handler->display->display_options['fields']['field_date_start']['table'] = 'search_api_index_news';
  $handler->display->display_options['fields']['field_date_start']['field'] = 'field_date_start';
  $handler->display->display_options['fields']['field_date_start']['label'] = '';
  $handler->display->display_options['fields']['field_date_start']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_date_start']['element_type'] = '0';
  $handler->display->display_options['fields']['field_date_start']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_date_start']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_date_start']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Indexed Node: Date end */
  $handler->display->display_options['fields']['field_date_end']['id'] = 'field_date_end';
  $handler->display->display_options['fields']['field_date_end']['table'] = 'search_api_index_news';
  $handler->display->display_options['fields']['field_date_end']['field'] = 'field_date_end';
  $handler->display->display_options['fields']['field_date_end']['label'] = '';
  $handler->display->display_options['fields']['field_date_end']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_date_end']['element_type'] = '0';
  $handler->display->display_options['fields']['field_date_end']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_date_end']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_date_end']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Indexed Node: News statuses */
  $handler->display->display_options['fields']['news_statuses']['id'] = 'news_statuses';
  $handler->display->display_options['fields']['news_statuses']['table'] = 'search_api_index_news';
  $handler->display->display_options['fields']['news_statuses']['field'] = 'news_statuses';
  $handler->display->display_options['fields']['news_statuses']['label'] = '';
  $handler->display->display_options['fields']['news_statuses']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['news_statuses']['list']['separator'] = '|';
  $handler->display->display_options['fields']['news_statuses']['link_to_entity'] = 0;
  /* Field: Indexed Node: News theme */
  $handler->display->display_options['fields']['field_news_type']['id'] = 'field_news_type';
  $handler->display->display_options['fields']['field_news_type']['table'] = 'search_api_index_news';
  $handler->display->display_options['fields']['field_news_type']['field'] = 'field_news_type';
  $handler->display->display_options['fields']['field_news_type']['label'] = '';
  $handler->display->display_options['fields']['field_news_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_news_type']['element_type'] = '0';
  $handler->display->display_options['fields']['field_news_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_news_type']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_news_type']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_news_type']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_news_type']['bypass_access'] = 0;
  /* Field: Indexed Node: Date created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'search_api_index_news';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['exclude'] = TRUE;
  $handler->display->display_options['fields']['created']['element_type'] = '0';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'time ago';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'short_without_time';
  $handler->display->display_options['fields']['created']['link_to_entity'] = 0;
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_news';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_type'] = '0';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: Indexed Node: Link to a promotion */
  $handler->display->display_options['fields']['field_promotion']['id'] = 'field_promotion';
  $handler->display->display_options['fields']['field_promotion']['table'] = 'search_api_index_news';
  $handler->display->display_options['fields']['field_promotion']['field'] = 'field_promotion';
  $handler->display->display_options['fields']['field_promotion']['label'] = '';
  $handler->display->display_options['fields']['field_promotion']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_promotion']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_promotion']['display'] = 'id';
  $handler->display->display_options['fields']['field_promotion']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_promotion']['bypass_access'] = 0;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Search: Fulltext search */
  $handler->display->display_options['arguments']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['table'] = 'search_api_index_news';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['search_api_views_fulltext']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['search_api_views_fulltext']['not'] = 0;
  $handler->display->display_options['arguments']['search_api_views_fulltext']['conjunction'] = 'OR';
  /* Contextual filter: Indexed Node: Date created */
  $handler->display->display_options['arguments']['created']['id'] = 'created';
  $handler->display->display_options['arguments']['created']['table'] = 'search_api_index_news';
  $handler->display->display_options['arguments']['created']['field'] = 'created';
  $handler->display->display_options['arguments']['created']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['created']['not'] = 0;
  /* Contextual filter: Indexed Node: Action date deadline */
  $handler->display->display_options['arguments']['field_action_date_deadline']['id'] = 'field_action_date_deadline';
  $handler->display->display_options['arguments']['field_action_date_deadline']['table'] = 'search_api_index_news';
  $handler->display->display_options['arguments']['field_action_date_deadline']['field'] = 'field_action_date_deadline';
  $handler->display->display_options['arguments']['field_action_date_deadline']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_action_date_deadline']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_action_date_deadline']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['field_action_date_deadline']['not'] = 0;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Indexed Node: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'search_api_index_news';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = array(
    1 => '1',
  );
  $handler->display->display_options['filters']['status']['group'] = 1;
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_news';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['operator'] = 'OR';
  $handler->display->display_options['filters']['search_api_views_fulltext']['group'] = 1;
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Search';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'query';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['min_length'] = '3';

  /* Display: News item pane */
  $handler = $view->new_display('panel_pane', 'News item pane', 'news_item_pane');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'News item';
  $handler->display->display_options['defaults']['use_ajax'] = FALSE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $translatables['news_search'] = array(
    t('Master'),
    t('News search'),
    t('more'),
    t('Search'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('.'),
    t('VIEW STANDARD'),
    t('VIEW DEADLINE'),
    t('All'),
    t('News list pane'),
    t('No results.'),
    t('View panes'),
    t('News item pane'),
    t('News item'),
  );
  $export['news_search'] = $view;

  return $export;
}
