<?php
/**
 * @file
 * iris_features_news_search.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_news_search_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'news_page';
  $page->task = 'page';
  $page->admin_title = 'News feed and search page';
  $page->admin_description = '';
  $page->path = 'news';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_news_page__panel';
  $handler->task = 'page';
  $handler->subtask = 'news_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'News',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'news_3_cols';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'news_sidebar' => NULL,
      'news_list' => NULL,
      'news_content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '5da3bdea-2211-4cf5-9f48-062b9cde88b0';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-9723a3fb-f141-4a95-b910-dd05dbaa4f50';
    $pane->panel = 'news_content';
    $pane->type = 'views_panes';
    $pane->subtype = 'news_search-news_item_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9723a3fb-f141-4a95-b910-dd05dbaa4f50';
    $display->content['new-9723a3fb-f141-4a95-b910-dd05dbaa4f50'] = $pane;
    $display->panels['news_content'][0] = 'new-9723a3fb-f141-4a95-b910-dd05dbaa4f50';
    $pane = new stdClass();
    $pane->pid = 'new-5899e7fd-b3de-4141-a08e-32135ce491d7';
    $pane->panel = 'news_list';
    $pane->type = 'views_panes';
    $pane->subtype = 'news_search-news_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5899e7fd-b3de-4141-a08e-32135ce491d7';
    $display->content['new-5899e7fd-b3de-4141-a08e-32135ce491d7'] = $pane;
    $display->panels['news_list'][0] = 'new-5899e7fd-b3de-4141-a08e-32135ce491d7';
    $pane = new stdClass();
    $pane->pid = 'new-4ec174fc-cddb-4676-abd2-c23247839561';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'views--exp-news_search-news_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'inherit_path' => 1,
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4ec174fc-cddb-4676-abd2-c23247839561';
    $display->content['new-4ec174fc-cddb-4676-abd2-c23247839561'] = $pane;
    $display->panels['news_sidebar'][0] = 'new-4ec174fc-cddb-4676-abd2-c23247839561';
    $pane = new stdClass();
    $pane->pid = 'new-c4bab5d5-3095-4d22-98de-1c88e650a638';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-m0RbYLiBmeDC98s6ICzg1R4b09oUeUkH';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'My news',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c4bab5d5-3095-4d22-98de-1c88e650a638';
    $display->content['new-c4bab5d5-3095-4d22-98de-1c88e650a638'] = $pane;
    $display->panels['news_sidebar'][1] = 'new-c4bab5d5-3095-4d22-98de-1c88e650a638';
    $pane = new stdClass();
    $pane->pid = 'new-9d8fca86-2604-43fc-85f2-47f7d70836ed';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-x3joQCSFyH9PfSNalcOuSISHe2XSZJO1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Themes',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '9d8fca86-2604-43fc-85f2-47f7d70836ed';
    $display->content['new-9d8fca86-2604-43fc-85f2-47f7d70836ed'] = $pane;
    $display->panels['news_sidebar'][2] = 'new-9d8fca86-2604-43fc-85f2-47f7d70836ed';
    $pane = new stdClass();
    $pane->pid = 'new-80ba23aa-cc21-4e6e-9923-d0f040d70c3b';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-5eet10bAhFk6qdYI9x9DWiXyEUWg4C5U';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Stores',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '80ba23aa-cc21-4e6e-9923-d0f040d70c3b';
    $display->content['new-80ba23aa-cc21-4e6e-9923-d0f040d70c3b'] = $pane;
    $display->panels['news_sidebar'][3] = 'new-80ba23aa-cc21-4e6e-9923-d0f040d70c3b';
    $pane = new stdClass();
    $pane->pid = 'new-36a8a0bd-fdce-4af2-ac28-8057c6d1ff6e';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-1EQhlKXYYYZJiHvPCCBJz3w3Y3R1NInX';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Universe',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '36a8a0bd-fdce-4af2-ac28-8057c6d1ff6e';
    $display->content['new-36a8a0bd-fdce-4af2-ac28-8057c6d1ff6e'] = $pane;
    $display->panels['news_sidebar'][4] = 'new-36a8a0bd-fdce-4af2-ac28-8057c6d1ff6e';
    $pane = new stdClass();
    $pane->pid = 'new-6026c4ff-f17e-474a-851b-3eb08236262f';
    $pane->panel = 'news_sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-C79HN4NUBVPtGuXLhndN0xUhEQBmFKxu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'News status',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '6026c4ff-f17e-474a-851b-3eb08236262f';
    $display->content['new-6026c4ff-f17e-474a-851b-3eb08236262f'] = $pane;
    $display->panels['news_sidebar'][5] = 'new-6026c4ff-f17e-474a-851b-3eb08236262f';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-4ec174fc-cddb-4676-abd2-c23247839561';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['news_page'] = $page;

  return $pages;

}
