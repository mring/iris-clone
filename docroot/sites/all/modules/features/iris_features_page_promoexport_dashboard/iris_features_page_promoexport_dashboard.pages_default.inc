<?php
/**
 * @file
 * iris_features_page_promoexport_dashboard.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function iris_features_page_promoexport_dashboard_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'asynchronous_export_interface';
  $page->task = 'page';
  $page->admin_title = 'Asynchronous export interface';
  $page->admin_description = '';
  $page->path = 'admin/promotions/promotion-export-interface';
  $page->access = array(
    'plugins' => array(
      1 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'access export promotions dashboard',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_asynchronous_export_interface__panel_context_3d7afe90-842a-4149-8e42-3ae458d1135e';
  $handler->task = 'page';
  $handler->subtask = 'asynchronous_export_interface';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Promotions export',
    'panels_breadcrumbs_paths' => 'admin/promotions/promotion-export-interface',
    'panels_breadcrumbs_home' => 1,
    'access' => array(
      'plugins' => array(
        1 => array(
          'name' => 'perm',
          'settings' => array(
            'perm' => 'access export promotions dashboard',
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Export dashboard';
  $display->uuid = '50399431-1196-49d4-a758-6cb75f996db4';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_asynchronous_export_interface__panel_context_3d7afe90-842a-4149-8e42-3ae458d1135e';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d2981fe7-e5ea-47be-ae36-9fdfb0391d2d';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'promotions_export-export';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd2981fe7-e5ea-47be-ae36-9fdfb0391d2d';
  $display->content['new-d2981fe7-e5ea-47be-ae36-9fdfb0391d2d'] = $pane;
  $display->panels['middle'][0] = 'new-d2981fe7-e5ea-47be-ae36-9fdfb0391d2d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['asynchronous_export_interface'] = $page;

  return $pages;

}
