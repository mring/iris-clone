<?php
/**
 * @file
 * Contains helping functions.
 */

/**
 * Validates date fields.
 */
function iris_features_ct_operation_validate_dates($form, &$form_state) {
  if (isset($form_state['values']['field_operation_end_date'][LANGUAGE_NONE][0]['value']) && !empty($form_state['values']['field_operation_end_date'][LANGUAGE_NONE][0]['value'])) {
    $end_date_str = $form_state['values']['field_operation_end_date'][LANGUAGE_NONE][0]['value'];
  }
  if (isset($form_state['values']['field_operation_start_date'][LANGUAGE_NONE][0]['value']) && !empty($form_state['values']['field_operation_start_date'][LANGUAGE_NONE][0]['value'])) {
    $start_date_str = $form_state['values']['field_operation_start_date'][LANGUAGE_NONE][0]['value'];
  }

  if (isset($start_date_str) && isset($end_date_str)) {
    $start_date = new DateObject($start_date_str);
    $end_date = new DateObject($end_date_str);

    if ($start_date > $end_date) {
      form_set_error('field_operation_start_date', t("'Start date' must be earlier than the 'End date'."));
    }
  }
}
