<?php
/**
 * @file
 * iris_features_ct_operation.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function iris_features_ct_operation_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function iris_features_ct_operation_node_info() {
  $items = array(
    'operation' => array(
      'name' => t('Operation'),
      'base' => 'node_content',
      'description' => t('The operations are imported via a CSV import before importing any promotions. All the promotions lines should be linked to an operation.'),
      'has_title' => '1',
      'title_label' => t('Operation number'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
