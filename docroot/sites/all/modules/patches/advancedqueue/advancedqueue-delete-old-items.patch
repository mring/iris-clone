From c9b7f8f1c5963e300f8d43e3448ebe8e6eece7ec Mon Sep 17 00:00:00 2001
From: Alex Lyzo <alyzo@adyax.com>
Date: Mon, 24 Oct 2016 17:32:18 +0300
Subject: [PATCH] ref #193082 - patch advancequeue module, delete old records

---
 .../contrib/advancedqueue/advancedqueue.admin.inc  |  28 ++++++
 .../contrib/advancedqueue/advancedqueue.info       |   1 +
 .../contrib/advancedqueue/advancedqueue.module     | 110 +++++++++++++++++++++
 .../advancedqueue/drush/advancedqueue.drush.inc    |   4 +
 4 files changed, 143 insertions(+)
 create mode 100644 sites/all/modules/contrib/advancedqueue/advancedqueue.admin.inc

diff --git a/sites/all/modules/contrib/advancedqueue/advancedqueue.admin.inc b/sites/all/modules/contrib/advancedqueue/advancedqueue.admin.inc
new file mode 100644
index 0000000..abf30a4
--- /dev/null
+++ b/sites/all/modules/contrib/advancedqueue/advancedqueue.admin.inc
@@ -0,0 +1,28 @@
+<?php
+
+/**
+ * @file
+ * Administrative page and form callbacks for the Advanced Queue module.
+ */
+
+
+/**
+ * Form callback: builds the Advanced Queue settings form.
+ */
+function advancedqueue_settings_form($form, &$form_state){
+  $form['advancedqueue_threshold'] = array(
+    '#type' => 'select',
+    '#title' => t('Number of completed items to keep in the database'),
+    '#default_value' => variable_get('advancedqueue_threshold', 0),
+    '#options' => array(0 => t('All')) + drupal_map_assoc(array(100, 1000, 10000, 100000, 1000000)),
+  );
+
+  $form['advancedqueue_release_timeout'] = array(
+    '#type' => 'select',
+    '#title' => t('Time to wait before releasing an expired item'),
+    '#default_value' => variable_get('advancedqueue_release_timeout', 0),
+    '#options' => array(0 => t('Never')) + drupal_map_assoc(array(3600, 10800, 21600, 43200, 86400, 604800), 'format_interval'),
+  );
+
+  return system_settings_form($form);
+}
\ No newline at end of file
diff --git a/sites/all/modules/contrib/advancedqueue/advancedqueue.info b/sites/all/modules/contrib/advancedqueue/advancedqueue.info
index c70185f..9fed84f 100644
--- a/sites/all/modules/contrib/advancedqueue/advancedqueue.info
+++ b/sites/all/modules/contrib/advancedqueue/advancedqueue.info
@@ -2,6 +2,7 @@ name = Advanced Queues
 description = Helper module for advanced queuing.
 package = Other
 core = 7.x
+configure = admin/config/system/advancedqueue
 
 files[] = advancedqueue.queue.inc
 
diff --git a/sites/all/modules/contrib/advancedqueue/advancedqueue.module b/sites/all/modules/contrib/advancedqueue/advancedqueue.module
index f5f682e..a1da393 100644
--- a/sites/all/modules/contrib/advancedqueue/advancedqueue.module
+++ b/sites/all/modules/contrib/advancedqueue/advancedqueue.module
@@ -48,9 +48,34 @@ function advancedqueue_entity_info() {
 }
 
 /**
+ * Implements hook_menu().
+ */
+function advancedqueue_menu(){
+  $items = array();
+
+  $items['admin/config/system/advancedqueue'] = array(
+    'title' => 'Advanced Queue',
+    'description' => 'Configure the Advanced Queue table clean up settings.',
+    'page callback' => 'drupal_get_form',
+    'page arguments' => array('advancedqueue_settings_form'),
+    'access arguments' => array('administer site configuration'),
+    'type' => MENU_NORMAL_ITEM,
+    'file' => 'advancedqueue.admin.inc',
+  );
+
+  return $items;
+}
+
+
+
+/**
  * Implements hook_cron().
  */
 function advancedqueue_cron() {
+    // Delete older entries and make sure there are no stale items in the table.
+    _advancedqueue_cleanup_table();
+
+
   if (!variable_get('advancedqueue_use_cron', FALSE)) {
     return;
   }
@@ -212,3 +237,88 @@ function advancedqueue_views_api() {
     'path' => drupal_get_path('module', 'advancedqueue') . '/views',
   );
 }
+
+/**
+ * Helper function to clean the advancedqueue table.
+ */
+function _advancedqueue_cleanup_table(){
+  _advancedqueue_purge_old_processed_items();
+  _advancedqueue_release_stale_items();
+}
+
+/**
+ * Helper function to remove data we don't need anymore.
+ *
+ * Removes old entries of processed items.
+ */
+function _advancedqueue_purge_old_processed_items(){
+  // The number of processed items we want to keep.
+  $row_limit = variable_get('advancedqueue_threshold', 0);
+
+  if (!$row_limit) {
+    // No limit means we don't remove old entries.
+    return;
+  }
+
+  // Item status we want to clean.
+  $statuses = array(
+    ADVANCEDQUEUE_STATUS_SUCCESS,
+    ADVANCEDQUEUE_STATUS_FAILURE,
+  );
+
+  // Find the row after which we consider items old enough to purge.
+  $min_row = db_select('advancedqueue', 'a')
+    ->fields('a', array('item_id'))
+    ->condition('status', $statuses, 'IN')
+    ->orderBy('item_id', 'DESC')
+    ->range($row_limit - 1, 1)
+    ->execute()->fetchField();
+
+  $rows = db_select('advancedqueue', 'a')
+    ->fields('a', array('item_id'))
+    ->condition('item_id', $min_row, '<')
+    ->condition('status', $statuses, 'IN')
+    ->execute()->fetchCol();
+
+  // Remove all rows above the limit.
+  if (!empty($rows)) {
+    db_delete('advancedqueue')
+      ->condition('item_id', $rows, 'IN')
+      ->execute();
+  }
+}
+
+/**
+ * Helper function to release stale items.
+ *
+ * Requeues long expired entries that are in processing state.
+ * Items can be stuck in the ADVANCEDQUEUE_STATUS_PROCESSING state
+ * if the PHP process crashes or is killed while processing an item.
+ */
+function _advancedqueue_release_stale_items(){
+  $timeout = variable_get('advancedqueue_release_timeout', 0);
+
+  if (!$timeout) {
+    // No timeout means we don't touch stale items.
+    return;
+  }
+
+  $before = REQUEST_TIME - $timeout;
+
+  $items = db_select('advancedqueue', 'a')
+    ->fields('a', array('item_id', 'name'))
+    ->condition('status', ADVANCEDQUEUE_STATUS_PROCESSING)
+    ->condition('expire', $before, '<=')
+    ->orderBy('name')
+    ->execute();
+
+  $queues = array();
+
+  // Releasing stale items to put them back in queued status.
+  foreach ($items as $item) {
+    // DrupalQueue::get() statically caches queues objects,
+    // we wouldn't improve performance by grouping items by queue.
+    $queue = DrupalQueue::get($item->name);
+    $queue->releaseItem($item);
+  }
+}
diff --git a/sites/all/modules/contrib/advancedqueue/drush/advancedqueue.drush.inc b/sites/all/modules/contrib/advancedqueue/drush/advancedqueue.drush.inc
index 5c11cfb..9d2c6b2 100644
--- a/sites/all/modules/contrib/advancedqueue/drush/advancedqueue.drush.inc
+++ b/sites/all/modules/contrib/advancedqueue/drush/advancedqueue.drush.inc
@@ -59,6 +59,10 @@ function drush_advancedqueue_process_queue($queue = NULL) {
     $queues = array_intersect_key($queues_info, $queues);
   }
 
+  // Delete older entries and make sure there are no stale items in the table.
+  drush_log(dt('Cleanup processed and locked items.'));
+  _advancedqueue_cleanup_table();
+
   // Run the worker for a certain period of time before killing it.
   $timeout = drush_get_option('timeout');
   $end = $timeout ? time() + $timeout : 0;
-- 
1.8.3.msysgit.0

