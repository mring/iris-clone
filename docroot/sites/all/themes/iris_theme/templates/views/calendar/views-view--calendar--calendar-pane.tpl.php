<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any.
 *
 * @ingroup views_templates
 */
?>
<div class="head">
  <div class="left-box">
    <div class="nav-agenda">
      <div class="print">
        <a href="javascript:window.print();">
          <span class="ico icon-print"></span>
        </a>
      </div>
      <div class="calendar">
        <a class="open-nav" href="#">
          <span class="ico icon-calendar"></span>
        </a>

        <div class="calendar-nav">
          <h4><?php print t('Go to'); ?>
            <span>(<a class="close-nav" href="#"><?php print t('Cancel'); ?></a>)</span>
          </h4>

          <div class="form-calendar">
            <?php print render($view->period_form); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="nav-month">
      <a href="<?php print $view->prev_date_link; ?>" class="prev"><span
          class="ico icon-prev"></span></a>
      <a href="<?php print $view->next_date_link; ?>" class="next"><span
          class="ico icon-next"></span></a>
    </div>
  </div>
  <div class="right-box">
    <div class="month-holder">
      <?php foreach ($view->period as $year => $months): ?><?php foreach ($months as $month => $weeks): ?>
        <div class="block">
        <div class="month"><span><?php print t($month) . ' ' . $year; ?></span>
        </div>
        <div class="week">
          <?php foreach ($weeks as $week => $dates): ?>
            <div class="<?php print $dates['class']; ?>">
              <span
                class="date"><?php print $dates['start_date'] . '-' . $dates['end_date']; ?></span>
              <span class="num-week">S<?php print $week; ?></span>
            </div>
          <?php endforeach; ?>
        </div>
       </div>
      <?php endforeach; ?><?php
      endforeach; ?>
    </div>
  </div>
</div>
<div class="<?php print $classes; ?> content-agenda jcf-scrollable">
  <div class="view-content">
    <?php if ($rows): ?>
      <?php print $rows; ?>
    <?php else: ?>
      <div class="empty-msg">
        <?php print $empty; ?>
      </div>
    <?php endif; ?>
  </div>
</div><?php /* class view */ ?>
