<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="row-event <?php print $class; ?>">
  <?php if (!empty($title)): ?>
    <div class="left-box same-height">
      <div class="event-name">
        <span><span
            class="ico icon-<?php print $icon; ?>"></span> <?php print $title; ?></span>
      </div>
    </div>
  <?php endif; ?>
  <div class="right-box same-height">
    <?php foreach ($grouped_items as $key => $item): ?>
      <?php $count = count($item['items']); ?>
      <div class="line">
        <?php foreach ($view->period as $months): ?><?php $im = 1; ?><?php foreach ($months as $month => $weeks): ?>
          <div class="block">
          <div class="week">
            <?php $wc = count($weeks);
            $iw = 1;
            foreach ($weeks as $week => $dates): ?>
              <div class="<?php print $dates['class']; ?>">
                <?php if ($item['start_time'] <= $dates['end_time'] && $item['end_time'] >= $dates['start_time']): ?>
                  <div class="frame">
                    <?php if ($dates['first_week'] && ($item['start_time'] < $dates['start_time'])): ?>
                      <div class="more-after"></div>
                    <?php endif; ?>
                    <?php if ($im == 2 && $iw == $wc && !((date('W', $item['end_time']) == $week && date('Y', $item['end_time']) == date('Y', $dates['end_time'])))): ?>
                      <div class="more-before-tablet"></div>
                    <?php endif; ?>
                    <?php if ((date('W', $item['start_time']) == $week && date('Y', $item['start_time']) == date('Y', $dates['start_time'])) || ($dates['first_week'] && ($item['start_time'] < $dates['start_time']))): ?>
                      <div
                        class="event <?php if (date('W', $item['start_time']) == $week || $dates['first_week']) {
                          print 'start-event';
                       } ?><?php if (date('W', $item['end_time']) == $week) {
                          print ' end-event';
                       } ?>
                        <?php if ($im == 2 && $iw == $wc && !((date('W', $item['end_time']) == $week && date('Y', $item['end_time']) == date('Y', $dates['end_time'])))) {
                          print ' end-event-tablet';
                        } ?>">
                        <a id="event<?php print $key . $item['news_type']; ?>"
                           data-placement="<?php print ($im == 3) ? 'left' : 'top'; ?>"
                           tabindex="0"
                           class="link-events"><?php print $count; ?></a>
                        <a
                          id="event<?php print $key . $item['news_type']; ?>-tablet"
                          data-placement="<?php print ($im == 2) ? 'left' : 'top'; ?>"
                          tabindex="0"
                          class="link-events"><?php print $count; ?></a>

                        <div class="event-popup hide">
                          <div class="title">
                            <?php print strtoupper(format_date($item['start_time'], 'custom', 'j M')); ?>
                            <?php if (date('Y', $item['start_time']) != date('Y', $item['end_time'])) {
                              print date(' Y', $item['start_time']);
                            } ?>
                            <span class="icon-arrow-next"></span>
                            <?php print strtoupper(format_date($item['end_time'], 'custom', 'j M Y')); ?>
                          </div>
                          <div class="content">
                            <?php if ($count > 3): ?>
                            <div class="jcf-scrollable"><?php
                            endif;
                              ?>
                              <ul>
                                <?php foreach ($item['items'] as $child_item): ?>
                                  <li><?php print $child_item['title']; ?></li>
                                <?php endforeach; ?>
                              </ul>
                              <?php if ($count > 3): ?></div><?php
                              endif;
                          ?>
                          </div>
                        </div>
                        <span
                          data-target="event<?php print $key . $item['news_type']; ?>"
                          class="trigger-event"></span>
                        <span
                          data-target="event<?php print $key . $item['news_type']; ?>-tablet"
                          class="trigger-event-tablet"></span>
                      </div>
                    <?php else: ?>
                      <?php if ($dates['last_week'] && ($item['end_time'] > $dates['end_time'])) {
                        print '<div class="more-before"></div>';
                      } ?>
                      <div
                        class="event <?php if ((date('W', $item['end_time']) == $week && date('Y', $item['end_time']) == date('Y', $dates['end_time']))) {
                          print 'end-event';
                       } ?>">
                        <span
                          data-target="event<?php print $key . $item['news_type']; ?>"
                          class="trigger-event"></span>
                        <span
                          data-target="event<?php print $key . $item['news_type']; ?>-tablet"
                          class="trigger-event-tablet"></span>
                      </div>
                    <?php endif; ?>
                  </div>
                <?php endif; ?>
              </div>
              <?php $iw++; ?>
            <?php endforeach; ?>
          </div>
          </div><?php $im++; ?><?php
        endforeach; ?><?php
        endforeach; ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>
