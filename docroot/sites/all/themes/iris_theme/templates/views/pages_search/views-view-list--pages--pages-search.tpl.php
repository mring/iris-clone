<?php

/**
 * @file
 * News search view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<?php print $wrapper_prefix; ?>
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php print $list_type_prefix; ?>
<?php foreach ($rows as $id => $row): ?>
  <li>
    <a class="node-link"
       href="<?php print url('node/' . $view->result[$id]->entity, array('absolute' => TRUE)); ?>"
       data-entity-id="<?php print $view->result[$id]->entity; ?>">
      <?php print $row; ?>
    </a>
  </li>
<?php endforeach; ?>
<?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>
