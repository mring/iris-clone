<?php

/**
 * @file
 * News search view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<ul class="news-list">
  <?php foreach ($items as $id => $item): ?>
    <li <?php $item['class'] ? print 'class="' . $item['class'] . '"' : ''; ?>>
      <a class="node-link" href="<?php print url('node/' . $item['nid'], array('absolute' => TRUE)); ?>" data-entity-id="<?php print $item['nid']; ?>" data-store="<?php print $data['global_store']; ?>">
        <span class="head">
          <?php if ($item['date_class'] || $item['date_status']): ?>
            <span class="sub-ttl<?php print $item['date_class']; ?>"><?php print $item['date_status']; ?></span>
          <?php
          endif; ?>
          <?php if ($item['start_date'] || $item['end_date']): ?>
            <span class="date"><?php print $item['start_date']; ?>
              <?php if ($item['end_date']): ?>
                <span class="icon-arrow-next"></span> <?php print $item['end_date']; ?>
              <?php
              endif; ?>
            </span>
          <?php
          endif; ?>
        </span>
        <span class="holder">
          <span class="<?php print $item['status_class']; ?>"></span>
          <span class="txt">
            <span class="title"><?php print $item['title']; ?></span>
            <span class="info"><?php print $item['type']; ?>
              <?php if (isset($item['publish_ago'])): ?>
              <span class="time">
                <span class="ico icon-time"></span> <?php print $item['publish_ago']; ?></span>
              <?php
              endif; ?>
              </span>
            </span>
          </span>
        </span>
      </a>
    </li>
  <?php
  endforeach; ?>
</ul>
