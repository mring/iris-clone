<?php

/**
 * @file
 * User login template.
 */
?>
<h2>
  <?php print $form['#title']; ?>
</h2>
<?php print drupal_render_children($form); ?>
