<?php

/**
 * @file
 * User edit form template.
 */
?>
<h2>
  <?php print $form['#title']; ?>
</h2>
<?php print drupal_render_children($form); ?>
