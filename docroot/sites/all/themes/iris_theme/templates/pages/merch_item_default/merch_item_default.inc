<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$plugin = array(
  'title' => t('Merch item default'),
  'category' => t('IRIS'),
  'icon' => 'merch_item_default.png',
  'theme' => 'merch_item_default',
  'regions' => array(
    'merch_sidebar' => t('Sidebar'),
    'merch_list' => t('List'),
    'merch_item_title' => t('Title'),
    'merch_item_image' => t('Banner image'),
    'merch_item_docs' => t('Documents'),
    'merch_item_links' => t('Links'),
    'merch_item_stores' => t('Stores'),
  ),
);
