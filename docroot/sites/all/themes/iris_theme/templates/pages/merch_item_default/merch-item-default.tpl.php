<?php

/**
 * @file
 * Merch item default template.
 */
?>
<aside id="sidebar" class="sidebar">
  <div class="wrapp-aside">
    <div class="aside-open-box"></div>
    <div class="aside-scroll jcf-scrollable">
      <div class="side-holder">
        <?php print $content['merch_sidebar']; ?>
      </div>
    </div>
  </div>
</aside>
<div id="content">
  <div class="home-news merchandising">
    <div class="news-list-container">
      <div class="news-list-scroll jcf-scrollable">
        <?php print $content['merch_list']; ?>
      </div>
    </div>
    <div class="news-content">
      <div class="news-content-scroll jcf-scrollable">
        <div id="content-item" class="news-content-frame">
          <div class="title">
            <?php print $content['merch_item_title']; ?>
          </div>
          <?php if (!empty($content['merch_item_image'])): ?>
            <div class="img-block">
              <?php print $content['merch_item_image']; ?>
            </div>
          <?php endif; ?>
          <div class="links-block">
            <div class="col">
              <h4><?php print t('Documents'); ?></h4>
              <?php if (!empty($content['merch_item_docs'])): ?>
                <?php print $content['merch_item_docs']; ?>
              <?php else: ?>
                <span class="empty-links">
                  <?php print t('No documents available'); ?>
                </span>
              <?php endif; ?>
            </div>
            <div class="col">
              <h4><?php print t('Links'); ?></h4>
              <?php if (!empty($content['merch_item_links'])): ?>
                <?php print $content['merch_item_links']; ?>
              <?php else: ?>
                <span class="empty-links">
                  <?php print t('No links available'); ?>
                </span>
              <?php endif; ?>
            </div>
          </div>
          <?php if (!empty($content['merch_item_stores'])): ?>
            <div class="linked-store">
              <?php print $content['merch_item_stores']; ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
