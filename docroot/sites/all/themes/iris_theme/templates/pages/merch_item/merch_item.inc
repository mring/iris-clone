<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$plugin = array(
  'title' => t('Merch rubric item'),
  'category' => t('IRIS'),
  'icon' => 'merch_item.png',
  'theme' => 'merch_item',
  'regions' => array(
    'merch_item_title' => t('Title'),
    'merch_item_image' => t('Banner image'),
    'merch_item_docs' => t('Documents'),
    'merch_item_links' => t('Links'),
    'merch_item_stores' => t('Stores'),
  ),
);
