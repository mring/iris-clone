<?php

/**
 * @file
 * Merch item template.
 */
?>
<div id="content-item" class="news-content-frame">
  <div class="title">
    <?php print $content['merch_item_title']; ?>
  </div>
  <?php if (!empty($content['merch_item_image'])): ?>
    <div class="img-block">
      <?php print $content['merch_item_image']; ?>
    </div>
  <?php endif; ?>
  <div class="links-block">
    <div class="col">
      <h4><?php print t('Documents'); ?></h4>
      <?php if (!empty($content['merch_item_docs'])): ?>
        <?php print $content['merch_item_docs']; ?>
      <?php else: ?>
        <span class="empty-links">
          <?php print t('No documents available'); ?>
        </span>
      <?php endif; ?>
    </div>
    <div class="col">
      <h4><?php print t('Links'); ?></h4>
      <?php if (!empty($content['merch_item_links'])): ?>
        <?php print $content['merch_item_links']; ?>
      <?php else: ?>
        <span class="empty-links">
          <?php print t('No links available'); ?>
        </span>
      <?php endif; ?>
    </div>
  </div>
  <?php if (!empty($content['merch_item_stores'])): ?>
    <div class="linked-store">
      <?php print $content['merch_item_stores']; ?>
    </div>
  <?php endif; ?>
</div>
