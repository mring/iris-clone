<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$plugin = array(
  'title'    => t('News item default'),
  'category' => t('IRIS'),
  'icon'     => 'news_item_default.png',
  'theme'    => 'news_item_default',
  'regions'  => array(
    'news_sidebar'              => t('Sidebar'),
    'news_list'                 => t('List'),
    'news_item_top_buttons'     => t('Top buttons'),
    'news_item_title'           => t('Title'),
    'news_item_start_end_dates' => t('Start and end dates'),
    'news_item_image'           => t('Banner image'),
    'news_item_docs'            => t('Documents'),
    'news_item_links'           => t('Links'),
    'news_item_author_photo'    => t('Author photo'),
    'news_item_author_name'     => t('Author name'),
    'news_item_date'            => t('Publication date'),
    'news_item_body'            => t('Body'),
    'news_item_status'          => t('Status'),
    'news_item_assign_photo'    => t('Assign photo'),
    'news_item_comments'        => t('Comments'),
    'news_item_categories'      => t('Categories'),
    'news_item_visibility'      => t('Scope of visibility'),
  ),
);
