<?php

/**
 * @file
 * Promo teaser default template.
 */
?>
<div  id="content-item" class="news-content-frame">
  <a href="#" class="close-news icon-close"></a>
  <?php print $content['promo_item_top_buttons']; ?>
  <div class="title">
    <?php print $content['promo_item_title']; ?>
    <?php print $content['promo_item_dates']; ?>
  </div>
  <div class="three-boxes">
    <?php if ($content['promo_item_operation']): ?>
    <div class="box">
      <?php print $content['promo_item_operation']; ?>
    </div>
    <?php endif; ?>
      <?php if ($content['promo_item_activity_sector']): ?>
      <div class="box">
        <?php print $content['promo_item_activity_sector']; ?>
      </div>
      <?php endif; ?>
    <?php if ($content['promo_item_slot']): ?>
      <div class="box">
        <?php print $content['promo_item_slot']; ?>
      </div>
    <?php endif; ?>
  </div>
  <div class="img-block">
    <?php if ($content['promo_item_banner']): ?>
      <?php print $content['promo_item_banner']; ?>
    <?php endif; ?>
  </div>
  <div class="linked-store">
    <?php if ($content['promo_item_visibility_scope']): ?>
      <?php print $content['promo_item_visibility_scope']; ?>
    <?php endif; ?>
  </div>
</div>
