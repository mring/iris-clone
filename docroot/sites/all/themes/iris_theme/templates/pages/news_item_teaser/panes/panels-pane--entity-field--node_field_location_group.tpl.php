<?php

/**
 * @file
 * Panels-pane--entity-field--node_field_news_type.tpl.php.
 *
 * Field pane template.
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>
<?php if (!empty($content['#items'])): ?>
<div class="linked-store">
  <?php if ($title): ?>
    <span class="ttl">
      <?php print $title; ?>
    </span>
  <?php
  endif; ?>

  <ul>
    <?php foreach ($content['#items'] as $item): ?>
      <?php if (isset($item['entity']->field_taxonomy_code[LANGUAGE_NONE][0]['value'])): ?>
        <li><?php print $item['entity']->field_taxonomy_code[LANGUAGE_NONE][0]['value']; ?></li>
      <?php elseif (isset($item['entity']->name)): ?>
        <li><?php print $item['entity']->name; ?></li>
      <?php
      endif; ?>
    <?php
    endforeach; ?>
  </ul>
</div>
<?php
endif; ?>
