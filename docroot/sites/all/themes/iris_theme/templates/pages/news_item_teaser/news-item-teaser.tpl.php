<?php

/**
 * @file
 * New item teaser default template.
 */
?>
<div id="content-item" class="news-content-frame">
  <a href="#" class="close-news icon-close"></a>
  <?php print $content['news_item_top_buttons']; ?>
  <div class="title">
    <?php print $content['news_item_title']; ?>
    <?php print $content['news_item_start_end_dates']; ?>
  </div>
  <?php if (!empty($content['news_item_image'])): ?>
    <div class="img-block">
      <?php print $content['news_item_image']; ?>
    </div>
  <?php endif; ?>
  <div class="links-block">
    <div class="col">
      <h4><?php print t('Documents'); ?></h4>
      <?php if (!empty($content['news_item_docs'])): ?>
        <?php print $content['news_item_docs']; ?>
      <?php else: ?>
        <span class="empty-links">
          <?php print t('No documents available'); ?>
        </span>
      <?php endif; ?>
    </div>
    <div class="col">
      <h4><?php print t('Links'); ?></h4>
      <?php if (!empty($content['news_item_links'])): ?>
        <?php print $content['news_item_links']; ?>
      <?php else: ?>
        <span class="empty-links">
          <?php print t('No links available'); ?>
        </span>
      <?php endif; ?>
    </div>
  </div>
  <div class="comment-box">
    <div class="comment-row">
      <?php if (!empty($content['news_item_author_photo'])): ?>
        <div class="photo">
          <?php print $content['news_item_author_photo']; ?>
        </div>
      <?php endif; ?>
      <div class="txt">
        <div class="head">
          <?php if (!empty($content['news_item_author_name'])): ?>
            <span class="name">
              <?php print $content['news_item_author_name']; ?>
            </span>
          <?php endif; ?>
          <?php if (!empty($content['news_item_date'])): ?>
            <span class="date">
              <?php print $content['news_item_date']; ?>
            </span>
          <?php endif; ?>
        </div>
        <?php if (!empty($content['news_item_body'])): ?>
          <?php print $content['news_item_body']; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <?php print $content['news_item_status']; ?>
  <div class="comment-block">
    <div class="two-comment-block">
      <?php print $content['news_item_assign_photo']; ?>
    </div>
    <?php print $content['news_item_comments']; ?>
  </div>
  <div class="three-boxes">
    <?php print $content['news_item_categories']; ?>
  </div>
  <?php print $content['news_item_visibility']; ?>
</div>
