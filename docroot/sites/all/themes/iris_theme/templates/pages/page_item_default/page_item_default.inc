<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$plugin = array(
  'title'    => t('Page item default'),
  'category' => t('IRIS'),
  'icon'     => 'page_item_default.png',
  'theme'    => 'page_item_default',
  'regions'  => array(
    'page_sidebar'           => t('Sidebar'),
    'page_list'              => t('Pages list'),
    'page_item_title'        => t('Title'),
    'page_item_image'        => t('Banner image'),
    'page_item_docs'         => t('Documents'),
    'page_item_links'        => t('Links'),
    'page_item_author_photo' => t('Author photo'),
    'page_item_author_name'  => t('Author name'),
    'page_item_date'         => t('Page date'),
    'page_item_body'         => t('Page body'),
    'page_item_stores'       => t('Stores'),
  ),
);
