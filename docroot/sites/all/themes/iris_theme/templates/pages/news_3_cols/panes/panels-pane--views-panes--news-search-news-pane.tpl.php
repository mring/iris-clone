<?php

/**
 * @file
 * Panels-pane--views-panes--news-search-news-pane.tpl.php.
 *
 * Search form panel pane template.
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>
<div class="news-title-nav">
  <ul class="news-nav" id="news-sort">
    <li class="active">
      <a href="#" data-sort-by="created">
        <span class="ico icon-list"></span>
        <?php print t('Publication'); ?>
      </a>
    </li>
    <li>
      <a href="#" data-sort-by="field_action_date_deadline">
        <span class="ico icon-calendar"></span>
        <?php print t('Application'); ?>
      </a>
    </li>
  </ul>
  <div id="current-search"></div>
</div>
<div class="news-list-scroll jcf-scrollable">
  <?php print render($content); ?>
</div>
