<?php

/**
 * @file
 * News 3 cols template.
 */
?>
<aside id="sidebar" class="sidebar">
  <div class="wrapp-aside">
    <div class="aside-open-box"></div>
    <div class="aside-scroll jcf-scrollable">
      <div class="side-holder">
        <?php print $content['news_sidebar']; ?>
      </div>
    </div>
  </div>
</aside>
<div id="content">
  <div class="home-news promotion">
    <div class="news-list-container">
      <?php print $content['news_list']; ?>
    </div>
    <div class="news-content">
      <div class="news-content-scroll jcf-scrollable">
        <?php if (!empty($content['news_content'])): ?>
          <?php print $content['news_content']; ?>
        <?php else: ?>
          <div id="content-item" class="news-content-frame"></div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
