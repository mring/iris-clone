<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$plugin = array(
  'title'    => t('News 3 columns'),
  'category' => t('IRIS'),
  'icon'     => 'news_3_cols.png',
  'theme'    => 'news_3_cols',
  'regions'  => array(
    'news_sidebar' => t('Sidebar'),
    'news_list'    => t('List'),
    'news_content' => t('Content'),
  ),
);
