<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$plugin = array(
  'title'    => t('Calendar 2 columns'),
  'category' => t('IRIS'),
  'icon'     => 'calendar_2_cols.png',
  'theme'    => 'calendar_2_cols',
  'regions'  => array(
    'calendar_sidebar' => t('Sidebar'),
    'calendar_content' => t('Content'),
  ),
);
