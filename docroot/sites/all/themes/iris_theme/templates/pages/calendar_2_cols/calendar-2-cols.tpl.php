<?php

/**
 * @file
 * Calendar 2 colons template.
 */
?>
<aside id="sidebar" class="sidebar">
  <div class="wrapp-aside">
    <div class="aside-open-box"></div>
    <div class="aside-scroll jcf-scrollable">
      <div class="side-holder">
        <?php print $content['calendar_sidebar']; ?>
      </div>
    </div>
  </div>
</aside>
<div id="content">
  <div class="agenda-holder">
    <?php print $content['calendar_content']; ?>
  </div>
</div>
