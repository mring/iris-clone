<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$plugin = array(
  'title'    => t('Site general'),
  'category' => t('IRIS'),
  'icon'     => 'site_general.png',
  'theme'    => 'site_general',
  'regions'  => array(
    'general_header'      => t('Header'),
    'general_main_nav'    => t('Main navigation'),
    'general_account_nav' => t('Account navigation'),
    'general_content'     => t('Content'),
  ),
);
