<?php

/**
 * @file
 * Site general template.
 */
?>
<div class="wrapper">
  <div class="overlay"></div>
  <header id="header">
    <div class="head">
      <?php print $content['general_header']; ?>
    </div>
    <?php print $content['general_main_nav']; ?>
    <?php if ($logged_in) : ?>
      <div class="account-nav">
        <?php print $content['general_account_nav']; ?>
      </div>
    <?php endif; ?>
  </header>
  <section id="main">
    <?php print $content['general_content']; ?>
  </section>
</div>
