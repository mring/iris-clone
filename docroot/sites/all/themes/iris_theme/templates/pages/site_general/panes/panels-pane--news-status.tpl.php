<?php

/**
 * @file
 * Panels-pane--news-status.tpl.php.
 *
 * User picture panel pane template.
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>
<?php if (isset($content['building_message'])): ?>
<div class="step-block">
  <?php print render($content['building_message']); ?>
</div>
<?php else: ?>
<div class="step-block">

  <span class="line"></span>
  <ul>
    <li class="new<?php if($content['status'] == 0): ?> active<?php
   endif; ?>">
      <a href="<?php if (isset($content['new_link'])): ?><?php print $content['new_link']; ?><?php else: ?>#<?php
     endif; ?>" class="<?php if (isset($content['new_link'])): ?>use-ajax<?php
     endif; ?>"><span class="bullet"><span></span></span></a>
      <span class="name"><?php print t('New'); ?></span>
      <?php if ($content['date']): ?>
        <span class="txt"><?php print t('Deadline @date', array('@date' => $content['date'])); ?></span>
      <?php
      endif; ?>
    </li>
    <li class="in-progress<?php if($content['status'] == 1): ?> active<?php
   endif; ?> <?php if (!$content['action']): ?> invisible<?php
   endif; ?>">
      <a href="<?php if (isset($content['in_progress_link'])): ?><?php print $content['in_progress_link']; ?><?php else: ?>#<?php
     endif; ?>" class="<?php if (isset($content['in_progress_link']) && $content['proper_store']): ?>use-ajax<?php
     endif; ?><?php if (!$content['proper_store']) {print ' open-popup';
     } ?>"><span class="bullet"><span></span></span></a>
      <span class="name"><?php print t('In progress'); ?></span>
      <?php if ($content['date']): ?>
        <span class="txt"><?php print t('Deadline @date', array('@date' => $content['date'])); ?></span>
      <?php
      endif; ?>
    </li>
    <li class="completed<?php if($content['status'] == 2): ?> active<?php
   endif; ?>">
      <a class="<?php if($content['status'] != 2 && isset($content['done_link'])): ?>open-popup<?php
     endif; ?>" href="<?php print $content['proper_store'] ? '#popup1' : '#popup2' ?>"><span class="bullet"><span></span></span></a>
      <span class="name"><?php print t('Done'); ?></span>
      <span class="txt">
        <?php print t('Done @date', array('@date' => $content['date_completed'])); ?><br/>
        <?php print t('By'); ?> <span class="status-changer"><?php print $content['author']; ?></span>
      </span>
    </li>
  </ul>
</div>
<div class="g-hidden">
  <div class="popup" id="<?php if($content['status'] != 2 || !isset($content['done_link'])): ?>popup1<?php
 endif; ?>">
    <h3><?php print t('The news is read ?'); ?></h3>
    <p><?php print t('This action is irreversible'); ?></p>
    <div class="btn-line">
      <a class="yes close-popup<?php if (isset($content['done_link'])): ?> use-ajax<?php
     endif; ?>" href="<?php if (isset($content['done_link'])): ?><?php print $content['done_link']; ?><?php
     endif; ?>"><span class="ico-yes"></span> <?php print t('Yes'); ?></a>
      <a class="not close-popup" href="#"><span class="ico-not"></span> <?php print t('No'); ?></a>
    </div>
  </div>
  <?php if (!$content['proper_store']): ?>
    <div class="popup" id="popup2">
      <h3><?php print t('Access denied'); ?></h3>
      <p><?php print t('Sorry, but you are not allowed to validate this news.'); ?></p>
      <div class="btn-line">
        <a class="not close-popup" href="#"><span class="ico-not"></span> <?php print t('OK'); ?></a>
      </div>
    </div>
  <?php
  endif; ?>
</div>
<?php
endif; ?>
