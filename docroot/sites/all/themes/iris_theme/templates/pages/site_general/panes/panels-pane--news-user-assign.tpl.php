<?php

/**
 * @file
 * Panels-pane--news-assign-photo.tpl.php.
 *
 * User picture panel pane template.
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>
<?php if (isset($content['building_message'])): ?>
  <div class="assigned-box" style="display: none;">
  </div>
<?php else: ?>
  <div class="assigned-box">
    <h4><span class="ico icon-user">
    </span>&nbsp;<?php print t('Assigned to'); ?>
    </h4>

    <div class="frame">
      <div class="form-assigned">
        <form action="#">
          <div>
            <div class="row-form">
              <input id="user-assign-autocomplete" type="text"
                     <?php if (!empty($content['read_only'])): ?>disabled="disabled"<?php
                     endif;
              ?> placeholder="<?php print t('Add a name ...'); ?>" value="">
            </div>
            <ul class="assigned-list">
              <?php foreach ($content['assigned_users'] as $uid => $username): ?>
                <li id="assigned-user-<?php print $uid; ?>">
                <span>
              <?php if (empty($content['read_only'])): ?><a
                href="<?php print url('user_assign/' . $content['rid'] . '/' . $uid . '/remove') ?>"
                data-uid="<?php print $uid; ?>"
                class="icon-close use-ajax"></a><?php
              endif; ?> <?php print $username; ?>
                </span>
                </li>
              <?php
              endforeach; ?>
            </ul>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php
endif; ?>
