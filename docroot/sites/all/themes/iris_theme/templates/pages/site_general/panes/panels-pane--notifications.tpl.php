<?php

/**
 * @file
 * Panels-pane--notifications.tpl.php.
 *
 * User picture panel pane template.
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>

<div class="notification">
  <a class="opener" href="#">
    <span class="ico icon-account-news"></span>
    <?php if ($content['notification_count']): ?>
      <span class="status new"><?php print $content['notification_count']; ?></span>
    <?php endif; ?>
  </a>
  <div class="notification-drop">
    <div class="frame jcf-scrollable">
      <ul>
        <?php if(!$content['notifications']): ?><li class="opacity"><a><?php print t('You don\'t have any notifications'); ?></a></li><?php
        endif; ?>
        <?php foreach($content['notifications'] as $key => $notification): ?>
        <li class="n-<?php print $notification->type ?><?php if ($notification->is_read): ?> opacity<?php
       endif ?>">
          <?php print $content['notification_links'][$key]; ?>
        </li>
        <?php
        endforeach; ?>
      </ul>
    </div>
  </div>
</div>
