<?php

/**
 * @file
 * Panels-pane--news-assign-photo.tpl.php.
 *
 * User picture panel pane template.
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>
<?php if (isset($content['building_message'])): ?>
<div class="photo-box" style="display: none;">
</div>
<?php else: ?>
<div class="photo-box">
  <h4>
    <span class="ico icon-photo"></span>&nbsp;<?php print t('Add a picture'); ?>
    <?php if (isset($content['remove_link_classes']) && !empty($content['remove_link_classes'])): ?>
      <?php print l('<svg class="svg-ico"><use xlink:href="#ico-basket2"/></svg>', "news-image/{$content['rid']}/delete", array('attributes' => array('class' => $content['remove_link_classes']), 'html' => TRUE)); ?>
    <?php
    endif; ?>
  </h4>
  <div class="frame">
    <div class="<?php if (!$content['news_status_image']): ?>photo-load<?php
   endif; ?>">
      <?php if ($content['news_status_value'] == 2) : ?>
        <a onclick="<?php print (!$content['read_only'] && !$content['news_status_image']) ? "document.getElementById('news-store-image').click(); return false;" : ''; ?>" href="#">
          <?php print $content['news_status_image']; ?>
        </a>
        <input type="file" name="NewsStoreImage" id="news-store-image" class="hidden" accept="image/*">
      <?php else : ?>
        <a href="#" style="cursor: default"></a>
      <?php
      endif; ?>
    </div>
  </div>
</div>
<?php
endif; ?>
