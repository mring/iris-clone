<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$plugin = array(
  'title'    => t('Promo item default'),
  'category' => t('IRIS'),
  'icon'     => 'promo_item_default.png',
  'theme'    => 'promo_item_default',
  'regions'  => array(
    'news_sidebar'                => t('Sidebar'),
    'news_list'                   => t('List'),
    'promo_item_top_buttons'      => t('Top buttons'),
    'promo_item_title'            => t('Title'),
    'promo_item_dates'            => t('Dates'),
    'promo_item_operation'        => t('Operation title'),
    'promo_item_activity_sector'  => t('Activity sector'),
    'promo_item_slot'             => t('Slot'),
    'promo_item_banner'           => t('Banner'),
    'promo_item_visibility_scope' => t('Visibility scope'),
  ),
);
