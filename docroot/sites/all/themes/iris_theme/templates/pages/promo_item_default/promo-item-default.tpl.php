<?php

/**
 * @file
 * Promo item default template.
 */
?>
<aside id="sidebar" class="sidebar">
  <div class="wrapp-aside">
    <div class="aside-open-box"></div>
    <div class="aside-scroll jcf-scrollable">
      <div class="side-holder">
        <?php print $content['news_sidebar']; ?>
      </div>
    </div>
  </div>
</aside>
<div id="content">
  <div class="home-news">
    <div class="news-list-container">
      <?php print $content['news_list']; ?>
    </div>
    <div class="news-content">
      <div class="news-content-scroll jcf-scrollable">
        <div  id="content-item" class="news-content-frame">
          <a href="#" class="close-news icon-close"></a>
          <?php print $content['promo_item_top_buttons']; ?>
          <div class="title">
            <?php print $content['promo_item_title']; ?>
            <?php print $content['promo_item_dates']; ?>
          </div>
          <div class="three-boxes">
            <?php if ($content['promo_item_operation']): ?>
              <div class="box">
                <?php print $content['promo_item_operation']; ?>
              </div>
            <?php endif; ?>
            <?php if ($content['promo_item_activity_sector']): ?>
              <div class="box">
                <?php print $content['promo_item_activity_sector']; ?>
              </div>
            <?php endif; ?>
            <?php if ($content['promo_item_slot']): ?>
              <div class="box">
                <?php print $content['promo_item_slot']; ?>
              </div>
            <?php endif; ?>
          </div>
          <div class="img-block">
            <?php if ($content['promo_item_banner']): ?>
              <?php print $content['promo_item_banner']; ?>
            <?php endif; ?>
          </div>
          <div class="linked-store">
            <?php if ($content['promo_item_visibility_scope']): ?>
              <?php print $content['promo_item_visibility_scope']; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
