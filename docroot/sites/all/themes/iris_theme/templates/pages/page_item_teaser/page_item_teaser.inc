<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$plugin = array(
  'title'    => t('Page item teaser'),
  'category' => t('IRIS'),
  'icon'     => 'page_item_teaser.png',
  'theme'    => 'page_item_teaser',
  'regions'  => array(
    'page_item_teaser_title'  => t('Title'),
    'page_item_teaser_image'  => t('Banner image'),
    'page_item_teaser_docs'   => t('Documents'),
    'page_item_teaser_links'  => t('Links'),
    'page_item_author_photo'  => t('Author photo'),
    'page_item_author_name'   => t('Author name'),
    'page_item_date'          => t('Page date'),
    'page_item_body'          => t('Page body'),
    'page_item_teaser_stores' => t('Stores'),
  ),
);
