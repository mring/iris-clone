<?php

/**
 * @file
 * Page item default template.
 */
?>
<div id="content-item" class="news-content-frame">
  <div class="title">
    <?php print $content['page_item_teaser_title']; ?>
  </div>
  <?php if (!empty($content['page_item_teaser_image'])): ?>
    <div class="img-block">
      <?php print $content['page_item_teaser_image']; ?>
    </div>
  <?php endif; ?>
  <div class="links-block">
    <div class="col">
      <h4><?php print t('Documents'); ?></h4>
      <?php if (!empty($content['page_item_teaser_docs'])): ?>
        <?php print $content['page_item_teaser_docs']; ?>
      <?php else: ?>
        <span class="empty-links">
          <?php print t('No documents available'); ?>
        </span>
      <?php endif; ?>
    </div>
    <div class="col">
      <h4><?php print t('Links'); ?></h4>
      <?php if (!empty($content['page_item_teaser_links'])): ?>
        <?php print $content['page_item_teaser_links']; ?>
      <?php else: ?>
        <span class="empty-links">
          <?php print t('No links available'); ?>
        </span>
      <?php endif; ?>
    </div>
  </div>
  <div class="comment-box">
    <div class="comment-row">
      <?php if (!empty($content['page_item_author_photo'])): ?>
        <div class="photo">
          <?php print $content['page_item_author_photo']; ?>
        </div>
      <?php endif; ?>
      <div class="txt">
        <div class="head">
          <?php if (!empty($content['page_item_author_name'])): ?>
            <span class="name">
              <?php print $content['page_item_author_name']; ?>
            </span>
          <?php endif; ?>
          <?php if (!empty($content['page_item_date'])): ?>
            <span class="date">
              <?php print $content['page_item_date']; ?>
            </span>
          <?php endif; ?>
        </div>
        <?php if (!empty($content['page_item_body'])): ?>
          <?php print $content['page_item_body']; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <?php if (!empty($content['page_item_teaser_stores'])): ?>
    <div class="linked-store">
      <?php print $content['page_item_teaser_stores']; ?>
    </div>
  <?php endif; ?>
</div>
