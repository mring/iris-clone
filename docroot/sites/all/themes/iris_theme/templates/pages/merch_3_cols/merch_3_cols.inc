<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$plugin = array(
  'title' => t('Merch 3 columns'),
  'category' => t('IRIS'),
  'icon' => 'merch_3_cols.png',
  'theme' => 'merch_3_cols',
  'regions' => array(
    'merch_sidebar' => t('Sidebar'),
    'merch_list' => t('List'),
    'merch_content' => t('Content'),
  ),
);
