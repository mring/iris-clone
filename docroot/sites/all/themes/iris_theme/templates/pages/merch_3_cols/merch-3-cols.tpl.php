<?php

/**
 * @file
 * Merch 3 colons template.
 */
?>
<aside id="sidebar" class="sidebar">
  <div class="wrapp-aside">
    <div class="aside-open-box"></div>
    <div class="aside-scroll jcf-scrollable">
      <div class="side-holder">
        <?php print $content['merch_sidebar']; ?>
      </div>
    </div>
  </div>
</aside>
<div id="content">
  <div class="home-news merchandising generic">
    <div class="news-list-container">
      <div class="news-list-scroll jcf-scrollable">
        <?php print $content['merch_list']; ?>
      </div>
    </div>
    <div class="news-content">
      <div class="news-content-scroll jcf-scrollable">
        <?php if (!empty($content['merch_content'])): ?>
          <?php print $content['merch_content']; ?>
        <?php else: ?>
          <div id="content-item" class="news-content-frame"></div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
