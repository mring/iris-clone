<?php

/**
 * @file
 * This file contains the main theme functions hooks and overrides.
 */

/**
 * Preprocess variables for html.tpl.php.
 */
function iris_theme_preprocess_html(&$vars) {
  $x_ua_compatible = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=edge',
    ),
  );
  $format_detection = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'format-detection',
      'content' => 'telephone=no',
    ),
  );
  $apple = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'apple-mobile-web-app-status-bar-style',
      'content' => 'black',
    ),
  );
  $apple_ipad = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'apple-mobile-web-app-capable',
      'content' => 'yes',
    ),
  );
  $viewport = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1, maximum-scale=1',
    ),
  );

  drupal_add_html_head($x_ua_compatible, 'x_ua_compatible');
  drupal_add_html_head($format_detection, 'format_detection');
  drupal_add_html_head($apple, 'apple_web_app');
  drupal_add_html_head($apple_ipad, 'apple_ipad_web_app');
  drupal_add_html_head($viewport, 'viewport');

  if (isset($_SERVER['HTTP_USER_AGENT']) && strstr($_SERVER['HTTP_USER_AGENT'], 'iPad') && !variable_get('appcache_enable_service_worker', FALSE) && !empty($vars['user']) && $vars['user']->uid > 0) {
    $vars['appcache_manifest'] = 'manifest="' . url('appcache.manifest') . '"';
    drupal_add_js(path_to_theme() . '/html/js/offline.min.js', array(
      'type' => 'file',
      'scope' => 'footer',
      'weight' => 150,
    ));
    drupal_add_js(path_to_theme() . '/html/js/behaviors/offline.js', array(
      'type' => 'file',
      'scope' => 'footer',
      'weight' => 15000,
    ));
  }

  drupal_add_js(array(
    'svgPath' => url(drupal_get_path('theme', 'iris_theme') . '/html/svg/sprite.svg', array('absolute' => TRUE)),
    'loaderPath' => url(drupal_get_path('theme', 'iris_theme') . '/html/images/iris-ajax-loader.gif', array('absolute' => TRUE)),
  ), array('type' => 'setting'));
}

/**
 * Preprocess variables for node templates.
 */
function iris_theme_preprocess_node(&$vars) {
}

/**
 * Implements hook_css_alter().
 */
function iris_theme_css_alter(&$css) {
  $paths = array(
    drupal_get_path('module', 'system') . '/system.theme.css',
    drupal_get_path('module', 'system') . '/system.menus.css',
    drupal_get_path('module', 'comment') . '/comment.css',
  );

  foreach ($paths as $path) {
    if (isset($css[$path])) {
      unset($css[$path]);
    }
  }
}

/**
 * Implements theme_menu_tree() for main menu.
 */
function iris_theme_menu_tree__main_menu(&$vars) {
  return '<ul>' . $vars['tree'] . '</ul>';
}

/**
 * Implements theme_menu_link() for main menu.
 */
function iris_theme_menu_link__main_menu(&$vars) {
  $element = $vars['element'];
  $sub_menu = '';
  $path = $element['#href'];

  unset($element['#attributes']['class']);

  if (($path == $_GET['q'] || ($path == '<front>' && drupal_is_front_page()))) {
    $element['#attributes']['class'][] = 'active';
  }

  if (strpos(current_path(), $path) !== FALSE) {
    $element['#attributes']['class'][] = 'active';
  }

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements theme_panels_default_style_render_region().
 */
function iris_theme_panels_default_style_render_region($vars) {
  return implode('', $vars['panes']);
}

/**
 * Implements theme_facetapi_deactivate_widget().
 */
function iris_theme_facetapi_deactivate_widget($variables) {
  return '';
}

/**
 * Implements theme_status_messages().
 */
function iris_theme_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {
    if ($type == 'status') {
      $output .= "<div class=\"messages\">\n";
    }
    else {
      $output .= "<div class=\"messages $type\">\n";
    }
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= reset($messages);
    }
    $output .= "</div>\n";
  }
  return $output;
}

/**
 * Implements template_preprocess_panels_pane().
 */
function iris_theme_preprocess_panels_pane(&$vars) {
  if (isset($vars['content']['#facet']['name'])) {
    $base = 'panels_pane';
    $delimiter = '__';

    $vars['theme_hook_suggestions'][] = $base . $delimiter . 'facet';
    $vars['theme_hook_suggestions'][] = $base . $delimiter . 'facet' . $delimiter . strtr(ctools_cleanstring($vars['content']['#facet']['name'], array('lower case' => TRUE)), '-', '_');
  }
}

/**
 * Implements theme_item_list().
 */
function iris_theme_item_list($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];
  $class = '';
  $output = '';

  foreach ($items as $key => $temp_item) {
    if (isset($temp_item['data']) && empty(strip_tags($temp_item['data']))) {
      unset($items[$key]);
    }
  }

  $items = array_values($items);

  if (isset($attributes['class']) && is_array($attributes['class'])) {
    if (in_array('facetapi-facet-scope-stores', $attributes['class'])) {
      $class = 'facetapi-facet-scope-stores';
    }
    elseif (in_array('facetapi-facet-merch-scope-stores', $attributes['class'])) {
      $class = 'facetapi-facet-merch-scope-stores';
    }
    elseif (in_array('facetapi-facet-page-scope-stores', $attributes['class'])) {
      $class = 'facetapi-facet-page-scope-stores';
    }
    elseif (in_array('facetapi-facet-calendar-scope-stores', $attributes['class'])) {
      $class = 'facetapi-facet-calendar-scope-stores';
    }
  }

  // Sort store filter in two columns.
  if ($class) {
    $class_key = array_search($class, $variables['attributes']['class']);

    if ($class_key) {
      $variables['attributes']['class'][$class_key] = 'facetapi-facet-stores';

      $count = count($items);
      $count_in_col = round($count / 2);
      $items = array_values($items);
      $cols = array();

      foreach ($items as $key => $item) {
        if ($key < $count_in_col) {
          $cols[1][] = $item;
        }
        else {
          $cols[2][] = $item;
        }
      }

      $id = $variables['attributes']['id'];

      foreach ($cols as $key => $col) {
        $variables['attributes']['id'] = $id . '-' . $key;
        $variables['items'] = $col;
        $output .= iris_theme_item_list($variables);
      }

      return $output;
    }
  }
  else {
    if (isset($attributes['class']) && is_array($attributes['class']) && in_array('facetapi-facetapi-links', $attributes['class'])) {
      $attributes['class'][] = 'aside-list';
    }

    if (isset($title) && $title !== '') {
      $output .= '<h3>' . $title . '</h3>';
    }

    if (!empty($items)) {
      $output .= "<$type" . drupal_attributes($attributes) . '>';
      $num_items = count($items);
      $i = 0;
      foreach ($items as $item) {
        $attributes = array();
        $children = array();
        $data = '';
        $i++;
        if (is_array($item)) {
          foreach ($item as $key => $value) {
            if ($key == 'data') {
              $data = $value;
            }
            elseif ($key == 'children') {
              $children = $value;
            }
            else {
              $attributes[$key] = $value;
            }
          }
        }
        else {
          $data = $item;
        }
        if (count($children) > 0) {
          // Render nested list.
          $data .= theme_item_list(array(
            'items' => $children,
            'title' => NULL,
            'type' => $type,
            'attributes' => $attributes,
          ));
        }
        if ($i == 1) {
          $attributes['class'][] = 'first';
        }
        if ($i == $num_items) {
          $attributes['class'][] = 'last';
        }

        $is_active = strpos($data, 'facetapi-active');

        if ($is_active != FALSE) {
          $attributes['class'][] = 'active';
        }

        $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
      }
      $output .= "</$type>";
    }
  }

  return $output;
}

/**
 * Implements theme_facetapi_link_active().
 */
function iris_theme_facetapi_link_active($variables) {
  $link_text = $variables['text'];

  if (isset($variables['options']['html']['icon_classes']) && !empty($variables['options']['html']['icon_classes'])) {
    $classes = $variables['options']['html']['icon_classes'];
    $link_text = '<span class="' . $classes . '"></span><span class="txt">' . $variables['text'] . '</span>';
  }

  if (isset($variables['options']['html']['value']) && !empty($variables['options']['html']['value'])) {
    return '<a class="facetapi-active" data-value="' . $variables['options']['html']['value'] . '">' . $link_text . '</a>';
  }
  else {
    return '<a class="facetapi-active">' . $link_text . '</a>';
  }
}

/**
 * Implements theme_facetapi_link_inactive().
 */
function iris_theme_facetapi_link_inactive($variables) {
  // Builds accessible markup.
  // @see http://drupal.org/node/1316580
  if (isset($variables['options']['html']['type'])) {
    $variables['options']['attributes']['class'][] = $variables['options']['html']['type'];
  }

  if (isset($variables['text']) && !empty($variables['text'])) {
    if (strpos($variables['path'], 'user_news/archived')) {
      unset($variables['count']);
    }

    $accessible_vars = array('text' => $variables['text'], 'active' => FALSE);
    $accessible_markup = theme('facetapi_accessible_markup', $accessible_vars);

    // Sanitizes the link text if necessary.
    $sanitize = (empty($variables['options']['html'] || (isset($variables['options']['html']['allowed']) && empty(isset($variables['options']['html']['allowed'])))));

    if (isset($variables['options']['html']['icon_classes']) && !empty($variables['options']['html']['icon_classes'])) {
      $classes = $variables['options']['html']['icon_classes'];
      $variables['text'] = '<span class="' . $classes . '"></span><span class="txt">' . $variables['text'] . '</span>';
    }

    $variables['text'] = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

    // Adds count to link if one was passed.
    if (isset($variables['count'])) {
      $variables['text'] .= ' ' . theme('facetapi_count', $variables);
    }

    // Resets link text, sets to options to HTML since we already sanitized the
    // Link text and are providing additional markup for accessibility.
    $variables['text'] .= $accessible_markup;
    $variables['options']['html'] = TRUE;
    $variables['options']['query']['type'] = 'search';

    return theme_link($variables);
  }
  else {
    return '';
  }
}

/**
 * Implements theme_facetapi_count().
 */
function iris_theme_facetapi_count($variables) {
  $count = (int) $variables['count'];
  return $count ? '<span class="num">' . $count . '</span>' : '';
}

/**
 * Implements iris_theme_file_link().
 */
function iris_theme_file_link($variables) {
  $file = $variables['file'];
  $access = file_file_download($file->uri);

  if ($access < 0) {
    return;
  }

  $url = file_create_url($file->uri);

  // Human-readable names, for use as text-alternatives to icons.
  $mime_name = array(
    'application/msword' => 'doc',
    'text/csv' => 'csv',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'doc',
    'application/vnd.ms-excel' => 'xls',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xls',
    'application/vnd.ms-powerpoint' => 'ppt',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'ppt',
    'application/pdf' => 'pdf',
    'video/quicktime' => 'filelink',
    'audio/mpeg' => 'filelink',
    'audio/wav' => 'filelink',
    'image/jpeg' => 'pic',
    'image/png' => 'pic',
    'image/gif' => 'pic',
    'image/x-ms-bmp' => 'pic',
    'image/tiff' => 'pic',
    'application/postscript' => 'pic',
    'application/zip' => 'zip',
    'text/html' => 'filelink',
    'text/plain' => 'txt',
    'application/octet-stream' => 'filelink',
  );

  $mimetype = file_get_mimetype($file->uri);

  // Set options as per anchor format described at.
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'html' => TRUE,
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
      'target' => '_blank',
    ),
  );

  // Use the description as the link text if available.
  if (empty($file->description)) {
    $link_text = strip_tags($file->filename);
  }
  else {
    $link_text = strip_tags($file->description);
    $options['attributes']['title'] = check_plain($file->filename);
  }

  $class = isset($mime_name[$mimetype]) ? $mime_name[$mimetype] : '';
  $file_time = iris_core_fo_date_format($file->timestamp);

  $output = '';
  $output .= '<li>';
  $link_html = '<span class="ico"><span class="' . $class . '"></span></span>
<span class="txt"><span title="' . $link_text . '">' . $link_text . '</span>';
  $link_html .= '&nbsp;<span class="time"><span class="ico icon-time"></span>&nbsp;' . $file_time . '</span></span>';
  $output .= l($link_html, $url, $options);
  $output .= '</li>';

  return $output;
}

/**
 * Formats a link.
 */
function iris_theme_link_formatter_link_default($vars) {
  $link_options = $vars['element'];
  unset($link_options['title']);
  unset($link_options['url']);

  if (isset($link_options['attributes']['class'])) {
    $link_options['attributes']['class'] = array($link_options['attributes']['class']);
  }

  $title = strip_tags($vars['element']['title']);

  $link_options['html'] = TRUE;
  $link_html = '<span class="ico"><span class="filelink"></span></span><span class="txt"><span title="' . $title . '">' . $title . '</span></span>';
  $prefix = '<li>';
  $suffix = '</li>';

  // Display a normal link if both title and URL are available.
  if (!empty($vars['element']['title']) && !empty($vars['element']['url'])) {
    return $prefix . l($link_html, $vars['element']['url'], $link_options) . $suffix;
  }
  // If only a title, display the title.
  elseif (!empty($vars['element']['title'])) {
    return $prefix . $link_options['html'] ? $vars['element']['title'] : check_plain($vars['element']['title']) . $suffix;
  }
  elseif (!empty($vars['element']['url'])) {
    return $prefix . l($link_html, $vars['element']['url'], $link_options) . $suffix;
  }
}

/**
 * Process variables for comment.tpl.php.
 *
 * @see comment.tpl.php
 */
function iris_theme_preprocess_comment(&$variables) {
  global $user;
  $comment = $variables['elements']['#comment'];
  $node = $variables['elements']['#node'];
  $variables['comment'] = $comment;
  $variables['node'] = $node;
  $variables['action_links'] = array();

  $comment_user = user_load($comment->uid);
  $comment_node = node_load($comment->nid);
  $author_wrapper = entity_metadata_wrapper('user', $comment_user);

  if ($author_wrapper->field_first_name->value() && $author_wrapper->field_last_name->value()) {
    $variables['author'] = $author_wrapper->field_first_name->value() . ' ' . $author_wrapper->field_last_name->value();
  }
  else {
    $variables['author'] = $comment_user->name;
  }

  $variables['created'] = iris_core_fo_date_format($comment->created);

  // Avoid calling format_date() twice on the same timestamp.
  if ($comment->changed == $comment->created) {
    $variables['changed'] = $variables['created'];
  }
  else {
    $variables['changed'] = format_date($comment->changed);
  }

  $variables['new'] = !empty($comment->new) ? t('new') : '';
  $variables['signature'] = $comment->signature;
  $variables['picture'] = '';

  $img_prefix = '<div class="photo"><div class="img">';
  $img_suffix = '</div></div>';

  if ($user_picture_val = $author_wrapper->field_user_picture->value()) {
    if (isset($user_picture_val['uri']) && !empty($user_picture_val['uri'])) {
      $style = 'user_picture';
      $alt = t("@user's picture", array('@user' => format_username($comment_user)));
      $variables['picture'] = theme('image_style', array(
        'style_name' => $style,
        'path' => $user_picture_val['uri'],
        'alt' => $alt,
        'title' => $alt,
      ));

      $variables['picture'] = $img_prefix . $variables['picture'] . $img_suffix;
    }
  }
  else {
    $variables['picture'] = $img_prefix . '<span class="ico icon-user3"></span>' . $img_suffix;
  }

  $uri = entity_uri('comment', $comment);
  $uri['options'] += array(
    'attributes' => array(
      'class' => array('permalink'),
      'rel' => 'bookmark',
    ),
  );

  $variables['title'] = l($comment->subject, $uri['path'], $uri['options']);
  $variables['permalink'] = l(t('Permalink'), $uri['path'], $uri['options']);
  $variables['submitted'] = t('Submitted by !username on !datetime', array(
    '!username' => $variables['author'],
    '!datetime' => $variables['created'],
  ));

  // Preprocess fields.
  field_attach_preprocess('comment', $comment, $variables['elements'], $variables);

  // Helpful $content variable for templates.
  foreach (element_children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  // Set status to a string representation of comment->status.
  if (isset($comment->in_preview)) {
    $variables['status'] = 'comment-preview';
  }
  else {
    $variables['status'] = ($comment->status == COMMENT_NOT_PUBLISHED) ? 'comment-unpublished' : 'comment-published';
  }

  // Gather comment classes.
  // 'comment-published' class is not needed, it is either 'comment-preview' or.
  // 'comment-unpublished'.
  if ($variables['status'] != 'comment-published') {
    $variables['classes_array'][] = $variables['status'];
  }
  if ($variables['new']) {
    $variables['classes_array'][] = 'comment-new';
  }
  if (!$comment->uid) {
    $variables['classes_array'][] = 'comment-by-anonymous';
  }
  else {
    if ($comment->uid == $variables['node']->uid) {
      $variables['classes_array'][] = 'comment-by-node-author';
    }
    if ($comment->uid == $variables['user']->uid) {
      $variables['classes_array'][] = 'comment-by-viewer';
    }
  }
  if ($comment->pid != 0) {
    $variables['classes_array'][] = 'secondary';
  }

  if (isset($variables['content']['comment_body'][0]['#markup'])) {
    $variables['content']['comment_body'][0]['#markup'] = nl2br($variables['content']['comment_body'][0]['#markup']);
  }

  $variables['reply'] = ($comment->pid == 0) ? FALSE : TRUE;

  if (in_array('manager', $variables['user']->roles) || ($variables['user']->uid != $comment_node->uid && array_intersect(array(
    'generic contributor',
    'operation planning responsible',
    'operation unvalidation',
    'operation upload and validation',
    'merchandising contributor',
  ), $variables['user']->roles) || $variables['reply'])
  ) {
    unset($variables['content']['links']['comment']['#links']['comment-reply']);
  }

  if (isset($variables['content']['links']['comment']['#links']['comment-delete']['href'])) {
    if (($comment_user->uid == $user->uid && user_access('delete own comments')) || user_access('delete any comments')) {
      $variables['content']['links']['comment']['#links']['comment-delete']['attributes']['class'][] = 'delete-link';

      $href = str_replace('comment', 'iris-cmnt', $variables['content']['links']['comment']['#links']['comment-delete']['href']);

      $variables['action_links'][] = l(t('Delete'), $href, array('attributes' => $variables['content']['links']['comment']['#links']['comment-delete']['attributes']));
    }
    else {
      unset($variables['content']['links']['comment']['#links']['comment-delete']);
    }
  }

  if (isset($variables['content']['links']['comment']['#links']['comment-reply']['href'])) {
    $variables['content']['links']['comment']['#links']['comment-reply']['attributes']['class'][] = 'reply-link';

    $variables['action_links'][] = l(t('Reply'), $variables['content']['links']['comment']['#links']['comment-reply']['href'], array('attributes' => $variables['content']['links']['comment']['#links']['comment-reply']['attributes']));
  }
}

/**
 * Process variables for comment-wrapper.tpl.php.
 */
function iris_theme_preprocess_comment_wrapper(&$variables) {
  // Provide contextual information.
  $variables['node'] = $variables['content']['#node'];
  $variables['display_mode'] = variable_get('comment_default_mode_' . $variables['node']->type, COMMENT_MODE_THREADED);
  // The comment form is optional and may not exist.
  $variables['content'] += array('comment_form' => array());

  if (count($variables['content']['comments']) > 0) {
    $variables['empty'] = FALSE;
  }
  else {
    $variables['empty'] = TRUE;
  }

  if (isset($variables['user'])) {
    $variables['user']->node_type = 'comment_node_news';
  }

  $variables['author_picture'] = isset($variables['user']) ? theme('user_picture', array('account' => $variables['user'])) : '';
}

/**
 * Process variables for user-picture.tpl.php.
 */
function iris_theme_preprocess_user_picture(&$variables) {
  $variables['user_picture'] = '';
  $account = $variables['account'];

  if (isset($account->uid) && !empty($account->uid)) {
    $user_wrapper = entity_metadata_wrapper('user', $account);

    if ($user_picture_val = $user_wrapper->field_user_picture->value()) {
      if (isset($user_picture_val['uri']) && !empty($user_picture_val['uri'])) {
        $style = 'user_picture';
        $alt = t("@user's picture", array('@user' => format_username($account)));
        $variables['user_picture'] = theme('image_style', array(
          'style_name' => $style,
          'path' => $user_picture_val['uri'],
          'alt' => $alt,
          'title' => $alt,
        ));

        if (!empty($account->uid)) {
          if (isset($account->node_type) && ($account->node_type == 'comment_node_news')) {
            $variables['user_picture'] = '<div class="img">' . $variables['user_picture'] . '</div>';
          }
        }
      }
    }
    else {
      $variables['user_picture'] = '<div class="img"><span class="ico icon-user3"></span></div>';
    }
  }
}

/**
 * Implements theme_form_element().
 */
function iris_theme_form_element($variables) {
  $element = & $variables['element'];

  // This function is invoked as theme wrapper, but the rendered form element.
  // May not necessarily have been processed by form_builder().
  $element += array('#title_display' => 'before');

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');

  if (isset($element['#name']) && ($element['#name'] == 'iris_calendar_month')) {
    $attributes['class'][] = 'w-sel';
    $attributes['class'][] = 'month';
  }

  if (isset($element['#name']) && ($element['#name'] == 'iris_calendar_year')) {
    $attributes['class'][] = 'w-sel';
    $attributes['class'][] = 'year';
  }

  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(
      ' ' => '-',
      '_' => '-',
      '[' => '-',
      ']' => '',
    ));
  }
  $attributes['class'][] = 'row-form';
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      if (!isset($element['#field_name']) || ($element['#field_name'] != 'comment_body')) {
        $output .= ' ' . theme('form_element_label', $variables);
      }
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";

      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";

      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";

      break;
  }

  if (!empty($element['#description'])) {
    $output .= '<div class="description">' . $element['#description'] . "</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}

/**
 * Implements hook_theme().
 */
function iris_theme_theme() {
  $items = array();
  $items['user_login'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'iris_theme') . '/templates',
    'template' => 'user-login',
  );
  $items['user_profile_form'] = array(
    'render element' => 'form',
    'arguments' => array('form' => NULL),
    'path' => drupal_get_path('theme', 'iris_theme') . '/templates',
    'template' => 'user-edit',
  );
  return $items;
}

/**
 * Returns HTML for a checkbox form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #id, #name, #attributes, #checked, #return_value.
 *
 * @return string
 *   Themed input.
 *
 * @ingroup themeable
 */
function iris_theme_checkbox($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'checkbox';
  $element['#attributes']['class'][] = 'custom-checkbox';
  element_set_attributes($element, array(
    'id',
    'name',
    '#return_value' => 'value',
  ));

  // Unchecked checkbox has #value of integer 0.
  if (!empty($element['#checked'])) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-checkbox'));

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

/**
 * Returns HTML for a radio button form element.
 *
 * Note: The input "name" attribute needs to be sanitized before output, which
 *       is currently done by passing all attributes to drupal_attributes().
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #required, #return_value, #value, #attributes, #title,
 *     #description.
 *
 * @return string
 *   Themed input.
 *
 * @ingroup themeable
 */
function iris_theme_radio($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'radio';
  $element['#attributes']['class'][] = 'custom-radio';
  element_set_attributes($element, array(
    'id',
    'name',
    '#return_value' => 'value',
  ));

  if (isset($element['#return_value']) && $element['#value'] !== FALSE && $element['#value'] == $element['#return_value']) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-radio'));

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}
