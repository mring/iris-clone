<div class="account-nav">
  <div class="user">
    <div class="photo">
      <a href="#">
        <!-- <img src="images/photo-04.png" alt=""> --><!-- user photo -->
        <span class="ico icon-user3"></span> <!-- Default user -->
      </a>
    </div>
  </div>
  <div class="logout">
    <a href="#"><span class="ico icon-logout"></span></a>
  </div>
  <div class="edit">
    <a href="#"><span class="ico icon-edit"></span></a>
  </div>
  <div class="setting">
    <a href="#"><span class="ico icon-settings"></span></a>
  </div>
  <div class="notification">
    <a class="opener" href="#">
      <span class="ico icon-account-news"></span>
      <span class="status new">3</span>
    </a>
    <div class="notification-drop">
      <div class="frame jcf-scrollable">
        <ul>
          <li class="n-comment"><a href="#link6"><strong>Gabriel</strong> a répondu à votre commentaire</a></li>
          <li class="n-comment opacity"><a href="#link7"><strong>Soline</strong> a commenté votre news</a></li>
          <li class="n-user opacity"><a href="#link8"><strong>Rony</strong> vous a assigné HPP Toblerone</a></li>
          <li class="n-user"><a href="#link9"><strong>Rony</strong> à traité Retours et destructions P&amp;C</a></li>
          <li class="n-comment opacity"><a href="#link10"><strong>Soline</strong> a commenté Dior prix de vente corrigé</a></li>
          <li class="n-user"><a href="#"><strong>Rony</strong> à traité Retours et destructions P&amp;C</a></li>
          <li class="n-comment opacity"><a href="#"><strong>Soline</strong> a commenté Dior prix de vente corrigé</a></li>
          <li class="n-user"><a href="#"><strong>Rony</strong> à traité Retours et destructions P&amp;C</a></li>
          <li class="n-comment opacity"><a href="#link11"><strong>Soline</strong> a commenté Dior prix de vente corrigé</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
