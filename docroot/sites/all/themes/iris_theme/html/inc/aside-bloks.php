<div class="aside-box opened no-active">
  <div class="title">
    <a href="#">MES NEWS</a>
  </div>
  <div class="slide">
    <ul class="aside-list">
      <li><a href="#"><span class="ico icon-star"></span> <span class="txt">Mes favoris</span></a><span class="num">2</span></li>
      <li><a href="#"><span class="ico icon-user"></span> <span class="txt">Mes news assignées</span></a><span class="num">3</span></li>
      <li><a href="#"><span class="ico icon-folder"></span> <span class="txt">News archivées</span></a></li>
    </ul>
  </div>
</div>
<div class="aside-box opened">
  <div class="title">
    <a href="#">THÈMES</a>
  </div>
  <div class="slide">
    <ul class="aside-list">
      <li><a href="#"><span class="ico icon-list"></span> <span class="txt">Toutes</span></a></li>
      <li class="active"><a href="#"><span class="ico icon-info"></span> <span class="txt">Infos diverses</span></a><span class="num">1</span></li>
      <li><a href="#"><span class="ico icon-label"></span> <span class="txt">Anim &amp; Promos</span></a><span class="num">4</span></li>
      <li><a href="#"><span class="ico icon-flag"></span> <span class="txt">Challenges</span></a></li>
      <li><a href="#"><span class="ico icon-car"></span> <span class="txt">Approvisionnement</span></a><span class="num">10</span></li>
      <li><a href="#"><span class="ico icon-basket"></span> <span class="txt">Merchandizing</span></a></li>
      <li><a href="#"><span class="ico icon-star2"></span> <span class="txt">Supports de vente</span></a></li>
    </ul>
  </div>
</div>
<div class="aside-box opened">
  <div class="title">
    <a href="#">BOUTIQUES</a>
  </div>
  <div class="slide">
    <div class="two-list">
      <ul class="aside-list">
        <li class="active"><a href="#"><span class="txt">LSM3</span></a></li>
        <li><a href="#"><span class="txt">LSM4</span></a></li>
        <li><a href="#"><span class="txt">LSM5</span></a></li>
      </ul>
      <ul class="aside-list">
        <li><a href="#"><span class="txt">LSM5</span></a></li>
        <li><a href="#"><span class="txt">LSM5</span></a></li>
        <li><a href="#"><span class="txt">LSM5</span></a></li>
      </ul>
    </div>
  </div>
</div>
<div class="aside-box opened no-visible-tablet-bar">
  <div class="title">
    <a href="#">UNIVERS</a>
  </div>
  <div class="slide">
    <ul class="aside-list">
      <li class="active"><a href="#"><span class="ico icon-list"></span> <span class="txt">Tous</span></a></li>
      <li><a href="#"><span class="ico icon-glass"></span> <span class="txt">ATGC</span></a></li>
      <li><a href="#"><span class="ico icon-pc"></span> <span class="txt">PC</span></a></li>
      <li><a href="#"><span class="ico icon-mode"></span> <span class="txt">MODE</span></a></li>
      <li><a href="#"><span class="ico icon-processor"></span> <span class="txt">SPECIALTIES</span></a></li>
    </ul>
  </div>
</div>
<div class="aside-box opened">
  <div class="title">
    <a href="#">TYPE DE NEWS</a>
  </div>
  <div class="slide">
    <ul class="aside-list">
      <li><a href="#"><span class="ico icon-list"></span><span class="txt">Toutes</span></a></li>
      <li class="active no-visible-tablet-text"><a href="#"><span class="status new"></span> <span class="txt">Nouvelles</span></a></li>
      <li><a href="#"><span class="status in-progress"></span> <span class="txt">En cours</span></a></li>
      <li><a href="#"><span class="status dealt"></span> <span class="txt">Traitées</span></a></li>
    </ul>
  </div>
</div>
