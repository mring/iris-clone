<meta charset="utf-8" />
<title>Iris</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="shortcut icon" href="favicon.png">
<link rel="icon" href="http://www.apererva.com/test/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://www.apererva.com/test/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

<!-- Third party libraries -->
<link type="text/css" rel="stylesheet" media="all" href="css/vendor.css">
<!-- Third party libraries -->

<!-- Theme specific styles -->
<link type="text/css" rel="stylesheet" media="all" href="css/screen.css">
<link type="text/css" rel="stylesheet" media="print" href="css/print.css">
<!-- Theme specific styles -->


<!-- Slice only jQuery update emulation -->
<script src="js/jquery.js"></script>
<script src="js/jquery.once.js"></script>
<!-- Slice only Query update emulation -->

<!-- Slice only Drupal.js emulation -->
<script src="js/slice.js"></script>
<!-- Slice only Drupal.js emulation -->


<!-- Third party libraries -->
<script src="js/vendor.js"></script>
<!-- Third party libraries -->

<script src="js/svg.js"></script>
<script src="js/jquery.openclose.js"></script>
<script src="js/jquery.matchHeight.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>


<!-- Drupal behaviors -->
<script src="js/behaviors/openclose.js"></script>
<script src="js/behaviors/sidebar.js"></script>
<script src="js/behaviors/placeholder.js"></script>
<script src="js/behaviors/scrollbar.js"></script>
<script src="js/behaviors/forms.js"></script>
<script src="js/behaviors/favorite-toggle.js"></script>
<script src="js/behaviors/agenda-page.js"></script>
<script src="js/behaviors/fancybox.js"></script>
<script src="js/behaviors/news-feed.js"></script>
<!-- Drupal behaviors -->
