<!DOCTYPE html>
<html lang="en" style="overflow: visible;">
<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Iris</title>

<link type="text/css" rel="stylesheet" media="all" href="css/screen.css">

</head>
<body style="overflow: visible;">
  <div class="main-header">
    <div class="container">
      <h1>Iris</h1>
    </div>
  </div>
  <div class="page">
    <div class="container">
      <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="homepage.php">
              <img alt="page" src="images/preview/home.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>Home Page</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/home.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="news.php">
              <img alt="page" src="images/preview/news.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>News Page</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/news.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="news-search.php">
              <img alt="page" src="images/preview/news-search.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>News Search Page</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/news-search.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="news-agenda.php">
              <img alt="page" src="images/preview/news-agenda.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>News Agenda Page</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/news-agenda.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="agenda.php">
              <img alt="page" src="images/preview/agenda.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>Agenda Page</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/agenda.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="generic.php">
              <img alt="page" src="images/preview/generic.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>Generic Page</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/generic.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="popup.php">
              <img alt="page" src="images/preview/popup.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>popup</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/popup.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="promotion.php">
              <img alt="page" src="images/preview/promotion.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>Promotion page</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/promotion.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="promotion-alt.php">
              <img alt="page" src="images/preview/promotion-alt.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>Promotion-alt page</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/promotion-alt.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="merchandising.php">
              <img alt="page" src="images/preview/merchandising.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>Merchandising page</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/merchandising.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="form-elemet.php">
              <img alt="page" src="images/preview/form-elemet.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>Form Elemet</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/form-elemet.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
          <figure class="thumbnail">
            <a href="email/email.html">
              <img alt="page" src="images/preview/email.jpg" style="width: 100%; display: block;">
            </a>
            <figcaption class="caption">
              <h4>Email</h4>
              <p><a class="btn btn-block btn-default" target="_blank" href="images/preview/email.jpg"><span class="glyphicon glyphicon-new-window"></span>&nbsp;PSD</a></p>
            </figcaption>
          </figure>
        </div>
      </div>
    </div>
  </div>
  <div class="page-footer">
    <div class="container">
      <p class="text-center">Adyax team. With <span class="text-danger">&#10084;</span>.</p>
    </div>
  </div>
</body>
</html>

