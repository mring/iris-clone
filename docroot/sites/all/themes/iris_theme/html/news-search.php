<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <?php include "inc/head.php"; ?>
</head>
<body>
  <div class="wrapper">
    <div class="overlay"></div>
    <header>
      <?php include "inc/navigation.php"; ?>
      <?php include "inc/header.php"; ?>
    </header>
    <section id="main">
      <aside id="sidebar" class="sidebar">
        <div class="wrapp-aside">
          <div class="aside-open-box"></div>
          <div class="aside-scroll jcf-scrollable">
            <div class="side-holder">
              <div class="aside-search">
                <form action="#">
                  <div>
                    <input type="text" value="HPP">
                    <button type="submit"><span class="ico icon-search"></span></button>
                  </div>
                </form>
              </div>
              <?php include "inc/aside-bloks.php"; ?>
            </div>
          </div>
        </div>
      </aside>
      <div id="content">
        <div class="home-news">
          <div class="news-list-container">
            <div class="news-title-nav">
              <ul class="news-nav">
                <li class="active"><a href="#"><span class="ico icon-list"></span> VUE STANDARD</a></li>
                <li><a href="#"><span class="ico icon-calendar"></span> VUE AGENDA</a></li>
              </ul>
              <div class="search-result">
                5 résultats pour : <span>HPP</span>
              </div>
            </div>
            <div class="news-list-scroll jcf-scrollable">
              <ul class="news-list">
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status new"></span>
                      <span class="txt">
                        <span class="title">Animation HPP Armani</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 1h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="active">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">HPP Toblerone</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Animation HPP Armani</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 5j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Animation HPP Armani</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 5j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 7j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="news-content">
            <div class="news-content-scroll jcf-scrollable">
              <div class="news-content-frame">
                <a href="#" class="close-news icon-close"></a>
                <a class="favorite-link" href="#"><span class="ico icon-star"></span></a>
                <div class="title">
                  <h2>HPP Toblerone</h2>
                  <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                </div>
                <div class="img-block">
                  <img src="images/img-01.jpg" alt="">
                </div>
                <div class="links-block">
                  <div class="col">
                    <h4>Documents</h4>
                    <ul class="links-list">
                      <li><a href="#">
                        <span class="ico"><span class="pdf"></span></span>
                        <span class="txt"><span>HPPToblerone22121801.pdf</span></span>
                      </a></li>
                      <li><a href="#">
                        <span class="ico"><span class="doc"></span></span>
                        <span class="txt"><span>Review-HPPToblerone-22121801.doc</span></span>
                      </a></li>
                      <li><a href="#">
                        <span class="ico"><span class="ppt"></span></span>
                        <span class="txt"><span>HPPToblerone22121801.pttx</span></span>
                      </a></li>
                    </ul>
                  </div>
                  <div class="col">
                    <h4>Liens</h4>
                    <ul class="links-list">
                      <li><a href="#">
                        <span class="ico"><span class="filelink"></span></span>
                        <span class="txt"><span>Exemples de HPP</span></span>
                      </a></li>
                      <li><a href="#">
                        <span class="ico"><span class="filelink"></span></span>
                        <span class="txt"><span>HPP Toblerone wiki</span></span>
                      </a></li>
                    </ul>
                  </div>
                </div>
                <div class="comment-box">
                  <div class="comment-row">
                    <div class="photo">
                      <div class="img">
                        <img src="images/photo-01.png" alt="">
                      </div>
                    </div>
                    <div class="txt">
                      <div class="head">
                        <span class="name">Paul Andriatafika</span>
                        <span class="date">15 décembre 2015</span>
                      </div>
                      <span class="author">Auteur</span>
                      <p>Veuillez trouver en PJ les documents utiles pour cette news merchandizing. N’hesitez pas à me contacter si vous avez besoin. Merci beaucoup !</p>
                    </div>
                  </div>
                </div>
                <div class="step-block">
                  <span class="line"></span>
                  <ul>
                    <li class="new">
                      <a href="#"><span class="bullet"><span></span></span></a>
                      <span class="name">Nouveau</span>
                      <span class="txt">Pour le 18/12/2015</span>
                    </li>
                    <li class="in-progress active">
                      <a href="#"><span class="bullet"><span></span></span></a>
                      <span class="name">En cours</span>
                      <span class="txt">Pour le 18/12/2015</span>
                    </li>
                    <li class="completed">
                      <a class="open-popup" href="#popup1"><span class="bullet"><span></span></span></a>
                      <span class="name">Terminé</span>
                      <span class="txt">Pour le 18/12/2015</span>
                    </li>
                  </ul>
                </div>
                <div class="comment-block">
                  <div class="two-comment-block">
                    <div class="assigned-box">
                      <h4><span class="ico icon-user"></span> Assigner à</h4>
                      <div class="frame">
                        <div class="form-assigned">
                          <form action="#">
                            <div>
                              <div class="row-form">
                                <input type="text" placeholder="Ajouter un nom…" value="">
                              </div>
                              <ul class="assigned-list">
                                <li>
                                  <span><a href="#" class="icon-close"></a> Soline Ledesert</span>
                                </li>
                                <li>
                                  <span><a href="#" class="icon-close"></a> Gabriel Ghnassia</span>
                                </li>
                              </ul>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="photo-box">
                      <h4><span class="ico icon-photo"></span> Ajouter une photo</h4>
                      <div class="frame">
                        <div class="photo-load"><a href="#"></a></div>
                      </div>
                    </div>
                  </div>
                  <div class="comment-box bg">
                    <div class="comment-row">
                      <div class="photo">
                        <div class="img">
                          <img src="images/photo-02.png" alt="">
                        </div>
                      </div>
                      <div class="txt">
                        <div class="head">
                          <span class="name">Soline Ledesert</span>
                          <span class="date"><span class="icon-time"></span> 5j</span>
                        </div>
                        <span class="remark">Commentaire</span>
                        <p>Attention ! Je crois qu’il y a une erreur, modifiez svp.</p>
                      </div>
                    </div>
                    <div class="comment-row secondary">
                      <div class="photo">
                        <div class="img">
                          <img src="images/photo-01.png" alt="">
                        </div>
                      </div>
                      <div class="txt">
                        <div class="head">
                          <span class="name">Paul Andriatafika</span>
                          <span class="date"><span class="icon-time"></span> 3j</span>
                        </div>
                        <p>Vu ! C’est corrigé. Merci.</p>
                      </div>
                    </div>
                    <div class="comment-row secondary">
                      <div class="photo">
                        <div class="img">
                          <img src="images/photo-03.png" alt="">
                        </div>
                      </div>
                      <div class="txt">
                        <div class="form-comment">
                          <form action="#">
                            <input type="text" placeholder="Laissez un commentaire...">
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="comment-box bg">
                    <div class="comment-row">
                      <div class="photo">
                        <div class="img">
                          <img src="images/photo-03.png" alt="">
                        </div>
                      </div>
                      <div class="txt">
                        <div class="form-comment">
                          <form action="#">
                            <input type="text" placeholder="Laissez un commentaire...">
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="three-boxes">
                  <div class="box">
                    <h4>NOM DE L'OPÉRATION</h4>
                    <ul>
                      <li>HPP Fragrance</li>
                    </ul>
                  </div>
                  <div class="box">
                    <h4>CATÉGORIES</h4>
                    <ul>
                      <li>Animation</li>
                    </ul>
                  </div>
                  <div class="box">
                    <h4>SECTEUR D'ACTIVITÉ</h4>
                    <ul>
                      <li>Champagne</li>
                    </ul>
                  </div>
                </div>
                <div class="linked-store">
                  <span class="ttl">BOUTIQUES CONCERNÉES</span>
                  <ul>
                    <li><a href="#">LSM3</a></li>
                    <li><a href="#">LSM4</a></li>
                    <li><a href="#">LSM5</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="g-hidden">
    <div class="popup" id="popup1">
      <h3>Passer en état "Terminé"?</h3>
      <p>Ceci est un sous-texte de complément.</p>
      <div class="btn-line">
        <a class="yes" href="#"><span class="ico-yes"></span> Oui</a>
        <a class="not close-popup" href="#"><span class="ico-not"></span> Non</a>
      </div>
    </div>
  </div>
</body>
</html>
