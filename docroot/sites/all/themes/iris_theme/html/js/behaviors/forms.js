var iris = iris || {};

iris.forms = function(context, settings) { "use strict";
  jQuery('.jcf-scrollable', context).once('scrollable').each(function () {
    // jcf.replaceAll();
    jcf.setOptions('Scrollable', {
      mouseWheelStep: 150,
      handleResize: true,
      alwaysShowScrollbars: false,
      alwaysPreventMouseWheel: false
    });

    var deviceAgent = navigator.userAgent.toLowerCase();
    var isTouchDevice = Modernizr.touch ||
      (deviceAgent.match(/(iphone|ipod|ipad)/) ||
          deviceAgent.match(/iphone/i) ||
          deviceAgent.match(/ipad/i) ||
          deviceAgent.match(/ipod/i));

    if(isTouchDevice){
      jcf.replace('.jcf-scrollable:not(.news-list-scroll)');
    } else {
      jcf.replace('.jcf-scrollable');
    }

  });

  // custom Select
  jcf.setOptions('Select', {
    useCustomScroll: false,
    wrapNative: false,
    fakeDropInBody: true,
    wrapNativeOnMobile: false,
    flipDropToFit: false
  });
  jcf.replace('.custom-select, .default-custom-select');

  // custom Checkbox
  jcf.setOptions('Checkbox', {});
  jcf.replace('.custom-checkbox');

  // custom Radio
  jcf.setOptions('Radio', {});
  jcf.replace('.custom-radio');
}

Drupal.behaviors.forms = {

  attach: function(context, settings){
    iris.forms(context, settings);
  }
}

// Init self in case we are loaded async.
if (jQuery.isReady) {
  // It is up to script to define scope and settings it should be applied to
  // in case of jquery mobile integration used you can use site_common.jquery_mobile.getPageContext()
  // to get only current page's context.
  // In this forms we use global scope.
  var context = document;
  var settings = Drupal.settings;
  Drupal.behaviors.forms.attach(context, settings);
}
