(function ($) {
  Drupal.behaviors.NewsUserStatus = {
    attach: function (context, settings) {
      if ($('div.step-block div.news-status-progress-placeholder', context).length) {
        var news_id = $('div.step-block div.news-status-progress-placeholder', context).attr('data-entity-id');
        var store_id = $('div.step-block div.news-status-progress-placeholder', context).attr('data-store');


        var element_settings = {
          url: Drupal.settings.basePath + 'news/' + news_id + '/generate/store/' + store_id + '/relation',
          selector: $('div.step-block'),
          progress: {
            type: 'none',
          }
        };
        var ajax = new Drupal.ajax(false, false, element_settings);
        ajax.eventResponse(ajax, {});
      }
    }
  }

  Drupal.behaviors.NewsStatusDone = {
    attach: function (context, settings) {
      $('div.photo-box .frame .photo-load a.allow-file-dialog').click(function() {
        document.getElementById('news-store-image').click();
        return false;
      });
    }
  }
})(jQuery);