var iris = iris || {};

iris.openclose = function(context, settings) { "use strict";

  jQuery('.account-nav .notification', context).once('openclose').each(function () {
    jQuery(this).openClose({
      activeClass: 'opened',
      opener: '.opener',
      slider: '.notification-drop',
      animSpeed: 200,
      addClassBeforeAnimation: true,
      hideOnClickOutside: true,
      effect: 'fade'
    });
  });
}

Drupal.behaviors.openclose = {

  attach: function(context, settings){
    iris.openclose(context, settings);
  }
}

// Init self in case we are loaded async.
if (jQuery.isReady) {
  // It is up to script to define scope and settings it should be applied to
  // in case of jquery mobile integration used you can use site_common.jquery_mobile.getPageContext()
  // to get only current page's context.
  // In this openclose we use global scope.
  var context = document;
  var settings = Drupal.settings;
  Drupal.behaviors.openclose.attach(context, settings);
}
