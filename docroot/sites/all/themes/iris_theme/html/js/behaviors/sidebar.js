var iris = iris || {};

iris.tablet_navigation = function(context, settings) { "use strict";
  jQuery('.aside-box', context).once('openclose').each(function () {
    jQuery(this).openClose({
      activeClass: 'opened',
      opener: '.title a',
      slider: '.aside-list li:not(.active, .selected)',
      animSpeed: 300,
      addClassBeforeAnimation: false,
      effect: 'slide'
    });
  });
  // destroy srroll for ipad - Bug #197953
  var deviceAgent = navigator.userAgent.toLowerCase();
  var isTouchDevice = Modernizr.touch ||
  (deviceAgent.match(/(iphone|ipod|ipad)/) ||
  deviceAgent.match(/iphone/i) ||
  deviceAgent.match(/ipad/i) ||
  deviceAgent.match(/ipod/i));

  jQuery('#sidebar', context).once('tablet_navigation').each(function () {
    var holder = jQuery('.wrapper');
    jQuery(".mobile-menu, .aside-open-box").click(function () {
      //jQuery(this).toggleClass('active');
      holder.toggleClass('open-aside');
      if (isTouchDevice) {
        jcf.destroy('.jcf-scrollable');
      } else{
        jcf.replace('.jcf-scrollable');
      }

      return false;
    });

    jQuery('.overlay').click(function(e){
      e = e || event;
      var t = e.target || e.srcElement;
      t = jQuery(t);
      if(t.hasClass('#sidebar') || t.parents('#sidebar').length != 0){
        if(jQuery('#sidebar').click) {
          return true;
        } else {
          return false;
        }
      }
      jQuery('.wrapper').removeClass('open-aside');
      //jcf.replace('.jcf-scrollable');
      if (isTouchDevice) {
        jcf.destroy('.jcf-scrollable');
      } else{
        jcf.replace('.jcf-scrollable');
      }
    });

    var win = jQuery(window);
    win.on('load resize', function() {
      if (win.width() > 1023) {
        jQuery('.wrapper').removeClass('open-aside');
      }
    });
  });
};

Drupal.behaviors.tablet_navigation = {
  attach: function(context, settings) {
    iris.tablet_navigation(context, settings);
  }
};

// Init self in case we are loaded async.
if (jQuery.isReady) {
  // It is up to script to define scope and settings it should be applied to
  // in case of jquery mobile integration used you can use site_common.jquery_mobile.getPageContext()
  // to get only current page's context.
  // In this tablet_navigation we use global scope.
  var context = document;
  var settings = Drupal.settings;
  Drupal.behaviors.tablet_navigation.attach(context, settings);
}
