var iris = iris || {};

iris.fancybox = function(context, settings) { "use strict";
  jQuery(".open-popup").fancybox({
    padding  : '0',
    closeBtn : false
  });
  jQuery('.close-popup').on('click', function(){
    jQuery.fancybox.close();
    return false;
  });
}

Drupal.behaviors.fancybox = {

  attach: function(context, settings){
    iris.fancybox(context, settings);
  }
}

// Init self in case we are loaded async.
if (jQuery.isReady) {
  // It is up to script to define scope and settings it should be applied to
  // in case of jquery mobile integration used you can use site_common.jquery_mobile.getPageContext()
  // to get only current page's context.
  // In this fancybox we use global scope.
  var context = document;
  var settings = Drupal.settings;
  Drupal.behaviors.fancybox.attach(context, settings);
}
