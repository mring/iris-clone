var iris = iris || {};

iris.newsFeed = function(context, settings) { "use strict";
  jQuery('.news-list', context).once('newsFeed').each(function () {
    var holder = jQuery(this);
    var link = holder.find('li > a');

    link.click(function(){
      jQuery('.news-list').find('>li').removeClass('active');
      jQuery(this).parent().addClass('active');
    });
  });

}

Drupal.behaviors.newsFeed = {

  attach: function(context, settings){
    iris.newsFeed(context, settings);
  }
}

// Init self in case we are loaded async.
if (jQuery.isReady) {
  // It is up to script to define scope and settings it should be applied to
  // in case of jquery mobile integration used you can use site_common.jquery_mobile.getPageContext()
  // to get only current page's context.
  // In this newsFeed we use global scope.
  var context = document;
  var settings = Drupal.settings;
  Drupal.behaviors.newsFeed.attach(context, settings);
}
