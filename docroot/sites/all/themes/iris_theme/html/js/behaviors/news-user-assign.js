(function ($) {
  Drupal.behaviors.AutocompleteUserAssign = {
    attach: function (context, settings) {
      $("#user-assign-autocomplete").autocomplete({
        source: Drupal.settings.basePath + 'user_assign/' + Drupal.settings.news_user_assign.rid + '/autocomplete',
        select: function(event, ui) {
          if (ui.item.value && !$('.form-assigned ul.assigned-list li a[data-uid=' + ui.item.value + ']').length) {
            var element_settings = {
              url: Drupal.settings.basePath + 'user_assign/' + Drupal.settings.news_user_assign.rid + '/' + ui.item.value + '/add',
            };

            var ajax = new Drupal.ajax(false, false, element_settings);
            ajax.eventResponse(ajax, {});
          }
          $(this).val('');
          return false
        },
        focus: function (event, ui) {
          $(this).val(ui.item.label);
          return false;
        }
      });
    }
  }

  Drupal.behaviors.AutocompleteUserAssignImage = {
    attach: function (context, settings) {
      $("#news-store-image").change(function() {
        if ($("#news-store-image").val()) {
          var formData = new FormData();
          formData.append('files[status_photo]', $('#news-store-image')[0].files[0]);
          $.ajax({
            url: Drupal.settings.basePath + 'user_assign/' + Drupal.settings.news_user_assign.rid + '/file',
            type: 'POST',
            processData: false,
            contentType: false,
            data : formData,
            success : function(data) {
              if (typeof data.error == "undefined") {
                $('.photo-box .frame .photo-load').removeClass('photo-load');
                $('.photo-box .frame a').html(data.image_html);
                $('.photo-box h4 a.delete-link').removeClass('hidden');
                $('.photo-box .frame a').attr('onclick', "");
              } else {
                $('.photo-box .frame .photo-load').removeClass('photo-load');
                $('.photo-box .frame a').html(data.error);
              }
            }
          });
        }
      });
    }
  }

  /**
   * Ajax delivery command to change related elements after removing image.
   */
  Drupal.ajax.prototype.commands.removeImage = function (ajax, response, status) {
    $('#news-store-image').val('');
    $('.photo-box h4').find('a.delete-link').addClass('hidden');
    $('.photo-box .frame').find('div').addClass('photo-load');
    $('.photo-box .frame').find('a').attr('onclick', "document.getElementById('news-store-image').click(); return false;");
    return false;
  };
})(jQuery);
