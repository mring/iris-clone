(function ($) {
    Offline.options.interceptRequests = true;
    Offline.options.checkOnLoad = false;


    Drupal.removeIrisRemoveCookie = function(cookie_name) {
        document.cookie = cookie_name + '= ;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
    }

    Drupal.removeIrisSetCookie = function(cookie_name, value) {
        document.cookie = cookie_name + '=' + value + '; expires=Thu, 18 Dec 2024 12:00:00 UTC; path=/';
    }

    if ($.browser.ipad) {
        var dbOpenPromise = $.indexedDB("PreCacheDB", {
            "upgrade": function (transaction) {
            },
            "schema": {
                "1": function (transaction) {
                    transaction.createObjectStore("Pages1");


                },
            }
        });
        if (dbOpenPromise && document.cookie.indexOf("indexeddb_loaded") === -1) {
            var objectStore = $.indexedDB("PreCacheDB").objectStore("Pages1");
            Drupal.irisGlobalThrobberStart();
            jQuery.getJSON('/precache-pages-list', function (json) {
                $.each(json, function (index, value) {
                    var db_key = JSON.stringify({'nid': index});
                    objectStore.add({
                        "commands": value.commands,
                    }, db_key).then(function () {
                    }, function (e, d) {

                    });
                });
                Drupal.irisGlobalThrobberFinish();
                Drupal.removeIrisSetCookie('indexeddb_loaded', 1);

            }).fail(function () {
                Drupal.removeIrisRemoveCookie('indexeddb_loaded');
                Drupal.irisGlobalThrobberFinish();
            });
        }

    }
    Drupal.behaviors.offLineMode = {
        attach: function (context, settings) {
            if (Drupal.settings.user_js_uid > 0) {

                if (context == document) {
                    if ('serviceWorker' in navigator) {
                        Drupal.removeIrisSetCookie('sw_unavailable', 0);
                        if (Drupal.settings.user_js_uid > 0 && Drupal.settings.enable_sw) {
                            navigator.serviceWorker.register(Drupal.settings.basePath + 'appCache.js', {scope: './'}).then(function () {
                                if (navigator.serviceWorker.controller) {
                                } else {
                                }
                            }).catch(function (error) {
                            });
                        }

                    } else {
                        Drupal.removeIrisSetCookie('sw_unavailable', 1);
                    }
                }

                document.cookie = "sw_unavailable=1; expires=Thu, 18 Dec 2024 12:00:00 UTC; path=/";
            } else {
                jQuery.indexedDB("PreCacheDB").deleteDatabase();
                Drupal.removeIrisRemoveCookie('indexeddb_loaded');
                Drupal.removeIrisSetCookie('sw_unavailable', 0);
            }
        }
    }

    Drupal.ajax = Drupal.ajax || {};

    if (Drupal.ajax.hasOwnProperty("prototype")) {

        /**
         * Handler for the form redirection completion.
         */
        Drupal.ajax.prototype.success = function (response, status) {
            if ($.browser.ipad) {

                if ("submit" in this && "url" in this) {
                    var request_data = {
                        'url': this.url,
                        'params': {}
                    };

                    for (var submit_key in this.submit) {

                        if (submit_key.indexOf('ajax_page_state') === -1 &&
                            submit_key.indexOf('ajax_html_ids') === -1) {
                            request_data['params'][submit_key] = this.submit[submit_key];
                        }
                    }
                }

                if (dbOpenPromise) {
                    var objectStore = $.indexedDB("PreCacheDB").objectStore("Pages1");
                    var db_key = JSON.stringify(request_data);
                    objectStore.delete(db_key);
                    var promise = objectStore.add({
                        'commands': response,
                    }, db_key);

                    promise.done(function (result, event) {
                    });

                    promise.fail(function (error, event) {
                    });
                }
            }

            // Remove the progress element.
            if (this.progress.element) {
                $(this.progress.element).remove();
            }
            if (this.progress.object) {
                this.progress.object.stopMonitoring();
            }
            $(this.element).removeClass('progress-disabled').removeAttr('disabled');

            Drupal.freezeHeight();

            for (var i in response) {
                if (response.hasOwnProperty(i) && response[i]['command'] && this.commands[response[i]['command']]) {
                    this.commands[response[i]['command']](this, response[i], status);
                }
            }

            // Reattach behaviors, if they were detached in beforeSerialize(). The
            // attachBehaviors() called on the new content from processing the response
            // commands is not sufficient, because behaviors from the entire form need
            // to be reattached.
            if (this.form) {
                var settings = this.settings || Drupal.settings;
                Drupal.attachBehaviors(this.form, settings);
            }

            Drupal.unfreezeHeight();

            // Remove any response-specific settings so they don't get used on the next
            // call by mistake.
            this.settings = null;
        };


        /**
         * Handler for the form redirection error.
         */
        Drupal.ajax.prototype.error = function (xmlhttprequest, uri, customMessage) {
            if ($.browser.ipad) {
                if ("submit" in this && "url" in this) {
                    var request_data = {
                        'url': this.url,
                        'params': {}
                    };

                    for (var submit_key in this.submit) {

                        if (submit_key.indexOf('ajax_page_state') === -1 &&
                            submit_key.indexOf('ajax_html_ids') === -1) {
                            request_data['params'][submit_key] = this.submit[submit_key];
                        }
                    }
                }

                if (dbOpenPromise) {
                    var objectStore = $.indexedDB("PreCacheDB").objectStore("Pages1");
                    var db_key = JSON.stringify(request_data);
                    var promise = objectStore.get(db_key);

                    var ajax = this;
                    promise.done(function (result, event) {
                        if ("params" in request_data && "nid" in request_data.params) {
                            var precached_db_key = JSON.stringify({"nid": request_data.params.nid});
                            objectStore.get(precached_db_key).done(function (result, event) {
                                if (result) {
                                    for (var i in result.commands) {
                                        if (result.commands.hasOwnProperty(i) && result.commands[i]['command'] && ajax.commands[result.commands[i]['command']]) {
                                            ajax.commands[result.commands[i]['command']](ajax, result.commands[i], status);
                                        }
                                    }
                                } else {
                                    var db_key = JSON.stringify(request_data);
                                    var promise = objectStore.get(db_key);
                                    promise.done(function (result, event) {
                                        if (result && "commands" in result) {
                                            for (var i in result.commands) {
                                                if (result.commands.hasOwnProperty(i) && result.commands[i]['command'] && ajax.commands[result.commands[i]['command']]) {
                                                    ajax.commands[result.commands[i]['command']](ajax, result.commands[i], status);
                                                }
                                            }
                                        } else {
                                            $("#content-item").html('Sorry, this page is not available without internet connection');
                                        }
                                    });
                                }
                            });

                        }
                        else if (result && "commands" in result) {
                            for (var i in result.commands) {
                                if (result.commands.hasOwnProperty(i) && result.commands[i]['command'] && ajax.commands[result.commands[i]['command']]) {
                                    ajax.commands[result.commands[i]['command']](ajax, result.commands[i], status);
                                }
                            }
                        } else {
                            $("#content-item").html('Sorry, this page is not available without internet connection');
                        }

                    });

                    promise.fail(function (error, event) {

                    });
                }
            }
            if (window.console) {
                console.log(Drupal.ajaxError(xmlhttprequest, uri, customMessage));
            }
            // Remove the progress element.
            if (this.progress.element) {
                $(this.progress.element).remove();
            }
            if (this.progress.object) {
                this.progress.object.stopMonitoring();
            }
            // Undo hide.
            $(this.wrapper).show();
            // Re-enable the element.
            $(this.element).removeClass('progress-disabled').removeAttr('disabled');
            // Reattach behaviors, if they were detached in beforeSerialize().
            if (this.form) {
                var settings = this.settings || Drupal.settings;
                Drupal.attachBehaviors(this.form, settings);
            }
        }
    }

    Drupal.behaviors.offLineAppcacheMode = {
        attach: function (context, settings) {
            console.log(window.applicationCache);

            if(window.applicationCache.status !== 0) {
                window.applicationCache.update();
            }

            function logEvent( event ){
            }
            var appCache = window.applicationCache;

            $(appCache).bind(
                "checking",
                function (event) {
                    logEvent("Checking for manifest");
                }
            );
            $(appCache).bind(
                "noupdate",
                function (event) {
                    logEvent("No cache updates");
                }
            );

            $(appCache).bind(
                "downloading",
                function (event) {
                    Drupal.irisGlobalThrobberStart();
                    logEvent("Downloading cache");

                }
            );

            $(appCache).bind(
                "progress",
                function (event) {
                    logEvent("File downloaded");

                }
            );

            $(appCache).bind(
                "cached",
                function (event) {
                    Drupal.irisGlobalThrobberFinish();

                    logEvent("All files downloaded");
                }
            );
            $(appCache).bind(
                "updateready",
                function (event) {
                    logEvent("New cache available");
                    window.applicationCache.update();
                    window.applicationCache.swapCache();
                    Drupal.irisGlobalThrobberFinish();
                    logEvent("Cache updated");
                }
            );
            $(appCache).bind(
                "obsolete",
                function (event) {
                    Drupal.irisGlobalThrobberFinish();
                    logEvent("Manifest cannot be found");
                }
            );
            $(appCache).bind(
                "error",
                function (event) {
                    Drupal.irisGlobalThrobberFinish();
                    logEvent("An error occurred");
                }
            );

        }
    }
})(jQuery);

