(function ($) {
  Drupal.ajax = Drupal.ajax || {};

  if (Drupal.ajax.hasOwnProperty("prototype")) {
    /**
     * Ajax delivery command to add class to comments block.
     */
    Drupal.ajax.prototype.commands.irisAddCommentClasses = function (ajax, response, status) {
      if (response.selector) {
        $selector = $(response.selector);
        if (!$selector.hasClass("bg")) {
          $selector.addClass("bg");
        }
      }
    };
  }
})(jQuery);