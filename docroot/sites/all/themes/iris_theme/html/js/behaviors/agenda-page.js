(function ($) {
var iris = iris || {};

iris.agenda = function(context, settings) { "use strict";

  jQuery('.agenda-holder', context).once('agenda').each(function () {

    //calendar form
    var calendarLink = jQuery('.open-nav, .close-nav');
    var calendar = jQuery('.nav-agenda .calendar');
    calendarLink.click(function() {
      calendar.toggleClass('opened');
      return false;
    });

    function same_search_height() {
      var byRow = jQuery('body');
      jQuery('.row-event').each(function() {
          jQuery(this).children('.same-height').matchHeight({
              byRow: byRow,
              property: 'height',
              target: null,
              remove: false
          });
      });
    }
    var win = jQuery(window);
    win.bind('load resize', function() {
      same_search_height();
      //jQuery('.link-events').popover('hide');
      // jcf.destroy('Scrollable');
      // jcf.destroy('.line .jcf-scrollable');
    });

    var tmpShow = $.fn.popover.Constructor.prototype.show;
    $.fn.popover.Constructor.prototype.show = function() {
      tmpShow.call(this); if (this.options.callbackShow) {
        this.options.callbackShow();
      }
    };
    var tmpHide = $.fn.popover.Constructor.prototype.hide;
    $.fn.popover.Constructor.prototype.hide = function() {
      tmpHide.call(this); if (this.options.callbackHide) {
        this.options.callbackHide();
      }
    };

    jQuery('.link-events').popover({
      container: 'body',
      trigger: 'focus',
      html: true,
      content: function () {
        return $(this).parent().find('.event-popup').html();
      },
      callbackShow: function () {
        //jcf.destroy('.line .jcf-scrollable');
        jcf.replaceAll();
      },
      callbackHide: function () {
        jcf.destroy('.line .jcf-scrollable');
      }
    });
    jQuery('.trigger-event, .trigger-event-tablet').click(function(){
        var target = "#" + jQuery(this).data("target");
        jQuery('.link-events').not(target).popover('hide');
        jQuery(target).popover('toggle');
    });

    jQuery('body').click(function(e){
      e = e || event;
      var t = e.target || e.srcElement;
      t = jQuery(t);
      if(t.hasClass('.event, .calendar, .jcf-select-drop') || t.parents('.event, .calendar, .jcf-select-drop').length != 0){
        if(jQuery('.link-events, .open-nav, .close-nav').click) {
          return true;
        } else {
          return false;
        }
      }
      jQuery('.link-events').popover('hide');
      calendar.removeClass('opened')
    });

    jQuery('.jcf-scrollable').scroll(function(){
      jQuery('.link-events').popover('hide');
    });



  });
}

Drupal.behaviors.agenda = {

  attach: function(context, settings){
    iris.agenda(context, settings);
  }
}

// Init self in case we are loaded async.
if (jQuery.isReady) {
  // It is up to script to define scope and settings it should be applied to
  // in case of jquery mobile integration used you can use site_common.jquery_mobile.getPageContext()
  // to get only current page's context.
  // In this agenda we use global scope.
  var context = document;
  var settings = Drupal.settings;
  Drupal.behaviors.agenda.attach(context, settings);
}
})(jQuery);
