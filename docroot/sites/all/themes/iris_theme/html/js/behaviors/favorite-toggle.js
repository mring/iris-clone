var iris = iris || {};

iris.favorite = function(context, settings) { "use strict";
  jQuery('.favorite-link', context).once('favorite').each(function () {
    jQuery(this).click(function () {
      jQuery(this).toggleClass('on');
      return false;
    });
  });



}

Drupal.behaviors.favorite = {

  attach: function(context, settings){
    iris.favorite(context, settings);
  }
}

// Init self in case we are loaded async.
if (jQuery.isReady) {
  // It is up to script to define scope and settings it should be applied to
  // in case of jquery mobile integration used you can use site_common.jquery_mobile.getPageContext()
  // to get only current page's context.
  // In this favorite we use global scope.
  var context = document;
  var settings = Drupal.settings;
  Drupal.behaviors.favorite.attach(context, settings);
}
