var iris = iris || {};

iris.scrollbar = function(context, settings) { "use strict";
  jQuery('.wrapper', context).once('height-page').each(function () {
    var win = jQuery(window);
    var aside = jQuery('.aside-scroll');
    var main = jQuery('#main');
    var newsList = jQuery('.news-list-scroll');
    var newsContent = jQuery('.news-content-scroll');
    var newsTitleNav = jQuery('.news-title-nav');
    var adminPanel = 29;
    var adminMobileMenu = jQuery('.slicknav_menu');

    var agendaContent = jQuery('.content-agenda');


    win.on('load resize', function() {

      if(jQuery('body').hasClass('admin-menu')){
        if(jQuery(window).width() > 1024){
          var winHeight = win.height();
          var withoutHeaderHeight = win.height() - 48 - adminPanel;
          var newsTitleNavHeight = newsTitleNav.height() + 48;
          var newsListHeight = win.height() - newsTitleNavHeight - adminPanel;

          var agendaContentHeight = win.height() - 144 - adminPanel;
        }
        else{
          var winHeight = win.height();
          var adminMobileMenuHeight = adminMobileMenu.height() ? adminMobileMenu.height() : 47;
          var withoutHeaderHeight = win.height() - 48 - adminMobileMenuHeight;
          var newsTitleNavHeight = newsTitleNav.height() + 48;
          var newsListHeight = win.height() - newsTitleNavHeight - adminMobileMenuHeight;

          var agendaContentHeight = win.height() - 144 - adminMobileMenuHeight;
        }
      }
      else{
        var winHeight = win.height();
        var withoutHeaderHeight = win.height() - 48;
        var newsTitleNavHeight = newsTitleNav.height() + 48;
        var newsListHeight = win.height() - newsTitleNavHeight;

        var agendaContentHeight = win.height() - 144;
      }

      //console.log(newsTitleNavHeight)
      //console.log(asideHeight)

      aside.css({
        'height': withoutHeaderHeight
      });
      main.css({
        'min-height': withoutHeaderHeight
      });
      newsContent.css({
        'height': withoutHeaderHeight
      });
      newsList.css({
        'height': newsListHeight
      });
      agendaContent.css({
        'height': agendaContentHeight
      });

      // destroy srroll for ipad - Bug #197953
      var deviceAgent = navigator.userAgent.toLowerCase();
      var isTouchDevice = Modernizr.touch ||
      (deviceAgent.match(/(iphone|ipod|ipad)/) ||
      deviceAgent.match(/iphone/i) ||
      deviceAgent.match(/ipad/i) ||
      deviceAgent.match(/ipod/i));

      if(!isTouchDevice){
        jcf.replace('.jcf-scrollable');
      }

      setTimeout(function() {
        if (isTouchDevice) {

          aside.css({
            'height': withoutHeaderHeight,
            'overflow': 'scroll',
            '-webkit-overflow-scrolling': 'touch'
          });
          main.css({
            'min-height': withoutHeaderHeight,
            'overflow': 'scroll',
            '-webkit-overflow-scrolling': 'touch'
          });
          newsContent.css({
            'height': withoutHeaderHeight,
            'overflow': 'scroll',
            '-webkit-overflow-scrolling': 'touch'
          });
          newsList.css({
            'height': newsListHeight,
            'overflow': 'scroll',
            '-webkit-overflow-scrolling': 'touch'
          });
          agendaContent.css({
            'height': agendaContentHeight,
            'overflow': 'scroll',
            '-webkit-overflow-scrolling': 'touch'
          });
        }
      }, 10);
    });
  });

}

Drupal.behaviors.scrollbar = {
  attach: function(context, settings){
    iris.scrollbar(context, settings);

    setTimeout(function() {
      jQuery(window).trigger('resize');
      jQuery("#block-system-main .content").css("overflow", "visible");
    }, 10);
  }
}

// Init self in case we are loaded async.
if (jQuery.isReady) {
  // It is up to script to define scope and settings it should be applied to
  // in case of jquery mobile integration used you can use site_common.jquery_mobile.getPageContext()
  // to get only current page's context.
  // In this scrollbar we use global scope.
  var context = document;
  var settings = Drupal.settings;
  Drupal.behaviors.scrollbar.attach(context, settings);
}

// Huge images may break scroll functionality.
// So we force drupal to attach scrollbar after banner is loaded.
jQuery(document).ajaxComplete(function (context, settings) {
    jQuery('.img-block img').load(function (context, settings) {
        Drupal.behaviors.scrollbar.attach(context, settings);
    })
});
