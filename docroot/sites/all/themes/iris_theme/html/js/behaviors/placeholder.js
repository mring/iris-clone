var iris = iris || {};

iris.placeholder = function(context, settings) { "use strict";
  if(jQuery('html').hasClass('no-textshadow')) {
    jQuery("form").find("input[type='text'], input[type='password'], input.form-text").each(function() {
      var tp = jQuery(this).attr("placeholder");
      jQuery(this).attr('value',tp).css({'color':'#9facb8', 'font-style':'italic'});
    }).focusin(function() {
      var val = jQuery(this).attr('placeholder');
      if(jQuery(this).val() == val) {
        jQuery(this).attr('value','').css({'color':'#9facb8', 'font-style':'italic'});
      }
    }).focusout(function() {
      var val = jQuery(this).attr('placeholder');
      if(jQuery(this).val() == "") {
        jQuery(this).attr('value', val).css({'color':'#9facb8', 'font-style':'italic'});
      }
    });

    /* Protected send form */
    jQuery("form").submit(function() {
      jQuery(this).find("input[type='text']").each(function() {
        var val = jQuery(this).attr('placeholder');
        if(jQuery(this).val() == val) {
          jQuery(this).attr('value','');
        }
      })
    });
  }

}

Drupal.behaviors.placeholder = {

  attach: function(context, settings){
    iris.placeholder(context, settings);
  }
}

// Init self in case we are loaded async.
if (jQuery.isReady) {
  // It is up to script to define scope and settings it should be applied to
  // in case of jquery mobile integration used you can use site_common.jquery_mobile.getPageContext()
  // to get only current page's context.
  // In this placeholder we use global scope.
  var context = document;
  var settings = Drupal.settings;
  Drupal.behaviors.placeholder.attach(context, settings);
}
