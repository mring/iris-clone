var CACHE_VERSION = 1;
var CURRENT_CACHES = {
    'read-through': 'iris-offline-' + CACHE_VERSION
};


self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open(CURRENT_CACHES['read-through']).then(function (cache) {
            fetch('/offline-page.html', {"credentials": "include"}).then(function (response) {
                cache.put('/offline-page.html', response.clone());

            })
        })
    );
});


self.addEventListener('activate', function (event) {
    var expectedCacheNames = Object.keys(CURRENT_CACHES).map(function (key) {
        return CURRENT_CACHES[key];
    });
    event.waitUntil(
        caches.keys().then(function (cacheNames) {
            return Promise.all(
                cacheNames.map(function (cacheName) {
                    if (expectedCacheNames.indexOf(cacheName) === -1) {
                        return caches.delete(cacheName);
                    }
                })
            );
        })
    );
});

self.addEventListener('fetch', function (event) {

    var event_request = event.request;
    var url = new URL(event_request.url);

    if (event.request.url.indexOf('/admin') !== -1 ||
        event.request.url.indexOf('/user') !== -1 ||
        event.request.url.indexOf('/node') !== -1) {
        return;
    }

    if (event_request.method !== 'GET') {
        event.respondWith(
            fetch(event_request).catch(function (error) {
                return caches.match('/offline-page.html');
            })
        );

        return;
    }

    var request = new Request(url, {
        method: 'GET',
        headers: event_request.headers,
        mode: event_request.mode == 'navigate' ? 'cors' : event_request.mode,
        credentials: event_request.credentials,
        redirect: event_request.redirect
    });

    event.respondWith(
        fetch(request).then(function (response) {
            if (response.status < 400) {
                caches.open(CURRENT_CACHES['read-through']).then(function (cache) {
                    if (response.ok) {
                        cache.put(request, response.clone());
                    }
                });

            }
            return response;
        }).catch(function (error) {
            return caches.match(request).then(function (response) {
                if (response) {
                    return response;
                }
                return caches.match('/offline-page.html');

            });

        })
    )

});
