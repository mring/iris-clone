(function($){
  Drupal.behaviors.loadSVG = {
    attach: function (context, settings) {
      var svgPath;

      if(Drupal && Drupal.settings && Drupal.settings.svgPath){
        svgPath = Drupal.settings.svgPath
      }
      else{
        svgPath = "svg/sprite.svg";
      }
      var ajax = new XMLHttpRequest();
      ajax.open("GET", svgPath, true);
      ajax.send();
      ajax.onload = function(e) {
        var div = document.createElement("div");
        div.style.display = 'none';
        div.innerHTML = ajax.responseText;
        document.body.insertBefore(div, document.body.childNodes[0]);
      }
    }
  };
})(jQuery)
