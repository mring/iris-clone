<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <?php include "inc/head.php"; ?>
</head>
<body>
  <div class="wrapper">
    <div class="overlay"></div>
    <header>
      <div class="head">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt=""></a>
        </div>
        <a href="#" class="mobile-menu">
          <span></span>
        </a>
      </div>
      <nav>
        <ul>
          <li><a href="#">MES NEWS</a></li>
          <li class="active"><a href="#">CALENDAR</a></li>
          <li><a href="#">MERCH</a></li>
          <li><a href="#">BONNES PRATIQUES </a></li>
          <li><a href="#">RESSOURCES</a></li>
        </ul>
      </nav>
      <?php include "inc/header.php"; ?>
    </header>
    <section id="main">
      <aside id="sidebar" class="sidebar">
        <div class="wrapp-aside">
          <div class="aside-open-box"></div>
          <div class="aside-scroll jcf-scrollable">
            <div class="side-holder">
              <?php include "inc/aside-agenda.php"; ?>
            </div>
          </div>
        </div>
      </aside>
      <div id="content">
        <div class="agenda-holder">
          <div class="head">
            <div class="left-box">
              <div class="nav-agenda">
                <div class="print">
                  <a href="#">
                    <span class="ico icon-print"></span>
                  </a>
                </div>
                <div class="calendar">
                  <a class="open-nav" href="#">
                    <span class="ico icon-calendar"></span>
                  </a>
                  <div class="calendar-nav">
                    <h4>ALLER À <span>(<a class="close-nav" href="#">Annuler</a>)</span></h4>
                    <div class="form-calendar">
                      <form action="#">
                        <div>
                          <div class="w-sel month">
                            <select class="custom-select">
                              <option>Janvier</option>
                              <option>Février</option>
                              <option>Mars</option>
                              <option>April</option>
                            </select>
                          </div>
                          <div class="w-sel year">
                            <select class="custom-select">
                              <option>2015</option>
                              <option>2016</option>
                              <option>2017</option>
                              <option>2018</option>
                            </select>
                          </div>
                          <div class="submit">
                            <input type="submit" value="OK">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div class="nav-month">
                <a href="#" class="prev"><span class="ico icon-prev"></span></a>
                <a href="#" class="next"><span class="ico icon-next"></span></a>
              </div>
            </div>
            <div class="right-box">
              <div class="month-holder">
                <div class="block">
                  <div class="month"><span>Janvier 2016</span></div>
                  <div class="week">
                    <div class="box">
                      <span class="date">4-10</span>
                      <span class="num-week">S1</span>
                    </div><div class="box current">
                      <span class="date">11-17</span>
                      <span class="num-week">S2</span>
                    </div><div class="box">
                      <span class="date">18-24</span>
                      <span class="num-week">S3</span>
                    </div><div class="box">
                      <span class="date">25-31</span>
                      <span class="num-week">S4</span>
                    </div>
                  </div>
                </div><div class="block">
                  <div class="month"><span>Février 2016</span></div>
                  <div class="week">
                    <div class="box">
                      <span class="date">1-7</span>
                      <span class="num-week">S5</span>
                    </div><div class="box">
                      <span class="date">8-14</span>
                      <span class="num-week">S6</span>
                    </div><div class="box">
                      <span class="date">15-21</span>
                      <span class="num-week">S7</span>
                    </div><div class="box">
                      <span class="date">22-28</span>
                      <span class="num-week">S8</span>
                    </div><div class="box">
                      <span class="date">29-6</span>
                      <span class="num-week">S9</span>
                    </div>
                  </div>
                </div><div class="block">
                  <div class="month"><span>Mars 2016</span></div>
                  <div class="week">
                    <div class="box">
                      <span class="date">7-13</span>
                      <span class="num-week">S10</span>
                    </div><div class="box">
                      <span class="date">14-20</span>
                      <span class="num-week">S11</span>
                    </div><div class="box">
                      <span class="date">21-27</span>
                      <span class="num-week">S12</span>
                    </div><div class="box">
                      <span class="date">28-3</span>
                      <span class="num-week">S13</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="content-agenda jcf-scrollable">
            <!-- Line Event(Challenges) -->
            <div class="row-event challenges">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Challenges</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event1" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event2" data-placement="top" tabindex="0" class="link-events">1</a>
                            <a id="event2-tablet" data-placement="left" tabindex="0" class="link-events">1</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                                                  <li><a href="#">Retours et destructions P&amp;C</a></li>
                                                                </ul></div>
                              </div>
                            </div>
                            <span data-target="event2" class="trigger-event"></span>
                            <span data-target="event2-tablet" class="trigger-event-tablet"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event2" class="trigger-event"></span>
                            <span data-target="event2-tablet" class="trigger-event-tablet"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event2" class="trigger-event"></span>
                            <span data-target="event2-tablet" class="trigger-event-tablet"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event3" data-placement="left" tabindex="0" class="link-events">1</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event3" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event3" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event4" data-placement="top" tabindex="0" class="link-events">1</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">HPP Toblerone</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event4" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event4" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event4" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event5" data-placement="left" tabindex="0" class="link-events">4</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event5" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event5" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event5" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before"></div>
                          <div class="event end-event">
                            <span data-target="event5" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event6" data-placement="top" tabindex="0" class="link-events">2</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">6</a></li>
                                    <li><a href="#">7</a></li>
                                    <li><a href="#">8</a></li>
                                    <li><a href="#">9</a></li>
                                    <li><a href="#">10</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Line Event (animations) -->
            <div class="row-event animations">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-label"></span> Animations</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="more-after"></div>
                          <div class="event start-event end-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">2</a>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">1</a>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">2</a>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event start-event end-event-tablet">
                            <a class="link-events" href="#">3</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event start-event end-event-tablet">
                            <a class="link-events" href="#">3</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Line Event (Promotions) -->
            <div class="row-event promotions">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Promotions</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event11" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event15" data-placement="left" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event15" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-event promotions">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Promotions</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event11" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event15" data-placement="left" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event15" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-event promotions">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Promotions</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event11" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event15" data-placement="left" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event15" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-event promotions">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Promotions</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event11" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event15" data-placement="left" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event15" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-event promotions">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Promotions</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event11" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event15" data-placement="left" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event15" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-event promotions">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Promotions</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event11" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event15" data-placement="left" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event15" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-event promotions">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Promotions</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event11" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event15" data-placement="left" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event15" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-event promotions">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Promotions</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event11" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event15" data-placement="left" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event15" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-event promotions">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Promotions</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event11" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event15" data-placement="left" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event15" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-event promotions">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Promotions</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event11" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event15" data-placement="left" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event15" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row-event promotions">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Promotions</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a class="link-events" href="#">4</a>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event"></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event11" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event11" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event15" data-placement="left" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event15" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event"><span data-target="event15" class="trigger-event"></span></div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-event challenges">
              <div class="left-box same-height">
                <div class="event-name">
                  <span><span class="ico icon-flag"></span> Challenges</span>
                </div>
              </div>
              <div class="right-box same-height">
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event1" data-placement="top" tabindex="0" class="link-events">3</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event1" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event2" data-placement="top" tabindex="0" class="link-events">1</a>
                            <a id="event2-tablet" data-placement="left" tabindex="0" class="link-events">1</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                                                  <li><a href="#">Retours et destructions P&amp;C</a></li>
                                                                </ul></div>
                              </div>
                            </div>
                            <span data-target="event2" class="trigger-event"></span>
                            <span data-target="event2-tablet" class="trigger-event-tablet"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event2" class="trigger-event"></span>
                            <span data-target="event2-tablet" class="trigger-event-tablet"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event2" class="trigger-event"></span>
                            <span data-target="event2-tablet" class="trigger-event-tablet"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event3" data-placement="left" tabindex="0" class="link-events">1</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C 1</a></li>
                                    <li><a href="#">HPP Toblerone 2</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 3</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 4</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 5</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 6</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 7</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 8</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 9</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 10</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 11</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 12</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C 13</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event3" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event3" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event4" data-placement="top" tabindex="0" class="link-events">1</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">HPP Toblerone</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event4" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event4" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event4" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event5" data-placement="left" tabindex="0" class="link-events">4</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">Retours et destructions P&amp;C</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event5" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event5" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event5" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before"></div>
                          <div class="event end-event">
                            <span data-target="event5" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line">
                  <div class="block"> <!-- Janvier 2016 -->
                    <div class="week">
                      <div class="box">
                      </div><div class="box current">
                      </div><div class="box">
                      </div><div class="box">
                        <div class="frame">
                          <div class="event start-event">
                            <a id="event6" data-placement="top" tabindex="0" class="link-events">2</a>
                            <div class="event-popup hide">
                              <div class="title">
                                26 JAN <span class="icon-arrow-next"></span> 10 MAR
                              </div>
                              <div class="content">
                                <div class="jcf-scrollable">
                                  <ul>
                                    <li><a href="#">Retours et destructions P&amp;C</a></li>
                                    <li><a href="#">HPP Toblerone</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">6</a></li>
                                    <li><a href="#">7</a></li>
                                    <li><a href="#">8</a></li>
                                    <li><a href="#">9</a></li>
                                    <li><a href="#">10</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Février 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="event">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                        <div class="frame">
                          <div class="more-before-tablet"></div>
                          <div class="event end-event-tablet">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><div class="block"> <!-- Mars 2016 -->
                    <div class="week">
                      <div class="box">
                        <div class="frame">
                          <div class="event end-event">
                            <span data-target="event6" class="trigger-event"></span>
                          </div>
                        </div>
                      </div><div class="box">
                      </div><div class="box">
                      </div><div class="box">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
</html>
