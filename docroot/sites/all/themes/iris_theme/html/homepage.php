<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <?php include "inc/head.php"; ?>
</head>
<body>
  <div class="wrapper">
    <div class="overlay"></div>
    <header>
      <?php include "inc/navigation.php"; ?>
      <?php include "inc/header.php"; ?>
    </header>
    <section id="main">
      <aside id="sidebar" class="sidebar">
        <div class="wrapp-aside">
          <div class="aside-open-box"></div>
          <div class="aside-scroll jcf-scrollable">
            <div class="side-holder">
              <div class="aside-search">
                <form action="#">
                  <div>
                    <input type="text" placeholder="Recherche…">
                    <button type="submit"><span class="ico icon-search"></span></button>
                  </div>
                </form>
              </div>
              <?php include "inc/aside-bloks.php"; ?>
            </div>
          </div>
        </div>
      </aside>
      <div id="content">
        <div class="home-news">
          <div class="news-list-container">
            <div class="news-title-nav">
              <ul class="news-nav">
                <li class="active"><a href="#"><span class="ico icon-list"></span> VUE STANDARD</a></li>
                <li><a href="#"><span class="ico icon-calendar"></span> VUE AGENDA</a></li>
              </ul>
            </div>
            <div class="news-list-scroll jcf-scrollable">
              <ul class="news-list">
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status new"></span>
                      <span class="txt">
                        <span class="title">Animation HPP Armani</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 1h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 3h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 7h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Animation HPP Armani</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 5j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 6j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Animation HPP Armani</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 5j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 7j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 6j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="news-content">
            <div class="news-content-scroll jcf-scrollable">
              <div class="news-content-frame">
                <div class="home-holder">
                  <div class="head">
                    <img src="images/img-02.png" alt="">
                    <div class="txt">
                      <h2>Bienvenue,</h2>
                      <div class="frame">
                        <span class="name">Charline Michu</span>
                        <span class="date">Mardi 2 Décembre 2015, 17h40.</span>
                      </div>
                    </div>
                  </div>
                  <div class="store-row">
                    <ul class="store">
                      <li>
                        <div class="head-store">
                          <span>BOUTIQUE</span>
                          <strong>LSM3</strong>
                        </div>
                        <a href="#"><div class="frame">
                          <div class="ico"><span class="ico icon-info"></span></div>
                          <div class="text">
                            <strong>5</strong>
                            <span>news non-lues</span>
                          </div>
                        </div></a>
                      </li>
                      <li>
                        <div class="head-store">
                          <span>BOUTIQUE</span>
                          <strong>LSM2</strong>
                        </div>
                        <a href="#"><div class="frame">
                          <div class="ico"><span class="ico icon-info"></span></div>
                          <div class="text">
                            <strong>2</strong>
                            <span>news non-lues</span>
                          </div>
                        </div></a>
                      </li>
                      <li>
                        <div class="head-store">
                          <span>BOUTIQUE</span>
                          <strong>LSM5</strong>
                        </div>
                        <a href="#"><div class="frame">
                          <div class="ico"><span class="ico icon-info"></span></div>
                          <div class="text">
                            <strong>1</strong>
                            <span>news non-lues</span>
                          </div>
                        </div></a>
                      </li>
                    </ul>
                  </div>
                  <div class="store-row">
                    <ul class="store">
                      <li>
                        <div class="head-store">
                          <span>BOUTIQUE</span>
                          <strong>LSM4</strong>
                        </div>
                        <a href="#"><div class="frame">
                          <div class="ico"><span class="ico icon-info"></span></div>
                          <div class="text">
                            <strong>3</strong>
                            <span>news non-lues</span>
                          </div>
                        </div></a>
                      </li>
                      <li>
                        <div class="head-store">
                          <span>BOUTIQUE</span>
                          <strong>LSM6</strong>
                        </div>
                        <a href="#"><div class="frame">
                          <div class="ico"><span class="ico icon-info"></span></div>
                          <div class="text">
                            <strong>1</strong>
                            <span>news non-lues</span>
                          </div>
                        </div></a>
                      </li>
                      <li>
                        <div class="head-store">
                          <span>BOUTIQUE</span>
                          <strong>LSM8</strong>
                        </div>
                        <a href="#"><div class="frame">
                          <div class="ico"><span class="ico icon-info"></span></div>
                          <div class="text">
                            <strong>2</strong>
                            <span>news non-lues</span>
                          </div>
                        </div></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
</html>
