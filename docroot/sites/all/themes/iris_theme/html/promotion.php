<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <?php include "inc/head.php"; ?>
</head>
<body>
  <div class="wrapper">
    <div class="overlay"></div>
    <header>
      <?php include "inc/navigation.php"; ?>
      <?php include "inc/header.php"; ?>
    </header>
    <section id="main">
      <aside id="sidebar" class="sidebar">
        <div class="wrapp-aside">
          <div class="aside-scroll jcf-scrollable">
            <div class="side-holder">
              <div class="aside-search">
                <form action="#">
                  <div>
                    <input type="text" placeholder="Recherche…">
                    <button type="submit"><span class="ico icon-search"></span></button>
                  </div>
                </form>
              </div>
              <?php include "inc/aside-bloks.php"; ?>
            </div>
          </div>
        </div>
      </aside>
      <div id="content">
        <div class="home-news promotion">
          <div class="news-list-container">
            <div class="news-title-nav">
              <ul class="news-nav">
                <li class="active"><a href="#"><span class="ico icon-list"></span> VUE STANDARD</a></li>
                <li><a href="#"><span class="ico icon-calendar"></span> VUE AGENDA</a></li>
              </ul>
            </div>
            <div class="news-list-scroll jcf-scrollable">
              <ul class="news-list">
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status new"></span>
                      <span class="txt">
                        <span class="title">HPP Dior</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 1h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="promo-link start">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 3h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="promo-link">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 3h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="promo-link end">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 3h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 3h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">HPP Toblerone</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Animation HPP Armani</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 5j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 6j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Animation HPP Armani</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 5j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 7j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 6j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="news-content">
            <div class="news-content-scroll jcf-scrollable">
              <div class="news-content-frame">
                <a href="#" class="close-news icon-close"></a>
                <a class="favorite-link" href="#"><span class="ico icon-star"></span></a>
                <div class="title">
                  <h2>HPP Dior</h2>
                  <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                </div>
                <div class="three-boxes">
                  <div class="box">
                    <h4>NOM DE L'OPÉRATION</h4>
                    <ul>
                      <li>AT1612-1</li>
                    </ul>
                  </div>
                  <div class="box">
                    <h4>SECTEUR D’ACTIVITÉ</h4>
                    <ul>
                      <li>Confiserie</li>
                    </ul>
                  </div>
                  <div class="box">
                    <h4>SLOT</h4>
                    <ul>
                      <li>Warsaw</li>
                    </ul>
                  </div>
                </div>
                <div class="img-block">
                  <img src="images/img-02.jpg" alt="">
                </div>
                <div class="linked-store">
                  <span class="ttl">BOUTIQUES CONCERNÉES</span>
                  <ul>
                    <li><a href="#">LSM3</a></li>
                    <li><a href="#">LSM4</a></li>
                    <li><a href="#">LSM5</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="g-hidden">
    <div class="popup" id="popup1">
      <h3>Passer en état "Terminé"?</h3>
      <p>Ceci est un sous-texte de complément.</p>
      <div class="btn-line">
        <a class="yes" href="#"><span class="ico-yes"></span> Oui</a>
        <a class="not close-popup" href="#"><span class="ico-not"></span> Non</a>
      </div>
    </div>
  </div>
</body>
</html>
