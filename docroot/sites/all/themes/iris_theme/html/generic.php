<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <?php include "inc/head.php"; ?>
</head>
<body>
  <div class="wrapper">
    <div class="overlay"></div>
    <header>
      <?php include "inc/navigation.php"; ?>
      <?php include "inc/header.php"; ?>
    </header>
    <section id="main">
      <aside id="sidebar" class="sidebar">
        <div class="wrapp-aside">
          <div class="aside-open-box"></div>
          <div class="aside-scroll jcf-scrollable">
            <div class="side-holder">
              <div class="aside-search">
                <form action="#">
                  <div>
                    <input type="text" placeholder="Recherche…">
                    <button type="submit"><span class="ico icon-search"></span></button>
                  </div>
                </form>
              </div>
              <div class="aside-box opened no-visible-tablet-bar">
                <div class="title">
                  <a href="#">TYPE DE PAGE</a>
                </div>
                <div class="slide">
                  <ul class="aside-list">
                    <li class="active"><a href="#"><span class="txt">Tous</span></a></li>
                    <li><a href="#"><span class="txt">Augmentation des prix</span></a></li>
                    <li><a href="#"><span class="txt">Sécurité alimentaire</span></a></li>
                    <li><a href="#"><span class="txt">Ressource</span></a></li>
                    <li><a href="#"><span class="txt">Plan de boutiques</span></a></li>
                    <li><a href="#"><span class="txt">Mentions légales</span></a></li>
                    <li><a href="#"><span class="txt">Planning des vacances</span></a></li>
                  </ul>
                </div>
              </div>
              <div class="aside-box opened">
                <div class="title">
                  <a href="#">BOUTIQUES</a>
                </div>
                <div class="slide">
                  <div class="two-list">
                    <ul class="aside-list">
                      <li class="active"><a href="#"><span class="txt">LSM3</span></a></li>
                      <li><a href="#"><span class="txt">LSM4</span></a></li>
                      <li><a href="#"><span class="txt">LSM5</span></a></li>
                    </ul>
                    <ul class="aside-list">
                      <li><a href="#"><span class="txt">LSM5</span></a></li>
                      <li><a href="#"><span class="txt">LSM5</span></a></li>
                      <li><a href="#"><span class="txt">LSM5</span></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </aside>
      <div id="content">
        <div class="home-news generic">
          <div class="news-list-container">
            <div class="news-list-scroll jcf-scrollable">
              <ul class="news-list">
                <li>
                  <a href="#">
                    <span class="holder">
                      <span class="txt">
                        <span class="title">Sécurité alimentaire M&amp;Ms…</span>
                        <span class="sub-txt">Sécurité alimentaire</span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="holder">
                      <span class="txt">
                        <span class="title">Guide de l’utilisateur IRIS</span>
                        <span class="sub-txt">Ressource</span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="active">
                  <a href="#">
                    <span class="holder">
                      <span class="txt">
                        <span class="title">Augmentation des prix 2016</span>
                        <span class="sub-txt">Augmentation des prix</span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="holder">
                      <span class="txt">
                        <span class="title">Planning des formations IRIS</span>
                        <span class="sub-txt">Ressource</span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="holder">
                      <span class="txt">
                        <span class="title">Plan des boutiques Roissy CDG</span>
                        <span class="sub-txt">Plan de boutique</span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="holder">
                      <span class="txt">
                        <span class="title">Portail Lagardère Travel Retail</span>
                        <span class="sub-txt">Ressource</span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="holder">
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="sub-txt">Animation</span>
                      </span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="news-content">
            <div class="news-content-scroll jcf-scrollable">
              <div class="news-content-frame">
                <div class="title">
                  <h2>Augmentation des prix 2016</h2>
                </div>
                <div class="links-block">
                  <div class="col">
                    <h4>Documents</h4>
                    <ul class="links-list">
                      <li><a href="#">
                        <span class="ico"><span class="pdf"></span></span>
                        <span class="txt">HPPToblerone22121801.pdf</span>
                      </a></li>
                      <li><a href="#">
                        <span class="ico"><span class="doc"></span></span>
                        <span class="txt">Review-HPPToblerone-22121801.doc</span>
                      </a></li>
                      <li><a href="#">
                        <span class="ico"><span class="ppt"></span></span>
                        <span class="txt">HPPToblerone22121801.pttx</span>
                      </a></li>
                    </ul>
                  </div>
                  <div class="col">
                    <h4>Liens</h4>
                    <ul class="links-list">
                      <li><a href="#">
                        <span class="ico"><span class="filelink"></span></span>
                        <span class="txt">Exemples de HPP</span>
                      </a></li>
                      <li><a href="#">
                        <span class="ico"><span class="filelink"></span></span>
                        <span class="txt">HPP Toblerone wiki</span>
                      </a></li>
                    </ul>
                  </div>
                </div>
                <div class="comment-box">
                  <div class="comment-row">
                    <div class="photo">
                      <div class="img">
                        <img src="images/photo-01.png" alt="">
                      </div>
                    </div>
                    <div class="txt">
                      <div class="head">
                        <span class="name">Paul Andriatafika</span>
                        <span class="date">15 décembre 2015</span>
                      </div>
                      <span class="author">Auteur</span>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nisi velit, efficitur vitae ipsum eu, ullamcorper mollis lacus. Maecenas purus enim, euismod eu placerat quis, vehicula sed lacus. Pellentesque tristique feugiat nulla, in viverra felis auctor sit amet. Phasellus leo ex, varius sed finibus at, sollicitudin posuere odio. In hac habitasse platea dictumst. Integer tempus nulla ut elit dapibus, ac luctus sem hendrerit. Morbi vehicula diam mauris. Mauris velit neque, blandit a ultricies id, auctor a dolor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vel dapibus massa, vitae tempus arcu. Duis sed cursus dui. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                    </div>
                  </div>
                </div>
                <div class="linked-store">
                  <span class="ttl">BOUTIQUES CONCERNÉES</span>
                  <ul>
                    <li><a href="#">LSM3</a></li>
                    <li><a href="#">LSM4</a></li>
                    <li><a href="#">LSM5</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
</html>
