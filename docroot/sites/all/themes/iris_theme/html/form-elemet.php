<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <?php include "inc/head.php"; ?>
</head>
<body>
  <div class="wrapper">
    <div class="overlay"></div>
    <header>
      <?php include "inc/navigation.php"; ?>
      <?php include "inc/header.php"; ?>
    </header>
    <section id="main">
      <aside id="sidebar" class="sidebar">
        <div class="wrapp-aside">
          <div class="aside-open-box"></div>
          <div class="aside-scroll jcf-scrollable">
            <div class="side-holder">
              <div class="aside-search">
                <form action="#">
                  <div>
                    <input type="text" placeholder="Recherche…">
                    <button type="submit"><span class="ico icon-search"></span></button>
                  </div>
                </form>
              </div>
              <?php include "inc/aside-bloks.php"; ?>
            </div>
          </div>
        </div>
      </aside>
      <div id="content">
        <div class="home-news">
          <div class="news-list-container">
            <div class="news-title-nav">
              <ul class="news-nav">
                <li class="active"><a href="#"><span class="ico icon-list"></span> VUE STANDARD</a></li>
                <li><a href="#"><span class="ico icon-calendar"></span> VUE AGENDA</a></li>
              </ul>
            </div>
            <div class="news-list-scroll jcf-scrollable">
              <ul class="news-list">
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status new"></span>
                      <span class="txt">
                        <span class="title">Animation HPP Armani</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 1h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 3h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 7h</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Animation HPP Armani</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 5j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 6j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color1">À VENIR</span>
                      <span class="date">18 NOV <span class="icon-arrow-next"></span> 24 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Animation HPP Armani</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 5j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl color2">EN CE MOMENT</span>
                      <span class="date"><span class="icon-arrow-next"></span> 22 NOV</span>
                    </span>
                    <span class="holder">
                      <span class="status in-progress"></span>
                      <span class="txt">
                        <span class="title">Retours et destructions P&amp;C</span>
                        <span class="info">
                          Animation
                          <span class="time"><span class="ico icon-time"></span> 7j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 6j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
                <li class="completed">
                  <a href="#">
                    <span class="head">
                      <span class="sub-ttl">TERMINÉ</span>
                    </span>
                    <span class="holder">
                      <span class="status dealt"></span>
                      <span class="txt">
                        <span class="title">Dior prix de vente corrigé</span>
                        <span class="info">
                          Challenges
                          <span class="time"><span class="ico icon-time"></span> 2j</span>
                        </span>
                      </span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="news-content">
            <div class="news-content-scroll jcf-scrollable">
              <div class="news-content-frame">
                <h2>Form elememt</h2>
                <div class="form">
                  <form action="#">
                    <div>
                      <div class="block">
                        <div class="row-form">
                          <input type="text" placeholder="Placeholder" disabled="disabled">
                        </div>
                        <div class="row-form">
                          <input class="form-text" type="password" placeholder="Password">
                        </div>
                        <div class="row-form">
                          <input type="text" placeholder="Placeholder">
                        </div>
                        <div class="row-form">
                          <input class="error" type="text" placeholder="">
                        </div>
                      </div>
                      <div class="block">
                        <div class="row-form">
                          <div class="w-sel">
                            <select class="default-custom-select" disabled>
                              <option class="placeholder">Placeholder</option>
                              <option>Option 1</option>
                              <option>Option 2</option>
                              <option>Option 3</option>
                              <option>Option 4</option>
                            </select>
                          </div>
                        </div>
                        <div class="row-form">
                          <div class="w-sel">
                            <select class="default-custom-select">
                              <option class="placeholder">Placeholder</option>
                              <option>Option 1</option>
                              <option>Option 2</option>
                              <option>Option 3</option>
                              <option>Option 4</option>
                            </select>
                          </div>
                        </div>
                        <div class="row-form">
                          <div class="w-sel">
                            <select class="default-custom-select">
                              <option>Option 1</option>
                              <option>Option 2</option>
                              <option>Option 3</option>
                              <option>Option 4</option>
                            </select>
                          </div>
                        </div>
                        <div class="row-form">
                          <div class="w-sel">
                            <select class="default-custom-select error">
                              <option class="placeholder">Placeholder</option>
                              <option>Option 1</option>
                              <option>Option 2</option>
                              <option>Option 3</option>
                              <option>Option 4</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="block">
                        <div class="row-form">
                          <ul class="choose-list">
                            <li>
                              <input class="custom-checkbox" type="checkbox">
                              <label>A normal checkbox (Label)</label>
                            </li>
                            <li>
                              <input class="custom-checkbox" type="checkbox" checked>
                              <label>A checked checkbox</label>
                            </li>
                            <li>
                              <input class="custom-checkbox" type="checkbox" checked disabled>
                              <label>A disabled checked checkbox</label>
                            </li>
                            <li>
                              <input class="custom-checkbox" type="checkbox" disabled>
                              <label>A disabled checkbox</label>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="block">
                        <div class="row-form">
                          <ul class="choose-list">
                            <li>
                              <input class="custom-radio" type="radio" name="rd1">
                              <label>A normal radio (Label)</label>
                            </li>
                            <li>
                              <input class="custom-radio" type="radio" name="rd1" checked>
                              <label>A checked radio</label>
                            </li>
                            <li>
                              <input class="custom-radio" type="radio" name="rd2" checked disabled>
                              <label>A disabled checked radio</label>
                            </li>
                            <li>
                              <input class="custom-radio" type="radio" name="rd2" disabled>
                              <label>A disabled radio</label>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="block">
                        <div class="row-form">
                          <button type="submit">Button</button>
                        </div>
                        <div class="row-form">
                          <input type="submit" value="Button">
                        </div>
                        <div class="row-form">
                          <input type="reset" value="Reset">
                        </div>
                        <div class="row-form">
                          <button type="submit"><span class="ico-yes"></span> Button</button>
                        </div>
                        <div class="row-form">
                          <button type="submit"><span class="ico-not"></span> Button</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
</html>
