<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <?php include "inc/head.php"; ?>
</head>
<body>
  <div class="wrapper">
    <div class="overlay"></div>
    <header>
      <?php include "inc/navigation.php"; ?>
      <?php include "inc/header.php"; ?>
    </header>
    <section id="main">
      <aside id="sidebar" class="sidebar">
        <div class="wrapp-aside">
          <div class="aside-open-box"></div>
          <div class="aside-scroll jcf-scrollable">
            <div class="side-holder">
              <div class="aside-search">
                <form action="#">
                  <div>
                    <input type="text" placeholder="Recherche…">
                    <button type="submit"><span class="ico icon-search"></span></button>
                  </div>
                </form>
              </div>
              <div class="aside-box opened no-visible-tablet-bar">
                <div class="title">
                  <a href="#">TYPE DE PAGE</a>
                </div>
                <div class="slide">
                  <ul class="aside-list">
                    <li><a href="#"><span class="txt">Tous</span></a></li>
                    <li><a href="#"><span class="txt">Mode</span></a></li>
                    <li><a href="#"><span class="txt">Perfume/Cosmetics</span></a></li>
                    <li class="active"><a href="#"><span class="txt">Alcohol</span></a></li>
                    <li><a href="#"><span class="txt">Gastronomy</span></a></li>
                    <li><a href="#"><span class="txt">Tabacco</span></a></li>
                    <li><a href="#"><span class="txt">PVS/Virgin</span></a></li>
                    <li><a href="#"><span class="txt">Travel</span></a></li>
                    <li><a href="#"><span class="txt">Art of living</span></a></li>
                    <li><a href="#"><span class="txt">Miscellaneous</span></a></li>
                  </ul>
                </div>
              </div>
              <div class="aside-box opened">
                <div class="title">
                  <a href="#">BOUTIQUES</a>
                </div>
                <div class="slide">
                  <div class="two-list">
                    <ul class="aside-list">
                      <li class="active"><a href="#"><span class="txt">LSM3</span></a></li>
                      <li><a href="#"><span class="txt">LSM4</span></a></li>
                      <li><a href="#"><span class="txt">LSM5</span></a></li>
                    </ul>
                    <ul class="aside-list">
                      <li><a href="#"><span class="txt">LSM5</span></a></li>
                      <li><a href="#"><span class="txt">LSM5</span></a></li>
                      <li><a href="#"><span class="txt">LSM5</span></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </aside>
      <div id="content">
        <div class="home-news merchandising">
          <div class="news-list-container">
            <div class="news-list-scroll jcf-scrollable">
              <ul class="news-list">
                <li class="active">
                  <a href="#">
                    <span class="holder">
                      <span class="pic">
                        <img src="images/img-04.jpg" alt="">
                      </span>
                      <span class="txt">
                        <span class="title">Whisky</span>
                        <span class="sub-txt">Alcohol <span class="time">&nbsp;</span></span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="holder">
                      <span class="pic">
                        <img src="images/img-05.jpg" alt="">
                      </span>
                      <span class="txt">
                        <span class="title">Cognac</span>
                        <span class="sub-txt">Alcohol <span class="time"><span class="ico icon-time"></span> 1h</span></span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="holder">
                      <span class="pic">
                        <img src="images/img-06.jpg" alt="">
                      </span>
                      <span class="txt">
                        <span class="title">White alcohol</span>
                        <span class="sub-txt">Alcohol <span class="time"><span class="ico icon-time"></span> 1h</span></span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="holder">
                      <span class="pic">
                        <img src="images/img-07.jpg" alt="">
                      </span>
                      <span class="txt">
                        <span class="title">Aperitif</span>
                        <span class="sub-txt">Alcohol <span class="time"><span class="ico icon-time"></span> 1h</span></span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="holder">
                      <span class="pic">
                        <img src="images/img-08.jpg" alt="">
                      </span>
                      <span class="txt">
                        <span class="title">Champagne</span>
                        <span class="sub-txt">Alcohol <span class="time"><span class="ico icon-time"></span> 1h</span></span>
                      </span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="holder">
                      <span class="pic">
                        <img src="images/img-09.jpg" alt="">
                      </span>
                      <span class="txt">
                        <span class="title">Vin</span>
                        <span class="sub-txt">Alcohol <span class="time"><span class="ico icon-time"></span> 1h</span></span>
                      </span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="news-content">
            <div class="news-content-scroll jcf-scrollable">
              <div class="news-content-frame">
                <div class="title">
                  <h2>Whisky</h2>
                </div>
                <div class="img-block">
                  <img src="images/img-03.jpg" alt="">
                </div>
                <div class="links-block">
                  <div class="col">
                    <h4>Documents</h4>
                    <ul class="links-list">
                      <li><a href="#">
                        <span class="ico"><span class="pdf"></span></span>
                        <span class="txt"><span title="Whisky Core">Whisky Core</span> <span class="time"><span class="ico icon-time"></span> 1h</span></span>
                      </a></li>
                      <li><a href="#">
                        <span class="ico"><span class="doc"></span></span>
                        <span class="txt"><span title="Whisky Core template Whisky Core template Whisky Core template">Whisky Core template Whisky Core template Whisky Core template</span> <span class="time"><span class="ico icon-time"></span> 23/05/2016</span></span>
                      </a></li>
                      <li><a href="#">
                        <span class="ico"><span class="pdf"></span></span>
                        <span class="txt"><span title="RP Whiskies In/Out">RP Whiskies In/Out</span> <span class="time"><span class="ico icon-time"></span> 1h</span></span>
                      </a></li>
                    </ul>
                  </div>
                  <div class="col">
                    <h4>Liens</h4>
                    <ul class="links-list">
                      <li><a href="#">
                        <span class="ico"><span class="filelink"></span></span>
                        <span class="txt"><span>Lien 1</span></span>
                      </a></li>
                      <li><a href="#">
                        <span class="ico"><span class="filelink"></span></span>
                        <span class="txt"><span>Lien 2</span></span>
                      </a></li>
                      <li><a href="#">
                        <span class="ico"><span class="filelink"></span></span>
                        <span class="txt"><span>Lien 3</span></span>
                      </a></li>
                    </ul>
                  </div>
                </div>
                <div class="linked-store">
                  <span class="ttl">BOUTIQUES CONCERNÉES</span>
                  <ul>
                    <li><a href="#">LSM3</a></li>
                    <li><a href="#">LSM4</a></li>
                    <li><a href="#">LSM5</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
</html>
