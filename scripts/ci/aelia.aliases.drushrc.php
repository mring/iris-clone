<?php
// Site CComptes, environment dev.
$aliases['dev'] = array(
  'root' => '/var/www/dev/docroot/',
  'uri' => 'dev.aelia-iris.adyax-dev.com',
  'path-aliases' => array(
    '%drush-script' => '/usr/bin/drush',
  ),
);
