#!/bin/bash
set -e # We want to fail at each command, to stop execution

cd /var/www/stage/
git checkout master
git pull
export COMPOSER_DISCARD_CHANGES=true
composer install --no-dev
cd docroot
drush sql-dump --gzip > /var/www/_backups/db/stage`date +%Y-%m-%d-%H%M`.sql.gz
drush cr
drush cim -y
drush updb -y
drush entity-updates -y
