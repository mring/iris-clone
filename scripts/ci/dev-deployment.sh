#!/bin/bash
set -e # We want to fail at each command, to stop execution

cd /var/www/dev/
# Make DB Backup.
cd docroot
drush sql-dump --gzip > /var/www/_backups/db/`date +%Y-%m-%d-%H%M`.sql.gz
find /var/www/_backups/db/* -mtime +5 -exec rm {} \;
cd ..
# Continue with deploy.
# Reset manual changes just in case.
git reset --hard
# Update code.
git checkout develop
git pull
export COMPOSER_DISCARD_CHANGES=true
composer install
# Do post-code-update procedures.
cd docroot
drush cr
drush cim -y
drush updb -y
drush entity-updates -y
varnishadm -T localhost:6082 -S /etc/varnish/secret "ban req.http.host == dev.aelia-iris.adyax-dev.com"
