#!/bin/sh

### Create Codesniffer symlink ###
# Target file.
TARGET=../../../../drupal/coder/coder_sniffer/Drupal
TARGET_DP=../../../../drupal/coder/coder_sniffer/DrupalPractice
# Link name.
LINK_NAME=vendor/squizlabs/php_codesniffer/CodeSniffer/Standards/Drupal
LINK_NAME_DP=vendor/squizlabs/php_codesniffer/CodeSniffer/Standards/DrupalPractice
# Link folder.
LINK_FOLDER=vendor/squizlabs/php_codesniffer

if [ -d "$LINK_FOLDER" ]; then
  # Create symlink Drupal standard.
  ln -sf ${TARGET} ${LINK_NAME}

  # Creates symlink to DrupalPractice.
  ln -sf ${TARGET_DP} ${LINK_NAME_DP}

  # Pre commit hook
  cp scripts/tools/pre-commit .git/hooks/pre-commit
  # Make files executable.
  chmod +x .git/hooks/pre-commit
fi
### End Create symlink ####
