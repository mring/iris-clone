/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        title: '#edit-title',
        searchFld: '#edit-query',
        searchBtn: '.ico.icon-search',
        searchResult: '.search-result',
        searchResultTitle: '#content-item>.title'
    },

    // element getters

    searchByKey(searchKey){

        I.say('I fill Search field by Search Key and click on Search button');

        I.fillField(this.elements.searchFld, searchKey);
        I.click(this.elements.searchBtn);

        I.say('I check Search Key in the Search result Title');

        I.waitForElement(this.elements.searchResult, 10);
        I.see(searchKey, this.elements.searchResultTitle);

    }
};
