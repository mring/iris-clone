/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        pageHeader: 'h1.page-title',
        selectVocabulary: '#edit-taxonomies-list',
        createATaxonomyTermBtn: '#edit-submit',
        taxonomySlct: '#edit-vid',
        nameFld: '#edit-name',
        applyBtn:'#edit-submit-taxonomies-dashboard',
        resetBtn:'#edit-reset',
        taxonomiesTable:'table.views-table.cols-5'
    },


    // element getters

    seeAllRequiredElementsExist(){

        I.say('I check that Taxonomies dashboard title exists');
        I.seeElement(this.elements.pageHeader);

        I.say('I check that Taxonomies dashboard Select vocabulary select exists');
        I.seeElement(this.elements.selectVocabulary);

        I.say('I check that Taxonomies dashboard Create a taxonomy term button exists');
        I.seeElement(this.elements.createATaxonomyTermBtn);

        I.say('I check that Taxonomies dashboard Taxonomy select exists');
        I.seeElement(this.elements.taxonomySlct);

        I.say('I check that Taxonomies dashboard Name field exists');
        I.seeElement(this.elements.nameFld);

        I.say('I check that Taxonomies dashboard Apply button exists');
        I.seeElement(this.elements.applyBtn);

        I.say('I check that Taxonomies dashboard Reset button exists');
        I.seeElement(this.elements.resetBtn);

        I.say('I check that Taxonomies dashboard table exists');
        I.seeElement(this.elements.taxonomiesTable);

        // I.saveScreenshot('promotionrules.png',true);
    }
};