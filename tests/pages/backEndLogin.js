
'use strict';

let I;

module.exports = {

  _init() {
    I = require('../steps_file.js')();
  },

    // locators here

  loginForm: "#user-login",

    fields: {
        login: '#edit-name',
        password: '#edit-pass'
    },
    submitButton: {
        id: '#edit-submit'
    },

  // methods here

    sendLoginForm(login, password){

      within(this.loginForm, () => {
          I.fillField(this.fields.login, login);
          I.fillField(this.fields.password, password);
          I.click(this.submitButton.id);
          // I.saveScreenshot('login.png',true);
      });

    }
};
