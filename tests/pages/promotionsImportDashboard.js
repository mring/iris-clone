/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        pageHeader: 'h1.page-title',
        importFileFld: '#edit-upload-file',
        uploadBtn: '#edit-submit',
        importIdFld: '#edit-id',
        beginDate: '#edit-date-filter-min',
        endDate: '#edit-date-filter-max',
        authorFld: '#edit-combine',
        emailFld: '#edit-uid',
        status: '#edit-status',
        applyBtn:'#edit-submit-promotions-import',
        resetBtn:'#edit-reset',
        promotionsCanalTable:'table.views-table.cols-6'
    },


    // element getters

    seeAllRequiredElementsExist(){

        I.say('I check that Promotions canal dashboard title exists');
        I.seeElement(this.elements.pageHeader);

        I.say('I check that Promotions canal dashboard Import file field exists');
        I.seeElement(this.elements.importFileFld);

        I.say('I check that Promotions canal dashboard Upload button exists');
        I.seeElement(this.elements.uploadBtn);

        I.say('I check that Promotions canal dashboard Import ID field exists');
        I.seeElement(this.elements.importIdFld);

        I.say('I check that Promotions canal dashboard Begin date field exists');
        I.seeElement(this.elements.beginDate);

        I.say('I check that Promotions canal dashboard End date field exists');
        I.seeElement(this.elements.endDate);

        I.say('I check that Promotions canal dashboard Author field exists');
        I.seeElement(this.elements.authorFld);

        I.say('I check that Promotions canal dashboard E-main field exists');
        I.seeElement(this.elements.emailFld);

        I.say('I check that Promotions canal dashboard Status select exists');
        I.seeElement(this.elements.status);

        I.say('I check that Promotions canal dashboard Apply button exists');
        I.seeElement(this.elements.applyBtn);

        I.say('I check that Promotions canal dashboard Reset button exists');
        I.seeElement(this.elements.resetBtn);

        I.say('I check that Promotions canal dashboard table exists');
        I.seeElement(this.elements.promotionsCanalTable);

        // I.saveScreenshot('promotionrules.png',true);
    }
};