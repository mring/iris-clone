/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        pageHeader: 'h1.page-title',
        searchFld: '#edit-name',
        applyBtn:'#edit-submit-promotion-rules-dashboard',
        resetBtn:'#edit-reset',
        promotionRulesTable:'table.views-table.cols-5',
        tableFirstRowFirstColumn:'.views-row-first>.views-field.views-field-name'
    },


    // element getters

    seeAllRequiredElementsExist(){

        I.say('I check that promotion rules dashboard title exists');
        I.seeElement(this.elements.pageHeader);

        I.say('I check that promotion rules dashboard search field exists');
        I.seeElement(this.elements.searchFld);

        I.say('I check that promotion rules dashboard apply button exists');
        I.seeElement(this.elements.applyBtn);

        I.say('I check that promotion rules dashboard reset button exists');
        I.seeElement(this.elements.resetBtn);

        I.say('I check that promotion rules dashboard promotion rules table exists');
        I.seeElement(this.elements.promotionRulesTable);

        // I.saveScreenshot('promotionrules.png',true);
    },
    checkPromotionRulesDashboardSearch(searchKey){

        I.say('I fill Search field by: '+ searchKey);
        I.fillField(this.elements.searchFld, searchKey);

        I.say('I click on an Apply button exists');
        I.click(this.elements.applyBtn);

        I.say('I check that promotion rules dashboard promotion rules table first row contain: '+ searchKey);
        I.see(searchKey, this.elements.tableFirstRowFirstColumn);


    }
};