/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        pageHeader: 'h1.page-title',
        requestExportType: '#edit-request-export-type',
        operationNumberFld: '#edit-field-operation-number-value',
        canal: '#edit-canal',
        startDate: '#edit-date-filter-value',
        endDate: '#edit-date-filter-1-value',
        activitySectorFld: '#edit-field-promo-line-activity-sector-tid-wrapper',
        validationStartDate: '#edit-date-filter-value',
        validationEndDate: '#edit-date-filter-1-value',
        thematic: '#edit-field-thematic-tid',
        applyBtn:'#edit-submit-promotions-canal-dashboard',
        resetBtn:'#edit-reset',
        promotionsCanalTable:'table.views-table.cols-9'
    },


    // element getters

    seeAllRequiredElementsExist(){

        I.say('I check that Promotions canal dashboard title exists');
        I.seeElement(this.elements.pageHeader);

        I.say('I check that Promotions canal dashboard Request export type select exists');
        I.seeElement(this.elements.requestExportType);

        I.say('I check that Promotions canal dashboard Operation number field exists');
        I.seeElement(this.elements.operationNumberFld);

        I.say('I check that Promotions canal dashboard Canal field exists');
        I.seeElement(this.elements.canal);

        I.say('I check that Promotions canal dashboard Start date field exists');
        I.seeElement(this.elements.startDate);

        I.say('I check that Promotions canal dashboard End date field exists');
        I.seeElement(this.elements.endDate);

        I.say('I check that Promotions canal dashboard Activity sector field exists');
        I.seeElement(this.elements.activitySectorFld);

        I.say('I check that Promotions canal dashboard Validation Start date field exists');
        I.seeElement(this.elements.validationStartDate);

        I.say('I check that Promotions canal dashboard Validation End date field exists');
        I.seeElement(this.elements.validationEndDate);

        I.say('I check that Promotions canal dashboard Thematic field exists');
        I.seeElement(this.elements.thematic);

        I.say('I check that Promotions canal dashboard Apply button exists');
        I.seeElement(this.elements.applyBtn);

        I.say('I check that Promotions canal dashboard Reset button exists');
        I.seeElement(this.elements.resetBtn);

        I.say('I check that Promotions canal dashboard table exists');
        I.seeElement(this.elements.promotionsCanalTable);

        // I.saveScreenshot('promotionrules.png',true);
    }
};