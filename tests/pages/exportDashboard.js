/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        pageHeader: 'h1.page-title',
        exportId: '#edit-id',
        startDate: '#edit-date-filter-value',
        endDate: '#edit-date-filter-1-value',
        authorFld: '#edit-combine',
        emailFld: '#edit-uid',
        exportType: '#edit-export-type',
        status: '#edit-status',
        applyBtn: '#edit-submit-promotions-export',
        resetBtn: '#edit-reset',
        exportDashboardTable: 'table.views-table.cols-7'
    },


    // element getters

    seeAllRequiredElementsExist(){

        I.say('I check that Export dashboard title exists');
        I.seeElement(this.elements.pageHeader);

        I.say('I check that Export dashboard Export ID field exists');
        I.seeElement(this.elements.exportId);

        I.say('I check that Export dashboard Start Date field exists');
        I.seeElement(this.elements.startDate);

        I.say('I check that Export dashboard End Date field exists');
        I.seeElement(this.elements.endDate);

        I.say('I check that Export dashboard Author field exists');
        I.seeElement(this.elements.authorFld);

        I.say('I check that Export dashboard E-main field exists');
        I.seeElement(this.elements.emailFld);
        I.say('I check that Export dashboard Export type select exists');
        I.seeElement(this.elements.exportType);

        I.say('I check that Export dashboard Status select exists');
        I.seeElement(this.elements.status);

        I.say('I check that Export dashboard Apply button exists');
        I.seeElement(this.elements.applyBtn);

        I.say('I check that Export dashboard Reset button exists');
        I.seeElement(this.elements.resetBtn);

        I.say('I check that Export dashboard table exists');
        I.seeElement(this.elements.exportDashboardTable);

        // I.saveScreenshot('promotionrules.png',true);
    }
};