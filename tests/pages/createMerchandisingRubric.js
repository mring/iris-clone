/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        title: '#edit-name-field-fr-0-value',
        // uploadThumbnailFld: '#edit-field-merch-thumbnail-und-0-upload',
        uploadThumbnailFld: '[type=file]',
        // uploadThumbnailBtn: '#edit-field-merch-thumbnail-und-0-upload-button',
        uploadThumbnailBtn: '[type=submit]',
        activityDomain: '#edit_field_activity_domain_und_chosen',
        activityDomainList: '.chosen-results>li',
        thumbnailForm: '#edit-field-merch-thumbnail',
        submitButton:'#edit-submit'
    },




    // element getters

    getElementByPositionInList(context, position, type='') {
        let el = context;

        if(position)
            el = [el, `:nth-child(${position})`, ` ${type}`].join('');

        return el;
    },

    sendMerchandisingRubricForm(titleMerchandisingRubric, pathToTestFile){

        I.say('I fill title field');

        I.fillField(this.elements.title, titleMerchandisingRubric);

        I.say('I fill activity domain');

        I.click(this.elements.activityDomain);
        I.checkOption(this.getElementByPositionInList(this.elements.activityDomainList, 3));

        // I.saveScreenshot('merch_rubric_create_page.png',true);

        I.say('I upload test thumbnail');
        within(this.elements.thumbnailForm, () => {
            I.attachFile(this.elements.uploadThumbnailFld, pathToTestFile);
            I.click(this.elements.uploadThumbnailBtn);

            // I.saveScreenshot('login.png',true);
        });


        I.say('I upload test thumbnail');
        I.attachFile(this.elements.uploadThumbnailFld, pathToTestFile);
        I.click(this.elements.uploadThumbnailBtn);

        // I.saveScreenshot('merchrubric.png',true);
        I.click(this.elements.submitButton);

    }
};
