/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        title: '#edit-title',
        merchandisingRubric: '#edit_field_merch_rubric_und_chosen',
        // addANewFile: '#edit-field-files-und-0-upload',
        addANewFile: '[type=file]',
        addANewFileForm: '#edit-field-files',
        // uploadFileBtn:'#edit-field-files-und-0-upload-button',
        uploadFileBtn:'[type=submit]',
        // selectVisibility: { locator: '.form-item-field-scope-selection-und>select', defaultValue: '.form-item-field-scope-selection-und>select>option:nth-child(2)'},
        selectVisibility: { locator: 'select#edit-field-scope-selection-und', defaultValue: 'select#edit-field-scope-selection-und>option:nth-child(2)'},
        slot: '.form-item-field-location-group-und>div>ul>li>input',
        slotList: '.form-item-field-location-group-und>div>div>.chosen-results>li',
        submitButton: '#form input[type=submit]'
    },

    // element getters

    getElementByPositionInList(context, position, type='') {
        let el = context;

        if(position)
            el = [el, `:nth-child(${position})`, ` ${type}`].join('');

        return el;
    },

    sendMerchandisingElementForm(titleMerchandisingElement, pathToTestFile){

        I.say('I fill Step 1 : Merchandising element content');

        I.fillField(this.elements.title, titleMerchandisingElement);

        I.say('I upload test file to the merchandising element');
        within(this.elements.addANewFileForm, () => {
            I.attachFile(this.elements.addANewFile, pathToTestFile);
            I.click(this.elements.uploadFileBtn);

        // I.saveScreenshot('login.png',true);
        });


        I.say('I fill Step 2 : Merchandising element Visibility scope');

        I.waitForVisible(this.elements.selectVisibility.locator);

        I.say('I select scope field');

        I.checkOption(this.elements.selectVisibility.defaultValue, this.elements.selectVisibility.locator);
        I.waitForVisible(this.elements.slot);

        I.say('I click on Slot and select random value from list');

        I.click(this.elements.slot);
        I.click(this.getElementByPositionInList(this.elements.slotList, 3));
        // I.saveScreenshot('merchelem.png',true);
        I.click(this.elements.submitButton);

    }
};