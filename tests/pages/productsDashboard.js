/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        pageHeader: 'h1.page-title',
        SKU: '#edit-sku',
        brandFld: '#edit-brand',
        publishedSlct: '#edit-status',
        applyBtn:'#edit-submit-products-dashboard',
        resetBtn:'#edit-reset',
        productsTable:'table.views-table.cols-7'
    },


    // element getters

    seeAllRequiredElementsExist(){

        I.say('I check that Products dashboard title exists');
        I.seeElement(this.elements.pageHeader);

        I.say('I check that Products dashboard SKU Field exists');
        I.seeElement(this.elements.SKU);

        I.say('I check that Products dashboard Brand Field exists');
        I.seeElement(this.elements.brandFld);

        I.say('I check that Products dashboard Published Select exists');
        I.seeElement(this.elements.publishedSlct);

        I.say('I check that Products dashboard Apply Button exists');
        I.seeElement(this.elements.applyBtn);

        I.say('I check that Products dashboard Reset Button exists');
        I.seeElement(this.elements.resetBtn);

        I.say('I check that Products dashboard products Table exists');
        I.seeElement(this.elements.productsTable);

        // I.saveScreenshot('promotionrules.png',true);
    }
};