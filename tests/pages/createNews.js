/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        title: '#edit-title',
        newsThemeElGroup: '#edit-field-news-type-und .form-type-radio',
        activityDomain: '.search-field>input',
        activityDomainList: '.chosen-results>li',
        selectVisibility: { locator: '.form-item-field-scope-selection-und>select', defaultValue: '.form-item-field-scope-selection-und>select>option:nth-child(2)'},
        slot: '.form-item-field-location-group-und>div>ul>li>input',
        slotList: '.form-item-field-location-group-und>div>div>.chosen-results>li'
    },

    submitButton: {
        id: '#edit-submit'
    },

    // element getters

    getElementByPositionInList(context, position, type='') {
        let el = context;

        if(position)
            el = [el, `:nth-child(${position})`, ` ${type}`].join('');

        return el;
    },

    sendNewsForm(titleNews){

        I.say('I fill Step 2 : News content mandatory fields');

        I.fillField(this.elements.title, titleNews);
        I.checkOption(this.getElementByPositionInList(this.elements.newsThemeElGroup, 3, 'label'));

        I.say('I fill Step 3 : Scope visibility mandatory fields');

        I.click(this.elements.activityDomain);
        I.checkOption(this.getElementByPositionInList(this.elements.activityDomainList, 3));
        I.waitForVisible(this.elements.selectVisibility.locator);

        I.say('I select scope field');

        I.checkOption(this.elements.selectVisibility.defaultValue, this.elements.selectVisibility.locator);
        I.waitForVisible(this.elements.slot);

        I.say('I click on Slot and select random value from list');

        I.click(this.elements.slot);
        I.click(this.getElementByPositionInList(this.elements.slotList, 3));
        // I.saveScreenshot('news.png',true);
        I.click(this.submitButton.id);

    }
};
