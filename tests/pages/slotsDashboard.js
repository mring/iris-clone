/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        pageHeader: 'h1.page-title',
        slotCodeFld: '#edit-name',
        storeFld: '#edit-tid',
        country: '#edit-tid-1',
        applyBtn: '#edit-submit-slots-dashboard',
        resetBtn: '#edit-reset',
        slotsTable: 'table.views-table.cols-4'
    },


    // element getters

    seeAllRequiredElementsExist(){

        I.say('I check that Slots dashboard title exists');
        I.seeElement(this.elements.pageHeader);

        I.say('I check that Slots dashboard Slot code field exists');
        I.seeElement(this.elements.slotCodeFld);

        I.say('I check that Slots dashboard Store field exists');
        I.seeElement(this.elements.storeFld);

        I.say('I check that Slots dashboard Country field exists');
        I.seeElement(this.elements.country);

        I.say('I check that Slots dashboard Apply button exists');
        I.seeElement(this.elements.applyBtn);

        I.say('I check that Slots dashboard Reset button exists');
        I.seeElement(this.elements.resetBtn);

        I.say('I check that Slots dashboard table exists');
        I.seeElement(this.elements.slotsTable);

        // I.saveScreenshot('promotionrules.png',true);
    }
};