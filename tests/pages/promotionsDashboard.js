/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        pageHeader: 'h1.page-title',
        requestExportType: '#edit-request-export-type',
        requestExportBtn: '#edit-submit',
        universeFld: '#edit-field-universe-tid-wrapper',
        activitySectorFld: '#edit-field-promo-line-activity-sector-tid-wrapper',
        countryFld: '#edit-tid-wrapper',
        companyFld: '#edit-tid-2-wrapper',
        slotFld: '#edit-tid-1',
        startDate: '#edit-date-filter-value',
        endDate: '#edit-date-filter-1-value',
        groupingKeyFld: '#edit-field-grouping-key-value',
        operationNumberFld: '#edit-field-operation-number-value',
        validation: '#edit-status',
        archives: '#edit-node-archived',
        applyBtn:'#edit-submit-promotions-dashboard',
        resetBtn:'#edit-reset',
        promotionsTable:'table.views-table.cols-10'
    },


    // element getters

    seeAllRequiredElementsExist(){

        I.say('I check that Promotions dashboard title exists');
        I.seeElement(this.elements.pageHeader);

        I.say('I check that Promotions dashboard Request export type select exists');
        I.seeElement(this.elements.requestExportType);

        I.say('I check that Promotions dashboard Request export button exists');
        I.seeElement(this.elements.requestExportBtn);

        I.say('I check that Promotions dashboard Universe field exists');
        I.seeElement(this.elements.universeFld);

        I.say('I check that Promotions dashboard Activity sector field exists');
        I.seeElement(this.elements.activitySectorFld);

        I.say('I check that Promotions dashboard Country field exists');
        I.seeElement(this.elements.countryFld);

        I.say('I check that Promotions dashboard Company field exists');
        I.seeElement(this.elements.companyFld);

        I.say('I check that Promotions dashboard Slot field exists');
        I.seeElement(this.elements.slotFld);

        I.say('I check that Promotions dashboard Start date field exists');
        I.seeElement(this.elements.startDate);

        I.say('I check that Promotions dashboard End date field exists');
        I.seeElement(this.elements.endDate);

        I.say('I check that Promotions dashboard Grouping key field exists');
        I.seeElement(this.elements.groupingKeyFld);

        I.say('I check that Promotions dashboard Operation number field exists');
        I.seeElement(this.elements.operationNumberFld);

        I.say('I check that Promotions dashboard Validation select exists');
        I.seeElement(this.elements.validation);

        I.say('I check that Promotions dashboard Archives select exists');
        I.seeElement(this.elements.archives);

        I.say('I check that Promotions dashboard Apply button exists');
        I.seeElement(this.elements.applyBtn);

        I.say('I check that Promotions dashboard Reset button exists');
        I.seeElement(this.elements.resetBtn);

        I.say('I check that Promotions dashboard promotion rules table exists');
        I.seeElement(this.elements.promotionsTable);

        // I.saveScreenshot('promotionrules.png',true);
    }
};