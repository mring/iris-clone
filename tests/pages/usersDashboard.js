/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        pageHeader: 'h1.page-title',
        manageRolesBtn: '.waves-effect.waves-button.waves-float.waves-classic',
        firstNameFld: '#edit-field-first-name-value',
        lastNameFld: '#edit-field-last-name-value',
        emailFld: '#edit-mail',
        rolesSlct: '#edit-rid',
        activeSlct: '#edit-status',
        applyBtn:'#edit-submit-users-dashboard',
        resetBtn:'#edit-reset',
        usersTable:'table.views-table.cols-8'
    },


    // element getters

    seeAllRequiredElementsExist(){

        I.say('I check that users dashboard title exists');
        I.seeElement(this.elements.pageHeader);
        I.see('Users dashboard');

        I.say('I check that users dashboard Manage roles button exists');
        I.seeElement(this.elements.manageRolesBtn);

        I.say('I check that users dashboard First name field exists');
        I.seeElement(this.elements.firstNameFld);

        I.say('I check that users dashboard Last name field exists');
        I.seeElement(this.elements.lastNameFld);

        I.say('I check that users dashboard Email field exists');
        I.seeElement(this.elements.emailFld);

        I.say('I check that users dashboard Roles filter list exists');
        I.seeElement(this.elements.rolesSlct);

        I.say('I check that users dashboard Active filter list exists');
        I.seeElement(this.elements.activeSlct);

        I.say('I check that users dashboard apply button exists');
        I.seeElement(this.elements.applyBtn);

        I.say('I check that users dashboard reset button exists');
        I.seeElement(this.elements.resetBtn);

        I.say('I check that users dashboard Users table exists');
        I.seeElement(this.elements.usersTable);

        // I.saveScreenshot('promotionrules.png',true);
    }
};