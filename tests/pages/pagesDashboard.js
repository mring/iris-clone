/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        pageHeader: 'h1.page-title',
        createAPlaybookFileBtn: '.waves-effect.waves-button.waves-float.waves-classic',
        titleFld: '#edit-title',
        authorFld: '#edit-combine',
        emailFld: '#edit-uid',
        publishedSlct: '#edit-status',
        locationGroup: '#edit_field_location_group_target_id_chosen',
        startDate: '#edit-date-filter-value-datepicker-popup-0',
        endDate: '#edit-date-filter-1-value-datepicker-popup-0',
        applyBtn:'#edit-submit-pages-dashboard',
        resetBtn:'#edit-reset',
        pagesTable:'table.views-table.cols-7'
    },


    // element getters

    seeAllRequiredElementsExist(){

        I.say('I check that Pages dashboard title exists');
        I.seeElement(this.elements.pageHeader);

        I.say('I check that Pages dashboard Create a Playbook file button exists');
        I.seeElement(this.elements.createAPlaybookFileBtn);

        I.say('I check that Pages dashboard Title field exists');
        I.seeElement(this.elements.titleFld);

        I.say('I check that Pages dashboard Author field exists');
        I.seeElement(this.elements.authorFld);

        I.say('I check that Pages dashboard E-mail field exists');
        I.seeElement(this.elements.emailFld);

        I.say('I check that Pages dashboard Published select exists');
        I.seeElement(this.elements.publishedSlct);

        I.say('I check that Pages dashboard Location group field exists');
        I.seeElement(this.elements.locationGroup);

        I.say('I check that Pages dashboard Publication date: Start field exists');
        I.seeElement(this.elements.startDate);

        I.say('I check that Pages dashboard Publication date: End field exists');
        I.seeElement(this.elements.endDate);

        I.say('I check that Pages dashboard Apply button exists');
        I.seeElement(this.elements.applyBtn);

        I.say('I check that Pages dashboard Reset button exists');
        I.seeElement(this.elements.resetBtn);

        I.say('I check that Pages dashboard promotion rules table exists');
        I.seeElement(this.elements.pagesTable);

        // I.saveScreenshot('promotionrules.png',true);
    }
};