/// <reference path="./steps.d.ts" />
'use strict';

let I;

module.exports = {

    _init() {
        I = require('../steps_file.js')();
    },

    // locators here

    elements: {

        pageHeader: 'h1.page-title',
        createANewsBtn: '.waves-effect.waves-button.waves-float.waves-classic',
        titleFld: '.waves-effect.waves-button.waves-float.waves-classic',
        authorFld: '#edit-combine',
        emailFld: '#edit-uid',
        universeSlct: '#edit-field-universe-tid',
        themeSlct: '#edit-news-type',
        publishedSlct: '#edit-status',
        locationGroup: '#edit_field_location_group_target_id_chosen',
        startDate: '#edit-date-from-value',
        endDate: '#edit-date-to-value',
        archived: '#edit-node-archived',
        applyBtn:'#edit-submit-news-dashboard',
        resetBtn:'#edit-reset',
        newsTable:'table.views-table.cols-10'
    },


    // element getters

    seeAllRequiredElementsExist(){

        I.say('I check that Pages dashboard title exists');
        I.seeElement(this.elements.pageHeader);

        I.say('I check that Pages dashboard Create a Playbook file button exists');
        I.seeElement(this.elements.createANewsBtn);

        I.say('I check that Pages dashboard Title field exists');
        I.seeElement(this.elements.titleFld);

        I.say('I check that Pages dashboard Author field exists');
        I.seeElement(this.elements.authorFld);

        I.say('I check that Pages dashboard E-mail field exists');
        I.seeElement(this.elements.emailFld);

        I.say('I check that Pages dashboard Universe select exists');
        I.seeElement(this.elements.universeSlct);

        I.say('I check that Pages dashboard Theme select exists');
        I.seeElement(this.elements.themeSlct);

        I.say('I check that Pages dashboard Published select exists');
        I.seeElement(this.elements.publishedSlct);

        I.say('I check that Pages dashboard Location group field exists');
        I.seeElement(this.elements.locationGroup);

        I.say('I check that Pages dashboard Publication date: Start field exists');
        I.seeElement(this.elements.startDate);

        I.say('I check that Pages dashboard Publication date: End field exists');
        I.seeElement(this.elements.endDate);

        I.say('I check that Pages dashboard Apply button exists');
        I.seeElement(this.elements.applyBtn);

        I.say('I check that Pages dashboard Reset button exists');
        I.seeElement(this.elements.resetBtn);

        I.say('I check that Pages dashboard table exists');
        I.seeElement(this.elements.newsTable);

        // I.saveScreenshot('promotionrules.png',true);
    }
};