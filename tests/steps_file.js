'use strict';
// in this file you can append custom step methods to 'I' object

module.exports = function () {
    return actor({

        // Define custom steps here, use 'this' to access default methods of I.
        // It is recommended to place a general 'login' function here.
        // locators here

        BOAdminNavBar: "#admin-menu-wrapper",
        BOAdminMenu: "#admin-menu",

        loginForm: "#user-login",

        fields: {
            login: '#edit-name',
            password: '#edit-pass'
        },
        submitButton: {
            id: '#edit-submit'
        },

        // methods here

        login: function(login, password){

            within(this.loginForm, () => {
                this.fillField(this.fields.login, login);
                this.fillField(this.fields.password, password);
                this.click(this.submitButton.id);
            });

        },

        seeBOAdminNavBar: function(){
            within(this.BOAdminMenu, () => {
                this.seeElement(this.BOAdminNavBar);
                // this.saveScreenshot('BOAdminNavBar.png',true);
            });

        }
    });

};
