/// <reference path="./steps.d.ts" />

Feature('As user with Generic contributor rights I would like to create a playbook file');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('As user with Generic contributor rights I would like login to the back-end ' +
    'I should go to Create Playbook File Page and fill all required mandatory fields and click PUBLISH button', (I, createPlaybookFilePage ) => {

    const user = "generic_contributor";
    const password = "5zMuGpJA";
    const titlePlaybookFile = "My Test title for a playbook file";
    const pathToTestFile = "data/fileForTests.txt";

    I.say("I try login to back-end as user with Generic contributor {login: generic_contributor, password: 5zMuGpJA}");
    // backEndLoginPage.sendLoginForm(user, password);
    I.login(user, password);
    I.amOnPage('/node/add/page');
    I.say('I try to create a random playbook file with title '+ titlePlaybookFile +' and only mandatory fields');
    createPlaybookFilePage.sendPlaybookFileForm(titlePlaybookFile, pathToTestFile);
    I.see('Playbook '+ titlePlaybookFile +' has been created.');

});