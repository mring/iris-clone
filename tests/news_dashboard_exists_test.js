/// <reference path="./steps.d.ts" />

Feature('As user with News Responsible rights I would like to check Pages dashboard exist');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('As user with News Responsible rights I would like login to the back-end ' +
    'I should go to News dashboard Page and check all required dashboard elements exist', (I, newsDashboardPage ) => {

    const user = "news_responsible";
    const password = "MaTciwo8";
    const titleOfPage = "News dashboard | IRIS";


    I.say("I try login to back-end as user with News Responsible {login: news_responsible, password: 5zMuGpJA}");
    // backEndLoginPage.sendLoginForm(user, password);
    I.login(user, password);
    I.amOnPage('/admin/news/news-dashboard');
    I.say('I try to check is required dashboard elements exist');
    newsDashboardPage.seeAllRequiredElementsExist();


});