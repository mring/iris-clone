/// <reference path="./steps.d.ts" />

Feature('As user with webmaster rights I would like to check promotion rules dashboard exist');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('As user with webmaster rights I would like login to the back-end ' +
    'I should go to Promotion rules dashboard Page and check search possibility', (I, promotionRulesDashboardPage ) => {

    const user = "webmaster";
    const password = "webmaster2015";
    const titleOfPage = "Promotion rules dashboard | IRIS";
    const searchKey = "1 FOR";

    I.say("I try login to back-end as user with webmaster {login: webmaster, password: webmaster2015}");
    // backEndLoginPage.sendLoginForm(user, password);
    I.login(user, password);
    I.amOnPage('/admin/promotions/promotion-rules-dashboard');
    I.say('I try to check Promotion rules dashboard search possibility');
    promotionRulesDashboardPage.checkPromotionRulesDashboardSearch(searchKey);


});