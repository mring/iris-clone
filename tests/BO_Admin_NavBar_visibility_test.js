/// <reference path="./steps.d.ts" />

Feature('As user with Webmaster rights I would like to see BO Admin NavBar');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('As user with Webmaster rights I would like login to the back-end ' +
    'I should go to Home Page and see #admin-menu-wrapper', (I ) => {

    const user = "webmaster";
    const password = "webmaster2015";

    I.say("I try login to back-end as user with Webmaster {login: webmaster, password: webmaster2015}");
    // backEndLoginPage.sendLoginForm(user, password);
    I.login(user, password);
    I.amOnPage('/home');
    I.say('I try to see BO Admin NavBar');
    I.seeBOAdminNavBar();

});
