/// <reference path="./steps.d.ts" />

Feature('As user with Webmaster rights I would like to check FO search possibility');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('As user with Webmaster rights I would like login to the back-end ' +
    'I should go to Home Page and fill Search field, click Search button and check Search result', (I, homePage) => {

    const user = "webmaster";
    const password = "webmaster2015";
    const searchKey = "TESTERS";

    I.say("I try login to back-end as user with Webmaster rights {login: webmaster, password: webmaster2015}");

    // backEndLoginPage.sendLoginForm(user, password);
    I.login(user, password);
    I.amOnPage('/home');
    I.say('I try to fill search field by '+ searchKey +' and check the search results');
    homePage.searchByKey(searchKey);
});
