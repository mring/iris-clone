/// <reference path="./steps.d.ts" />

Feature('As user with Webmaster rights I would like to create a Merchandising rubric');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('As user with Webmaster rights I would like login to the back-end ' +
    'I should go to Create Merchandising rubric Page and fill all required mandatory fields and click PUBLISH button', (I, createMerchandisingRubricPage ) => {

    const user = "webmaster";
    const password = "webmaster2015";
    const titleMerchandisingRubric = "My Test title for a merchandising rubric";
    const pathToTestFile = "data/testimagefile.png";

    I.say("I try login to back-end as user with Webmaster {login: webmaster, password: webmaster2015}");
    // backEndLoginPage.sendLoginForm(user, password);
    I.login(user, password);
    I.amOnPage('/admin/structure/taxonomy/merchandising_rubric/add');
    I.say('I try to create a random Merchandising rubric with title '+ titleMerchandisingRubric +' and only mandatory fields');
    createMerchandisingRubricPage.sendMerchandisingRubricForm(titleMerchandisingRubric, pathToTestFile);
    I.see('Created new term '+ titleMerchandisingRubric);

});

