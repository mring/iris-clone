/// <reference path="./steps.d.ts" />

Feature('As user with Webmaster rights I would like to check login to site possibility');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('Using invalid pair password and login I will try to login to site ', (I, backEndLoginPage) => {
    const user = "webmaster";
    const password = "webmaster";

    I.say("I try login to back-end as user with Webmaster {login: webmaster, password: webmaster}");
    backEndLoginPage.sendLoginForm(user, password);
    // I.login(user, password);

    I.see('Sorry, unrecognized username or password.');
});


Scenario('Using valid webmaster pair password and login I will try to login to site', (I, backEndLoginPage) => {
    const user = "webmaster";
    const password = "webmaster2015";

    I.say("I try login to back-end as user with Webmaster {login: webmaster, password: webmaster2015}");
    backEndLoginPage.sendLoginForm(user, password);
    // I.login(user, password);
    I.see('Hello,');
});