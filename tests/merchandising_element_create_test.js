/// <reference path="./steps.d.ts" />

Feature('As user with Merchandising contributor rights I would like to create a merchandising element');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('As user with Merchandising contributor rights I would like login to the back-end ' +
    'I should go to Create Merchandising element Page and fill all required mandatory fields and click PUBLISH button', (I, createMerchandisingElementPage ) => {

    const user = "merchandising_contributor";
    const password = "uvHzGuK4";
    const titleMerchandisingElement = "My Test title for a merchandising element";
    const pathToTestFile = "data/testfile.png";

    I.say("I try login to back-end as user with Merchandising contributor {login: merchandising_contributor, password: uvHzGuK4}");
    // backEndLoginPage.sendLoginForm(user, password);
    I.login(user, password);
    I.amOnPage('/node/add/merchandising-element');
    I.say('I try to create a random merchandising element with title '+ titleMerchandisingElement +' and only mandatory fields');
    createMerchandisingElementPage.sendMerchandisingElementForm(titleMerchandisingElement, pathToTestFile);
    I.see('Merchandising element '+ titleMerchandisingElement +' has been created.');

});
