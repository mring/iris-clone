/// <reference path="./steps.d.ts" />

Feature('As user with News Responsible rights I would like to create a news');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('As user with News Responsible rights I would like login to the back-end ' +
    'I should go to Create News Page and fill all required mandatory fields and click PUBLISH button', (I, createNewsPage) => {

    const user = "news_responsible";
    const password = "MaTciwo8";
    const titleNews = "My Test title for a news";

    I.say("I try login to back-end as user with News Responsible rights {login: news_responsible, password: MaTciwo8}");

    // backEndLoginPage.sendLoginForm(user, password);
    I.login(user, password);
    I.amOnPage('/node/add/news');
    I.say('I try to create a random news with title '+ titleNews +' and only mandatory fields');
    createNewsPage.sendNewsForm(titleNews);
    I.see('News '+ titleNews +' has been created.');

});
