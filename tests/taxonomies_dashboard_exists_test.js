/// <reference path="./steps.d.ts" />

Feature('As user with webmaster rights I would like to check Taxonomies dashboard exist');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('As user with webmaster rights I would like login to the back-end ' +
    'I should go to Taxonomies dashboard Page and check all required dashboard elements exist', (I, taxonomiesDashboardPage ) => {

    const user = "webmaster";
    const password = "webmaster2015";
    const titleOfPage = "Taxonomies dashboard | IRIS";


    I.say("I try login to back-end as user with webmaster {login: webmaster, password: webmaster2015}");
    // backEndLoginPage.sendLoginForm(user, password);
    I.login(user, password);
    I.amOnPage('/admin/taxonomy/taxonomies-dashboard/');
    I.say('I try to check is required dashboard elements exist');
    taxonomiesDashboardPage.seeAllRequiredElementsExist();


});