/// <reference path="./steps.d.ts" />

Feature('As user with webmaster rights I would like to check Export dashboard exist');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('As user with webmaster rights I would like login to the back-end ' +
    'I should go to Export dashboard Page and check all required dashboard elements exist', (I, exportDashboardPage ) => {

    const user = "webmaster";
    const password = "webmaster2015";
    const titleOfPage = "Exports dashboard | IRIS";


    I.say("I try login to back-end as user with webmaster {login: webmaster, password: webmaster2015}");
    // backEndLoginPage.sendLoginForm(user, password);
    I.login(user, password);
    I.amOnPage('/admin/promotions/promotion-export-interface');
    I.say('I try to check is required dashboard elements exist');
    exportDashboardPage.seeAllRequiredElementsExist();


});