/// <reference path="./steps.d.ts" />

Feature('As user with generic_contributor rights I would like to check Pages dashboard exist');

Before((I) => {
    I.amOnPage('/home');
});

Scenario('As user with generic_contributor rights I would like login to the back-end ' +
    'I should go to Pages dashboard Page and check all required dashboard elements exist', (I, pagesDashboardPage ) => {

    const user = "generic_contributor";
    const password = "5zMuGpJA";
    const titleOfPage = "Generic pages dashboard | IRIS";


    I.say("I try login to back-end as user with generic_contributor {login: generic_contributor, password: 5zMuGpJA}");
    // backEndLoginPage.sendLoginForm(user, password);
    I.login(user, password);
    I.amOnPage('/admin/pages/pages-dashboard');
    I.say('I try to check is required dashboard elements exist');
    pagesDashboardPage.seeAllRequiredElementsExist();


});