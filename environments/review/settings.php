<?php

/**
 * @file
 * Drupal site-specific configuration file.
 */

$databases = array (
  'default' =>
  array (
    'default' =>
    array (
      'database' => 'aelia_CI_BUILD_REF_SLUG',
      'username' => 'aelia',
      'password' => 'dEsHF7riBPE2LIP',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

$update_free_access = FALSE;
$drupal_hash_salt = 'DOFlXoPw8MoYwyuUBaLrGhjU586HmJb1s24cpQUb9fY';

$base_url = 'https://CI_BUILD_REF_SLUG.review.aelia-iris.adyax-dev.com';  // NO trailing slash!

/**
 * Some distributions of Linux (most notably Debian) ship their PHP
 * installations with garbage collection (gc) disabled. Since Drupal depends on
 * PHP's garbage collection for clearing sessions, ensure that garbage
 * collection occurs by using the most common settings.
 */
ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);

/**
 * Set session lifetime (in seconds), i.e. the time from the user's last visit
 * to the active session may be deleted by the session garbage collector. When
 * a session is deleted, authenticated users are logged out, and the contents
 * of the user's $_SESSION variable is discarded.
 */
ini_set('session.gc_maxlifetime', 200000);

/**
 * Set session cookie lifetime (in seconds), i.e. the time from the session is
 * created to the cookie expires, i.e. when the browser is expected to discard
 * the cookie. The value 0 means "until the browser is closed".
 */
ini_set('session.cookie_lifetime', 2000000);


/**
 * Fast 404 pages:
 */
$conf['404_fast_paths_exclude'] = '/\/(?:styles)|(?:system\/files)\//';
$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';
drupal_fast_404();

/**
 * Environment settings.
 */
$conf['cache_backends'][] = 'sites/all/modules/contrib/memcache/memcache.inc';
$conf['cache_default_class'] = 'MemCacheDrupal';
$conf['cache_class_cache_form'] = 'DrupalDatabaseCache';

$conf['memcache_key_prefix'] = 'a_MEMCACHE_PREFIX';

$conf['en_iris_import_csv'] = 'ftp://ADYAX:Aelia%402015%3F@ftp.aelia.com/DEV/ENGLISH/EN_Stucture_Orga_Lagardere_Travel_Duty_Free.CSV';
$conf['fr_iris_import_csv'] = 'ftp://ADYAX:Aelia%402015%3F@ftp.aelia.com/DEV/FRENCH/FR_Stucture_Orga_Lagardere_Travel_Duty_Free.CSV';

$conf['googleanalytics_account'] = 'UA-NONE';
